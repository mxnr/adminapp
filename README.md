admin-app
=========

Admin App is one of the major parts of our application. 
Entity Bundle is the core bundle of the application located in the separate repository and loaded through composer in `vendors` folder.
All database migrations are made through this app - so it needs to be updated regularly.
That's why almost all Application updates need to be started with updating Admin App.

Deployment and Update of major changes:
=========

#Adminapp side:

##For the first deployment:

###1.1 Clone the repository:
    `git clone git@bitbucket.org:mxnr/adminapp.git`
    
###1.2 Go to the project's folder:
    `cd adminapp`

###1.3 Proceed to the 2.1 part

##For Update:

###1.1 Go to the project's folder:
    `cd adminapp`
    
###1.2 Clear the cache:
    `rm -rf var/cache/dev/*`
    
###1.3 Pull the latest master:
    `git pull origin master`
    
###2.1 Install new dependencies in composer.lock:
    `composer install`
    
###2.2 Check that Entity Bundle has the latest version(or the version that you need):
    - For update:
        `composer update 3web1/entity-bundle`
    - The problem can be in that you forgot to tag entity bundle's version. You need to go to Entity Bundle project and check Readme file:
        https://bitbucket.org/the-captain/entitybundle/overview
    
###3. Reboot your DB and perform migrations
    `bin/console doctrine:database:drop --force && bin/console doctrine:database:create && bin/console doctrine:migrations:migrate`

###4.1 Load fixtures:
    `bin/console doctrine:fixtures:load`

###4.2 Update Billing plans:
    `bin/console admin:billing:update:plans`
    
###5. Install npm dependencies:
    `npm install`
    
###6. Webpack (http://symfony.com/doc/current/frontend.html - read this article):
    - Compile
        `yarn run encore production`

    - If you need to install yarn (https://yarnpkg.com/lang/en/docs/install/#mac-tab)
        `brew install yarn` - for mac 
        or
        `curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
        echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list`
       
        `sudo apt-get update && sudo apt-get install yarn` - on Linux

###6. Update routes for js
    `bin/console fos:js-routing:dump --format=json`
    
    - This command parse our symfony routing and provide using routing alias in js side
## Optional
    - For assets cleaning use 
        `bin/console assets:install web/admin`
    - We don't use assetic and bower anymore - make sure we migrated all parts
    
Requirements
=========

ImageMagick php library