<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170902201405 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE income_lists (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_D43AD87C9395C3F3 (customer_id), INDEX IDX_D43AD87C7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventory_item_barcodes (id INT AUTO_INCREMENT NOT NULL, inventory_item_id INT DEFAULT NULL, status SMALLINT NOT NULL, barcode VARCHAR(255) DEFAULT NULL, purchase_price INT DEFAULT NULL, sell_price INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_B048AE5B536BF4A2 (inventory_item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE income_customers (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, firstName VARCHAR(255) NOT NULL, lastName VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, driver_license VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, address_line_1 VARCHAR(255) DEFAULT NULL, address_line_2 VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, state VARCHAR(255) DEFAULT NULL, zip VARCHAR(255) DEFAULT NULL, INDEX IDX_25C8E0D57E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE income_lists ADD CONSTRAINT FK_D43AD87C9395C3F3 FOREIGN KEY (customer_id) REFERENCES income_customers (id)');
        $this->addSql('ALTER TABLE income_lists ADD CONSTRAINT FK_D43AD87C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE inventory_item_barcodes ADD CONSTRAINT FK_B048AE5B536BF4A2 FOREIGN KEY (inventory_item_id) REFERENCES inventory_items (id)');
        $this->addSql('ALTER TABLE income_customers ADD CONSTRAINT FK_25C8E0D57E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE inventories CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE inventory_items ADD income_list_id INT DEFAULT NULL, ADD category_id INT DEFAULT NULL, ADD owner_id INT DEFAULT NULL, ADD price_type_id INT DEFAULT NULL, ADD title VARCHAR(255) DEFAULT NULL, ADD price_value NUMERIC(10, 0) DEFAULT NULL, DROP customer_name, DROP barcode, CHANGE purchase_price purchase_price NUMERIC(10, 0) DEFAULT NULL, CHANGE updated_at updated_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE inventory_items ADD CONSTRAINT FK_3D82424D85EC60F2 FOREIGN KEY (income_list_id) REFERENCES income_lists (id)');
        $this->addSql('ALTER TABLE inventory_items ADD CONSTRAINT FK_3D82424D12469DE2 FOREIGN KEY (category_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE inventory_items ADD CONSTRAINT FK_3D82424D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE inventory_items ADD CONSTRAINT FK_3D82424DAE6A44CF FOREIGN KEY (price_type_id) REFERENCES price_types (id)');
        $this->addSql('CREATE INDEX IDX_3D82424D85EC60F2 ON inventory_items (income_list_id)');
        $this->addSql('CREATE INDEX IDX_3D82424D12469DE2 ON inventory_items (category_id)');
        $this->addSql('CREATE INDEX IDX_3D82424D7E3C61F9 ON inventory_items (owner_id)');
        $this->addSql('CREATE INDEX IDX_3D82424DAE6A44CF ON inventory_items (price_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inventory_items DROP FOREIGN KEY FK_3D82424D85EC60F2');
        $this->addSql('ALTER TABLE income_lists DROP FOREIGN KEY FK_D43AD87C9395C3F3');
        $this->addSql('DROP TABLE income_lists');
        $this->addSql('DROP TABLE inventory_item_barcodes');
        $this->addSql('DROP TABLE income_customers');
        $this->addSql('ALTER TABLE inventories CHANGE updated_at updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE inventory_items DROP FOREIGN KEY FK_3D82424D12469DE2');
        $this->addSql('ALTER TABLE inventory_items DROP FOREIGN KEY FK_3D82424D7E3C61F9');
        $this->addSql('ALTER TABLE inventory_items DROP FOREIGN KEY FK_3D82424DAE6A44CF');
        $this->addSql('DROP INDEX IDX_3D82424D85EC60F2 ON inventory_items');
        $this->addSql('DROP INDEX IDX_3D82424D12469DE2 ON inventory_items');
        $this->addSql('DROP INDEX IDX_3D82424D7E3C61F9 ON inventory_items');
        $this->addSql('DROP INDEX IDX_3D82424DAE6A44CF ON inventory_items');
        $this->addSql('ALTER TABLE inventory_items ADD barcode VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP income_list_id, DROP category_id, DROP owner_id, DROP price_type_id, DROP price_value, CHANGE purchase_price purchase_price NUMERIC(10, 2) DEFAULT NULL, CHANGE updated_at updated_at DATETIME NOT NULL, CHANGE title customer_name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
