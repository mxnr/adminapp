<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170904205024 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE products ADD clone_parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5A37B7BB65 FOREIGN KEY (clone_parent_id) REFERENCES products (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_B3BA5A5A37B7BB65 ON products (clone_parent_id)');
        $this->addSql('ALTER TABLE categories ADD clone_parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF3466837B7BB65 FOREIGN KEY (clone_parent_id) REFERENCES categories (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_3AF3466837B7BB65 ON categories (clone_parent_id)');
        $this->addSql('ALTER TABLE filters ADD clone_parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE filters ADD CONSTRAINT FK_7877678D37B7BB65 FOREIGN KEY (clone_parent_id) REFERENCES filters (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_7877678D37B7BB65 ON filters (clone_parent_id)');
        $this->addSql('ALTER TABLE models ADD clone_parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE models ADD CONSTRAINT FK_E4D6300937B7BB65 FOREIGN KEY (clone_parent_id) REFERENCES models (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_E4D6300937B7BB65 ON models (clone_parent_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE categories DROP FOREIGN KEY FK_3AF3466837B7BB65');
        $this->addSql('DROP INDEX IDX_3AF3466837B7BB65 ON categories');
        $this->addSql('ALTER TABLE categories DROP clone_parent_id');
        $this->addSql('ALTER TABLE filters DROP FOREIGN KEY FK_7877678D37B7BB65');
        $this->addSql('DROP INDEX IDX_7877678D37B7BB65 ON filters');
        $this->addSql('ALTER TABLE filters DROP clone_parent_id');
        $this->addSql('ALTER TABLE models DROP FOREIGN KEY FK_E4D6300937B7BB65');
        $this->addSql('DROP INDEX IDX_E4D6300937B7BB65 ON models');
        $this->addSql('ALTER TABLE models DROP clone_parent_id');
        $this->addSql('ALTER TABLE products DROP FOREIGN KEY FK_B3BA5A5A37B7BB65');
        $this->addSql('DROP INDEX IDX_B3BA5A5A37B7BB65 ON products');
        $this->addSql('ALTER TABLE products DROP clone_parent_id');
    }
}
