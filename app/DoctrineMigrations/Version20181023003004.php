<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181023003004 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_seo ADD model_id INT DEFAULT NULL, ADD category_id INT DEFAULT NULL, ADD inventroy_item_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_seo ADD CONSTRAINT FK_87678DBB7975B7E7 FOREIGN KEY (model_id) REFERENCES models (id)');
        $this->addSql('ALTER TABLE user_seo ADD CONSTRAINT FK_87678DBB12469DE2 FOREIGN KEY (category_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE user_seo ADD CONSTRAINT FK_87678DBBE4597736 FOREIGN KEY (inventroy_item_id) REFERENCES inventory_items (id)');
        $this->addSql('CREATE INDEX IDX_87678DBB7975B7E7 ON user_seo (model_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_87678DBB12469DE2 ON user_seo (category_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_87678DBBE4597736 ON user_seo (inventroy_item_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_seo DROP FOREIGN KEY FK_87678DBB7975B7E7');
        $this->addSql('ALTER TABLE user_seo DROP FOREIGN KEY FK_87678DBB12469DE2');
        $this->addSql('ALTER TABLE user_seo DROP FOREIGN KEY FK_87678DBBE4597736');
        $this->addSql('DROP INDEX IDX_87678DBB7975B7E7 ON user_seo');
        $this->addSql('DROP INDEX UNIQ_87678DBB12469DE2 ON user_seo');
        $this->addSql('DROP INDEX UNIQ_87678DBBE4597736 ON user_seo');
        $this->addSql('ALTER TABLE user_seo DROP model_id, DROP category_id, DROP inventroy_item_id');
    }
}
