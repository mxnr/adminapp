<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170711093522 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE categories ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF346687E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_3AF346687E3C61F9 ON categories (owner_id)');
        $this->addSql('ALTER TABLE images ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE images ADD CONSTRAINT FK_E01FBE6A7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_E01FBE6A7E3C61F9 ON images (owner_id)');
        $this->addSql('ALTER TABLE filters ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE filters ADD CONSTRAINT FK_7877678D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_7877678D7E3C61F9 ON filters (owner_id)');
        $this->addSql('ALTER TABLE models ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE models ADD CONSTRAINT FK_E4D630097E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_E4D630097E3C61F9 ON models (owner_id)');
        $this->addSql('ALTER TABLE prices ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE prices ADD CONSTRAINT FK_E4CB6D597E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_E4CB6D597E3C61F9 ON prices (owner_id)');
        $this->addSql('ALTER TABLE price_types ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE price_types ADD CONSTRAINT FK_72F981507E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_72F981507E3C61F9 ON price_types (owner_id)');
        $this->addSql('ALTER TABLE products ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5A7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_B3BA5A5A7E3C61F9 ON products (owner_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE categories DROP FOREIGN KEY FK_3AF346687E3C61F9');
        $this->addSql('DROP INDEX IDX_3AF346687E3C61F9 ON categories');
        $this->addSql('ALTER TABLE categories DROP owner_id');
        $this->addSql('ALTER TABLE filters DROP FOREIGN KEY FK_7877678D7E3C61F9');
        $this->addSql('DROP INDEX IDX_7877678D7E3C61F9 ON filters');
        $this->addSql('ALTER TABLE filters DROP owner_id');
        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6A7E3C61F9');
        $this->addSql('DROP INDEX IDX_E01FBE6A7E3C61F9 ON images');
        $this->addSql('ALTER TABLE images DROP owner_id');
        $this->addSql('ALTER TABLE models DROP FOREIGN KEY FK_E4D630097E3C61F9');
        $this->addSql('DROP INDEX IDX_E4D630097E3C61F9 ON models');
        $this->addSql('ALTER TABLE models DROP owner_id');
        $this->addSql('ALTER TABLE price_types DROP FOREIGN KEY FK_72F981507E3C61F9');
        $this->addSql('DROP INDEX IDX_72F981507E3C61F9 ON price_types');
        $this->addSql('ALTER TABLE price_types DROP owner_id');
        $this->addSql('ALTER TABLE prices DROP FOREIGN KEY FK_E4CB6D597E3C61F9');
        $this->addSql('DROP INDEX IDX_E4CB6D597E3C61F9 ON prices');
        $this->addSql('ALTER TABLE prices DROP owner_id');
        $this->addSql('ALTER TABLE products DROP FOREIGN KEY FK_B3BA5A5A7E3C61F9');
        $this->addSql('DROP INDEX IDX_B3BA5A5A7E3C61F9 ON products');
        $this->addSql('ALTER TABLE products DROP owner_id');
    }
}
