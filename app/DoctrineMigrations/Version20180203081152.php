<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180203081152 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE model_categories (model_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_53CC4DFF7975B7E7 (model_id), INDEX IDX_53CC4DFF12469DE2 (category_id), PRIMARY KEY(model_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE model_categories ADD CONSTRAINT FK_53CC4DFF7975B7E7 FOREIGN KEY (model_id) REFERENCES models (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE model_categories ADD CONSTRAINT FK_53CC4DFF12469DE2 FOREIGN KEY (category_id) REFERENCES categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fos_user_user DROP FOREIGN KEY FK_C560D76119E9AC5F');
        $this->addSql('ALTER TABLE fos_user_user ADD CONSTRAINT FK_C560D76119E9AC5F FOREIGN KEY (supervisor_id) REFERENCES fos_user_user (id) ON DELETE SET NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE model_categories');
        $this->addSql('ALTER TABLE fos_user_user DROP FOREIGN KEY FK_C560D76119E9AC5F');
        $this->addSql('ALTER TABLE fos_user_user ADD CONSTRAINT FK_C560D76119E9AC5F FOREIGN KEY (supervisor_id) REFERENCES fos_user_user (id)');
    }
}
