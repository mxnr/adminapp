<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171126123650 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders ADD payment_type SMALLINT NOT NULL, ADD site_type SMALLINT NOT NULL, ADD shipping_address VARCHAR(255) DEFAULT NULL, ADD pickup_location VARCHAR(255) DEFAULT NULL, CHANGE payment delivery_type SMALLINT NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders ADD payment SMALLINT NOT NULL, DROP delivery_type, DROP payment_type, DROP site_type, DROP shipping_address, DROP pickup_location');
    }
}
