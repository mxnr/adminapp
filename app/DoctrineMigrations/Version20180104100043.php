<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20180104100043.
 */
class Version20180104100043 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE billing_address DROP FOREIGN KEY FK_6660E4569395C3F3');
        $this->addSql('ALTER TABLE billing_hosted_page DROP FOREIGN KEY FK_22A22839395C3F3');
        $this->addSql('ALTER TABLE billing_subscription DROP FOREIGN KEY FK_16912F269395C3F3');
        $this->addSql('ALTER TABLE billing_subscription DROP FOREIGN KEY FK_16912F26E899029B');
        $this->addSql('DROP TABLE billing_address');
        $this->addSql('DROP TABLE billing_customer');
        $this->addSql('DROP TABLE billing_hosted_page');
        $this->addSql('DROP TABLE billing_plan');
        $this->addSql('DROP TABLE billing_subscription');
    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE billing_address (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, zip VARCHAR(20) DEFAULT NULL COLLATE utf8_unicode_ci, country VARCHAR(50) DEFAULT NULL COLLATE utf8_unicode_ci, validation_status VARCHAR(30) NOT NULL COLLATE utf8_unicode_ci, first_name VARCHAR(150) DEFAULT NULL COLLATE utf8_unicode_ci, last_name VARCHAR(150) DEFAULT NULL COLLATE utf8_unicode_ci, email VARCHAR(70) DEFAULT NULL COLLATE utf8_unicode_ci, company VARCHAR(250) DEFAULT NULL COLLATE utf8_unicode_ci, phone VARCHAR(50) DEFAULT NULL COLLATE utf8_unicode_ci, line1 VARCHAR(150) DEFAULT NULL COLLATE utf8_unicode_ci, line2 VARCHAR(150) DEFAULT NULL COLLATE utf8_unicode_ci, line3 VARCHAR(150) DEFAULT NULL COLLATE utf8_unicode_ci, city VARCHAR(50) DEFAULT NULL COLLATE utf8_unicode_ci, state_code VARCHAR(50) DEFAULT NULL COLLATE utf8_unicode_ci, state VARCHAR(50) DEFAULT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX UNIQ_6660E4569395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billing_customer (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, charge_bee_id VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, card_status INT NOT NULL, trial_started_at DATETIME DEFAULT NULL, trial_ended_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_99E5E7F5577602A (charge_bee_id), UNIQUE INDEX UNIQ_99E5E7F5A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billing_hosted_page (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, charge_bee_id VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, type INT NOT NULL, state INT NOT NULL, created_at DATETIME NOT NULL, expires_at DATETIME DEFAULT NULL, entity_id INT NOT NULL, payload LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_22A2283577602A (charge_bee_id), INDEX IDX_22A22839395C3F3 (customer_id), INDEX search_main (customer_id, type, entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billing_plan (id INT AUTO_INCREMENT NOT NULL, charge_bee_id VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, name VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, price INT NOT NULL, currency_code VARCHAR(3) NOT NULL COLLATE utf8_unicode_ci, active TINYINT(1) NOT NULL, charge_interval INT NOT NULL, charge_interval_unit INT NOT NULL, setup_fee INT DEFAULT NULL, meta_data LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:json_array)\', has_trial TINYINT(1) NOT NULL, trial_interval INT DEFAULT NULL, trial_interval_unit INT DEFAULT NULL, UNIQUE INDEX UNIQ_A22865BA577602A (charge_bee_id), INDEX plan_active_state (active), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billing_subscription (id INT AUTO_INCREMENT NOT NULL, plan_id INT DEFAULT NULL, customer_id INT DEFAULT NULL, charge_bee_id VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, items LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:json_array)\', status SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, next_charge_at DATETIME DEFAULT NULL, has_card TINYINT(1) NOT NULL, started_at DATETIME DEFAULT NULL, activated_at DATETIME DEFAULT NULL, trial_start DATETIME DEFAULT NULL, trial_end DATETIME DEFAULT NULL, current_period_start DATETIME DEFAULT NULL, current_period_end DATETIME DEFAULT NULL, charge_interval INT NOT NULL, charge_period INT NOT NULL, cancelled_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_16912F26577602A (charge_bee_id), INDEX IDX_16912F26E899029B (plan_id), INDEX subscription_status_idx (status), INDEX IDX_16912F269395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE billing_address ADD CONSTRAINT FK_6660E4569395C3F3 FOREIGN KEY (customer_id) REFERENCES billing_customer (id)');
        $this->addSql('ALTER TABLE billing_customer ADD CONSTRAINT FK_99E5E7F5A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE billing_hosted_page ADD CONSTRAINT FK_22A22839395C3F3 FOREIGN KEY (customer_id) REFERENCES billing_customer (id)');
        $this->addSql('ALTER TABLE billing_subscription ADD CONSTRAINT FK_16912F269395C3F3 FOREIGN KEY (customer_id) REFERENCES billing_customer (id)');
        $this->addSql('ALTER TABLE billing_subscription ADD CONSTRAINT FK_16912F26E899029B FOREIGN KEY (plan_id) REFERENCES billing_plan (id)');
    }
}
