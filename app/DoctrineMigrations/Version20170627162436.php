<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170627162436 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, status SMALLINT NOT NULL, type SMALLINT NOT NULL, INDEX IDX_3AF34668727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_categories (category_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9D94808412469DE2 (category_id), INDEX IDX_9D948084A76ED395 (user_id), PRIMARY KEY(category_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filters (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, status SMALLINT NOT NULL, type SMALLINT NOT NULL, INDEX IDX_7877678D727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_filters (filter_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_207A7987D395B25E (filter_id), INDEX IDX_207A7987A76ED395 (user_id), PRIMARY KEY(filter_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user_group (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_583D1F3E5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE images (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, model_id INT DEFAULT NULL, updated_at DATETIME DEFAULT NULL, imageName VARCHAR(255) NOT NULL, imageSize INT DEFAULT NULL, INDEX IDX_E01FBE6A4584665A (product_id), INDEX IDX_E01FBE6A7975B7E7 (model_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_images (image_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_854DA5573DA5256D (image_id), INDEX IDX_854DA557A76ED395 (user_id), PRIMARY KEY(image_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventories (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, status SMALLINT NOT NULL, type SMALLINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_936C863DA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inventory_items (id INT AUTO_INCREMENT NOT NULL, price_id INT DEFAULT NULL, model_id INT DEFAULT NULL, inventory_id INT DEFAULT NULL, customer_name VARCHAR(255) DEFAULT NULL, status SMALLINT NOT NULL, type SMALLINT NOT NULL, barcode VARCHAR(255) DEFAULT NULL, quantity INT NOT NULL, purchase_price NUMERIC(10, 2) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_3D82424DD614C7E7 (price_id), INDEX IDX_3D82424D7975B7E7 (model_id), INDEX IDX_3D82424D9EEA759 (inventory_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE models (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, status SMALLINT NOT NULL, INDEX IDX_E4D630094584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_models (model_id INT NOT NULL, filter_id INT NOT NULL, INDEX IDX_C9FD6E777975B7E7 (model_id), INDEX IDX_C9FD6E77D395B25E (filter_id), PRIMARY KEY(model_id, filter_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_models (model_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_81842B347975B7E7 (model_id), INDEX IDX_81842B34A76ED395 (user_id), PRIMARY KEY(model_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prices (id INT AUTO_INCREMENT NOT NULL, model_id INT DEFAULT NULL, value NUMERIC(10, 2) NOT NULL, INDEX IDX_E4CB6D597975B7E7 (model_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_prices (price_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_81997664D614C7E7 (price_id), INDEX IDX_81997664A76ED395 (user_id), PRIMARY KEY(price_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prices_price_types (price_id INT NOT NULL, pricetype_id INT NOT NULL, INDEX IDX_F153ADBDD614C7E7 (price_id), INDEX IDX_F153ADBD71E6C340 (pricetype_id), PRIMARY KEY(price_id, pricetype_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE price_types (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, status SMALLINT NOT NULL, site SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_price_types (pricetype_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_DBE248C571E6C340 (pricetype_id), INDEX IDX_DBE248C5A76ED395 (user_id), PRIMARY KEY(pricetype_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, title_old VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, status SMALLINT NOT NULL, first_rate VARCHAR(100) DEFAULT NULL, remark VARCHAR(100) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_products (product_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_4C0DE214584665A (product_id), INDEX IDX_4C0DE2112469DE2 (category_id), PRIMARY KEY(product_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_products (product_id INT NOT NULL, filter_id INT NOT NULL, INDEX IDX_8ED55FFA4584665A (product_id), INDEX IDX_8ED55FFAD395B25E (filter_id), PRIMARY KEY(product_id, filter_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_products (product_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_5337BE5A4584665A (product_id), INDEX IDX_5337BE5AA76ED395 (user_id), PRIMARY KEY(product_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, date_of_birth DATETIME DEFAULT NULL, firstname VARCHAR(64) DEFAULT NULL, lastname VARCHAR(64) DEFAULT NULL, website VARCHAR(64) DEFAULT NULL, biography VARCHAR(1000) DEFAULT NULL, gender VARCHAR(1) DEFAULT NULL, locale VARCHAR(8) DEFAULT NULL, timezone VARCHAR(64) DEFAULT NULL, phone VARCHAR(64) DEFAULT NULL, facebook_uid VARCHAR(255) DEFAULT NULL, facebook_name VARCHAR(255) DEFAULT NULL, facebook_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', twitter_uid VARCHAR(255) DEFAULT NULL, twitter_name VARCHAR(255) DEFAULT NULL, twitter_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', gplus_uid VARCHAR(255) DEFAULT NULL, gplus_name VARCHAR(255) DEFAULT NULL, gplus_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', token VARCHAR(255) DEFAULT NULL, two_step_code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_C560D76192FC23A8 (username_canonical), UNIQUE INDEX UNIQ_C560D761A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_C560D761C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fos_user_user_group (user_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_B3C77447A76ED395 (user_id), INDEX IDX_B3C77447FE54D947 (group_id), PRIMARY KEY(user_id, group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_classes (id INT UNSIGNED AUTO_INCREMENT NOT NULL, class_type VARCHAR(200) NOT NULL, UNIQUE INDEX UNIQ_69DD750638A36066 (class_type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_security_identities (id INT UNSIGNED AUTO_INCREMENT NOT NULL, identifier VARCHAR(200) NOT NULL, username TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8835EE78772E836AF85E0677 (identifier, username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_object_identities (id INT UNSIGNED AUTO_INCREMENT NOT NULL, parent_object_identity_id INT UNSIGNED DEFAULT NULL, class_id INT UNSIGNED NOT NULL, object_identifier VARCHAR(100) NOT NULL, entries_inheriting TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_9407E5494B12AD6EA000B10 (object_identifier, class_id), INDEX IDX_9407E54977FA751A (parent_object_identity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_object_identity_ancestors (object_identity_id INT UNSIGNED NOT NULL, ancestor_id INT UNSIGNED NOT NULL, INDEX IDX_825DE2993D9AB4A6 (object_identity_id), INDEX IDX_825DE299C671CEA1 (ancestor_id), PRIMARY KEY(object_identity_id, ancestor_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE acl_entries (id INT UNSIGNED AUTO_INCREMENT NOT NULL, class_id INT UNSIGNED NOT NULL, object_identity_id INT UNSIGNED DEFAULT NULL, security_identity_id INT UNSIGNED NOT NULL, field_name VARCHAR(50) DEFAULT NULL, ace_order SMALLINT UNSIGNED NOT NULL, mask INT NOT NULL, granting TINYINT(1) NOT NULL, granting_strategy VARCHAR(30) NOT NULL, audit_success TINYINT(1) NOT NULL, audit_failure TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4 (class_id, object_identity_id, field_name, ace_order), INDEX IDX_46C8B806EA000B103D9AB4A6DF9183C9 (class_id, object_identity_id, security_identity_id), INDEX IDX_46C8B806EA000B10 (class_id), INDEX IDX_46C8B8063D9AB4A6 (object_identity_id), INDEX IDX_46C8B806DF9183C9 (security_identity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF34668727ACA70 FOREIGN KEY (parent_id) REFERENCES categories (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE user_categories ADD CONSTRAINT FK_9D94808412469DE2 FOREIGN KEY (category_id) REFERENCES categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_categories ADD CONSTRAINT FK_9D948084A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filters ADD CONSTRAINT FK_7877678D727ACA70 FOREIGN KEY (parent_id) REFERENCES filters (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE user_filters ADD CONSTRAINT FK_207A7987D395B25E FOREIGN KEY (filter_id) REFERENCES filters (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_filters ADD CONSTRAINT FK_207A7987A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE images ADD CONSTRAINT FK_E01FBE6A4584665A FOREIGN KEY (product_id) REFERENCES products (id)');
        $this->addSql('ALTER TABLE images ADD CONSTRAINT FK_E01FBE6A7975B7E7 FOREIGN KEY (model_id) REFERENCES models (id)');
        $this->addSql('ALTER TABLE user_images ADD CONSTRAINT FK_854DA5573DA5256D FOREIGN KEY (image_id) REFERENCES images (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_images ADD CONSTRAINT FK_854DA557A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE inventories ADD CONSTRAINT FK_936C863DA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE inventory_items ADD CONSTRAINT FK_3D82424DD614C7E7 FOREIGN KEY (price_id) REFERENCES prices (id)');
        $this->addSql('ALTER TABLE inventory_items ADD CONSTRAINT FK_3D82424D7975B7E7 FOREIGN KEY (model_id) REFERENCES models (id)');
        $this->addSql('ALTER TABLE inventory_items ADD CONSTRAINT FK_3D82424D9EEA759 FOREIGN KEY (inventory_id) REFERENCES inventories (id)');
        $this->addSql('ALTER TABLE models ADD CONSTRAINT FK_E4D630094584665A FOREIGN KEY (product_id) REFERENCES products (id)');
        $this->addSql('ALTER TABLE filter_models ADD CONSTRAINT FK_C9FD6E777975B7E7 FOREIGN KEY (model_id) REFERENCES models (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filter_models ADD CONSTRAINT FK_C9FD6E77D395B25E FOREIGN KEY (filter_id) REFERENCES filters (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_models ADD CONSTRAINT FK_81842B347975B7E7 FOREIGN KEY (model_id) REFERENCES models (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_models ADD CONSTRAINT FK_81842B34A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prices ADD CONSTRAINT FK_E4CB6D597975B7E7 FOREIGN KEY (model_id) REFERENCES models (id)');
        $this->addSql('ALTER TABLE user_prices ADD CONSTRAINT FK_81997664D614C7E7 FOREIGN KEY (price_id) REFERENCES prices (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_prices ADD CONSTRAINT FK_81997664A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prices_price_types ADD CONSTRAINT FK_F153ADBDD614C7E7 FOREIGN KEY (price_id) REFERENCES prices (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prices_price_types ADD CONSTRAINT FK_F153ADBD71E6C340 FOREIGN KEY (pricetype_id) REFERENCES price_types (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_price_types ADD CONSTRAINT FK_DBE248C571E6C340 FOREIGN KEY (pricetype_id) REFERENCES price_types (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_price_types ADD CONSTRAINT FK_DBE248C5A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_products ADD CONSTRAINT FK_4C0DE214584665A FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_products ADD CONSTRAINT FK_4C0DE2112469DE2 FOREIGN KEY (category_id) REFERENCES categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filter_products ADD CONSTRAINT FK_8ED55FFA4584665A FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filter_products ADD CONSTRAINT FK_8ED55FFAD395B25E FOREIGN KEY (filter_id) REFERENCES filters (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_products ADD CONSTRAINT FK_5337BE5A4584665A FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_products ADD CONSTRAINT FK_5337BE5AA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fos_user_user_group ADD CONSTRAINT FK_B3C77447A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fos_user_user_group ADD CONSTRAINT FK_B3C77447FE54D947 FOREIGN KEY (group_id) REFERENCES fos_user_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_object_identities ADD CONSTRAINT FK_9407E54977FA751A FOREIGN KEY (parent_object_identity_id) REFERENCES acl_object_identities (id)');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors ADD CONSTRAINT FK_825DE2993D9AB4A6 FOREIGN KEY (object_identity_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors ADD CONSTRAINT FK_825DE299C671CEA1 FOREIGN KEY (ancestor_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B806EA000B10 FOREIGN KEY (class_id) REFERENCES acl_classes (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B8063D9AB4A6 FOREIGN KEY (object_identity_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B806DF9183C9 FOREIGN KEY (security_identity_id) REFERENCES acl_security_identities (id) ON UPDATE CASCADE ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE categories DROP FOREIGN KEY FK_3AF34668727ACA70');
        $this->addSql('ALTER TABLE user_categories DROP FOREIGN KEY FK_9D94808412469DE2');
        $this->addSql('ALTER TABLE category_products DROP FOREIGN KEY FK_4C0DE2112469DE2');
        $this->addSql('ALTER TABLE filters DROP FOREIGN KEY FK_7877678D727ACA70');
        $this->addSql('ALTER TABLE user_filters DROP FOREIGN KEY FK_207A7987D395B25E');
        $this->addSql('ALTER TABLE filter_models DROP FOREIGN KEY FK_C9FD6E77D395B25E');
        $this->addSql('ALTER TABLE filter_products DROP FOREIGN KEY FK_8ED55FFAD395B25E');
        $this->addSql('ALTER TABLE fos_user_user_group DROP FOREIGN KEY FK_B3C77447FE54D947');
        $this->addSql('ALTER TABLE user_images DROP FOREIGN KEY FK_854DA5573DA5256D');
        $this->addSql('ALTER TABLE inventory_items DROP FOREIGN KEY FK_3D82424D9EEA759');
        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6A7975B7E7');
        $this->addSql('ALTER TABLE inventory_items DROP FOREIGN KEY FK_3D82424D7975B7E7');
        $this->addSql('ALTER TABLE filter_models DROP FOREIGN KEY FK_C9FD6E777975B7E7');
        $this->addSql('ALTER TABLE user_models DROP FOREIGN KEY FK_81842B347975B7E7');
        $this->addSql('ALTER TABLE prices DROP FOREIGN KEY FK_E4CB6D597975B7E7');
        $this->addSql('ALTER TABLE inventory_items DROP FOREIGN KEY FK_3D82424DD614C7E7');
        $this->addSql('ALTER TABLE user_prices DROP FOREIGN KEY FK_81997664D614C7E7');
        $this->addSql('ALTER TABLE prices_price_types DROP FOREIGN KEY FK_F153ADBDD614C7E7');
        $this->addSql('ALTER TABLE prices_price_types DROP FOREIGN KEY FK_F153ADBD71E6C340');
        $this->addSql('ALTER TABLE user_price_types DROP FOREIGN KEY FK_DBE248C571E6C340');
        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6A4584665A');
        $this->addSql('ALTER TABLE models DROP FOREIGN KEY FK_E4D630094584665A');
        $this->addSql('ALTER TABLE category_products DROP FOREIGN KEY FK_4C0DE214584665A');
        $this->addSql('ALTER TABLE filter_products DROP FOREIGN KEY FK_8ED55FFA4584665A');
        $this->addSql('ALTER TABLE user_products DROP FOREIGN KEY FK_5337BE5A4584665A');
        $this->addSql('ALTER TABLE user_categories DROP FOREIGN KEY FK_9D948084A76ED395');
        $this->addSql('ALTER TABLE user_filters DROP FOREIGN KEY FK_207A7987A76ED395');
        $this->addSql('ALTER TABLE user_images DROP FOREIGN KEY FK_854DA557A76ED395');
        $this->addSql('ALTER TABLE inventories DROP FOREIGN KEY FK_936C863DA76ED395');
        $this->addSql('ALTER TABLE user_models DROP FOREIGN KEY FK_81842B34A76ED395');
        $this->addSql('ALTER TABLE user_prices DROP FOREIGN KEY FK_81997664A76ED395');
        $this->addSql('ALTER TABLE user_price_types DROP FOREIGN KEY FK_DBE248C5A76ED395');
        $this->addSql('ALTER TABLE user_products DROP FOREIGN KEY FK_5337BE5AA76ED395');
        $this->addSql('ALTER TABLE fos_user_user_group DROP FOREIGN KEY FK_B3C77447A76ED395');
        $this->addSql('ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B806EA000B10');
        $this->addSql('ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B806DF9183C9');
        $this->addSql('ALTER TABLE acl_object_identities DROP FOREIGN KEY FK_9407E54977FA751A');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors DROP FOREIGN KEY FK_825DE2993D9AB4A6');
        $this->addSql('ALTER TABLE acl_object_identity_ancestors DROP FOREIGN KEY FK_825DE299C671CEA1');
        $this->addSql('ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B8063D9AB4A6');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE user_categories');
        $this->addSql('DROP TABLE filters');
        $this->addSql('DROP TABLE user_filters');
        $this->addSql('DROP TABLE fos_user_group');
        $this->addSql('DROP TABLE images');
        $this->addSql('DROP TABLE user_images');
        $this->addSql('DROP TABLE inventories');
        $this->addSql('DROP TABLE inventory_items');
        $this->addSql('DROP TABLE models');
        $this->addSql('DROP TABLE filter_models');
        $this->addSql('DROP TABLE user_models');
        $this->addSql('DROP TABLE prices');
        $this->addSql('DROP TABLE user_prices');
        $this->addSql('DROP TABLE prices_price_types');
        $this->addSql('DROP TABLE price_types');
        $this->addSql('DROP TABLE user_price_types');
        $this->addSql('DROP TABLE products');
        $this->addSql('DROP TABLE category_products');
        $this->addSql('DROP TABLE filter_products');
        $this->addSql('DROP TABLE user_products');
        $this->addSql('DROP TABLE fos_user_user');
        $this->addSql('DROP TABLE fos_user_user_group');
        $this->addSql('DROP TABLE acl_classes');
        $this->addSql('DROP TABLE acl_security_identities');
        $this->addSql('DROP TABLE acl_object_identities');
        $this->addSql('DROP TABLE acl_object_identity_ancestors');
        $this->addSql('DROP TABLE acl_entries');
    }
}
