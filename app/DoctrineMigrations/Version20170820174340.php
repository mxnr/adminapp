<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170820174340 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE default_image (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_seo CHANGE site site VARCHAR(16) NOT NULL');
        $this->addSql('ALTER TABLE config_logo ADD default_image_id INT DEFAULT NULL, ADD first_color VARCHAR(6) DEFAULT NULL, ADD second_color VARCHAR(6) DEFAULT NULL, ADD third_color VARCHAR(6) DEFAULT NULL, DROP default_image');
        $this->addSql('ALTER TABLE config_logo ADD CONSTRAINT FK_742EC6E33BE11523 FOREIGN KEY (default_image_id) REFERENCES default_image (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_742EC6E33BE11523 ON config_logo (default_image_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE config_logo DROP FOREIGN KEY FK_742EC6E33BE11523');
        $this->addSql('DROP TABLE default_image');
        $this->addSql('DROP INDEX IDX_742EC6E33BE11523 ON config_logo');
        $this->addSql('ALTER TABLE config_logo ADD default_image VARCHAR(48) DEFAULT NULL COLLATE utf8_unicode_ci, DROP default_image_id, DROP first_color, DROP second_color, DROP third_color');
        $this->addSql('ALTER TABLE user_seo CHANGE site site SMALLINT NOT NULL');
    }
}
