<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170811210101 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE config_social_links (id INT AUTO_INCREMENT NOT NULL, config_id INT DEFAULT NULL, twitter_link VARCHAR(64) DEFAULT NULL, facebook_link VARCHAR(64) DEFAULT NULL, instagram_link VARCHAR(64) DEFAULT NULL, UNIQUE INDEX UNIQ_4816B4FD24DB0683 (config_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE config_social_links ADD CONSTRAINT FK_4816B4FD24DB0683 FOREIGN KEY (config_id) REFERENCES site_config (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE config_social_links');
    }
}
