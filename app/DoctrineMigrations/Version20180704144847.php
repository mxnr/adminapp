<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180704144847 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE client_domain (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, domain VARCHAR(255) DEFAULT \'1\' NOT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, domain_type TINYINT(1) DEFAULT \'0\' NOT NULL, INDEX IDX_F20F55B87E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_domain ADD CONSTRAINT FK_F20F55B87E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE client_domain');
    }
}
