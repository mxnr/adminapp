<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180429214235 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE inventory_items_images (inventory_item_id INT NOT NULL, inventory_item_image_id INT NOT NULL, INDEX IDX_B70725B9536BF4A2 (inventory_item_id), INDEX IDX_B70725B932A52E52 (inventory_item_image_id), PRIMARY KEY(inventory_item_id, inventory_item_image_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE inventory_items_images ADD CONSTRAINT FK_B70725B9536BF4A2 FOREIGN KEY (inventory_item_id) REFERENCES images (id)');
        $this->addSql('ALTER TABLE inventory_items_images ADD CONSTRAINT FK_B70725B932A52E52 FOREIGN KEY (inventory_item_image_id) REFERENCES inventory_items (id)');
        $this->addSql('DROP TABLE accessory_inventory_items');
        $this->addSql('DROP TABLE accessory_model');
        $this->addSql('ALTER TABLE inventory_items ADD slug VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE accessory_inventory_items (inventoryitem_id INT NOT NULL, accessory_id INT NOT NULL, INDEX IDX_C13635A5EB802976 (inventoryitem_id), INDEX IDX_C13635A527E8CC78 (accessory_id), PRIMARY KEY(inventoryitem_id, accessory_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE accessory_model (model_id INT NOT NULL, accessory_id INT NOT NULL, INDEX IDX_5DECE2797975B7E7 (model_id), INDEX IDX_5DECE27927E8CC78 (accessory_id), PRIMARY KEY(model_id, accessory_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE accessory_inventory_items ADD CONSTRAINT FK_C13635A527E8CC78 FOREIGN KEY (accessory_id) REFERENCES accessory (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE accessory_inventory_items ADD CONSTRAINT FK_C13635A5EB802976 FOREIGN KEY (inventoryitem_id) REFERENCES inventory_items (id)');
        $this->addSql('ALTER TABLE accessory_model ADD CONSTRAINT FK_5DECE27927E8CC78 FOREIGN KEY (accessory_id) REFERENCES accessory (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE accessory_model ADD CONSTRAINT FK_5DECE2797975B7E7 FOREIGN KEY (model_id) REFERENCES models (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE inventory_items_images');
        $this->addSql('ALTER TABLE inventory_items DROP slug');
    }
}
