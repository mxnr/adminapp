<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180306034737 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE domain_zone (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, zone_id VARCHAR(255) NOT NULL, distribution_id VARCHAR(255) NOT NULL, domain VARCHAR(255) NOT NULL, ns_list LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\', UNIQUE INDEX UNIQ_D37ED607E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE domain_zone ADD CONSTRAINT FK_D37ED607E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE fos_user_user ADD store_address VARCHAR(255) DEFAULT NULL');
        $this->addSql('CREATE INDEX storeAddressSearch ON fos_user_user (store_address)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE domain_zone');
        $this->addSql('DROP INDEX storeAddressSearch ON fos_user_user');
        $this->addSql('ALTER TABLE fos_user_user DROP store_address');
    }
}
