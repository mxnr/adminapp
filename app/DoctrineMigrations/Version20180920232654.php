<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180920232654 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders ADD store_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX owner_store_id ON orders (owner_id, store_id)');
        $this->addSql('ALTER TABLE inventory_items ADD store_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX owner_store_id ON inventory_items (owner_id, store_id)');
        $this->addSql('ALTER TABLE fos_user_customer ADD store_id INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX owner_store_id ON fos_user_customer (owner_id, store_id)');
        $this->addSql('CREATE TABLE sequential_next_val (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, next_val INT DEFAULT NULL, type SMALLINT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX owner_store_id ON fos_user_customer');
        $this->addSql('ALTER TABLE fos_user_customer DROP store_id');
        $this->addSql('DROP INDEX owner_store_id ON inventory_items');
        $this->addSql('ALTER TABLE inventory_items DROP store_id');
        $this->addSql('DROP INDEX owner_store_id ON orders');
        $this->addSql('ALTER TABLE orders DROP store_id');
        $this->addSql('DROP TABLE sequential_next_val');
    }
}
