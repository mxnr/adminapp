<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170810212544 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE slider_image_setting (id INT AUTO_INCREMENT NOT NULL, slider_image_id INT DEFAULT NULL, is_display_landing TINYINT(1) NOT NULL, is_display_buy TINYINT(1) NOT NULL, is_display_sell TINYINT(1) NOT NULL, is_display_repair TINYINT(1) NOT NULL, link VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_62934A59E2BB318B (slider_image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE config_texts (id INT AUTO_INCREMENT NOT NULL, config_id INT DEFAULT NULL, is_business_name_visible TINYINT(1) NOT NULL, business_name VARCHAR(64) DEFAULT NULL, font VARCHAR(64) DEFAULT NULL, business_name_color VARCHAR(6) NOT NULL, business_name_size VARCHAR(16) NOT NULL, landing_page_title VARCHAR(255) DEFAULT NULL, testimonials_title VARCHAR(255) DEFAULT NULL, buy_title VARCHAR(255) DEFAULT NULL, sell_title VARCHAR(255) DEFAULT NULL, repair_title VARCHAR(255) DEFAULT NULL, buy_description VARCHAR(255) DEFAULT NULL, sell_description VARCHAR(255) DEFAULT NULL, repair_description VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_A31841FF24DB0683 (config_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE offer_image_setting (id INT AUTO_INCREMENT NOT NULL, offer_image_id INT DEFAULT NULL, is_active TINYINT(1) NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_5630BC4F44D8950F (offer_image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE faq_question (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, question VARCHAR(255) NOT NULL, answer LONGTEXT NOT NULL, category VARCHAR(255) NOT NULL, INDEX IDX_4A55B0597E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE testimonial (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, testimonial VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, location VARCHAR(255) NOT NULL, INDEX IDX_E6BDCDF77E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE config_logo (id INT AUTO_INCREMENT NOT NULL, config_id INT DEFAULT NULL, default_image VARCHAR(48) DEFAULT NULL, is_use_default_image TINYINT(1) NOT NULL, is_use_selected_as_favicon TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_742EC6E324DB0683 (config_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE config_slider (id INT AUTO_INCREMENT NOT NULL, config_id INT DEFAULT NULL, is_display_landing TINYINT(1) NOT NULL, is_display_buy TINYINT(1) NOT NULL, is_display_sell TINYINT(1) NOT NULL, is_display_repair TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_B9A67CC524DB0683 (config_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE slider_image_setting ADD CONSTRAINT FK_62934A59E2BB318B FOREIGN KEY (slider_image_id) REFERENCES images (id)');
        $this->addSql('ALTER TABLE config_texts ADD CONSTRAINT FK_A31841FF24DB0683 FOREIGN KEY (config_id) REFERENCES site_config (id)');
        $this->addSql('ALTER TABLE offer_image_setting ADD CONSTRAINT FK_5630BC4F44D8950F FOREIGN KEY (offer_image_id) REFERENCES images (id)');
        $this->addSql('ALTER TABLE faq_question ADD CONSTRAINT FK_4A55B0597E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE testimonial ADD CONSTRAINT FK_E6BDCDF77E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE config_logo ADD CONSTRAINT FK_742EC6E324DB0683 FOREIGN KEY (config_id) REFERENCES site_config (id)');
        $this->addSql('ALTER TABLE config_slider ADD CONSTRAINT FK_B9A67CC524DB0683 FOREIGN KEY (config_id) REFERENCES site_config (id)');
        $this->addSql('ALTER TABLE site_config DROP FOREIGN KEY FK_A424812359027487');
        $this->addSql('DROP INDEX IDX_A424812359027487 ON site_config');
        $this->addSql('ALTER TABLE site_config ADD theme_sell_id INT DEFAULT NULL, ADD theme_buy_id INT DEFAULT NULL, ADD theme_repair_id INT DEFAULT NULL, ADD background_color VARCHAR(6) NOT NULL, CHANGE theme_id theme_root_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE site_config ADD CONSTRAINT FK_A424812320656DA5 FOREIGN KEY (theme_root_id) REFERENCES site_theme (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE site_config ADD CONSTRAINT FK_A4248123F58440D7 FOREIGN KEY (theme_sell_id) REFERENCES site_theme (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE site_config ADD CONSTRAINT FK_A42481239AC3A705 FOREIGN KEY (theme_buy_id) REFERENCES site_theme (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE site_config ADD CONSTRAINT FK_A42481238D40E672 FOREIGN KEY (theme_repair_id) REFERENCES site_theme (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_A424812320656DA5 ON site_config (theme_root_id)');
        $this->addSql('CREATE INDEX IDX_A4248123F58440D7 ON site_config (theme_sell_id)');
        $this->addSql('CREATE INDEX IDX_A42481239AC3A705 ON site_config (theme_buy_id)');
        $this->addSql('CREATE INDEX IDX_A42481238D40E672 ON site_config (theme_repair_id)');
        $this->addSql('ALTER TABLE site_theme ADD type SMALLINT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE slider_image_setting');
        $this->addSql('DROP TABLE config_texts');
        $this->addSql('DROP TABLE offer_image_setting');
        $this->addSql('DROP TABLE faq_question');
        $this->addSql('DROP TABLE testimonial');
        $this->addSql('DROP TABLE config_logo');
        $this->addSql('DROP TABLE config_slider');
        $this->addSql('ALTER TABLE site_config DROP FOREIGN KEY FK_A424812320656DA5');
        $this->addSql('ALTER TABLE site_config DROP FOREIGN KEY FK_A4248123F58440D7');
        $this->addSql('ALTER TABLE site_config DROP FOREIGN KEY FK_A42481239AC3A705');
        $this->addSql('ALTER TABLE site_config DROP FOREIGN KEY FK_A42481238D40E672');
        $this->addSql('DROP INDEX IDX_A424812320656DA5 ON site_config');
        $this->addSql('DROP INDEX IDX_A4248123F58440D7 ON site_config');
        $this->addSql('DROP INDEX IDX_A42481239AC3A705 ON site_config');
        $this->addSql('DROP INDEX IDX_A42481238D40E672 ON site_config');
        $this->addSql('ALTER TABLE site_config ADD theme_id INT DEFAULT NULL, DROP theme_root_id, DROP theme_sell_id, DROP theme_buy_id, DROP theme_repair_id, DROP background_color');
        $this->addSql('ALTER TABLE site_config ADD CONSTRAINT FK_A424812359027487 FOREIGN KEY (theme_id) REFERENCES site_theme (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_A424812359027487 ON site_config (theme_id)');
        $this->addSql('ALTER TABLE site_theme DROP type');
    }
}
