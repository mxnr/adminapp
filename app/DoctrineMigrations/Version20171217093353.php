<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171217093353 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders ADD pickup_location_id INT DEFAULT NULL, DROP pickup_location');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEC77EA60D FOREIGN KEY (pickup_location_id) REFERENCES locations (id)');
        $this->addSql('CREATE INDEX IDX_E52FFDEEC77EA60D ON orders (pickup_location_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEEC77EA60D');
        $this->addSql('DROP INDEX IDX_E52FFDEEC77EA60D ON orders');
        $this->addSql('ALTER TABLE orders ADD pickup_location VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP pickup_location_id');
    }
}
