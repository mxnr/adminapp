<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171126124951 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE accessory (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, title VARCHAR(50) NOT NULL, status SMALLINT NOT NULL, INDEX IDX_A1B1251C727ACA70 (parent_id), INDEX IDX_A1B1251C7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE accessory_user (accessory_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_91CA9A5227E8CC78 (accessory_id), INDEX IDX_91CA9A52A76ED395 (user_id), PRIMARY KEY(accessory_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE advertise (id INT AUTO_INCREMENT NOT NULL, advertise_category_id INT DEFAULT NULL, description LONGTEXT NOT NULL, name VARCHAR(255) NOT NULL, orderNumber INT NOT NULL, url VARCHAR(500) NOT NULL, INDEX IDX_37B29FD9752D3EC (advertise_category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE advertise_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, orderNumber INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE accessory_model (model_id INT NOT NULL, accessory_id INT NOT NULL, INDEX IDX_5DECE2797975B7E7 (model_id), INDEX IDX_5DECE27927E8CC78 (accessory_id), PRIMARY KEY(model_id, accessory_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE accessory_product (product_id INT NOT NULL, accessory_id INT NOT NULL, INDEX IDX_8D42A1794584665A (product_id), INDEX IDX_8D42A17927E8CC78 (accessory_id), PRIMARY KEY(product_id, accessory_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE accessory ADD CONSTRAINT FK_A1B1251C727ACA70 FOREIGN KEY (parent_id) REFERENCES accessory (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE accessory ADD CONSTRAINT FK_A1B1251C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE accessory_user ADD CONSTRAINT FK_91CA9A5227E8CC78 FOREIGN KEY (accessory_id) REFERENCES accessory (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE accessory_user ADD CONSTRAINT FK_91CA9A52A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE advertise ADD CONSTRAINT FK_37B29FD9752D3EC FOREIGN KEY (advertise_category_id) REFERENCES advertise_category (id)');
        $this->addSql('ALTER TABLE accessory_model ADD CONSTRAINT FK_5DECE2797975B7E7 FOREIGN KEY (model_id) REFERENCES models (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE accessory_model ADD CONSTRAINT FK_5DECE27927E8CC78 FOREIGN KEY (accessory_id) REFERENCES accessory (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE accessory_product ADD CONSTRAINT FK_8D42A1794584665A FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE accessory_product ADD CONSTRAINT FK_8D42A17927E8CC78 FOREIGN KEY (accessory_id) REFERENCES accessory (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE accessory DROP FOREIGN KEY FK_A1B1251C727ACA70');
        $this->addSql('ALTER TABLE accessory_user DROP FOREIGN KEY FK_91CA9A5227E8CC78');
        $this->addSql('ALTER TABLE accessory_model DROP FOREIGN KEY FK_5DECE27927E8CC78');
        $this->addSql('ALTER TABLE accessory_product DROP FOREIGN KEY FK_8D42A17927E8CC78');
        $this->addSql('ALTER TABLE advertise DROP FOREIGN KEY FK_37B29FD9752D3EC');
        $this->addSql('DROP TABLE accessory');
        $this->addSql('DROP TABLE accessory_user');
        $this->addSql('DROP TABLE advertise');
        $this->addSql('DROP TABLE advertise_category');
        $this->addSql('DROP TABLE accessory_model');
        $this->addSql('DROP TABLE accessory_product');
    }
}
