<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170720130405 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE prices DROP FOREIGN KEY FK_E4CB6D59BE5BFADC');
        $this->addSql('DROP INDEX IDX_E4CB6D59BE5BFADC ON prices');
        $this->addSql('ALTER TABLE prices CHANGE pricetype_id price_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE prices ADD CONSTRAINT FK_E4CB6D59AE6A44CF FOREIGN KEY (price_type_id) REFERENCES price_types (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_E4CB6D59AE6A44CF ON prices (price_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE prices DROP FOREIGN KEY FK_E4CB6D59AE6A44CF');
        $this->addSql('DROP INDEX IDX_E4CB6D59AE6A44CF ON prices');
        $this->addSql('ALTER TABLE prices CHANGE price_type_id priceType_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE prices ADD CONSTRAINT FK_E4CB6D59BE5BFADC FOREIGN KEY (priceType_id) REFERENCES price_types (id)');
        $this->addSql('CREATE INDEX IDX_E4CB6D59BE5BFADC ON prices (priceType_id)');
    }
}
