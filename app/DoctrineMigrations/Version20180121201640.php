<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180121201640 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE accessory_product DROP FOREIGN KEY FK_8D42A1794584665A');
        $this->addSql('ALTER TABLE category_products DROP FOREIGN KEY FK_4C0DE214584665A');
        $this->addSql('ALTER TABLE filter_products DROP FOREIGN KEY FK_8ED55FFA4584665A');
        $this->addSql('ALTER TABLE models DROP FOREIGN KEY FK_E4D630094584665A');
        $this->addSql('ALTER TABLE user_products DROP FOREIGN KEY FK_5337BE5A4584665A');
        $this->addSql('DROP TABLE accessory_product');
        $this->addSql('DROP TABLE category_products');
        $this->addSql('DROP TABLE filter_products');
        $this->addSql('DROP TABLE products');
        $this->addSql('DROP TABLE user_products');
        $this->addSql('DROP INDEX IDX_E4D630094584665A ON models');
        $this->addSql('ALTER TABLE models DROP product_id');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE accessory_product (product_id INT NOT NULL, accessory_id INT NOT NULL, INDEX IDX_8D42A1794584665A (product_id), INDEX IDX_8D42A17927E8CC78 (accessory_id), PRIMARY KEY(product_id, accessory_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_products (product_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_4C0DE214584665A (product_id), INDEX IDX_4C0DE2112469DE2 (category_id), PRIMARY KEY(product_id, category_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_products (product_id INT NOT NULL, filter_id INT NOT NULL, INDEX IDX_8ED55FFA4584665A (product_id), INDEX IDX_8ED55FFAD395B25E (filter_id), PRIMARY KEY(product_id, filter_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE products (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, title_old VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, status SMALLINT NOT NULL, first_rate VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, remark VARCHAR(100) DEFAULT NULL COLLATE utf8_unicode_ci, INDEX IDX_B3BA5A5A7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_products (product_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_5337BE5A4584665A (product_id), INDEX IDX_5337BE5AA76ED395 (user_id), PRIMARY KEY(product_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE accessory_product ADD CONSTRAINT FK_8D42A17927E8CC78 FOREIGN KEY (accessory_id) REFERENCES accessory (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE accessory_product ADD CONSTRAINT FK_8D42A1794584665A FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_products ADD CONSTRAINT FK_4C0DE2112469DE2 FOREIGN KEY (category_id) REFERENCES categories (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_products ADD CONSTRAINT FK_4C0DE214584665A FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filter_products ADD CONSTRAINT FK_8ED55FFA4584665A FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filter_products ADD CONSTRAINT FK_8ED55FFAD395B25E FOREIGN KEY (filter_id) REFERENCES filters (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE products ADD CONSTRAINT FK_B3BA5A5A7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE user_products ADD CONSTRAINT FK_5337BE5A4584665A FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_products ADD CONSTRAINT FK_5337BE5AA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE models ADD product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE models ADD CONSTRAINT FK_E4D630094584665A FOREIGN KEY (product_id) REFERENCES products (id)');
        $this->addSql('CREATE INDEX IDX_E4D630094584665A ON models (product_id)');
    }
}
