<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171022221940 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user_user ADD supervisor_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user_user ADD CONSTRAINT FK_C560D76119E9AC5F FOREIGN KEY (supervisor_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_C560D76119E9AC5F ON fos_user_user (supervisor_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fos_user_user DROP FOREIGN KEY FK_C560D76119E9AC5F');
        $this->addSql('DROP INDEX IDX_C560D76119E9AC5F ON fos_user_user');
        $this->addSql('ALTER TABLE fos_user_user DROP supervisor_id');
    }
}
