<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170627191051 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE prices_price_types');
        $this->addSql('ALTER TABLE prices ADD priceType_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE prices ADD CONSTRAINT FK_E4CB6D59BE5BFADC FOREIGN KEY (priceType_id) REFERENCES price_types (id)');
        $this->addSql('CREATE INDEX IDX_E4CB6D59BE5BFADC ON prices (priceType_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE prices_price_types (price_id INT NOT NULL, pricetype_id INT NOT NULL, INDEX IDX_F153ADBDD614C7E7 (price_id), INDEX IDX_F153ADBD71E6C340 (pricetype_id), PRIMARY KEY(price_id, pricetype_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE prices_price_types ADD CONSTRAINT FK_F153ADBD71E6C340 FOREIGN KEY (pricetype_id) REFERENCES price_types (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prices_price_types ADD CONSTRAINT FK_F153ADBDD614C7E7 FOREIGN KEY (price_id) REFERENCES prices (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prices DROP FOREIGN KEY FK_E4CB6D59BE5BFADC');
        $this->addSql('DROP INDEX IDX_E4CB6D59BE5BFADC ON prices');
        $this->addSql('ALTER TABLE prices DROP priceType_id');
    }
}
