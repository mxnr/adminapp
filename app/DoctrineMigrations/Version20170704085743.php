<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170704085743 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6A4584665A');
        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6A7975B7E7');
        $this->addSql('DROP INDEX IDX_E01FBE6A4584665A ON images');
        $this->addSql('DROP INDEX IDX_E01FBE6A7975B7E7 ON images');
        $this->addSql('ALTER TABLE images ADD entity_id INT DEFAULT NULL, DROP product_id, DROP model_id');
        $this->addSql('ALTER TABLE images ADD CONSTRAINT FK_E01FBE6A81257D5D FOREIGN KEY (entity_id) REFERENCES models (id)');
        $this->addSql('CREATE INDEX IDX_E01FBE6A81257D5D ON images (entity_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE images DROP FOREIGN KEY FK_E01FBE6A81257D5D');
        $this->addSql('DROP INDEX IDX_E01FBE6A81257D5D ON images');
        $this->addSql('ALTER TABLE images ADD model_id INT DEFAULT NULL, CHANGE entity_id product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE images ADD CONSTRAINT FK_E01FBE6A4584665A FOREIGN KEY (product_id) REFERENCES products (id)');
        $this->addSql('ALTER TABLE images ADD CONSTRAINT FK_E01FBE6A7975B7E7 FOREIGN KEY (model_id) REFERENCES models (id)');
        $this->addSql('CREATE INDEX IDX_E01FBE6A4584665A ON images (product_id)');
        $this->addSql('CREATE INDEX IDX_E01FBE6A7975B7E7 ON images (model_id)');
    }
}
