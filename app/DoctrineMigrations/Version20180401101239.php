<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180401101239 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_inventory_items DROP FOREIGN KEY FK_99F4EB16EB802976');
        $this->addSql('ALTER TABLE orders_inventory_items ADD CONSTRAINT FK_99F4EB16EB802976 FOREIGN KEY (inventoryitem_id) REFERENCES inventory_items (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE orders_inventory_items DROP FOREIGN KEY FK_99F4EB16EB802976');
        $this->addSql('ALTER TABLE orders_inventory_items ADD CONSTRAINT FK_99F4EB16EB802976 FOREIGN KEY (inventoryitem_id) REFERENCES inventory_items (id)');
    }
}
