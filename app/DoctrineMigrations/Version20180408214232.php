<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180408214232 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_F9B6A51129439E58');
        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_F9B6A511536BF4A2');
        $this->addSql('DROP INDEX idx_f9b6a511536bf4a2 ON order_items');
        $this->addSql('CREATE INDEX IDX_62809DB0536BF4A2 ON order_items (inventory_item_id)');
        $this->addSql('DROP INDEX idx_f9b6a51129439e58 ON order_items');
        $this->addSql('CREATE INDEX IDX_62809DB029439E58 ON order_items (barcode_id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_F9B6A51129439E58 FOREIGN KEY (barcode_id) REFERENCES inventory_item_barcodes (id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_F9B6A511536BF4A2 FOREIGN KEY (inventory_item_id) REFERENCES inventory_items (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_62809DB0536BF4A2');
        $this->addSql('ALTER TABLE order_items DROP FOREIGN KEY FK_62809DB029439E58');
        $this->addSql('DROP INDEX idx_62809db0536bf4a2 ON order_items');
        $this->addSql('CREATE INDEX IDX_F9B6A511536BF4A2 ON order_items (inventory_item_id)');
        $this->addSql('DROP INDEX idx_62809db029439e58 ON order_items');
        $this->addSql('CREATE INDEX IDX_F9B6A51129439E58 ON order_items (barcode_id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB0536BF4A2 FOREIGN KEY (inventory_item_id) REFERENCES inventory_items (id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB029439E58 FOREIGN KEY (barcode_id) REFERENCES inventory_item_barcodes (id)');
    }
}
