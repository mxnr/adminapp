<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180825175933 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client_domain DROP FOREIGN KEY FK_F20F55B87E3C61F9');
        $this->addSql('ALTER TABLE client_domain ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME DEFAULT NULL, DROP pay_status, CHANGE owner_id owner_id INT NOT NULL, CHANGE domain_type domain_type INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE client_domain ADD CONSTRAINT FK_F20F55B87E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client_domain DROP FOREIGN KEY FK_F20F55B87E3C61F9');
        $this->addSql('ALTER TABLE client_domain ADD pay_status INT DEFAULT 0, DROP created_at, DROP updated_at, CHANGE owner_id owner_id INT DEFAULT NULL, CHANGE domain_type domain_type TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE client_domain ADD CONSTRAINT FK_F20F55B87E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
    }
}
