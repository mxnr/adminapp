<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180108002708 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE income_lists DROP FOREIGN KEY FK_D43AD87C9395C3F3');
        $this->addSql('ALTER TABLE inventory_items DROP FOREIGN KEY FK_3D82424D85EC60F2');
        $this->addSql('ALTER TABLE inventory_items DROP FOREIGN KEY FK_3D82424DE415FB15');
        $this->addSql('CREATE TABLE orders_inventory_items (inventoryitem_id INT NOT NULL, order_id INT NOT NULL, INDEX IDX_99F4EB16EB802976 (inventoryitem_id), INDEX IDX_99F4EB168D9F6D38 (order_id), PRIMARY KEY(inventoryitem_id, order_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filter_inventory_items (inventoryitem_id INT NOT NULL, filter_id INT NOT NULL, INDEX IDX_33C7C070EB802976 (inventoryitem_id), INDEX IDX_33C7C070D395B25E (filter_id), PRIMARY KEY(inventoryitem_id, filter_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE accessory_inventory_items (inventoryitem_id INT NOT NULL, accessory_id INT NOT NULL, INDEX IDX_C13635A5EB802976 (inventoryitem_id), INDEX IDX_C13635A527E8CC78 (accessory_id), PRIMARY KEY(inventoryitem_id, accessory_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE orders_inventory_items ADD CONSTRAINT FK_99F4EB16EB802976 FOREIGN KEY (inventoryitem_id) REFERENCES inventory_items (id)');
        $this->addSql('ALTER TABLE orders_inventory_items ADD CONSTRAINT FK_99F4EB168D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filter_inventory_items ADD CONSTRAINT FK_33C7C070EB802976 FOREIGN KEY (inventoryitem_id) REFERENCES inventory_items (id)');
        $this->addSql('ALTER TABLE filter_inventory_items ADD CONSTRAINT FK_33C7C070D395B25E FOREIGN KEY (filter_id) REFERENCES filters (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE accessory_inventory_items ADD CONSTRAINT FK_C13635A5EB802976 FOREIGN KEY (inventoryitem_id) REFERENCES inventory_items (id)');
        $this->addSql('ALTER TABLE accessory_inventory_items ADD CONSTRAINT FK_C13635A527E8CC78 FOREIGN KEY (accessory_id) REFERENCES accessory (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE income_customers');
        $this->addSql('DROP TABLE income_lists');
        $this->addSql('DROP TABLE order_items');
        $this->addSql('ALTER TABLE fos_user_customer ADD owner_id INT DEFAULT NULL, ADD driver_license VARCHAR(255) DEFAULT NULL, ADD address_line_1 VARCHAR(255) DEFAULT NULL, ADD address_line_2 VARCHAR(255) DEFAULT NULL, ADD city VARCHAR(255) DEFAULT NULL, ADD state VARCHAR(255) DEFAULT NULL, ADD zip VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE fos_user_customer ADD CONSTRAINT FK_A2EB18337E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_A2EB18337E3C61F9 ON fos_user_customer (owner_id)');
        $this->addSql('ALTER TABLE orders ADD signature_image_name VARCHAR(255) DEFAULT NULL, ADD asked_quantity LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE inventory_items DROP INDEX IDX_3D82424DD614C7E7, ADD UNIQUE INDEX UNIQ_3D82424DD614C7E7 (price_id)');
        $this->addSql('DROP INDEX UNIQ_3D82424DE415FB15 ON inventory_items');
        $this->addSql('DROP INDEX IDX_3D82424D85EC60F2 ON inventory_items');
        $this->addSql('ALTER TABLE inventory_items ADD description LONGTEXT DEFAULT NULL, DROP order_item_id, DROP income_list_id, CHANGE type type SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE inventory_item_barcodes ADD sale_order_id INT DEFAULT NULL, ADD purchase_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE inventory_item_barcodes ADD CONSTRAINT FK_B048AE5B93EB8192 FOREIGN KEY (sale_order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE inventory_item_barcodes ADD CONSTRAINT FK_B048AE5BA45D7E6A FOREIGN KEY (purchase_order_id) REFERENCES orders (id)');
        $this->addSql('CREATE INDEX IDX_B048AE5B93EB8192 ON inventory_item_barcodes (sale_order_id)');
        $this->addSql('CREATE INDEX IDX_B048AE5BA45D7E6A ON inventory_item_barcodes (purchase_order_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE income_customers (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, firstName VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, lastName VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, phone VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, driver_license VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, email VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, address_line_1 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, address_line_2 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, city VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, state VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, zip VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_25C8E0D57E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE income_lists (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, signature_image_name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, INDEX IDX_D43AD87C9395C3F3 (customer_id), INDEX IDX_D43AD87C7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_items (id INT AUTO_INCREMENT NOT NULL, price_id INT DEFAULT NULL, order_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_62809DB0D614C7E7 (price_id), INDEX IDX_62809DB08D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE income_customers ADD CONSTRAINT FK_25C8E0D57E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE income_lists ADD CONSTRAINT FK_D43AD87C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('ALTER TABLE income_lists ADD CONSTRAINT FK_D43AD87C9395C3F3 FOREIGN KEY (customer_id) REFERENCES income_customers (id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB08D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_62809DB0D614C7E7 FOREIGN KEY (price_id) REFERENCES prices (id)');
        $this->addSql('DROP TABLE orders_inventory_items');
        $this->addSql('DROP TABLE filter_inventory_items');
        $this->addSql('DROP TABLE accessory_inventory_items');
        $this->addSql('ALTER TABLE fos_user_customer DROP FOREIGN KEY FK_A2EB18337E3C61F9');
        $this->addSql('DROP INDEX IDX_A2EB18337E3C61F9 ON fos_user_customer');
        $this->addSql('ALTER TABLE fos_user_customer DROP owner_id, DROP driver_license, DROP address_line_1, DROP address_line_2, DROP city, DROP state, DROP zip');
        $this->addSql('ALTER TABLE inventory_item_barcodes DROP FOREIGN KEY FK_B048AE5B93EB8192');
        $this->addSql('ALTER TABLE inventory_item_barcodes DROP FOREIGN KEY FK_B048AE5BA45D7E6A');
        $this->addSql('DROP INDEX IDX_B048AE5B93EB8192 ON inventory_item_barcodes');
        $this->addSql('DROP INDEX IDX_B048AE5BA45D7E6A ON inventory_item_barcodes');
        $this->addSql('ALTER TABLE inventory_item_barcodes DROP sale_order_id, DROP purchase_order_id');
        $this->addSql('ALTER TABLE inventory_items DROP INDEX UNIQ_3D82424DD614C7E7, ADD INDEX IDX_3D82424DD614C7E7 (price_id)');
        $this->addSql('ALTER TABLE inventory_items ADD order_item_id INT DEFAULT NULL, ADD income_list_id INT DEFAULT NULL, DROP description, CHANGE type type SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE inventory_items ADD CONSTRAINT FK_3D82424D85EC60F2 FOREIGN KEY (income_list_id) REFERENCES income_lists (id)');
        $this->addSql('ALTER TABLE inventory_items ADD CONSTRAINT FK_3D82424DE415FB15 FOREIGN KEY (order_item_id) REFERENCES order_items (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3D82424DE415FB15 ON inventory_items (order_item_id)');
        $this->addSql('CREATE INDEX IDX_3D82424D85EC60F2 ON inventory_items (income_list_id)');
        $this->addSql('ALTER TABLE orders DROP signature_image_name, DROP asked_quantity');
    }
}
