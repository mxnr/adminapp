<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170913205450 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inventories DROP FOREIGN KEY FK_936C863DA76ED395');
        $this->addSql('DROP INDEX IDX_936C863DA76ED395 ON inventories');
        $this->addSql('ALTER TABLE inventories CHANGE user_id owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE inventories ADD CONSTRAINT FK_936C863D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_936C863D7E3C61F9 ON inventories (owner_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE inventories DROP FOREIGN KEY FK_936C863D7E3C61F9');
        $this->addSql('DROP INDEX IDX_936C863D7E3C61F9 ON inventories');
        $this->addSql('ALTER TABLE inventories CHANGE owner_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE inventories ADD CONSTRAINT FK_936C863DA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user_user (id)');
        $this->addSql('CREATE INDEX IDX_936C863DA76ED395 ON inventories (user_id)');
    }
}
