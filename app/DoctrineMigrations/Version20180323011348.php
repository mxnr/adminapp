<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180323011348 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE order_items (id INT AUTO_INCREMENT NOT NULL, inventory_item_id INT DEFAULT NULL, barcode_id INT DEFAULT NULL, description LONGTEXT DEFAULT NULL, admin_note LONGTEXT DEFAULT NULL, INDEX IDX_F9B6A511536BF4A2 (inventory_item_id), INDEX IDX_F9B6A51129439E58 (barcode_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_F9B6A511536BF4A2 FOREIGN KEY (inventory_item_id) REFERENCES inventory_items (id)');
        $this->addSql('ALTER TABLE order_items ADD CONSTRAINT FK_F9B6A51129439E58 FOREIGN KEY (barcode_id) REFERENCES inventory_item_barcodes (id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE order_items');
    }
}
