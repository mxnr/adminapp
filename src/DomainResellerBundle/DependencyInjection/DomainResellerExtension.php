<?php

namespace DomainResellerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class DomainResellerExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $configuration = new Configuration();

        //reading config
        $config = $processor->processConfiguration($configuration, $configs);

        //preparing bundle service params
        $container->setParameter('api_host_port', $config['api_host_port']);
        $container->setParameter('api_key', $config['api_key']);
        $container->setParameter('reseller_username', $config['reseller_username']);
    }
}
