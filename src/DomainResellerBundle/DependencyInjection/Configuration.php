<?php

namespace DomainResellerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('domain_reseller');
        $rootNode
            ->children()
            ->scalarNode('api_host_port')->defaultValue('')->end()
            ->scalarNode('api_key')->defaultValue('')->end()
            ->scalarNode('reseller_username')->defaultValue('')->end();

        return $treeBuilder;
    }
}
