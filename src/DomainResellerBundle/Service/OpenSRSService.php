<?php

namespace DomainResellerBundle\Service;

use DomainResellerBundle\OpenSRSApi\DNSZoneCommands\GetDNSZoneCommand;
use DomainResellerBundle\OpenSRSApi\DNSZoneCommands\SetDNSZoneCommand;
use DomainResellerBundle\OpenSRSApi\DomainForwardingCommands\CreateDomainForwardingCommand;
use DomainResellerBundle\OpenSRSApi\DomainForwardingCommands\SetDomainForwardingCommand;
use DomainResellerBundle\OpenSRSApi\LookupCommands\GetDomainExpireCommand;
use DomainResellerBundle\OpenSRSApi\LookupCommands\GetPriceCommand;
use DomainResellerBundle\OpenSRSApi\LookupCommands\LookupCommand;
use DomainResellerBundle\OpenSRSApi\LookupCommands\NameSuggestCommand;
use DomainResellerBundle\OpenSRSApi\ProvisioningCommands\RegisterCommand;

class OpenSRSService
{
    /**
     * Open SRS statuses
     */
    const SUCCESS_REQUEST = 200;
    const DOMAIN_AVAILABLE_STATUS = 210;
    const DOMAIN_TAKEN_STATUS = 211;
    const OPEN_SRS_FORWARDING_IP = '64.99.64.37';

    /**
     * @var CommandService
     */
    private $commandService;

    /**
     * @var string
     */
    private $loadBalancerDomainName;

    /**
     * OpenSRSService constructor.
     * @param CommandService $commandService
     */
    public function __construct(CommandService $commandService, string $loadBalancerDomainName)
    {
        $this->commandService = $commandService;
        $this->loadBalancerDomainName = $loadBalancerDomainName;
    }

    /**
     * Is Domain Available
     *
     * @param String $domainName
     * @return bool
     */
    public function isDomainAvailable(String $domainName): bool
    {
        $response = $this->commandService
            ->execute(
                LookupCommand::class,
                [
                    'domain_name' => $domainName
                ]
            );

        return $response['responseCode'] == self::DOMAIN_AVAILABLE_STATUS;
    }

    /**
     * Get Suggest Domain Names
     *
     * @param String $searchedDomainName
     * @param array $tlds
     * @return array
     */
    public function getSuggestDomainNames(String $searchedDomainName, array $tlds = []): array
    {
        $options = [
            'searchstring' => $searchedDomainName,
            'services' => ['lookup', 'suggestion', 'premium'],
            'languages' => ['en', 'de', 'it', 'es'],
            'tlds' => $tlds
        ];

        $response = $this->commandService
            ->execute(
                NameSuggestCommand::class,
                $options
            );

        return $response;
    }

    /**
     * Register New Domain
     *
     * @param string $domain
     * @param string $regUserName
     * @param string $regUserPassword
     * @param array $contacts
     * @param string $comments
     * @return array
     */
    public function registerNewDomain(
        string $domain,
        string $regUserName,
        string $regUserPassword,
        array $contacts,
        string $comments = ''
    ): array {
        $response = $this->commandService
            ->execute(RegisterCommand::class,
                [
                    'f_parkp' => 'N',
                    'affiliate_id' => '',
                    'auto_renew' => '0',
                    'comments' => $comments,
                    'domain' => $domain,
                    'reg_type' => 'new',
                    'reg_username' => $regUserName,
                    'reg_password' => $regUserPassword,
                    'f_whois_privacy' => '1',
                    'period' => '1',
                    'link_domains' => '0',
                    'custom_nameservers' => '1',
                    'f_lock_domain' => '1',
                    'reg_domain' => '',
                    'contact_set' => $contacts,
                    'dns_template' => 'new_registered_domain'
                ]
            );

        return $response;
    }

    /**
     * Get Price
     *
     * @param String $domainName
     * @return array
     */
    public function getPrice(String $domainName): array
    {
        $response = $this->commandService
            ->execute(
                GetPriceCommand::class,
                [
                    'domain_name' => $domainName,
                    'period' => '1',
                    'reg_type' => 'new'
                ]
            );

        return $response;
    }

    /**
     * Get Domain Info
     *
     * @param String $domainName
     * @return array
     */
    public function getDomainExpireDate(String $domainName): array
    {
        $response = $this->commandService
            ->execute(
                GetDomainExpireCommand::class,
                [
                    'domain_name' => $domainName,
                ]
            );

        return $response;
    }

    /**
     * Create Domain Forwarding
     *
     * @param string $domainName
     * @return array
     */
    public function createDomainForwarding(string $domainName): array
    {
        $response = $this->commandService
            ->execute(
                CreateDomainForwardingCommand::class,
                [
                    'domain_name' => $domainName,
                ]
            );

        return $response;
    }

    /**
     * Set Domain Forwarding
     *
     * @param string $domainName
     * @return array
     */
    public function setDomainForwarding(string $domainName): array
    {
        $response = $this->commandService
            ->execute(
                SetDomainForwardingCommand::class,
                [
                    'domain_name' => $domainName,
                    'records' => [
                        [
                            "destination_url" => 'http://www.' . $domainName,
                            "enabled" => 1,
                        ]
                    ]
                ]
            );

        return $response;
    }

    /**
     * Set DNS Zones
     *
     * @param $domainName
     * @param $records = [
     *     'A' => [
     *         [
     *             'subdomain' => '',
     *             'ip_address' => '123.123.123.2'
     *         ],
     *         [
     *             'subdomain' => '*',
     *             'ip_address' => '123.123.123.3'
     *         ],
     *         [
     *             'subdomain' => 'www',
     *             'ip_address' => '123.123.123.4'
     *         ]
     *     ],
     *     'CNAME' => [
     *         [
     *             'subdomain' => 'portal1',
     *             'hostname' => 'www.example.com'
     *         ]
     *     ],
     *     'MX' => [
     *         [
     *             'subdomain' => 'portal',
     *             'hostname' => 'mx.example.com.cust.te',
     *             'priority' => 10
     *         ]
     *     ],
     *     'TXT' => [
     *         [
     *             'subdomain' => 'mail',
     *             'text' => 'v=spf1exists:example.com -all',
     *         ]
     *     ],
     *     'AAAA' => [
     *         [
     *             'subdomain' => 'itportal',
     *             'ipv6_address' => '2001:00ab:0000:00a',
     *         ]
     *     ],
     *     'SRV' => [
     *         [
     *             'subdomain' => 'itcontrol1',
     *             'hostname' => 'control.example.com',
     *             'port' => 443,
     *             'priority' => 10,
     *             'weight' => 1,
     *         ]
     *     ]
     * ];
     * @return array
     */
    public function setDNSZones($domainName, $records)
    {
        $response = $this->commandService
            ->execute(
                SetDNSZoneCommand::class,
                [
                    'domain_name' => $domainName,
                    'records' => $records
                ]
            );

        return $response;
    }

    /**
     * Set SSL Domain Ownership
     *
     * @param $resourceRecord = [
     *     [
     *         'DomainName' => '3web1-i-shop.com',
     *         'ValidationStatus' => 'SUCCESS',
     *         'ResourceRecord' => [
     *             'Name' => '_038085e7327ceb4f4a40ebc9fd33f946.3web1-i-shop.com.',
     *             'Type' => 'CNAME',
     *             'Value' => '_d5f53f0eadc14ded3c328795d7cb329e.tljzshvwok.acm-validations.aws.',
     *         ]
     *     ], [
     *         'DomainName' => 'www.3web1-i-shop.com',
     *         'ValidationStatus' => 'SUCCESS',
     *         'ResourceRecord' => [
     *             'Name' => '_www038085e7327ceb4f4a40ebc9fd33f946.www.3web1-i-shop.com.',
     *             'Type' => 'CNAME',
     *             'Value' => '_wwwd5f53f0eadc14ded3c328795d7cb329e.tljzshvwok.acm-validations.aws.',
     *         ]
     *     ],
     * ];
     * @return array
     * @throws \Exception
     */
    public function setSSLDomainOwnership($resourceRecord): array
    {
        $resourceRecordData = $this->prepareResourceRecordData($resourceRecord);

        $records = $resourceRecordData['records'];

        $records['CNAME'][] = [
            'subdomain' => 'www',
            'hostname' => $this->loadBalancerDomainName,
        ];

        $records['A'][] = [
            'subdomain' => '',
            'ip_address' => self::OPEN_SRS_FORWARDING_IP,
        ];

        $response = $this->commandService
            ->execute(
                SetDNSZoneCommand::class,
                [
                    'domain_name' => $resourceRecordData['domain_name'],
                    'records' => $records
                ]
            );

        return $response;
    }

    /**
     * @param array $resourceRecord
     * @return array
     * @throws \Exception
     */
    private function prepareResourceRecordData(array $resourceRecord)
    {
        $domainName = null;
        $records = [];
        foreach ($resourceRecord as $key => $value) {
            $domainName = strstr($value['DomainName'], 'www') ? substr($value['DomainName'], 4) : $value['DomainName'];
            $subDomain = trim($value['ResourceRecord']['Name'], "\-\.");
            $records[$value['ResourceRecord']['Type']][] = [
                'subdomain' => trim(str_replace($domainName, "", $subDomain), "\-\."),
                'hostname' => trim($value['ResourceRecord']['Value'], "\-\_\."),
            ];
        }

        if (is_null($domainName) || $domainName === '') {
            throw new \Exception('Wrong params');
        }

        return [
            'domain_name' => $domainName,
            'records' => $records,
        ];
    }

    /**
     * Check SSL Domain Ownership
     *
     * @param $resourceRecord = [
     *     [
     *         'DomainName' => '3web1-i-shop.com',
     *         'ValidationStatus' => 'SUCCESS',
     *         'ResourceRecord' => [
     *             'Name' => '_038085e7327ceb4f4a40ebc9fd33f946.3web1-i-shop.com.',
     *             'Type' => 'CNAME',
     *             'Value' => '_d5f53f0eadc14ded3c328795d7cb329e.tljzshvwok.acm-validations.aws.',
     *         ]
     *     ], [
     *         'DomainName' => 'www.3web1-i-shop.com',
     *         'ValidationStatus' => 'SUCCESS',
     *         'ResourceRecord' => [
     *             'Name' => '_www038085e7327ceb4f4a40ebc9fd33f946.www.3web1-i-shop.com.',
     *             'Type' => 'CNAME',
     *             'Value' => '_wwwd5f53f0eadc14ded3c328795d7cb329e.tljzshvwok.acm-validations.aws.',
     *         ]
     *     ],
     * ];
     * @return bool
     * @throws \Exception
     */
    public function checkSSLDomainOwnership($resourceRecord): bool
    {
        $resourceRecordData = $this->prepareResourceRecordData($resourceRecord);

        $response = $this->commandService
            ->execute(
                GetDNSZoneCommand::class,
                [
                    'domain_name' => $resourceRecordData['domain_name']
                ]
            );

        return $response['content']['CNAME'] === $resourceRecordData['records']['CNAME'];
    }
}
