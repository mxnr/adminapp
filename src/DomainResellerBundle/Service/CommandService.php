<?php

namespace DomainResellerBundle\Service;

use GuzzleHttp\Psr7\Response;
use DomainResellerBundle\OpenSRSApi\BaseCommand;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CommandService
{
    /**
     * @var array
     */
    private $connectionOptions;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * CommandService constructor.
     *
     * @param String $host
     * @param String $key
     * @param String $reseller
     */
    public function __construct(String $host, String $key, String $reseller)
    {
        $this->connectionOptions = [
            "api_host_port" => $host,
            "api_key" => $key,
            "reseller_username" => $reseller,
        ];

        $this->serializer = $this->getSerializer('OPS_envelope');
    }

    /**
     * Execute
     *
     * @param String $commandName
     * @param array $options
     * @return array
     */
    public function execute(String $commandName, array $options = []): array
    {
        $command = $this->getCommand($commandName);

        $response = $this->apiRequest($command, $options);

        return $command->getResponse($response);
    }

    /**
     * Get Command
     *
     * @param String $commandName
     * @return BaseCommand
     */
    private function getCommand(String $commandName): BaseCommand
    {
        $className = '\\' . $commandName;

        return new $className();
    }

    /**
     * Get Serializer
     *
     * @param $rootNode
     * @return Serializer
     */
    private function getSerializer($rootNode): Serializer
    {
        $encoders = array(new XmlEncoder($rootNode), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        return new Serializer($normalizers, $encoders);
    }

    /**
     * Api Request
     *
     * @param BaseCommand $command
     * @param array $options
     * @return Response
     */
    private function apiRequest(BaseCommand $command, array $options): Response
    {
        $client = new \GuzzleHttp\Client();
        $xmlBody = $command->getXMLBody($options);
        return $client->request('POST', $this->connectionOptions['api_host_port'], [
            'body' => $xmlBody,
            'headers' => [
                'Content-Type' => 'text/xml',
                'X-Username' => $this->connectionOptions['reseller_username'],
                'X-Signature' => $this->getSignature($xmlBody),
            ]
        ]);
    }

    /**
     * Get Signature
     *
     * @param String $xmlBody
     * @return string
     */
    private function getSignature(String $xmlBody)
    {
        return md5(
            md5($xmlBody . $this->connectionOptions['api_key']) .
            $this->connectionOptions['api_key']
        );
    }
}
