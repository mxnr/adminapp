<?php

namespace DomainResellerBundle\OpenSRSApi\LookupCommands;

use GuzzleHttp\Psr7\Response;
use DomainResellerBundle\OpenSRSApi\BaseCommand;
use DomainResellerBundle\Service\OpenSRSService;

/**
 * Class GetDomainExpireCommand
 *
 * Determines the availability of a specified domain name.
 *
 * @link http://domains.opensrs.guide/docs/get-domain-1
 * @package DomainResellerBundle\Api\DomainLookup
 */
class GetDomainExpireCommand extends BaseCommand
{
    /**
     * Get XML Body
     *
     * @param array $options
     * $options['domain_name'] checked domain name
     * $options['period']
     * $options['reg_type']
     * @return String
     */
    public function getXMLBody(array $options): String
    {
        $this->xmlNode->setAPIAction('GET');
        $this->xmlNode->addItemNode($this->xmlNode->dt_assoc, $options['domain_name'], 'domain');
        $attributeNode = $this->xmlNode->addAssocNode($this->xmlNode->dt_assoc, 'attributes');
        $this->xmlNode->addItemNode($attributeNode, 'expire_action', 'type');

        return $this->xmlNode->getXML();
    }

    /**
     * @inheritdoc
     */
    public function getResponse(Response $response): array
    {
        $response = $this->xmlNode->parseResponse($response);

        if ($response['responseCode'] != OpenSRSService::SUCCESS_REQUEST) {
            return $response;
        }

        return $this->prepareResponse($response, (string)$response['item'][1]);
    }
}
