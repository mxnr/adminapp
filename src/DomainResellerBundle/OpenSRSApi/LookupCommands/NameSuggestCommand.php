<?php

namespace DomainResellerBundle\OpenSRSApi\LookupCommands;

use GuzzleHttp\Psr7\Response;
use DomainResellerBundle\OpenSRSApi\BaseCommand;
use DomainResellerBundle\Service\OpenSRSService;
use SimpleXMLElement;

/**
 * Class NameSuggestCommand
 *
 * Checks whether a specified name, word, or phrase is available for registration
 * in gTLDs and ccTLDs, suggests other similar domain names for .COM,
 * .NET, .ORG, .INFO, .BIZ, .US, and .MOBI domains, and checks whether they
 * are available. Reseller must be enabled for the specified TLDs. Can also be
 * used to search for domains owned by external domain suppliers.
 *
 * @link http://domains.opensrs.guide/docs/name_suggest-domain-1
 * @package DomainResellerBundle\Api\DomainLookup
 */
class NameSuggestCommand extends BaseCommand
{
    const ITEMS_OPTIONS = ['max_wait_time', 'search_key', 'searchstring', 'skip_registry_lookup'];
    const ARRAY_OPTIONS = ['languages', 'tlds', 'services', 'suggestion'];

    /**
     * Get XML Body
     *
     * @param array $options
     * $options['languages'] => ['en', 'de']
     * $options['max_wait_time']
     * $options['search_key']
     * $options['searchstring']
     * $options['service_override'] => [
     *      'suggestion' => [
     *          'tlds' => ['.com', '.info'],
     *          'maximum' => 25
     *      ]
     * ]
     * $options['services'] => [
     *      'lookup',
     *      'suggestion',
     *      'personal_names',
     *      'premium',
     *      'premium_brokered_transfer',
     *      'premium_make_offer',
     * ]
     * $options['skip_registry_lookup']
     * $options['suggestion'] => [
     *          'maximum' => '',
     *          'price_max' => '',
     *          'price_min' => '',
     *          'tlds' => ['.com', '.info']
     * ]
     * $options['tlds']
     *
     * @return String
     */
    public function getXMLBody(array $options): String
    {
        $this->xmlNode->setAPIAction('name_suggest');
        $attributesNode = $this->xmlNode->addAssocNode($this->xmlNode->dt_assoc, 'attributes');

        // Add all items to attributes node
        foreach (self::ITEMS_OPTIONS as $value) {
            if (array_key_exists($value, $options)) {
                $this->xmlNode->addItemNode($attributesNode, $options[$value], $value);
            }
        }

        // Add all array items to attributes node
        foreach (self::ARRAY_OPTIONS as $value) {
            if (array_key_exists($value, $options)) {
                $this->xmlNode->addArrayNode($attributesNode, $options[$value], $value);
            }
        }

        // Add all nodes to service override node
        if (array_key_exists('service_override', $options)) {
            $serviceOverrideNode = $this->xmlNode->addAssocNode($attributesNode, 'service_override');

            foreach ($options['service_override'] as $serviceName => $service) {
                $serviceNode = $this->xmlNode->addAssocNode($serviceOverrideNode, $serviceName);

                foreach ($options['service_override'][$serviceName] as $key => $value) {
                    if (is_array($value)) {
                        $this->xmlNode->addArrayNode($serviceNode, $value, $key);
                    } else {
                        $this->xmlNode->addItemNode($serviceNode, $value, $key);
                    }
                }
            }
        }

        return $this->xmlNode->getXML();
    }

    /**
     * @inheritdoc
     */
    public function getResponse(Response $response): array
    {
        $response = $this->xmlNode->parseResponse($response);

        if ($response['responseCode'] != OpenSRSService::SUCCESS_REQUEST) {
            return $response;
        }

        $result = [];
        foreach ($response['item'] as $item) {
            if ($this->xmlNode->isKey($item, 'lookup')) {
                $result['lookup'] = $this->getLookupDomains($item);
            }

            if ($this->xmlNode->isKey($item, 'premium')) {
                $result['premium'] = $this->getPremiumDomains($item);
            }
        }

        return $this->prepareResponse($response, $result);
    }

    /**
     * Get Items Array
     *
     * @param SimpleXMLElement $node
     * @return SimpleXMLElement
     */
    private function getItemsArray(SimpleXMLElement $node): ?SimpleXMLElement
    {
        foreach ($this->xmlNode->getNodeItem($node) as $item) {
            if ($this->xmlNode->isKey($item, 'items')) {
                return $this->xmlNode->getNodeArrayItems($item);
            }
        }

        return null;
    }

    /**
     * Get Lookup Domains
     *
     * @param SimpleXMLElement $node
     * @return array
     */
    private function getLookupDomains(SimpleXMLElement $node): array
    {
        $result = [];

        $items = $this->getItemsArray($node);
        foreach ($items as $arrItem) {
            list($domain, $status) = $this->xmlNode->getNodeItem($arrItem);
            $result[] = [
                'domain' => (string)$domain,
                'status' => (string)$status,
            ];
        }

        return $result;
    }

    /**
     * Get Premium Domains
     *
     * @param SimpleXMLElement $node
     * @return array
     */
    private function getPremiumDomains(SimpleXMLElement $node): array
    {
        $result = [];

        $items = $this->getItemsArray($node);
        foreach ($items as $arrItem) {
            list($domain, $status, $price) = $this->xmlNode->getNodeItem($arrItem);
            $result[] = [
                'domain' => (string)$domain,
                'status' => (string)$status,
                'price' => (string)$price,
            ];
        }

        return $result;
    }
}
