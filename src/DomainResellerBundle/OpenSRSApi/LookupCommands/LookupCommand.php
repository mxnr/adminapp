<?php

namespace DomainResellerBundle\OpenSRSApi\LookupCommands;

use DomainResellerBundle\OpenSRSApi\BaseCommand;
use DomainResellerBundle\Service\OpenSRSService;
use GuzzleHttp\Psr7\Response;

/**
 * Class LookupCommand
 *
 * Determines the availability of a specified domain name.
 *
 * @link http://domains.opensrs.guide/docs/lookup-domain-2
 * @package DomainResellerBundle\Api\DomainLookup
 */
class LookupCommand extends BaseCommand
{
    /**
     * getXMLBody
     *
     * @param array $options
     * $options['domain_name'] checked domain name
     * @return String
     */
    public function getXMLBody(array $options): String
    {
        $this->xmlNode->setAPIAction('LOOKUP');
        $attributeNode = $this->xmlNode->addAssocNode($this->xmlNode->dt_assoc, 'attributes');
        $this->xmlNode->addItemNode($attributeNode, $options['domain_name'], 'domain');

        return $this->xmlNode->getXML();
    }

    /**
     * @inheritdoc
     */
    public function getResponse(Response $response): array
    {
        $response = $this->xmlNode->parseResponse($response);

        return $this->prepareResponse($response, (int)$response['responseCode'] === OpenSRSService::DOMAIN_AVAILABLE_STATUS);
    }
}
