<?php

namespace DomainResellerBundle\OpenSRSApi\LookupCommands;

use GuzzleHttp\Psr7\Response;
use DomainResellerBundle\OpenSRSApi\BaseCommand;
use DomainResellerBundle\Service\OpenSRSService;

/**
 * Class LookupCommand
 *
 * Determines the availability of a specified domain name.
 *
 * @link http://domains.opensrs.guide/docs/lookup-domain-2
 * @package DomainResellerBundle\Api\DomainLookup
 */
class GetPriceCommand extends BaseCommand
{
    /**
     * Get XML Body
     *
     * @param array $options
     * $options['domain_name'] checked domain name
     * $options['period']
     * $options['reg_type']
     * @return String
     */
    public function getXMLBody(array $options): String
    {
        $this->xmlNode->setAPIAction('GET_PRICE');
        $attributeNode = $this->xmlNode->addAssocNode( $this->xmlNode->dt_assoc, 'attributes');
        $this->xmlNode->addItemNode($attributeNode, $options['domain_name'], 'domain');
        $this->xmlNode->addItemNode($attributeNode, $options['period'], 'period');
        $this->xmlNode->addItemNode($attributeNode, $options['reg_type'], 'reg_type');

        return $this->xmlNode->getXML();
    }

    /**
     * @inheritdoc
     */
    public function getResponse(Response $response): array
    {
        $response = $this->xmlNode->parseResponse($response);

        if ($response['responseCode'] != OpenSRSService::SUCCESS_REQUEST) {
            return $response;
        }

        return $this->prepareResponse($response, (string)$response['item']);
    }
}
