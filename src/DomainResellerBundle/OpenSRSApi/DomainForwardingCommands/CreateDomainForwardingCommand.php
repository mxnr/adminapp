<?php

namespace DomainResellerBundle\OpenSRSApi\DomainForwardingCommands;

use GuzzleHttp\Psr7\Response;
use DomainResellerBundle\OpenSRSApi\BaseCommand;
use DomainResellerBundle\Service\OpenSRSService;

/**
 * Class CreateDomainForwarding
 *
 * Create domain forwarding service that is used to redirect visitors from one web address to another.
 *
 * @link http://domains.opensrs.guide/v1.0/docs/create_domain_forwarding
 * @package DomainResellerBundle\Api\DomainForwardingCommands
 */
class CreateDomainForwardingCommand extends BaseCommand
{
    /**
     * Get XML Body
     *
     * @param array $options
     * $options['domain_name']
     * @return String
     */
    public function getXMLBody(array $options): String
    {
        $this->xmlNode->setAPIAction('create_domain_forwarding');
        $attributeNode = $this->xmlNode->addAssocNode($this->xmlNode->dt_assoc, 'attributes');
        $this->xmlNode->addItemNode($attributeNode, $options['domain_name'], 'domain');

        return $this->xmlNode->getXML();
    }

    /**
     * @inheritdoc
     */
    public function getResponse(Response $response): array
    {
        $response = $this->xmlNode->parseResponse($response);

        if ($response['responseCode'] != OpenSRSService::SUCCESS_REQUEST) {
            return $response;
        }

        return $this->prepareResponse($response, '');
    }
}
