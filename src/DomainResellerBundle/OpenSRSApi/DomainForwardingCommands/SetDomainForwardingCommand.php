<?php

namespace DomainResellerBundle\OpenSRSApi\DomainForwardingCommands;

use GuzzleHttp\Psr7\Response;
use DomainResellerBundle\OpenSRSApi\BaseCommand;
use DomainResellerBundle\Service\OpenSRSService;
use SimpleXMLElement;

/**
 * Class SetDomainForwarding
 *
 * Configures the domain forwarding settings for a domain.
 * Domain forwarding must already have been created for the domain.
 *
 * @link http://domains.opensrs.guide/v1.0/docs/set_domain_forwarding
 * @package DomainResellerBundle\Api\DomainForwardingCommands
 */
class SetDomainForwardingCommand extends BaseCommand
{
    /**
     * Get XML Body
     *
     * @param array $options
     * $options['domain_name']
     * @return String
     */
    public function getXMLBody(array $options): String
    {
        $this->xmlNode->setAPIAction('set_domain_forwarding');
        $attributeNode = $this->xmlNode->addAssocNode($this->xmlNode->dt_assoc, 'attributes');
        $this->xmlNode->addItemNode($attributeNode, $options['domain_name'], 'domain');

        $this->xmlNode->addAssocArrayNode($attributeNode, $options['records'], 'forwarding');

        return $this->xmlNode->getXML();
    }

    /**
     * @inheritdoc
     */
    public function getResponse(Response $response): array
    {
        $response = $this->xmlNode->parseResponse($response);

        if ($response['responseCode'] != OpenSRSService::SUCCESS_REQUEST) {
            return $response;
        }

        return $this->prepareResponse($response, '');
    }
}
