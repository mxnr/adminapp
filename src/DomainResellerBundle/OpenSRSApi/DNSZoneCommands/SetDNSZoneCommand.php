<?php

namespace DomainResellerBundle\OpenSRSApi\DNSZoneCommands;

use DomainResellerBundle\OpenSRSApi\BaseCommand;
use DomainResellerBundle\Service\OpenSRSService;
use GuzzleHttp\Psr7\Response;


/**
 * Class SetDNSZoneCommand
 *
 * Set the records for a domain's DNS zone.
 *
 * @link http://domains.opensrs.guide/v1.0/docs/set_dns_zone-
 * @package DomainResellerBundle\OpenSRSApi\DNSZoneCommands
 */
class SetDNSZoneCommand extends BaseCommand
{
    /**
     * Get XML Body
     *
     * @param array $options
     * $options['domain_name']
     * $options['records']
     * @return String
     */
    public function getXMLBody(array $options): String
    {
        $this->xmlNode->setAPIAction('SET_DNS_ZONE');
        $attributeNode = $this->xmlNode->addAssocNode($this->xmlNode->dt_assoc, 'attributes');
        $this->xmlNode->addItemNode($attributeNode, $options['domain_name'], 'domain');

        $records = $this->xmlNode->addAssocNode($attributeNode, 'records');

        foreach ($options['records'] as $key => $record) {
            $this->xmlNode->addAssocArrayNode($records, $record, $key);
        }

        return $this->xmlNode->getXML();
    }

    /**
     * @inheritdoc
     */
    public function getResponse(Response $response): array
    {
        $response = $this->xmlNode->parseResponse($response);

        if ($response['responseCode'] != OpenSRSService::SUCCESS_REQUEST) {
            return $response;
        }

        return $this->prepareResponse($response, '');
    }
}
