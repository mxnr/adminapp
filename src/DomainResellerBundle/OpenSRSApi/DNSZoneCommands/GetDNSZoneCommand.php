<?php

namespace DomainResellerBundle\OpenSRSApi\DNSZoneCommands;

use DomainResellerBundle\OpenSRSApi\BaseCommand;
use DomainResellerBundle\Service\OpenSRSService;
use GuzzleHttp\Psr7\Response;


/**
 * Class GetDNSZoneCommand
 *
 * View the DNS records for a specified domain.
 *
 * @link http://domains.opensrs.guide/v1.0/docs/get_dns_zone
 * @package DomainResellerBundle\OpenSRSApi\DNSZoneCommands
 */
class GetDNSZoneCommand extends BaseCommand
{
    /**
     * Get XML Body
     *
     * @param array $options
     * $options['domain_name']
     * @return String
     */
    public function getXMLBody(array $options): String
    {
        $this->xmlNode->setAPIAction('get_dns_zone');
        $attributeNode = $this->xmlNode->addAssocNode($this->xmlNode->dt_assoc, 'attributes');
        $this->xmlNode->addItemNode($attributeNode, $options['domain_name'], 'domain');

        return $this->xmlNode->getXML();
    }

    /**
     * @inheritdoc
     */
    public function getResponse(Response $response): array
    {
        $response = $this->xmlNode->parseResponse($response);

        if ($response['responseCode'] != OpenSRSService::SUCCESS_REQUEST) {
            return $response;
        }

        $result = [];
        $itemSet = (array)$response['item']->dt_assoc;

        if (is_array($itemSet['item'])) {
            foreach ($itemSet['item'] as $item) {
                $recordName = $this->getRecordType($item);
                if ($recordName === 'CNAME') {
                    $result = $this->getCNAMERecord($item);
                }
            }
        } else {
            $record = $this->getRecordType($itemSet['item']);
            if ($record === 'CNAME') {
                $result = $this->getCNAMERecord($itemSet['item']);
            }
        }

        return $this->prepareResponse($response, $result);
    }

    /**
     * @param $item
     * @return mixed
     */
    private function getRecordType($item)
    {
        $ar = (array)$item;

        return $ar['@attributes']['key'];
    }

    /**
     * @param $itemSet
     * @return array
     */
    private function getCNAMERecord($itemSet)
    {
        $result = [];
        $recordData = (array)$itemSet->dt_array;
        if (is_array($recordData['item'])) {
            foreach ($recordData['item'] as $item) {
                $recordInfo = (array)$item->dt_assoc->item;
                $result['CNAME'][] = [
                    'subdomain' => $recordInfo[0],
                    'hostname' => $recordInfo[1],
                ];
            }
        } else {
            $recordInfo = (array)$recordData['item']->dt_assoc->item;
            $result['CNAME'][] = [
                'subdomain' => $recordInfo[0],
                'hostname' => $recordInfo[1],
            ];
        }

        return $result;
    }
}
