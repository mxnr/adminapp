<?php

namespace DomainResellerBundle\OpenSRSApi\DNSZoneCommands;

use GuzzleHttp\Psr7\Response;
use DomainResellerBundle\OpenSRSApi\BaseCommand;
use DomainResellerBundle\Service\OpenSRSService;

/**
 * Class CreateDNSZoneCommand
 *
 * Enables the DNS service for a domain.
 *
 * @link http://domains.opensrs.guide/docs/create_dns_zone
 * @package DomainResellerBundle\Api\DNSZoneCommands
 */
class CreateDNSZoneCommand extends BaseCommand
{
    /**
     * Get XML Body
     *
     * @param array $options
     * $options['domain_name']
     * @return String
     */
    public function getXMLBody(array $options): String
    {
        $this->xmlNode->setAPIAction('create_dns_zone');
        $attributeNode = $this->xmlNode->addAssocNode($this->xmlNode->dt_assoc, 'attributes');
        $this->xmlNode->addItemNode($attributeNode, $options['domain_name'], 'domain');
        $this->xmlNode->addItemNode($attributeNode, $options['dns_template'], 'dns_template');

        return $this->xmlNode->getXML();
    }

    /**
     * @inheritdoc
     */
    public function getResponse(Response $response): array
    {
        $response = $this->xmlNode->parseResponse($response);

        if ($response['responseCode'] != OpenSRSService::SUCCESS_REQUEST) {
            return $response;
        }

        return $this->prepareResponse($response, '');
    }
}
