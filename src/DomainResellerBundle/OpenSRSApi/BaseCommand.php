<?php

namespace DomainResellerBundle\OpenSRSApi;

use GuzzleHttp\Psr7\Response;

abstract class BaseCommand
{
    const API_VERSION = '0.9';

    /**
     * @var XMLNode
     */
    protected $xmlNode;

    /**
     * BaseCommand constructor.
     */
    public function __construct()
    {
        $this->xmlNode = new XMLNode();
    }

    /**
     * Get XML Body
     *
     * @param array $options
     * @return String
     */
    abstract public function getXMLBody(array $options): String;

    /**
     * Get Response
     *
     * @param Response $response
     * @return array
     */
    abstract function getResponse(Response $response): array;

    /**
     * @param array $response
     * @param $content
     * @return array
     */
    protected function prepareResponse(array $response, $content): array
    {
        $result = [
            'responseCode' => (integer)$response['responseCode'],
            'responseText' => (string)$response['responseText'],
            'content' => $content
        ];

        return $result;
    }
}
