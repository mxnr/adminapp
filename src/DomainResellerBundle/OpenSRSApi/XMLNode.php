<?php

namespace DomainResellerBundle\OpenSRSApi;

use GuzzleHttp\Psr7\Response;
use SimpleXMLElement;

class XMLNode
{
    const API_VERSION = '0.9';

    /**
     * @var SimpleXMLElement
     */
    protected $rootNode;

    /**
     * @var SimpleXMLElement
     */
    public $dt_assoc;

    public function __construct()
    {
        $this->initRootNode();
    }

    private function initRootNode()
    {
        $rootNode = new SimpleXMLElement(
            "<?xml version='1.0' encoding='UTF-8' standalone='no'?>
                      <!DOCTYPE OPS_envelope SYSTEM 'ops.dtd'>
                      <OPS_envelope></OPS_envelope>"
        );

        $rootNode->addChild('header')->addChild('version', self::API_VERSION);
        $dt_assoc = $rootNode->addChild('body')
            ->addChild('data_block')
            ->addChild('dt_assoc');

        $this->rootNode = $rootNode;
        $this->dt_assoc = $dt_assoc;
    }

    /**
     * Add Attributes Node
     *
     * @param SimpleXMLElement $node
     * @return SimpleXMLElement
     */
    public function addAssocNode(SimpleXMLElement $node, string $attribute): SimpleXMLElement
    {
        $attributeItem = $this->addItemNode($node, '', $attribute);

        return $this->addNode($attributeItem, 'dt_assoc');
    }

    /**
     * Add Item Node
     *
     * @param SimpleXMLElement $node
     * @param string $value
     * @param string|null $attribute
     * @return SimpleXMLElement
     */
    public function addItemNode(SimpleXMLElement $node, string $value = '', string $attribute = null): SimpleXMLElement
    {
        $itemNode = $node->addChild('item', $value);

        if (!is_null($attribute)) {
            $itemNode->addAttribute('key', $attribute);
        }

        return $itemNode;
    }

    /**
     * Add Node
     *
     * @param SimpleXMLElement $node
     * @param String $nodeName
     * @return SimpleXMLElement
     */
    public function addNode(SimpleXMLElement $node, String $nodeName): SimpleXMLElement
    {
        return $node->addChild($nodeName);
    }

    /**
     * Add Array Node
     *
     * @param SimpleXMLElement $node
     * @param array $array
     * @param string|null $attribute
     * @return SimpleXMLElement
     */
    public function addArrayNode(SimpleXMLElement $node, array $array, string $attribute = null): SimpleXMLElement
    {
        if (!count($array)) {
            return $node;
        }

        $itemNode = $this->addItemNode($node, '', $attribute);
        $arrayNode = $this->addNode($itemNode, 'dt_array');

        for ($i = 0; $i < count($array); $i++) {
            $this->addItemNode($arrayNode, $array[$i], $i);
        }

        return $node;
    }

    /**
     * AddA Assoc Array Node
     *
     * @param SimpleXMLElement $node
     * @param array $array
     * @param string|null $attribute
     * @return SimpleXMLElement
     */
    public function addAssocArrayNode(SimpleXMLElement $node, array $array, string $attribute = null): SimpleXMLElement
    {
        if (!count($array)) {
            return $node;
        }

        $itemNode = $this->addItemNode($node, '', $attribute);
        $arrayNode = $this->addNode($itemNode, 'dt_array');

        for ($i = 0; $i < count($array); $i++) {
            if (is_array($array[$i])) {
                $arrayElementNode = $this->addAssocNode($arrayNode, $i);
                foreach ($array[$i] as $key => $value) {
                    $this->addItemNode($arrayElementNode, $value, $key);
                }
            }
        }

        return $node;
    }

    /**
     * Parse Response
     *
     * @param Response $response
     * @return array
     */
    public function parseResponse(Response $response): array
    {
        $data = new SimpleXMLElement($response->getBody()->getContents());

        $response = [];
        foreach ($data->body->data_block->dt_assoc->item as $item) {
            if ($this->isKey($item, 'attributes')) {
                $response['item'] = $item->dt_assoc->item;
            }

            if ($this->isKey($item, 'response_text')) {
                $response['responseText'] = (string)$item;
            }

            if ($this->isKey($item, 'response_code')) {
                $response['responseCode'] = (string)$item;
            }
        }

        return $response;
    }

    /**
     * @param string $action
     */
    public function setAPIAction(string $action)
    {
        $this->addItemNode($this->dt_assoc,'XCP', 'protocol');
        $this->addItemNode($this->dt_assoc,'DOMAIN', 'object');
        $this->addItemNode($this->dt_assoc, $action, 'action');
    }

    /**
     * @return string
     */
    public function getXML(): string
    {
        return $this->rootNode->asXML();
    }

    /**
     * Is Key
     *
     * @param SimpleXMLElement $item
     * @param string $key
     * @return bool
     */
    public function isKey(SimpleXMLElement $item, string $key): bool
    {
        return (string)$item['key'] === $key;
    }

    /**
     * Get Node Array Items
     *
     * @param SimpleXMLElement $item
     * @return SimpleXMLElement
     */
    public function getNodeArrayItems(SimpleXMLElement $item): SimpleXMLElement
    {
        return $item->dt_array->item;
    }

    /**
     * Get Node Item
     * @param SimpleXMLElement $item
     * @return SimpleXMLElement
     */
    public function getNodeItem(SimpleXMLElement $item): SimpleXMLElement
    {
        return $item->dt_assoc->item;
    }
}
