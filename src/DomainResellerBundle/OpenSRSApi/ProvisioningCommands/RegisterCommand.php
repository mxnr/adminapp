<?php

namespace DomainResellerBundle\OpenSRSApi\ProvisioningCommands;

use GuzzleHttp\Psr7\Response;
use DomainResellerBundle\OpenSRSApi\BaseCommand;
use DomainResellerBundle\Service\OpenSRSService;
use SimpleXMLElement;

/**
 * Class RegisterCommand
 *
 * Submits a new registration or transfer order that obeys the reseller's 'process immediately' flag setting.
 *
 * @link http://domains.opensrs.guide/docs/sw_register-domain
 * @package DomainResellerBundle\Api\DomainLookup
 */
class RegisterCommand extends BaseCommand
{
    /**
     * @param array $options
     * $options['comments']
     * $options['domain']
     * $options['reg_username']
     * $options['reg_password']
     * $options['contact_set']
     * $options['dns_template']
     * $options['ns']
     * $options['contact_set'] = [
     *   admin => [
     *     'country' => "US",
     *     'address3' => "Admin",
     *     'org_name' => "Example Inc.",
     *     'phone' => "+1.4165550123x1902",
     *     'state' => "CA",
     *     'address2' => "Suite 500",
     *     'email' => "adams@example.com",
     *     'city' => "SomeCity",
     *     'postal_code' => "90210",
     *     'fax' => "+1.4165550124",
     *     'address1' => "32 Oak Street",
     *     'last_name' => "Ottway",
     *     'first_name' => "Adler",
     *   ]
     * ]
     * @return String
     */
    public function getXMLBody(array $options): String
    {
        $this->xmlNode->setAPIAction('SW_REGISTER');

        $attributesNode = $this->xmlNode->addAssocNode($this->xmlNode->dt_assoc, 'attributes');

        $this->xmlNode->addItemNode($attributesNode, $options['f_parkp'], 'f_parkp');
        $this->xmlNode->addItemNode($attributesNode, $options['affiliate_id'], 'affiliate_id');
        $this->xmlNode->addItemNode($attributesNode, $options['auto_renew'], 'auto_renew');
        $this->xmlNode->addItemNode($attributesNode, $options['comments'], 'comments');
        $this->xmlNode->addItemNode($attributesNode, $options['domain'], 'domain');
        $this->xmlNode->addItemNode($attributesNode, $options['reg_type'], 'reg_type');
        $this->xmlNode->addItemNode($attributesNode, $options['reg_username'], 'reg_username');
        $this->xmlNode->addItemNode($attributesNode, $options['reg_password'], 'reg_password');
        $this->xmlNode->addItemNode($attributesNode, $options['f_whois_privacy'], 'f_whois_privacy');
        $this->xmlNode->addItemNode($attributesNode, $options['period'], 'period');
        $this->xmlNode->addItemNode($attributesNode, $options['link_domains'], 'link_domains');
        $this->xmlNode->addItemNode($attributesNode, $options['custom_nameservers'], 'custom_nameservers');
        $this->xmlNode->addItemNode($attributesNode, $options['f_lock_domain'], 'f_lock_domain');
        $this->xmlNode->addItemNode($attributesNode, $options['reg_domain'], 'reg_domain');
        $this->xmlNode->addItemNode($attributesNode, $options['dns_template'], 'dns_template');

        $contactSetNode = $this->xmlNode->addAssocNode($attributesNode, 'contact_set');

        foreach ($options['contact_set'] as $key => $contact) {
            $this->addContact($contactSetNode, $key, $contact);
        }

        return $this->xmlNode->getXML();
    }

    /**
     * @inheritdoc
     */
    public function getResponse(Response $response): array
    {
        $response = $this->xmlNode->parseResponse($response);

        if ($response['responseCode'] != OpenSRSService::SUCCESS_REQUEST) {
            return $response;
        }

        return $this->prepareResponse(
            $response,
            array_key_exists('item', $response ) ? (string)$response['item'] : []
        );
    }

    /**
     * Add Contact
     *
     * @param SimpleXMLElement $rootNode
     * @param string $contactName
     * @param array $options
     * @return SimpleXMLElement
     */
    private function addContact(SimpleXMLElement $rootNode, string $contactName, array $options): SimpleXMLElement
    {
        $contactNode = $this->xmlNode->addAssocNode($rootNode, $contactName);

        foreach ($options as $key => $option) {
            $this->xmlNode->addItemNode($contactNode, $option, $key);
        }

        return $contactNode;
    }

    /**
     * Add Name Server Array
     *
     * @param SimpleXMLElement $node
     * @param array $ns
     * @return SimpleXMLElement
     */
    private function addNameServerArray(SimpleXMLElement $node, array $ns)
    {
        if (!count($ns)) {
            return $node;
        }

        $nameserverList = $this->xmlNode->addItemNode($node, '', 'nameserver_list');
        $arrayNode = $this->xmlNode->addNode($nameserverList, 'dt_array');

        for ($i = 0; $i < count($ns); $i++) {
            $arrayItemNode = $this->xmlNode->addAssocNode($arrayNode, $i);
            $this->xmlNode->addItemNode($arrayItemNode, $ns[$i], 'name');
            $this->xmlNode->addItemNode($arrayItemNode, $i + 1, 'sortorder');
        }

        $this->xmlNode->addItemNode($node, '', 'encoding_type');
        $this->xmlNode->addItemNode($node, '0', 'custom_tech_contact');

        return $node;
    }
}
