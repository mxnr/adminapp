<?php

namespace ChargeBeeBundle\Contract;

/**
 * Class PacketMethodAttributesInteface.
 */
interface PacketMethodAttributesInteface
{
    /**
     * Returns list of method attributes.
     *
     * @return array
     */
    public function getAttributes(): array;
}
