<?php

namespace ChargeBeeBundle\Contract;

use ChargeBeeBundle\Service\EnvironmentService;

/**
 * Interface PacketMethodInterface.
 */
interface PacketMethodInterface
{
    /**
     * @param EnvironmentService $environmentService
     *
     * @return PacketMethodInterface
     */
    public function setEnvironment(EnvironmentService $environmentService): self;

    /**
     * @return PacketMethodInterface
     */
    public function execute(): self;

    /**
     * @return array
     */
    public function getResult(): array;
}
