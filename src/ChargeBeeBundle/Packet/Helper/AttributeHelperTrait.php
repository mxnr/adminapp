<?php

namespace ChargeBeeBundle\Packet\Helper;

use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use InvalidArgumentException;

/**
 * Trait AttributeHelperTrait.
 */
trait AttributeHelperTrait
{
    /**
     * @param array $map
     * @param array $attributes
     * @param bool  $mergeAttributeParams
     *
     * @return array
     */
    public function getAttributesByMap(array $map, array $attributes = [], bool $mergeAttributeParams = false): array
    {
        foreach ($map as $getterInfo) {
            $field = null;
            $getMethod = null;
            $hasMethod = null;

            if (is_array($getterInfo)) {
                if (isset($getterInfo['get'])) {
                    $getMethod = $getterInfo['get'];
                }

                if (isset($getterInfo['has'])) {
                    $hasMethod = $getterInfo['has'];
                }

                if (null === $getMethod || null === $hasMethod) {
                    if (!isset($getterInfo['field'])) {
                        throw new InvalidArgumentException(
                            'if map item is array, and get or has is not configured - field parameter is required'
                        );
                    }

                    $field = $getterInfo['field'];
                    if (null === $hasMethod) {
                        $hasMethod = 'has' . ucfirst($field);
                    }
                    if (null === $getMethod) {
                        $getMethod = 'get' . ucfirst($field);
                    }
                } else {
                    $field = lcfirst(substr($hasMethod, 3));
                }
            } else {
                $field = lcfirst($getterInfo);
                $hasMethod = 'has' . ucfirst($getterInfo);
                $getMethod = 'get' . ucfirst($getterInfo);
            }

            if (false === method_exists($this, $getMethod)) {
                throw new \RuntimeException('unknown get method: <![CDATA[' . $getMethod . ']]>');
            }

            if (false === method_exists($this, $hasMethod)) {
                throw new \RuntimeException('unknown has method: <![CDATA[' . $hasMethod . ']]>');
            }

            if (true === call_user_func([$this, $hasMethod])) {
                $getResult = call_user_func([$this, $getMethod]);
                if ($getResult instanceof PacketMethodAttributesInteface) {
                    $getResult = $getResult->getAttributes();
                }
                if (false === $mergeAttributeParams) {
                    $attributes[$field] = $getResult;
                } else {
                    $attributes += $getResult;
                }
            }
        }

        return $attributes;
    }
}
