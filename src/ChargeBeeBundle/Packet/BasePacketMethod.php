<?php

namespace ChargeBeeBundle\Packet;

use ChargeBee_Result;
use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Service\EnvironmentService;

/**
 * Class Base.
 */
abstract class BasePacketMethod implements PacketMethodInterface, PacketMethodAttributesInteface
{
    /**
     * @var ChargeBee_Result
     */
    protected $result;

    /**
     * @var \ChargeBee_Environment
     */
    protected $environment;

    /**
     * {@inheritdoc}
     */
    public function setEnvironment(EnvironmentService $environmentService): PacketMethodInterface
    {
        $this->environment = $environmentService->getEnvInstance();

        return $this;
    }
}
