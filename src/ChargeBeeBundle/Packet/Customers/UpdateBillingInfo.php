<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Customer;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Embed\Address;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateBillingInfo extends BasePacketMethod
{
    use AttributeHelperTrait;

    private $id;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="20")
     */
    private $vatNumber;

    /**
     * @var bool
     *
     * @Assert\Type(type="boolean")
     */
    private $registeredForGst;

    /**
     * @var Address
     *
     * @Assert\NotNull()
     *
     * @Assert\Type(type="ChargeBeeBundle\Packet\Embed\Address")
     * @Assert\Valid()
     */
    private $billingAddress;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;
        $this->result = ChargeBee_Customer::updateBillingInfo($id, $this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId()
    {
        return !is_null($this->id);
    }

    /**
     * @param mixed $id
     *
     * @return UpdateBillingInfo
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'vatNumber',
                [
                    'get' => 'isRegisteredForGst',
                    'has' => 'hasRegisteredForGst',
                ],
                'billingAddress',
            ]
        );
    }

    /**
     * @return string
     */
    public function getVatNumber(): string
    {
        return $this->vatNumber;
    }

    /**
     * @return bool
     */
    public function hasVatNumber(): bool
    {
        return !is_null($this->vatNumber);
    }

    /**
     * @param string $vatNumber
     *
     * @return UpdateBillingInfo
     */
    public function setVatNumber(string $vatNumber): UpdateBillingInfo
    {
        $this->vatNumber = $vatNumber;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRegisteredForGst(): bool
    {
        return $this->registeredForGst;
    }

    /**
     * @return bool
     */
    public function hasRegisteredForGst(): bool
    {
        return !is_null($this->registeredForGst);
    }

    /**
     * @param bool $registeredForGst
     *
     * @return UpdateBillingInfo
     */
    public function setRegisteredForGst(bool $registeredForGst): UpdateBillingInfo
    {
        $this->registeredForGst = $registeredForGst;

        return $this;
    }

    /**
     * @return Address
     */
    public function getBillingAddress(): Address
    {
        return $this->billingAddress;
    }

    /**
     * @return bool
     */
    public function hasBillingAddress(): bool
    {
        return !is_null($this->billingAddress);
    }

    /**
     * @param Address $billingAddress
     *
     * @return UpdateBillingInfo
     */
    public function setBillingAddress(Address $billingAddress): UpdateBillingInfo
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var \ChargeBee_Card $card */
        $card = $this->result->card();

        return [
            'customer' => $customer->getValues(),
            'card' => empty($card) ? null : $card->getValues(),
        ];
    }
}
