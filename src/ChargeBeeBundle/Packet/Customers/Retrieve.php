<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Card;
use ChargeBee_Customer;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Retrieve.
 */
class Retrieve extends BasePacketMethod
{
    /**
     * id
     * Id for the new customer. If not given, this will be auto-generated.
     * optional, string, max chars=50.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $customerId = $this->hasId() ? $this->getId() : null;

        $this->result = ChargeBee_Customer::retrieve($customerId, $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string|null $id
     *
     * @return Retrieve
     */
    public function setId(string $id = null): Retrieve
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var ChargeBee_Card $card */
        $card = $this->result->card();

        return [
            'customer' => $customer->getValues(),
            'card' => is_null($card) ? null : $card->getValues(),
        ];
    }
}
