<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Customer;
use ChargeBeeBundle\Contract\PacketMethodInterface;

class DeleteContacts extends AddContacts
{
    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;
        $this->result = $result = ChargeBee_Customer::deleteContact($id, $this->getAttributes(), $this->environment);

        return $this;
    }
}
