<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Customer;
use ChargeBee_PaymentSource;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class AssignPaymentRole extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     *
     * id
     * Id for the new customer. If not given, this will be auto-generated.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\NotNull()
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     *
     * id
     * Id for the new customer. If not given, this will be auto-generated.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\NotNull()
     * @Assert\Type(type="string")
     * @Assert\Length(max="40")
     */
    private $paymentSourceId;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\Choice(choices={"primary","backup","none"})
     */
    private $role;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;
        $this->result = ChargeBee_Customer::assignPaymentRole($id, $this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return AssignPaymentRole
     */
    public function setId(string $id): AssignPaymentRole
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'paymentSourceId',
                'role',
            ]
        );
    }

    /**
     * @return string
     */
    public function getPaymentSourceId(): string
    {
        return $this->paymentSourceId;
    }

    /**
     * @return bool
     */
    public function hasPaymentSourceId(): bool
    {
        return !is_null($this->paymentSourceId);
    }

    /**
     * @param string $paymentSourceId
     *
     * @return AssignPaymentRole
     */
    public function setPaymentSourceId(string $paymentSourceId): AssignPaymentRole
    {
        $this->paymentSourceId = $paymentSourceId;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @return bool
     */
    public function hasRole(): bool
    {
        return !is_null($this->role);
    }

    /**
     * @param string $role
     *
     * @return AssignPaymentRole
     */
    public function setRole(string $role): AssignPaymentRole
    {
        $this->role = $role;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var ChargeBee_PaymentSource $paymentSource */
        $paymentSource = $this->result->paymentSource();

        return [
            'customer' => $customer->getValues(),
            'paymentSource' => is_null($paymentSource) ? null : $paymentSource->getValues(),
        ];
    }
}
