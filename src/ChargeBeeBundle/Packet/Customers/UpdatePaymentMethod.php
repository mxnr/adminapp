<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Card;
use ChargeBee_Customer;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Customers\Embed\PaymentMethod;
use Symfony\Component\Validator\Constraints as Assert;

class UpdatePaymentMethod extends BasePacketMethod
{
    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * @var PaymentMethod
     *
     * @Assert\NotNull()
     *
     * @Assert\Type(type="\ChargeBeeBundle\Packet\Customers\Embed\BillingAddress")
     * @Assert\Valid()
     */
    private $paymentMethod;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;
        $this->result = ChargeBee_Customer::updatePaymentMethod($id, $this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return UpdatePaymentMethod
     */
    public function setId(string $id): UpdatePaymentMethod
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        $attributes = [];
        if ($this->hasPaymentMethod()) {
            $attributes['PAYMENTMETHOD'] = $this->getPaymentMethod()->getAttributes();
        }

        return $attributes;
    }

    /**
     * @return PaymentMethod
     */
    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * @return bool
     */
    public function hasPaymentMethod(): bool
    {
        return !is_null($this->paymentMethod);
    }

    /**
     * @param PaymentMethod $paymentMethod
     *
     * @return UpdatePaymentMethod
     */
    public function setPaymentMethod(PaymentMethod $paymentMethod): UpdatePaymentMethod
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var ChargeBee_Card $card */
        $card = $this->result->card();

        return [
            'customer' => $customer->getValues(),
            'card' => empty($card) ? null : $card->getValues(),
        ];
    }
}
