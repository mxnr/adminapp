<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Customer;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Customers\Embed\Card;
use ChargeBeeBundle\Packet\Customers\Embed\PaymentMethod;
use ChargeBeeBundle\Packet\Embed\Address;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Create.
 */
class Create extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * id
     * Id for the new customer. If not given, this will be auto-generated.
     * optional, string, max chars=50.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * firstName
     * First name of the customer.
     * optional, string, max chars=150.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $firstName;

    /**
     * lastName
     * Last name of the customer.
     * optional, string, max chars=150.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $lastName;

    /**
     * email
     * Email of the customer. Configured email notifications will be sent to this email.
     * optional, string, max chars=70.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="70")
     * @Assert\Email()
     */
    private $email;

    /**
     * preferredCurrencyCode
     * The currency code (ISO 4217 format) of the customer. Applicable if Multicurrency is enabled.
     * optional, string, max chars=3.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="3")
     */
    private $preferredCurrencyCode;

    /**
     * phone
     * Phone number of the customer.
     * optional, string, max chars=50.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $phone;

    /**
     * company
     * Company name of the customer.
     * optional, string, max chars=250.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="250")
     */
    private $company;

    /**
     * autoCollection
     * Whether payments needs to be collected automatically for this customer.
     * optional, enumerated string, default=on
     * Possible values are
     * on
     * Whenever an invoice is created, an automatic attempt to charge the customer's payment method is made.
     * off
     * Automatic collection of charges will not be made. All payments must be recorded offline.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"on","off"})
     */
    private $autoCollection = 'on';

    /**
     * netTermDays
     * The number of days within which the customer has to make payment for the invoice.
     * optional, integer, default=0.
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="0")
     */
    private $netTermDays = 0;

    /**
     * allowDirectDebit
     * Whether the customer can pay via Direct Debit.
     * optional, boolean, default=false.
     *
     * @var bool
     * @Assert\Type(type="boolean")
     */
    private $allowDirectDebit = false;

    /**
     * vatNumber
     * VAT/ Tax registration number of the customer. Learn more.
     * optional, string, max chars=20.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="20")
     */
    private $vatNumber;

    /**
     * registeredForGst
     * Confirms that a customer is registered under GST. This field is available for Australia only.
     * optional, boolean.
     *
     * @var bool
     * @Assert\Type(type="boolean")
     */
    private $registeredForGst;

    /**
     * taxability
     * Specifies if the customer is liable for tax.
     * optional, enumerated string, default=taxable
     * Possible values are
     * taxable
     * Customer is taxable.
     * exempt
     * Customer is exempted from tax.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"taxable","exempt"})
     */
    private $taxability = 'taxable';

    /**
     * locale
     * Determines which region-specific language Chargebee uses to communicate with the customer. In the absence of the
     * locale attribute, Chargebee will use your site's default
     * language for customer communication.
     * optional, string, max chars=50.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $locale;

    /**
     * entityCode
     * The exemption category of the customer, for the USA and Canada. applicable if you use Chargebee's Avalara
     * integration.
     * optional, enumerated string
     * Possible values are
     * a Federal government (United States).
     * b State government (United States).
     * c Tribe/Status Indian/Indian Band (both).
     * d Foreign diplomat (both). Show all
     * e Charitable or benevolent org (both).
     * f Religious or educational org (both).
     * g Resale (both).
     * h Commercial agricultural production (both).
     * i Industrial production/manufacturer (both).
     * j Direct pay permit (United States).
     * k Direct mail (United States).
     * l Other (both).
     * n Local government (United States).
     * p Commercial aquaculture (Canada).
     * q Commercial Fishery (Canada).
     * r Non-resident (Canada).
     * med1 US Medical Device Excise Tax with exempt sales tax.
     * med2.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"a","b","c","d","e","f","g","h","i","j","k","l","n","p","q","r","med1","med2"})
     */
    private $entityCode;

    /**
     * exemptNumber
     * Any string value that will cause the sale to be exempted. Use this if your finance team manually verifies and
     * tracks exemption certificates.
     * Applicable only for Avalara.
     * optional, string, max chars=100.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="100")
     */
    private $exemptNumber;

    /**
     * metaData
     * Additional data about this resource can be stored here in the JSON Format. Learn more.
     * optional, jsonobject.
     *
     * @var string
     * @Assert\Type(type="json")
     */
    private $metaData;

    /**
     * consolidatedInvoicing
     * Applicable when consolidated invoicing is enabled. Indicates whether invoice consolidation should happen during
     * subscription renewals. Needs to be set only if this value is different from the defaults configured.
     * optional, boolean.
     *
     * @var bool
     * @Assert\Type(type="boolean")
     */
    private $consolidatedInvoicing;

    /**
     * invoiceNotes
     * Invoice Notes for this resource. Read More.
     * optional, string, max chars=1000.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="1000")
     */
    private $invoiceNotes;

    /**
     * @var PaymentMethod
     *
     * @Assert\Type(type="\ChargeBeeBundle\Packet\Customers\Embed\PaymentMethod")
     * @Assert\Valid()
     */
    private $paymentMethod;

    /**
     * @var Address
     *
     * @Assert\Type(type="\ChargeBeeBundle\Packet\Embed\Address")
     * @Assert\Valid()
     */
    private $billingAddress;

    /**
     * @var Card
     *
     * @Assert\Type(type="\ChargeBeeBundle\Packet\Customers\Embed\Card")
     * @Assert\Valid()
     */
    private $card;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $this->result = ChargeBee_Customer::create($this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        $attributes = $this->getAttributesByMap(
            [
                'id',
                'firstName',
                'lastName',
                'email',
                'preferredCurrencyCode',
                'phone',
                'company',
                'autoCollection',
                'netTermDays',
                [
                    'has' => 'hasAllowDirectDebit',
                    'get' => 'isAllowDirectDebit',
                ],
                'vatNumber',
                [
                    'has' => 'hasRegisteredForGst',
                    'get' => 'isRegisteredForGst',
                ],
                'taxability',
                'locale',
                'entityCode',
                'exemptNumber',
                'metaData',
                [
                    'has' => 'hasConsolidatedInvoicing',
                    'get' => 'isConsolidatedInvoicing',
                ],
                'invoiceNotes',
                'card',
            ]
        );

        if ($this->hasPaymentMethod()) {
            $attributes['PAYMENTMETHOD'] = $this->getPaymentMethod()->getAttributes();
        }

        if ($this->hasBillingAddress()) {
            $attributes['BILLINGADDRESS'] = $this->getBillingAddress()->getAttributes();
        }

        return $attributes;
    }

    /**
     * @return PaymentMethod
     */
    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * @return bool
     */
    public function hasPaymentMethod(): bool
    {
        return !is_null($this->paymentMethod);
    }

    /**
     * @param PaymentMethod $paymentMethod
     *
     * @return Create
     */
    public function setPaymentMethod(PaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * @return Address
     */
    public function getBillingAddress(): Address
    {
        return $this->billingAddress;
    }

    /**
     * @return bool
     */
    public function hasBillingAddress(): bool
    {
        return !is_null($this->billingAddress);
    }

    /**
     * @param Address $billingAddress
     *
     * @return Create
     */
    public function setBillingAddress(Address $billingAddress): self
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRegisteredForGst(): bool
    {
        return $this->registeredForGst;
    }

    /**
     * @return bool
     */
    public function hasRegisteredForGst(): bool
    {
        return !is_null($this->registeredForGst);
    }

    /**
     * @param bool $registeredForGst
     *
     * @return Create
     */
    public function setRegisteredForGst(bool $registeredForGst): self
    {
        $this->registeredForGst = $registeredForGst;

        return $this;
    }

    /**
     * @return string
     */
    public function getTaxability(): string
    {
        return $this->taxability;
    }

    /**
     * @return bool
     */
    public function hasTaxability(): bool
    {
        return !is_null($this->taxability);
    }

    /**
     * @param string $taxability
     *
     * @return Create
     */
    public function setTaxability(string $taxability): self
    {
        $this->taxability = $taxability;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @return bool
     */
    public function hasLocale(): bool
    {
        return !is_null($this->locale);
    }

    /**
     * @param string $locale
     *
     * @return Create
     */
    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntityCode(): string
    {
        return $this->entityCode;
    }

    /**
     * @return bool
     */
    public function hasEntityCode(): bool
    {
        return !is_null($this->entityCode);
    }

    /**
     * @param string $entityCode
     *
     * @return Create
     */
    public function setEntityCode(string $entityCode): self
    {
        $this->entityCode = $entityCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getExemptNumber(): string
    {
        return $this->exemptNumber;
    }

    /**
     * @return bool
     */
    public function hasExemptNumber(): bool
    {
        return !is_null($this->exemptNumber);
    }

    /**
     * @param string $exemptNumber
     *
     * @return Create
     */
    public function setExemptNumber(string $exemptNumber): self
    {
        $this->exemptNumber = $exemptNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getMetaData(): string
    {
        return $this->metaData;
    }

    /**
     * @return bool
     */
    public function hasMetaData(): bool
    {
        return !is_null($this->metaData);
    }

    /**
     * @param string $metaData
     *
     * @return Create
     */
    public function setMetaData(string $metaData): self
    {
        $this->metaData = $metaData;

        return $this;
    }

    /**
     * @return bool
     */
    public function isConsolidatedInvoicing(): bool
    {
        return $this->consolidatedInvoicing;
    }

    /**
     * @return bool
     */
    public function hasConsolidatedInvoicing(): bool
    {
        return !is_null($this->consolidatedInvoicing);
    }

    /**
     * @param bool $consolidatedInvoicing
     *
     * @return Create
     */
    public function setConsolidatedInvoicing(bool $consolidatedInvoicing): self
    {
        $this->consolidatedInvoicing = $consolidatedInvoicing;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNotes(): string
    {
        return $this->invoiceNotes;
    }

    /**
     * @return bool
     */
    public function hasInvoiceNotes(): bool
    {
        return !is_null($this->invoiceNotes);
    }

    /**
     * @param string $invoiceNotes
     *
     * @return Create
     */
    public function setInvoiceNotes(string $invoiceNotes): self
    {
        $this->invoiceNotes = $invoiceNotes;

        return $this;
    }

    /**
     * @return Card
     */
    public function getCard(): Card
    {
        return $this->card;
    }

    /**
     * @return bool
     */
    public function hasCard(): bool
    {
        return !is_null($this->card);
    }

    /**
     * @param Card $card
     *
     * @return Create
     */
    public function setCard(Card $card): self
    {
        $this->card = $card;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Create
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return bool
     */
    public function hasFirstName(): bool
    {
        return !is_null($this->firstName);
    }

    /**
     * @param string $firstName
     *
     * @return Create
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return bool
     */
    public function hasLastName(): bool
    {
        return !is_null($this->lastName);
    }

    /**
     * @param string $lastName
     *
     * @return Create
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !is_null($this->email);
    }

    /**
     * @param string $email
     *
     * @return Create
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPreferredCurrencyCode(): string
    {
        return $this->preferredCurrencyCode;
    }

    /**
     * @return bool
     */
    public function hasPreferredCurrencyCode(): bool
    {
        return !is_null($this->preferredCurrencyCode);
    }

    /**
     * @param string $preferredCurrencyCode
     *
     * @return Create
     */
    public function setPreferredCurrencyCode(string $preferredCurrencyCode): self
    {
        $this->preferredCurrencyCode = $preferredCurrencyCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return bool
     */
    public function hasPhone(): bool
    {
        return !is_null($this->phone);
    }

    /**
     * @param string $phone
     *
     * @return Create
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @return bool
     */
    public function hasCompany(): bool
    {
        return !is_null($this->company);
    }

    /**
     * @param string $company
     *
     * @return Create
     */
    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getAutoCollection(): string
    {
        return $this->autoCollection;
    }

    /**
     * @return bool
     */
    public function hasAutoCollection(): bool
    {
        return !is_null($this->autoCollection);
    }

    /**
     * @param string $autoCollection
     *
     * @return Create
     */
    public function setAutoCollection(string $autoCollection): self
    {
        $this->autoCollection = $autoCollection;

        return $this;
    }

    /**
     * @return int
     */
    public function getNetTermDays(): int
    {
        return $this->netTermDays;
    }

    /**
     * @return bool
     */
    public function hasNetTermDays(): bool
    {
        return !is_null($this->netTermDays);
    }

    /**
     * @param int $netTermDays
     *
     * @return Create
     */
    public function setNetTermDays(int $netTermDays): self
    {
        $this->netTermDays = $netTermDays;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowDirectDebit(): bool
    {
        return $this->allowDirectDebit;
    }

    /**
     * @return bool
     */
    public function hasAllowDirectDebit(): bool
    {
        return !is_null($this->allowDirectDebit);
    }

    /**
     * @param bool $allowDirectDebit
     *
     * @return Create
     */
    public function setAllowDirectDebit(bool $allowDirectDebit): self
    {
        $this->allowDirectDebit = $allowDirectDebit;

        return $this;
    }

    /**
     * @return string
     */
    public function getVatNumber(): string
    {
        return $this->vatNumber;
    }

    /**
     * @return bool
     */
    public function hasVatNumber(): bool
    {
        return !is_null($this->vatNumber);
    }

    /**
     * @param string $vatNumber
     *
     * @return Create
     */
    public function setVatNumber(string $vatNumber): self
    {
        $this->vatNumber = $vatNumber;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var \ChargeBee_Card $card */
        $card = $this->result->card();

        return [
            'customer' => $customer->getValues(),
            'card' => is_null($card) ? null : $card->getValues(),
        ];
    }
}
