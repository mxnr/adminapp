<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Customer;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class Delete extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * id
     * Id for the new customer. If not given, this will be auto-generated.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * @var bool
     *
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $deletePaymentMethod = true;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;
        $this->result = ChargeBee_Customer::delete($id, $this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Delete
     */
    public function setId(string $id): Delete
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                [
                    'has' => 'hasDeletePaymentMethod',
                    'get' => 'isDeletePaymentMethod',
                ],
            ]
        );
    }

    /**
     * @return bool
     */
    public function isDeletePaymentMethod(): bool
    {
        return $this->deletePaymentMethod;
    }

    /**
     * @return bool
     */
    public function hasDeletePaymentMethod(): bool
    {
        return !is_null($this->deletePaymentMethod);
    }

    /**
     * @param bool $deletePaymentMethod
     *
     * @return Delete
     */
    public function setDeletePaymentMethod(bool $deletePaymentMethod): Delete
    {
        $this->deletePaymentMethod = $deletePaymentMethod;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var \ChargeBee_Card $card */
        $card = $this->result->card();

        return [
            'customer' => $customer->getValues(),
            'card' => empty($card) ? null : $card->getValues(),
        ];
    }
}
