<?php

namespace ChargeBeeBundle\Packet\Customers\Embed;

use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class PaymentMethod.
 */
class PaymentMethod implements PacketMethodAttributesInteface
{
    use AttributeHelperTrait;

    /**
     * type
     * The type of payment method.
     * For more details refer Update payment method for a customer API under Customer resource.
     * optional, enumerated string
     * Possible values are:
     * card - Card based payment including credit cards and debit cards. Details about the card can be obtained from
     * the card resource.
     * paypal_express_checkout - Payments made via PayPal Express Checkout.
     * amazon_payments -  Payments made via Amazon Payments.
     * direct_debit - Represents bank account for which the direct debit or ACH agreement/mandate is created.
     * generic -  Generic Payment Method.
     * alipay - Alipay Payments.
     * unionpay - UnionPay Payments.
     * apple_pay - Apple Pay Payments.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={
     *     "card","paypal_express_checkout","amazon_payments","direct_debit","generic","alipay","unionpay","apple_pay"
     * })
     */
    private $type;

    /**
     * gatewayAccountId
     * The gateway account in which this payment source is stored.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $gatewayAccountId;

    /**
     * referenceId
     * The reference id. In the case of Amazon and PayPal this will be the billing agreement id. For GoCardless direct
     * debit this will be 'mandate id'. In the case of card this will be the identifier provided by the gateway/card
     * vault for the specific payment method resource. Note: This is not the one-time temporary token provided by
     * gateways like Stripe. For more details refer Update payment method for a customer API under Customer resource.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $referenceId;

    /**
     * tmpToken
     * Single-use tokens created by payment gateways. In Stripe, a single-use token is created for Apple Pay Wallet,
     * card details or direct debit. In Braintree, a nonce is created for Apple Pay Wallet, PayPal, or card details. In
     * Authorize.Net, a nonce is created for card details.
     * required if reference_id not provided, string, max chars=300
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="300")
     */
    private $tmpToken;

    /**
     * @Assert\Callback()
     *
     * @param ExecutionContextInterface $context
     * @param                           $payload
     */
    public function checkReferenceIdOrTmpToken(ExecutionContextInterface $context, $payload)
    {
        if (empty($this->tmpToken) && empty($this->referenceId)) {
            $context->buildViolation('Must be fullfilled if tmpToken empty')
                ->atPath('referenceId')
                ->addViolation();

            $context->buildViolation('Must be fullfilled if referenceId is empty')
                ->atPath('tmpToken')
                ->addViolation();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'type',
                'gatewayAccountId',
                'referenceId',
                'tmpToken',
            ]
        );
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function hasType(): bool
    {
        return !is_null($this->type);
    }

    /**
     * @param string $type
     *
     * @return PaymentMethod
     */
    public function setType(string $type): PaymentMethod
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getGatewayAccountId(): string
    {
        return $this->gatewayAccountId;
    }

    /**
     * @return bool
     */
    public function hasGatewayAccountId(): bool
    {
        return !is_null($this->gatewayAccountId);
    }

    /**
     * @param string $gatewayAccountId
     *
     * @return PaymentMethod
     */
    public function setGatewayAccountId(string $gatewayAccountId): PaymentMethod
    {
        $this->gatewayAccountId = $gatewayAccountId;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferenceId(): string
    {
        return $this->referenceId;
    }

    /**
     * @return bool
     */
    public function hasReferenceId(): bool
    {
        return !is_null($this->referenceId);
    }

    /**
     * @param string $referenceId
     *
     * @return PaymentMethod
     */
    public function setReferenceId(string $referenceId): PaymentMethod
    {
        $this->referenceId = $referenceId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTmpToken(): string
    {
        return $this->tmpToken;
    }

    /**
     * @return bool
     */
    public function hasTmpToken(): bool
    {
        return !is_null($this->tmpToken);
    }

    /**
     * @param string $tmpToken
     *
     * @return PaymentMethod
     */
    public function setTmpToken(string $tmpToken): PaymentMethod
    {
        $this->tmpToken = $tmpToken;

        return $this;
    }
}
