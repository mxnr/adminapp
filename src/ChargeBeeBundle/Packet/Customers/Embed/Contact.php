<?php

namespace ChargeBeeBundle\Packet\Customers\Embed;

use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class Contact implements PacketMethodAttributesInteface
{
    use AttributeHelperTrait;

    /**
     * id
     * Unique reference ID provided for the contact.
     * optional, string, max chars=150
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $id;

    /**
     * firstName
     * First name of the contact.
     * optional, string, max chars=150
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $firstName;

    /**
     * lastName
     * Last name of the contact.
     * optional, string, max chars=150
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $lastName;

    /**
     * email
     * Email of the contact.
     * required, string, max chars=70
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="70")
     * @Assert\NotNull()
     */
    private $email;

    /**
     * phone
     * Phone number of the contact.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $phone;

    /**
     * label
     * Label/Tag provided for contact.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $label;

    /**
     * enabled
     * Contact enabled / disabled.
     * optional, boolean, default=false
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $enabled = false;

    /**
     * sendBillingEmail
     * Whether Billing Emails option is enabled for the contact.
     * optional, boolean, default=false
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $sendBillingEmail = false;

    /**
     * sendAccountEmail
     * Whether Account Emails option is enabled for the contact.
     * optional, boolean, default=false
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $sendAccountEmail;

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return bool
     */
    public function hasPhone(): bool
    {
        return !is_null($this->phone);
    }

    /**
     * @param string $phone
     *
     * @return Contact
     */
    public function setPhone(string $phone): Contact
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'id',
                'firstName',
                'lastName',
                'email',
                'label',
                [
                    'has' => 'hasEnabled',
                    'get' => 'isEnabled',
                ],
                [
                    'has' => 'hasSendBillingEmail',
                    'get' => 'isSendBillingEmail',
                ],
                [
                    'has' => 'hasSendAccountEmail',
                    'get' => 'isSendAccountEmail',
                ],
            ]
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Contact
     */
    public function setId(string $id): Contact
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return bool
     */
    public function hasFirstName(): bool
    {
        return !is_null($this->firstName);
    }

    /**
     * @param string $firstName
     *
     * @return Contact
     */
    public function setFirstName(string $firstName): Contact
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return bool
     */
    public function hasLastName(): bool
    {
        return !is_null($this->lastName);
    }

    /**
     * @param string $lastName
     *
     * @return Contact
     */
    public function setLastName(string $lastName): Contact
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !is_null($this->email);
    }

    /**
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail(string $email): Contact
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return bool
     */
    public function hasLabel(): bool
    {
        return !is_null($this->label);
    }

    /**
     * @param string $label
     *
     * @return Contact
     */
    public function setLabel(string $label): Contact
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function hasEnabled(): bool
    {
        return !is_null($this->enabled);
    }

    /**
     * @param bool $enabled
     *
     * @return Contact
     */
    public function setEnabled(bool $enabled): Contact
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSendBillingEmail(): bool
    {
        return $this->sendBillingEmail;
    }

    /**
     * @return bool
     */
    public function hasSendBillingEmail(): bool
    {
        return !is_null($this->sendBillingEmail);
    }

    /**
     * @param bool $sendBillingEmail
     *
     * @return Contact
     */
    public function setSendBillingEmail(bool $sendBillingEmail): Contact
    {
        $this->sendBillingEmail = $sendBillingEmail;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSendAccountEmail(): bool
    {
        return $this->sendAccountEmail;
    }

    /**
     * @return bool
     */
    public function hasSendAccountEmail(): bool
    {
        return !is_null($this->sendAccountEmail);
    }

    /**
     * @param bool $sendAccountEmail
     *
     * @return Contact
     */
    public function setSendAccountEmail(bool $sendAccountEmail): Contact
    {
        $this->sendAccountEmail = $sendAccountEmail;

        return $this;
    }
}
