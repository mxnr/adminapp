<?php

namespace ChargeBeeBundle\Packet\Customers\Embed;

use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class Transaction implements PacketMethodAttributesInteface
{
    use AttributeHelperTrait;

    /**
     * amount
     * The payment transaction amount.
     * required, in cents, min=1
     *
     * @var int
     * @Assert\NotNull()
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $amount;

    /**
     * currencyCode
     * The currency code (ISO 4217 format) for the transaction.
     * required if Multicurrency is enabled, string, max chars=3
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\NotNull()
     * @Assert\Length(max="3")
     */
    private $currencyCode;

    /**
     * date
     * Indicates when this transaction occurred.
     * required, timestamp(UTC) in seconds
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\NotNull()
     */
    private $date;

    /**
     * paymentMethod
     * The Payment Method of this transaction.
     * required, enumerated string, default=card
     * Possible values are
     * cash Cash.
     * check Check.
     * bank_transfer Bank Transfer.
     * other
     * Payment Methods other than the above types.
     *
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\NotNull()
     * @Assert\Choice(choices={"cash","check","bank_transfer","other","card"})
     */
    private $paymentMethod = 'card';

    /**
     * referenceNumber
     * The reference number for this transaction. e.g check number in case of 'check' payments.
     * optional, string, max chars=100
     *
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="100")
     */
    private $referenceNumber;

    /**
     * @return string
     */
    public function getReferenceNumber(): string
    {
        return $this->referenceNumber;
    }

    /**
     * @return bool
     */
    public function hasReferenceNumber(): bool
    {
        return !is_null($this->referenceNumber);
    }

    /**
     * @param string $referenceNumber
     *
     * @return Transaction
     */
    public function setReferenceNumber(string $referenceNumber): Transaction
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'amount',
                'currencyCode',
                'date',
                'paymentMethod',
            ]
        );
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return bool
     */
    public function hasAmount(): bool
    {
        return !is_null($this->amount);
    }

    /**
     * @param int $amount
     *
     * @return Transaction
     */
    public function setAmount(int $amount): Transaction
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @return bool
     */
    public function hasCurrencyCode(): bool
    {
        return !is_null($this->currencyCode);
    }

    /**
     * @param string $currencyCode
     *
     * @return Transaction
     */
    public function setCurrencyCode(string $currencyCode): Transaction
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * @return int
     */
    public function getDate(): int
    {
        return $this->date;
    }

    /**
     * @return bool
     */
    public function hasDate(): bool
    {
        return !is_null($this->date);
    }

    /**
     * @param int $date
     *
     * @return Transaction
     */
    public function setDate(int $date): Transaction
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentMethod(): string
    {
        return $this->paymentMethod;
    }

    /**
     * @return bool
     */
    public function hasPaymentMethod(): bool
    {
        return !is_null($this->paymentMethod);
    }

    /**
     * @param string $paymentMethod
     *
     * @return Transaction
     */
    public function setPaymentMethod(string $paymentMethod): Transaction
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }
}
