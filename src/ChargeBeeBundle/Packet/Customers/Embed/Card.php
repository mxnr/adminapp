<?php

namespace ChargeBeeBundle\Packet\Customers\Embed;

use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class Card implements PacketMethodAttributesInteface
{
    use AttributeHelperTrait;

    /**
     * gatewayAccountId
     * The gateway account in which this payment source is stored.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $gatewayAccountId;

    /**
     * firstName
     * Cardholder's first name.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $firstName;

    /**
     * lastName
     * Cardholder's last name.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $lastName;

    /**
     * number
     * The credit card number without any format.
     * If you are using Braintree.js, you can specify the Braintree
     * encrypted card number here.
     * required if card provided, string, max chars=1500
     *
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(max="1500")
     */
    private $number;

    /**
     * expiryMonth
     * Card expiry month.
     * required if card provided, integer, min=1, max=12
     *
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1",max="12")
     */
    private $expiryMonth;

    /**
     * expiryYear
     * Card expiry year.
     * required if card provided, integer
     *
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     */
    private $expiryYear;

    /**
     * cvv
     * The card verification value. If you are using Braintree.js, you can specify the Braintree encrypted cvv here.
     * optional, string, max chars=520
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="520")
     */
    private $cvv;

    /**
     * billingAddr1
     * Address line 1, as available in card billing address.
     * optional, string, max chars=150
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $billingAddr1;

    /**
     * billingAddr2
     * Address line 2, as available in card billing address.
     * optional, string, max chars=150
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $billingAddr2;

    /**
     * billingCity
     * City, as available in card billing address.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $billingCity;

    /**
     * billingStateCode
     * The ISO 3166-2 state/province code. The recommended way of passing the state/province information. Supported for
     * US, Canada and India now. Further if this is specified, 'state' attribute should not be specified as it will be
     * set automatically. optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $billingStateCode;

    /**
     * billingState
     * The state/province name. Use this to pass the state/province information for cases where 'state_code' is not
     * supported or cannot be passed.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $billingState;

    /**
     * billingZip
     * Postal or Zip code, as available in card billing address.
     * optional, string, max chars=20
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="20")
     */
    private $billingZip;

    /**
     * billingCountry
     * 2-letter ISO 3166 alpha-2 country code.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $billingCountry;

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'gatewayAccountId',
                'firstName',
                'lastName',
                'number',
                'expiryMonth',
                'cvv',
                'billingAddr1',
                'billingAddr2',
                'billingCity',
                'billingZip',
                'billingCountry',
            ]
        );
    }

    /**
     * @return string
     */
    public function getGatewayAccountId(): string
    {
        return $this->gatewayAccountId;
    }

    /**
     * @return bool
     */
    public function hasGatewayAccountId(): bool
    {
        return !is_null($this->gatewayAccountId);
    }

    /**
     * @param string $gatewayAccountId
     *
     * @return Card
     */
    public function setGatewayAccountId(string $gatewayAccountId): Card
    {
        $this->gatewayAccountId = $gatewayAccountId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return bool
     */
    public function hasFirstName(): bool
    {
        return !is_null($this->firstName);
    }

    /**
     * @param string $firstName
     *
     * @return Card
     */
    public function setFirstName(string $firstName): Card
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return bool
     */
    public function hasLastName(): bool
    {
        return !is_null($this->lastName);
    }

    /**
     * @param string $lastName
     *
     * @return Card
     */
    public function setLastName(string $lastName): Card
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @return bool
     */
    public function hasNumber(): bool
    {
        return !is_null($this->number);
    }

    /**
     * @param string $number
     *
     * @return Card
     */
    public function setNumber(string $number): Card
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return int
     */
    public function getExpiryMonth(): int
    {
        return $this->expiryMonth;
    }

    /**
     * @return bool
     */
    public function hasExpiryMonth(): bool
    {
        return !is_null($this->expiryMonth);
    }

    /**
     * @param int $expiryMonth
     *
     * @return Card
     */
    public function setExpiryMonth(int $expiryMonth): Card
    {
        $this->expiryMonth = $expiryMonth;

        return $this;
    }

    /**
     * @return int
     */
    public function getExpiryYear(): int
    {
        return $this->expiryYear;
    }

    /**
     * @return bool
     */
    public function hasExpiryYear(): bool
    {
        return !is_null($this->expiryYear);
    }

    /**
     * @param int $expiryYear
     *
     * @return Card
     */
    public function setExpiryYear(int $expiryYear): Card
    {
        $this->expiryYear = $expiryYear;

        return $this;
    }

    /**
     * @return string
     */
    public function getCvv(): string
    {
        return $this->cvv;
    }

    /**
     * @return bool
     */
    public function hasCvv(): bool
    {
        return !is_null($this->cvv);
    }

    /**
     * @param string $cvv
     *
     * @return Card
     */
    public function setCvv(string $cvv): Card
    {
        $this->cvv = $cvv;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddr1(): string
    {
        return $this->billingAddr1;
    }

    /**
     * @return bool
     */
    public function hasBillingAddr1(): bool
    {
        return !is_null($this->billingAddr1);
    }

    /**
     * @param string $billingAddr1
     *
     * @return Card
     */
    public function setBillingAddr1(string $billingAddr1): Card
    {
        $this->billingAddr1 = $billingAddr1;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddr2(): string
    {
        return $this->billingAddr2;
    }

    /**
     * @return bool
     */
    public function hasBillingAddr2(): bool
    {
        return !is_null($this->billingAddr2);
    }

    /**
     * @param string $billingAddr2
     *
     * @return Card
     */
    public function setBillingAddr2(string $billingAddr2): Card
    {
        $this->billingAddr2 = $billingAddr2;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingCity(): string
    {
        return $this->billingCity;
    }

    /**
     * @return bool
     */
    public function hasBillingCity(): bool
    {
        return !is_null($this->billingCity);
    }

    /**
     * @param string $billingCity
     *
     * @return Card
     */
    public function setBillingCity(string $billingCity): Card
    {
        $this->billingCity = $billingCity;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingStateCode(): string
    {
        return $this->billingStateCode;
    }

    /**
     * @return bool
     */
    public function hasBillingStateCode(): bool
    {
        return !is_null($this->billingStateCode);
    }

    /**
     * @param string $billingStateCode
     *
     * @return Card
     */
    public function setBillingStateCode(string $billingStateCode): Card
    {
        $this->billingStateCode = $billingStateCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingState(): string
    {
        return $this->billingState;
    }

    /**
     * @return bool
     */
    public function hasBillingState(): bool
    {
        return !is_null($this->billingState);
    }

    /**
     * @param string $billingState
     *
     * @return Card
     */
    public function setBillingState(string $billingState): Card
    {
        $this->billingState = $billingState;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingZip(): string
    {
        return $this->billingZip;
    }

    /**
     * @return bool
     */
    public function hasBillingZip(): bool
    {
        return !is_null($this->billingZip);
    }

    /**
     * @param string $billingZip
     *
     * @return Card
     */
    public function setBillingZip(string $billingZip): Card
    {
        $this->billingZip = $billingZip;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingCountry(): string
    {
        return $this->billingCountry;
    }

    /**
     * @return bool
     */
    public function hasBillingCountry(): bool
    {
        return !is_null($this->billingCountry);
    }

    /**
     * @param string $billingCountry
     *
     * @return Card
     */
    public function setBillingCountry(string $billingCountry): Card
    {
        $this->billingCountry = $billingCountry;

        return $this;
    }
}
