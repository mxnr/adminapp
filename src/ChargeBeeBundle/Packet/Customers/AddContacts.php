<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Customer;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Customers\Embed\Contact;
use Symfony\Component\Validator\Constraints as Assert;

class AddContacts extends BasePacketMethod
{
    /**
     * id
     * Id for the new customer. If not given, this will be auto-generated.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * @var Contact
     *
     * @Assert\Type(type="\ChargeBeeBundle\Packet\Customers\Embed\Contact")
     * @Assert\Valid()
     * @Assert\NotNull()
     */
    private $contact;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;
        $this->result = $result = ChargeBee_Customer::addContact($id, $this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return AddContacts
     */
    public function setId(string $id): AddContacts
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        $attributes = [];

        if ($this->hasContact()) {
            $attributes['CONTACT'] = $this->getContact()->getAttributes();
        }

        return $attributes;
    }

    /**
     * @return Contact
     */
    public function getContact(): Contact
    {
        return $this->contact;
    }

    /**
     * @return bool
     */
    public function hasContact(): bool
    {
        return !is_null($this->contact);
    }

    /**
     * @param Contact $contact
     *
     * @return AddContacts
     */
    public function setContact(Contact $contact): AddContacts
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var \ChargeBee_Card $card */
        $card = $this->result->card();

        return [
            'customer' => $customer->getValues(),
            'card' => is_null($card) ? null : $card->getValues(),
        ];
    }
}
