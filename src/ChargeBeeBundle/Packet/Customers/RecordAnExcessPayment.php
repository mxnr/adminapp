<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Customer;
use ChargeBee_Transaction;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Customers\Embed\Transaction;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class RecordAnExcessPayment extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * id
     * Id for the new customer. If not given, this will be auto-generated.
     * optional, string, max chars=50
     *
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * comment
     * Remarks, if any, on the payment.
     * optional, string, max chars=300
     *
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="300")
     */
    private $comment;

    /**
     * transaction
     * Associative array of parameters for transaction
     * pass parameters as transaction => array("<param name>" => "<value>",...)
     *
     * @var Transaction
     * @Assert\NotNull()
     *
     * @Assert\Type(type="\ChargeBeeBundle\Packet\Customers\Embed\Transaction")
     * @Assert\Valid()
     */
    private $transaction;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;

        $this->result = ChargeBee_Customer::assignPaymentRole($id, $this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return RecordAnExcessPayment
     */
    public function setId(string $id): RecordAnExcessPayment
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'comment',
                'transaction',
            ]
        );
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return bool
     */
    public function hasComment(): bool
    {
        return !is_null($this->comment);
    }

    /**
     * @param string $comment
     *
     * @return RecordAnExcessPayment
     */
    public function setComment(string $comment): RecordAnExcessPayment
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return Transaction
     */
    public function getTransaction(): Transaction
    {
        return $this->transaction;
    }

    /**
     * @return bool
     */
    public function hasTransaction(): bool
    {
        return !is_null($this->transaction);
    }

    /**
     * @param Transaction $transaction
     *
     * @return RecordAnExcessPayment
     */
    public function setTransaction(Transaction $transaction): RecordAnExcessPayment
    {
        $this->transaction = $transaction;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var ChargeBee_Transaction $transaction */
        $transaction = $this->result->transaction();

        return [
            'customer' => $customer->getValues(),
            'transaction' => empty($transaction) ? null : $transaction->getValues(),
        ];
    }
}
