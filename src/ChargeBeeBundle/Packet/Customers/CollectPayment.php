<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Customers\Embed\PaymentMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class CollectPayment extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * id
     * Id for the new customer. If not given, this will be auto-generated.
     * optional, string, max chars=50
     *
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * amount
     * Amount to be collected. If this parameter is not passed then the invoice(s) amount to collect will be collected.
     * optional, in cents, min=0
     *
     * @var int
     *
     * @Assert\Type(type="integer")
     * @Assert\Range(min=0)
     */
    private $amount;

    /**
     * paymentSourceId
     * Payment source used for the payment.
     * optional, string, max chars=40
     *
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="40")
     */
    private $paymentSourceId;

    /**
     * replacePrimaryPaymentSource
     * Indicates whether the primary payment source need to be replaced with the payment source given.
     * optional, boolean, default=false
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $replacePrimaryPaymentSource = false;

    /**
     * retainPaymentSource
     * Indicates whether the payment source should be retained for the customer.
     * optional, boolean, default=false
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $retainPaymentSource = false;

    /**
     * @var PaymentMethod
     *
     * @Assert\Type(type="\ChargeBeeBundle\Packet\Customers\Embed\PaymentMethod")
     * @Assert\Valid()
     */
    private $paymentMethod;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;

        $this->result = \ChargeBee_Customer::collectPayment($id, $this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return CollectPayment
     */
    public function setId(string $id): CollectPayment
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'amount',
                'paymentSourceId',
                [
                    'has' => 'hasReplacePrimaryPaymentSource',
                    'get' => 'isReplacePrimaryPaymentSource',
                ],
                [
                    'has' => 'hasRetainPaymentSource',
                    'get' => 'isRetainPaymentSource',
                ],
                'paymentMethod',
            ]
        );
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return bool
     */
    public function hasAmount(): bool
    {
        return !is_null($this->amount);
    }

    /**
     * @param int $amount
     *
     * @return CollectPayment
     */
    public function setAmount(int $amount): CollectPayment
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentSourceId(): string
    {
        return $this->paymentSourceId;
    }

    /**
     * @return bool
     */
    public function hasPaymentSourceId(): bool
    {
        return !is_null($this->paymentSourceId);
    }

    /**
     * @param string $paymentSourceId
     *
     * @return CollectPayment
     */
    public function setPaymentSourceId(string $paymentSourceId): CollectPayment
    {
        $this->paymentSourceId = $paymentSourceId;

        return $this;
    }

    /**
     * @return bool
     */
    public function isReplacePrimaryPaymentSource(): bool
    {
        return $this->replacePrimaryPaymentSource;
    }

    /**
     * @return bool
     */
    public function hasReplacePrimaryPaymentSource(): bool
    {
        return !is_null($this->replacePrimaryPaymentSource);
    }

    /**
     * @param bool $replacePrimaryPaymentSource
     *
     * @return CollectPayment
     */
    public function setReplacePrimaryPaymentSource(bool $replacePrimaryPaymentSource): CollectPayment
    {
        $this->replacePrimaryPaymentSource = $replacePrimaryPaymentSource;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRetainPaymentSource(): bool
    {
        return $this->retainPaymentSource;
    }

    /**
     * @return bool
     */
    public function hasRetainPaymentSource(): bool
    {
        return !is_null($this->retainPaymentSource);
    }

    /**
     * @param bool $retainPaymentSource
     *
     * @return CollectPayment
     */
    public function setRetainPaymentSource(bool $retainPaymentSource): CollectPayment
    {
        $this->retainPaymentSource = $retainPaymentSource;

        return $this;
    }

    /**
     * @return PaymentMethod
     */
    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * @return bool
     */
    public function hasPaymentMethod(): bool
    {
        return !is_null($this->paymentMethod);
    }

    /**
     * @param PaymentMethod $paymentMethod
     *
     * @return CollectPayment
     */
    public function setPaymentMethod(PaymentMethod $paymentMethod): CollectPayment
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var \ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var \ChargeBee_Transaction $transaction */
        $transaction = $this->result->transaction();

        return [
            'customer' => $customer->getValues(),
            'transaction' => is_null($transaction) ? null : $transaction->getValues(),
        ];
    }
}
