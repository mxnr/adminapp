<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Customer;
use ChargeBee_ListResult;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class ListAll extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * @var ChargeBee_ListResult
     */
    protected $result;

    /**
     * limit
     * Limits the number of resources to be returned.
     * optional, integer, default=10, min=1, max=100
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\NotNull()
     * @Assert\Range(max="100",min="1")
     */
    private $limit = 10;

    /**
     * offset
     * Allows you to fetch the next set of resources. The value used for this parameter must be the value returned for
     * next_offset parameter in the previous API call. optional, string, max chars=1000
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="1000")
     */
    private $offset;

    /**
     * includeDeleted
     * If set to true, includes the deleted resources in the response. For the deleted resources in the response, the
     * 'deleted' attribute will be 'true'. optional, boolean, default=false
     *
     * @var bool
     * @Assert\NotNull()
     * @Assert\Type(type="boolean")
     */
    private $includeDeleted = false;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"created_at"})
     */
    private $sortBy;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"asc","desc"})
     */
    private $sortOrder;

    /**
     * id[<operator>]
     * To filter based on Customer Id.
     * Supported operators : is, isNot, startsWith, in, notIn
     *
     * Example → "id[is]" => "9bsvnHgsvmsI"
     * optional, string filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"is","isNot", "startsWith","in","notIn"})
     */
    private $idOperator;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $id;

    /**
     * firstName[<operator>]
     * To filter based on Customer First Name.
     * Supported operators : is, isNot, startsWith, isPresent
     * Example → "firstName[is]" => "John"
     * optional, string filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"is","isNot","startsWith","isPresent"})
     */
    private $firstNameOperator;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(min="1")
     */
    private $firstName;

    /**
     * lastName[<operator>]
     * To filter based on Customer Last Name.
     * Supported operators : is, isNot, startsWith, isPresent
     * Example → "lastName[isNot]" => "Clint"
     * optional, string filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"is","isNot","startsWith","isPresent"})
     */
    private $lastNameOperator;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(min="1")
     */
    private $lastName;

    /**
     * email[<operator>]
     * To filter based on Customer Email.
     * Supported operators : is, isNot, startsWith, isPresent
     * Example → "email[is]" => "john@test.com"
     * optional, string filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"is","isNot","startsWith","isPresent"})
     */
    private $emailOperator;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(min="1")
     */
    private $email;

    /**
     * company[<operator>]
     * To filter based on Customer Company.
     * Supported operators : is, isNot, startsWith, isPresent
     * Example → "company[is]" => "Globex Corp"
     * optional, string filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"is","isNot","startsWith","isPresent"})
     */
    private $companyOperator;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(min="1")
     */
    private $company;

    /**
     * phone[<operator>]
     * To filter based on Customer Phone.
     * Supported operators : is, isNot, startsWith, isPresent
     *
     * Example → "phone[is]" => "(541) 754-3010"
     * optional, string filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"is","isNot","startsWith","isPresent"})
     */
    private $phoneOperator;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(min="1")
     */
    private $phone;

    /**
     * autoCollection[<operator>]
     * To filter based on Customer Auto Collection. Possible values are : on, off.
     * Supported operators : is, isNot, in, notIn
     *
     * Example → "autoCollection[is]" => "on"
     * optional, enumerated string filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"is","isNot","in","notIn"})
     */
    private $autoCollectionOperator;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"on","off"})
     */
    private $autoCollection;

    /**
     * taxability[<operator>]
     * To filter based on Customer Taxability. Possible values are : taxable, exempt.
     * Supported operators : is, isNot, in, notIn
     * Example → "taxability[isNot]" => "taxable"
     * optional, enumerated string filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"is","isNot","in","notIn"})
     */
    private $taxabilityOperator;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(min="1")
     */
    private $taxability;

    /**
     * createdAt[<operator>]
     * To filter based on Customer Created At.
     * Supported operators : after, before, on, between
     *
     * Example → "createdAt[on]" => "1435054328"
     * optional, timestamp(UTC) in seconds filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"after","before","on","between"})
     */
    private $createdAtOperator;

    /**
     * @var int
     * @Assert\Type(type="iteger")
     * @Assert\Range(min="1")
     */
    private $createdAt;

    /**
     * updatedAt[<operator>]
     * To filter based on updated at. This attribute will be present only if the resource has been updated after
     * 2016-09-28. Supported operators : after, before, on, between
     *
     * Example → "updatedAt[after]" => "1243545465"
     * optional, timestamp(UTC) in seconds filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"after","before","on","between"})
     */
    private $updatedAtOperator;

    /**
     * @var int
     * @Assert\Type(type="iteger")
     * @Assert\Range(min="1")
     */
    private $updatedAt;

    /**
     * @param string $operator
     * @param string $firstName
     *
     * @return ListAll
     */
    public function setFirstNameFilter(string $operator, string $firstName): ListAll
    {
        $this->firstNameOperator = $operator;
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @param strin  $operator
     * @param string $lastName
     *
     * @return ListAll
     */
    public function setLastNameFilter(strin $operator, string $lastName): ListAll
    {
        $this->lastNameOperator = $operator;
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @param string $operator
     * @param string $email
     *
     * @return ListAll
     */
    public function setEmailFilter(string $operator, string $email): ListAll
    {
        $this->emailOperator = $operator;
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $operator
     * @param string $company
     *
     * @return ListAll
     */
    public function setCompanyFilter(string $operator, string $company): ListAll
    {
        $this->companyOperator = $operator;
        $this->company = $company;

        return $this;
    }

    /**
     * @param string $operator
     * @param string $phone
     *
     * @return ListAll
     */
    public function setPhoneFilter(string $operator, string $phone): ListAll
    {
        $this->phoneOperator = $operator;
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $autoCollection
     *
     * @return ListAll
     */
    public function setAutoCollection(string $operator, array $autoCollection): ListAll
    {
        $this->autoCollectionOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->autoCollection = $autoCollection;
        } else {
            $this->autoCollection = array_pop($autoCollection);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $taxability
     *
     * @return ListAll
     */
    public function setTaxabilityFilter(string $operator, array $taxability): ListAll
    {
        $this->taxabilityOperator = $operator;

        if (in_array($operator, ['in', 'notIn'])) {
            $this->taxability = $taxability;
        } else {
            $this->taxability = array_pop($taxability);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $createdAt
     *
     * @return ListAll
     */
    public function setCreatedAtFilter(string $operator, array $createdAt): ListAll
    {
        $this->createdAtOperator = $operator;
        if (in_array($operator, ['between'])) {
            $this->createdAt = $createdAt;
        } else {
            $this->createdAt = array_pop($createdAt);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $updatedAt
     *
     * @return ListAll
     */
    public function setUpdatedAtFilter(string $operator, array $updatedAt): ListAll
    {
        $this->updatedAtOperator = $operator;

        if (in_array($operator, ['between'])) {
            $this->updatedAt = $updatedAt;
        } else {
            $this->updatedAt = array_pop($updatedAt);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $id
     *
     * @return ListAll
     */
    public function setIdFilter(string $operator, array $id): ListAll
    {
        $this->idOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->id = $id;
        } else {
            $this->id = array_pop($id);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $this->result = ChargeBee_Customer::all($this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        $attributes = $this->getAttributesByMap(
            [
                'limit',
                'offset',
                [
                    'get' => 'isIncludeDeleted',
                    'has' => 'hasIncludeDeleted',
                ],
            ]
        );

        $attributes = $this->getAttributesByMap(
            [
                'sortBy',
                'idFilter',
                'firstNameFilter',
                'lastNameFilter',
                'emailFilter',
                'companyFilter',
                'phoneFilter',
                'autoCollectionFilter',
                'taxabilityFilter',
                'createdAtFilter',
                'updatedAtFilter',
            ],
            $attributes,
            true
        );

        return $attributes;
    }

    /**
     * @return bool
     */
    public function hasSortBy(): bool
    {
        return !is_null($this->sortBy);
    }

    /**
     * @return array
     */
    public function getSortBy(): array
    {
        return ['sortBy[' . $this->sortOrder . ']' => $this->sortBy];
    }

    /**
     * @param string $by
     * @param string $order
     *
     * @return ListAll
     */
    public function setSortBy(string $by, string $order): ListAll
    {
        $this->sortBy = $by;
        $this->sortOrder = $order;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasIdFilter(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @return array
     */
    public function getIdFilter(): array
    {
        return ['id[' . $this->idOperator . ']' => $this->id];
    }

    /**
     * @return bool
     */
    public function hasFirstNameFilter(): bool
    {
        return !is_null($this->firstName);
    }

    /**
     * @return array
     */
    public function getFirstNameFilter(): array
    {
        return ['firstName[' . $this->firstNameOperator . ']' => $this->firstName];
    }

    /**
     * @return bool
     */
    public function hasLastNameFilter(): bool
    {
        return !is_null($this->lastName);
    }

    /**
     * @return array
     */
    public function getLastNameFilter(): array
    {
        return ['lastName[' . $this->lastNameOperator . ']' => $this->lastName];
    }

    /**
     * @return bool
     */
    public function hasEmailFilter(): bool
    {
        return !is_null($this->email);
    }

    /**
     * @return array
     */
    public function getEmailFilter(): array
    {
        return ['email[' . $this->emailOperator . ']' => $this->email];
    }

    /**
     * @return bool
     */
    public function hasCompanyFilter(): bool
    {
        return !is_null($this->company);
    }

    /**
     * @return array
     */
    public function getCompanyFilter(): array
    {
        return ['company[' . $this->companyOperator . ']' => $this->company];
    }

    /**
     * @return bool
     */
    public function hasPhoneFilter(): bool
    {
        return !is_null($this->phone);
    }

    /**
     * @return array
     */
    public function getPhoneFilter(): array
    {
        return ['phone[' . $this->phoneOperator . ']' => $this->phone];
    }

    /**
     * @return bool
     */
    public function hasAutoCollectionFilter(): bool
    {
        return !is_null($this->autoCollection);
    }

    /**
     * @return array
     */
    public function getAutoCollectionFilter(): array
    {
        return ['autoCollection[' . $this->autoCollectionOperator . ']' => $this->autoCollection];
    }

    /**
     * @return bool
     */
    public function hasTaxabilityFilter(): bool
    {
        return !is_null($this->taxability);
    }

    /**
     * @return array
     */
    public function getTaxabilityFilter(): array
    {
        return ['taxability[' . $this->taxabilityOperator . ']' => $this->taxability];
    }

    /**
     * @return bool
     */
    public function hasCreatedAtFilter(): bool
    {
        return !is_null($this->createdAt);
    }

    /**
     * @return array
     */
    public function getCreatedAtFilter(): array
    {
        return ['createdAt[' . $this->createdAtOperator . ']' => $this->createdAt];
    }

    /**
     * @return bool
     */
    public function hasUpdatedAtFilter(): bool
    {
        return !is_null($this->updatedAt);
    }

    /**
     * @return array
     */
    public function getUpdatedAtFilter(): array
    {
        return ['updatedAt[' . $this->updatedAtOperator . ']' => $this->updatedAt];
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return bool
     */
    public function hasLimit(): bool
    {
        return !is_null($this->limit);
    }

    /**
     * @param int $limit
     *
     * @return ListAll
     */
    public function setLimit(int $limit): ListAll
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return string
     */
    public function getOffset(): string
    {
        return $this->offset;
    }

    /**
     * @return bool
     */
    public function hasOffset(): bool
    {
        return !is_null($this->offset);
    }

    /**
     * @param string $offset
     *
     * @return ListAll
     */
    public function setOffset(string $offset): ListAll
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return bool
     */
    public function isIncludeDeleted(): bool
    {
        return $this->includeDeleted;
    }

    /**
     * @return bool
     */
    public function hasIncludeDeleted(): bool
    {
        return !is_null($this->includeDeleted);
    }

    /**
     * @param bool $includeDeleted
     *
     * @return ListAll
     */
    public function setIncludeDeleted(bool $includeDeleted): ListAll
    {
        $this->includeDeleted = $includeDeleted;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        $result = ['nextOffset' => $this->result->nextOffset(), 'list' => []];
        /** @var ChargeBee_Customer $entry */
        foreach ($this->result as $entry) {
            /** @var ChargeBee_Customer $customer */
            $customer = $entry->customer();
            /** @var \ChargeBee_Card $card */
            $card = $entry->card();

            $result['list'][] = [
                'customer' => $customer->getValues(),
                'card' => empty($card) ? null : $card->getValues(),
            ];
        }

        return $result;
    }
}
