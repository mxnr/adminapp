<?php

namespace ChargeBeeBundle\Packet\Customers;

use ChargeBee_Customer;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class Update extends Create
{
    use AttributeHelperTrait;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * fraudFlag
     * Indicates whether or not the customer has been identified as fraudulent.
     * optional, enumerated string
     * Possible values are
     * safe
     * The customer has been marked as safe.
     * fraudulent
     * The customer has been marked as fraudulent.
     *
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"safe","fraudulent"})
     */
    private $fraudFlag;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $customerId = $this->hasId() ? $this->getId() : null;
        $this->result = ChargeBee_Customer::update($customerId, $this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Update
     */
    public function setId(string $id): Create
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'id',
                'firstName',
                'lastName',
                'email',
                'preferredCurrencyCode',
                'phone',
                'company',
                'autoCollection',
                'netTermDays',
                [
                    'has' => 'hasAllowDirectDebit',
                    'get' => 'isAllowDirectDebit',
                ],
                'vatNumber',
                [
                    'has' => 'hasRegisteredForGst',
                    'get' => 'isRegisteredForGst',
                ],
                'taxability',
                'locale',
                'entityCode',
                'exemptNumber',
                'metaData',
                [
                    'has' => 'hasConsolidatedInvoicing',
                    'get' => 'isConsolidatedInvoicing',
                ],
                'invoiceNotes',
                'fraudFlag',
            ]
        );
    }

    /**
     * @return string
     */
    public function getFraudFlag(): string
    {
        return $this->fraudFlag;
    }

    /**
     * @return bool
     */
    public function hasFraudFlag(): bool
    {
        return !is_null($this->fraudFlag);
    }

    /**
     * @param string $fraudFlag
     *
     * @return Update
     */
    public function setFraudFlag(string $fraudFlag): Update
    {
        $this->fraudFlag = $fraudFlag;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var \ChargeBee_Card $card */
        $card = $this->result->card();

        return [
            'customer' => $customer->getValues(),
            'card' => empty($card) ? null : $card->getValues(),
        ];
    }
}
