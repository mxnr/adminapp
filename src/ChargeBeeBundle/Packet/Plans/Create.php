<?php

namespace ChargeBeeBundle\Packet\Plans;

use ChargeBee_Plan;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Create.
 */
class Create extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * id
     * A unique ID for your system to identify the plan.
     * string, max chars=100.
     *
     * @var string
     * @Assert\Type(type="string")s
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     */
    private $id;

    /**
     * name
     * The display name used in web interface for identifying the plan.
     * string, max chars=50.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     */
    private $name;

    /**
     * invoiceName
     * Display name used in invoice. If it is not configured then name is used in invoice.
     * optional, string, max chars=100
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="100")
     */
    private $invoiceName;

    /**
     * description
     * Description about the plan to show in the customer portal when users change their subscription.
     * optional, string, max chars=500
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max=500)
     */
    private $description;

    /**
     * price
     * The price of the plan.
     * in cents, default=0, min=0
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="0")
     */
    private $price = 0;

    /**
     * currencyCode
     * The currency code (ISO 4217 format) of the plan.
     * string, max chars=3
     *
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(min="3", max="3")
     */
    private $currencyCode;

    /**
     * period
     * Defines billing frequency. Example: to bill customer every 3 months, provide "3" here.
     * integer, default=1, min=1
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $period = 1;

    /**
     * periodUnit
     * Defines billing frequency in association with billing period.
     * enumerated string, default=month
     * Possible values are
     * week
     * Charge based on week(s).
     * month
     * Charge based on month(s).
     * year
     * Charge based on year(s).
     *
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"week","month","year"})
     */
    private $periodUnit = 'month';

    /**
     * trialPeriod
     * The free time window for your customer to try your product.
     * optional, integer
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $trialPeriod;

    /**
     * trialPeriodUnit
     * Time unit for the trial period.
     * optional, enumerated string
     * Possible values are
     * day
     * In days.
     * month
     * In months.
     *
     * @var string
     * @Assert\Choice(choices={"day","month"})
     */
    private $trialPeriodUnit;

    /**
     * chargeModel
     * Defines how the subscription recurring charge for this plan should be calculated.
     * enumerated string, default=flat_fee
     * Possible values are
     * flat_fee
     * Charge a single price on recurring basis.
     * per_unit
     * Charge the price for each unit of the plan for the subscription on recurring basis.
     *
     * @var string
     * @Assert\Choice(choices={"flat_fee","per_unit"})
     * @Assert\NotBlank()
     */
    private $chargeModel = 'flat_fee';

    /**
     * freeQuantity
     * Free quantity the subscriptions of this plan will have.
     * Only the quantity more than this will be charged for the subscription.
     * integer, default=0, min=0
     *
     * @var int
     * @Assert\Range(min="0")
     * @Assert\Type(type="integer")
     */
    private $freeQuantity = 0;

    /**
     * setupCost
     * One-time setup fee charged as part of the first invoice.
     * optional, in cents, min=1
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $setupCost;

    /**
     * status
     * The plan state. enumerated
     * string, default=active
     * Possible values are
     * active New subscriptions can be created with the plan.
     * archived No new subscriptions allowed for the plan Existing subscriptions on this plan will remain as-is
     *      and can be migrated to another active plan if required.
     * deleted Indicates the plan has been deleted.
     *
     * @var string
     * @Assert\Choice(choices={"active", "archived", "deleted"})
     */
    private $status = 'active';

    /**
     * archivedAt
     * Time at which the plan was moved to archived status. optional,
     * timestamp(UTC) in seconds
     *
     * @var int
     * @Assert\Type(type="integer")
     */
    private $archivedAt;

    /**
     * billingCycles The number of billing cycles the subscription is active.
     * The subscription is moved to non renewing state and then to cancelled state automatically.
     * optional, integer, min=1
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $billingCycles = 1;

    /**
     * redirectUrl
     * The url to redirect on successful checkout. Eg: https://yoursite.com/success.html?plan=basic.
     * optional, string, max chars=500
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="500")
     */
    private $redirectUrl;

    /**
     * enabledInHostedPages
     * If true, allow checkout through plan specific hosted page URL for this plan.
     * boolean, default=true
     *
     * @var bool
     * @Assert\Type(type="boolean")
     */
    private $enabledInHostedPages = true;

    /**
     * enabledInPortal
     * If enabled, customers can switch to this plan using the
     * 'Change Subscription' option in the customer portal.
     * boolean, default=true
     *
     * @var bool
     * @Assert\Type(type="boolean")
     */
    private $enabledInPortal = true;

    /**
     * taxCode
     * The Avalara tax codes to which items are mapped to should be provided here.
     * Applicable only for Avalara.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $taxCode;

    /**
     * sku
     * The field is used as Product name/code in your third party accounting application.
     * Chargebee will use it as an alternate name in your accounting application.
     * optional, string, max chars=100
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="100")
     */
    private $sku;

    /**
     * accountingCode
     * This field is to capture the Account code setup in your Accounting system for integration purposes only.
     * optional, string, max chars=100
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="100")
     */
    private $accountingCode;

    /**
     * accountingCategory1
     * The name of the category of your product in Xero.
     * If you've integrated with QuickBooks, this will be the "Class".
     * Use the format "<Category>:<Name>". E.g. "Region: North". optional,
     * string, max chars=100
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="100")
     */
    private $accountingCategory1;

    /**
     * accountingCategory2
     * The name of the category of your product in Xero.
     * Use the format<Category>:<Name>". E.g. "Region: North".
     * optional, string, max chars=100
     *
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="100")
     */
    private $accountingCategory2;

    /**
     * resourceVersion
     * Version number of this resource.
     * Each update of this resource results in incremental change of this number.
     * This attribute will be present only if the resource has been updated after 2016-09-28.
     * optional, long
     *
     * @var int
     * @Assert\Type(type="integer")
     */
    private $resourceVersion;

    /**
     * updatedAt
     * Timestamp indicating when this plan was last updated.
     * This attribute will be present only if the resource has been updated after 2016-11-09.
     * optional, timestamp(UTC) in seconds
     *
     * @var int
     * @Assert\Type(type="integer")
     */
    private $updatedAt;

    /**
     * invoiceNotes
     * Invoice Notes for this resource. Read More.
     * optional, string, max chars=1000
     *
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="1000")
     */
    private $invoiceNotes;

    /**
     * taxable
     * Specifies if the plan should be taxed or not.
     * optional, boolean, default=true
     *
     * @var bool
     * @Assert\Type(type="boolean")
     */
    private $taxable = true;

    /**
     * taxProfileId
     * Tax profile of the plan. optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $taxProfileId;

    /**
     * metaData
     *
     * Additional data about
     * this resource can be stored here in the JSON Format. Learn more.
     * optional, jsonobject.
     *
     * @var string
     * @Assert\Type(type="json")
     */
    private $metaData;

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Plan $plan */
        $plan = $this->result->plan();

        $planAsArray = $plan->getValues();
        if (!is_array($planAsArray)) {
            throw  new \RuntimeException('got unexpected response from api');
        }

        return $planAsArray;
    }

    /**
     * @return PacketMethodInterface
     */
    public function execute(): PacketMethodInterface
    {
        $this->result = ChargeBee_Plan::create($this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'period',
                'currencyCode',
                'price',
                'description',
                'invoiceName',
                'name',
                'id',
                'metaData',
                'taxProfileId',
                [
                    'has' => 'hasTaxable',
                    'get' => 'isTaxable',
                ],
                'invoiceNotes',
                'updatedAt',
                'resourceVersion',
                'accountingCategory2',
                'accountingCategory1',
                'accountingCode',
                'sku',
                'taxCode',
                [
                    'get' => 'isEnabledInPortal',
                    'has' => 'hasEnabledInPortal',
                ],
                [
                    'get' => 'isEnabledInHostedPages',
                    'has' => 'hasEnabledInHostedPages',
                ],
                'redirectUrl',
                'billingCycles',
                'archivedAt',
                'status',
                'setupCost',
                'freeQuantity',
                'chargeModel',
                'trialPeriodUnit',
                'trialPeriod',
                'periodUnit',
            ]
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Create
     */
    public function setId(string $id): Create
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function hasName(): bool
    {
        return !is_null($this->name);
    }

    /**
     * @param string $name
     *
     * @return Create
     */
    public function setName(string $name): Create
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceName(): string
    {
        return $this->invoiceName;
    }

    /**
     * @return bool
     */
    public function hasInvoiceName(): bool
    {
        return !is_null($this->invoiceName);
    }

    /**
     * @param string $invoiceName
     *
     * @return Create
     */
    public function setInvoiceName(string $invoiceName): Create
    {
        $this->invoiceName = $invoiceName;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function hasDescription(): bool
    {
        return !is_null($this->description);
    }

    /**
     * @param string $description
     *
     * @return Create
     */
    public function setDescription(string $description): Create
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return bool
     */
    public function hasPrice(): bool
    {
        return !is_null($this->price);
    }

    /**
     * @param int $price
     *
     * @return Create
     */
    public function setPrice(int $price): Create
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyCode(): string
    {
        return $this->currencyCode;
    }

    /**
     * @return bool
     */
    public function hasCurrencyCode(): bool
    {
        return !is_null($this->currencyCode);
    }

    /**
     * @param string $currencyCode
     *
     * @return Create
     */
    public function setCurrencyCode(string $currencyCode): Create
    {
        $this->currencyCode = $currencyCode;

        return $this;
    }

    /**
     * @return int
     */
    public function getPeriod(): int
    {
        return $this->period;
    }

    /**
     * @return bool
     */
    public function hasPeriod(): bool
    {
        return !is_null($this->period);
    }

    /**
     * @param int $period
     *
     * @return Create
     */
    public function setPeriod(int $period): Create
    {
        $this->period = $period;

        return $this;
    }

    /**
     * @return string
     */
    public function getPeriodUnit(): string
    {
        return $this->periodUnit;
    }

    /**
     * @return bool
     */
    public function hasPeriodUnit(): bool
    {
        return !is_null($this->periodUnit);
    }

    /**
     * @param string $periodUnit
     *
     * @return Create
     */
    public function setPeriodUnit(string $periodUnit): Create
    {
        $this->periodUnit = $periodUnit;

        return $this;
    }

    /**
     * @return int
     */
    public function getTrialPeriod(): int
    {
        return $this->trialPeriod;
    }

    /**
     * @return bool
     */
    public function hasTrialPeriod(): bool
    {
        return !is_null($this->trialPeriod);
    }

    /**
     * @param int $trialPeriod
     *
     * @return Create
     */
    public function setTrialPeriod(int $trialPeriod): Create
    {
        $this->trialPeriod = $trialPeriod;

        return $this;
    }

    /**
     * @return string
     */
    public function getTrialPeriodUnit(): string
    {
        return $this->trialPeriodUnit;
    }

    /**
     * @return bool
     */
    public function hasTrialPeriodUnit(): bool
    {
        return !is_null($this->trialPeriodUnit);
    }

    /**
     * @param string $trialPeriodUnit
     *
     * @return Create
     */
    public function setTrialPeriodUnit(string $trialPeriodUnit): Create
    {
        $this->trialPeriodUnit = $trialPeriodUnit;

        return $this;
    }

    /**
     * @return string
     */
    public function getChargeModel(): string
    {
        return $this->chargeModel;
    }

    /**
     * @return bool
     */
    public function hasChargeModel(): bool
    {
        return !is_null($this->chargeModel);
    }

    /**
     * @param string $chargeModel
     *
     * @return Create
     */
    public function setChargeModel(string $chargeModel): Create
    {
        $this->chargeModel = $chargeModel;

        return $this;
    }

    /**
     * @return int
     */
    public function getFreeQuantity(): int
    {
        return $this->freeQuantity;
    }

    /**
     * @return bool
     */
    public function hasFreeQuantity(): bool
    {
        return !is_null($this->freeQuantity) || $this->freeQuantity === 0;
    }

    /**
     * @param int $freeQuantity
     *
     * @return Create
     */
    public function setFreeQuantity(int $freeQuantity): Create
    {
        $this->freeQuantity = $freeQuantity;

        return $this;
    }

    /**
     * @return int
     */
    public function getSetupCost(): int
    {
        return $this->setupCost;
    }

    /**
     * @return bool
     */
    public function hasSetupCost(): bool
    {
        return !is_null($this->setupCost);
    }

    /**
     * @param int $setupCost
     *
     * @return Create
     */
    public function setSetupCost(int $setupCost): Create
    {
        $this->setupCost = $setupCost;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function hasStatus(): bool
    {
        return !is_null($this->status);
    }

    /**
     * @param string $status
     *
     * @return Create
     */
    public function setStatus(string $status): Create
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getArchivedAt(): int
    {
        return $this->archivedAt;
    }

    /**
     * @return bool
     */
    public function hasArchivedAt(): bool
    {
        return !is_null($this->archivedAt);
    }

    /**
     * @param int $archivedAt
     *
     * @return Create
     */
    public function setArchivedAt(int $archivedAt): Create
    {
        $this->archivedAt = $archivedAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getBillingCycles(): int
    {
        return $this->billingCycles;
    }

    /**
     * @return bool
     */
    public function hasBillingCycles(): bool
    {
        return !is_null($this->billingCycles);
    }

    /**
     * @param int $billingCycles
     *
     * @return Create
     */
    public function setBillingCycles(int $billingCycles): Create
    {
        $this->billingCycles = $billingCycles;

        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }

    /**
     * @return bool
     */
    public function hasRedirectUrl(): bool
    {
        return !is_null($this->redirectUrl);
    }

    /**
     * @param string $redirectUrl
     *
     * @return Create
     */
    public function setRedirectUrl(string $redirectUrl): Create
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabledInHostedPages(): bool
    {
        return $this->enabledInHostedPages;
    }

    /**
     * @return bool
     */
    public function hasEnabledInHostedPages(): bool
    {
        return !is_null($this->enabledInHostedPages);
    }

    /**
     * @param bool $enabledInHostedPages
     *
     * @return Create
     */
    public function setEnabledInHostedPages(bool $enabledInHostedPages): Create
    {
        $this->enabledInHostedPages = $enabledInHostedPages;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabledInPortal(): bool
    {
        return $this->enabledInPortal;
    }

    /**
     * @return bool
     */
    public function hasEnabledInPortal(): bool
    {
        return !is_null($this->enabledInPortal);
    }

    /**
     * @param bool $enabledInPortal
     *
     * @return Create
     */
    public function setEnabledInPortal(bool $enabledInPortal): Create
    {
        $this->enabledInPortal = $enabledInPortal;

        return $this;
    }

    /**
     * @return string
     */
    public function getTaxCode(): string
    {
        return $this->taxCode;
    }

    /**
     * @return bool
     */
    public function hasTaxCode(): bool
    {
        return !is_null($this->taxCode);
    }

    /**
     * @param string $taxCode
     *
     * @return Create
     */
    public function setTaxCode(string $taxCode): Create
    {
        $this->taxCode = $taxCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @return bool
     */
    public function hasSku(): bool
    {
        return !is_null($this->sku);
    }

    /**
     * @param string $sku
     *
     * @return Create
     */
    public function setSku(string $sku): Create
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccountingCode(): string
    {
        return $this->accountingCode;
    }

    /**
     * @return bool
     */
    public function hasAccountingCode(): bool
    {
        return !is_null($this->accountingCode);
    }

    /**
     * @param string $accountingCode
     *
     * @return Create
     */
    public function setAccountingCode(string $accountingCode): Create
    {
        $this->accountingCode = $accountingCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccountingCategory1(): string
    {
        return $this->accountingCategory1;
    }

    /**
     * @return bool
     */
    public function hasAccountingCategory1(): bool
    {
        return !is_null($this->accountingCategory1);
    }

    /**
     * @param string $accountingCategory1
     *
     * @return Create
     */
    public function setAccountingCategory1(string $accountingCategory1): Create
    {
        $this->accountingCategory1 = $accountingCategory1;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccountingCategory2(): string
    {
        return $this->accountingCategory2;
    }

    /**
     * @return bool
     */
    public function hasAccountingCategory2(): bool
    {
        return !is_null($this->accountingCategory2);
    }

    /**
     * @param string $accountingCategory2
     *
     * @return Create
     */
    public function setAccountingCategory2(string $accountingCategory2): Create
    {
        $this->accountingCategory2 = $accountingCategory2;

        return $this;
    }

    /**
     * @return int
     */
    public function getResourceVersion(): int
    {
        return $this->resourceVersion;
    }

    /**
     * @return bool
     */
    public function hasResourceVersion(): bool
    {
        return !is_null($this->resourceVersion);
    }

    /**
     * @param int $resourceVersion
     *
     * @return Create
     */
    public function setResourceVersion(int $resourceVersion): Create
    {
        $this->resourceVersion = $resourceVersion;

        return $this;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): int
    {
        return $this->updatedAt;
    }

    /**
     * @return bool
     */
    public function hasUpdatedAt(): bool
    {
        return !is_null($this->updatedAt);
    }

    /**
     * @param int $updatedAt
     *
     * @return Create
     */
    public function setUpdatedAt(int $updatedAt): Create
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNotes(): string
    {
        return $this->invoiceNotes;
    }

    /**
     * @return bool
     */
    public function hasInvoiceNotes(): bool
    {
        return !is_null($this->invoiceNotes);
    }

    /**
     * @param string $invoiceNotes
     *
     * @return Create
     */
    public function setInvoiceNotes(string $invoiceNotes): Create
    {
        $this->invoiceNotes = $invoiceNotes;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTaxable(): bool
    {
        return $this->taxable;
    }

    /**
     * @return bool
     */
    public function hasTaxable(): bool
    {
        return !is_null($this->taxable);
    }

    /**
     * @param bool $taxable
     *
     * @return Create
     */
    public function setTaxable(bool $taxable): Create
    {
        $this->taxable = $taxable;

        return $this;
    }

    /**
     * @return string
     */
    public function getTaxProfileId(): string
    {
        return $this->taxProfileId;
    }

    /**
     * @return bool
     */
    public function hasTaxProfileId(): bool
    {
        return !is_null($this->taxProfileId);
    }

    /**
     * @param string $taxProfileId
     *
     * @return Create
     */
    public function setTaxProfileId(string $taxProfileId): Create
    {
        $this->taxProfileId = $taxProfileId;

        return $this;
    }

    /**
     * @return string
     */
    public function getMetaData(): string
    {
        return $this->metaData;
    }

    /**
     * @return bool
     */
    public function hasMetaData(): bool
    {
        return !is_null($this->metaData);
    }

    /**
     * @param string $metaData
     *
     * @return Create
     */
    public function setMetaData(string $metaData): Create
    {
        $this->metaData = $metaData;

        return $this;
    }
}
