<?php

namespace ChargeBeeBundle\Packet\Plans;

use ChargeBee_Plan;
use ChargeBeeBundle\Contract\PacketMethodInterface;

/**
 * Class Update.
 */
class Update extends Create
{
    /**
     * @return PacketMethodInterface
     */
    public function execute(): PacketMethodInterface
    {
        $planId = $this->hasId() ? $this->getId() : null;
        $this->result = ChargeBee_Plan::update($planId, $this->toParams(), $this->environment);

        return $this;
    }
}
