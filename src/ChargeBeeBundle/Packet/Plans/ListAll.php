<?php

namespace ChargeBeeBundle\Packet\Plans;

use ChargeBee_Plan;
use ChargeBee_Result;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ListAll.
 */
class ListAll extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * @var \ChargeBee_ListResult
     */
    protected $result;

    /**
     *
     * limit
     * Limits the number of resources to be returned.
     * optional, integer, default=10, min=1, max=100
     *
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1", max="100")
     */
    private $limit = 10;

    /**
     * offset
     * Allows you to fetch the next set of resources. The value used for this parameter must be the value returned for
     * next_offset parameter in the previous API call.
     * optional, string, max chars=1000 Filter Params
     *
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="1000")
     */
    private $offset;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","startsWith","in","notIn"})
     */
    private $idOperator;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","startsWith","in","notIn"})
     */
    private $nameOperator;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","lt","lte","qt","qte","between"})
     */
    private $priceOperator;

    /**
     * @var int
     */
    private $price;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","lt","lte","qt","qte","between"})
     */
    private $periodOperator;

    /**
     * @var int
     *
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $period;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","in","notIn"})
     */
    private $periodUnitOperator;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"week","month","year"})
     */
    private $periodUnit;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","lt","lte","qt","qte","between","isPresent"})
     */
    private $trialPeriodOperator;

    /**
     * @var int
     *
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $trialPeriod;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","in","notIn"})
     */
    private $trialPeriodUnitOperator;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"month","day"})
     */
    private $trialPeriodUnit;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","in","notIn"})
     */
    private $chargeModelOperator;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"flat_fee","per_unit"})
     */
    private $chargeModel;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","in","notIn"})
     */
    private $statusOperator;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"active","archived","deleted"})
     */
    private $status;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"after","before","on","between"})
     */
    private $updatedAtOperator;

    /**
     * @var string
     *
     * @Assert\Type(type="integer")
     */
    private $updatedAt;

    /**
     * @param string $operator
     * @param array  $id
     *
     * @return ListAll
     */
    public function setIdFilter(string $operator, array $id): ListAll
    {
        $this->idOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->id = $id;
        } else {
            $this->id = array_pop($id);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $name
     *
     * @return ListAll
     */
    public function setNameFilter(string $operator, array $name): ListAll
    {
        $this->nameOperator = $operator;
        if (in_array($operator, ['in', 'noIn'])) {
            $this->name = $name;
        } else {
            $this->name = array_pop($name);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $price
     *
     * @return ListAll
     */
    public function setPriceFilter(string $operator, array $price): ListAll
    {
        $this->priceOperator = $operator;
        if (in_array($operator, ['between'])) {
            $this->price = $price;
        } else {
            $this->price = array_pop($price);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $period
     *
     * @return ListAll
     */
    public function setPeriodFilter(string $operator, array $period): ListAll
    {
        $this->periodOperator = $operator;
        if (in_array($operator, ["between"])) {
            $this->period = $period;
        } else {
            $this->period = array_pop($period);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $periodUnit
     *
     * @return ListAll
     */
    public function setPeriodUnitFilter(string $operator, array $periodUnit): ListAll
    {
        $this->periodUnitOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->periodUnit = $periodUnit;
        } else {
            $this->periodUnit = array_pop($periodUnit);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $trialPeriod
     *
     * @return ListAll
     */
    public function setTrialPeriod(string $operator, array $trialPeriod): ListAll
    {
        $this->trialPeriodOperator = $operator;
        if (in_array($operator, ["between"])) {
            $this->trialPeriod = $trialPeriod;
        } else {
            $this->trialPeriod = array_pop($trialPeriod);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $trialPeriodUnit
     *
     * @return ListAll
     */
    public function setTrialPeriodUnitFilter(string $operator, array $trialPeriodUnit): ListAll
    {
        $this->trialPeriodUnitOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->trialPeriodUnit = $trialPeriodUnit;
        } else {
            $this->trialPeriodUnit = array_pop($trialPeriodUnit);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $chargeModel
     *
     * @return ListAll
     */
    public function setChargeModelFilter(string $operator, array $chargeModel): ListAll
    {
        $this->chargeModelOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->chargeModel = $chargeModel;
        } else {
            $this->chargeModel = array_pop($chargeModel);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $status
     *
     * @return ListAll
     */
    public function setStatusFilter(string $operator, array $status): ListAll
    {
        $this->statusOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->status = $status;
        } else {
            $this->status = array_pop($status);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param int    $updatedAt
     *
     * @return ListAll
     */
    public function setUpdatedAtFilter(string $operator, int $updatedAt): ListAll
    {
        $this->updatedAtOperator = $operator;
        if (in_array($operator, ["between"])) {
            $this->updatedAt = $updatedAt;
        } else {
            $this->updatedAt = array_pop($updatedAt);
        }

        return $this;
    }

    /**
     * @return PacketMethodInterface
     */
    public function execute(): PacketMethodInterface
    {
        $this->result = ChargeBee_Plan::all(
            $this->getAttributes(),
            $this->environment
        );

        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        $attributes = $this->getAttributesByMap(['limit', 'offset']);

        $attributes = $this->getAttributesByMap(
            [
                'idFilter',
                'nameFilter',
                'priceFilter',
                'periodFilter',
                'periodUnitFilter',
                'trialPeriodFilter',
                'trialPeriodUnitFilter',
                'chargeModelFilter',
                'statusFilter',
                'updatedAtFilter',
            ],
            $attributes,
            true
        );

        return $attributes;
    }

    /**
     * @return bool
     */
    public function hasPeriodUnitFilter(): bool
    {
        return !is_null($this->periodUnit);
    }

    /**
     * @return array
     */
    public function getPeriodUnitFilter(): array
    {
        return ['periodUnit[' . $this->periodUnitOperator . ']' => $this->periodUnit];
    }

    /**
     * @return bool
     */
    public function hasTrialPeriodFilter(): bool
    {
        return !is_null($this->trialPeriod);
    }

    /**
     * @return array
     */
    public function getTrialPeriodFilter(): array
    {
        return ['trialPeriod[' . $this->trialPeriodOperator . ']' => $this->trialPeriod];
    }

    /**
     * @return bool
     */
    public function hasTrialPeriodUnitFilter(): bool
    {
        return !is_null($this->trialPeriodUnit);
    }

    /**
     * @return array
     */
    public function getTrialPeriodUnitFilter(): array
    {
        return ['trialPeriodUnit[' . $this->trialPeriodUnitOperator . ']' => $this->trialPeriodUnit];
    }

    /**
     * @return bool
     */
    public function hasChargeModelFilter(): bool
    {
        return !is_null($this->chargeModel);
    }

    /**
     * @return array
     */
    public function getChargeModelFilter(): array
    {
        return ['chargeModel[' . $this->chargeModelOperator . ']' => $this->chargeModel];
    }

    /**
     * @return bool
     */
    public function hasStatusFilter(): bool
    {
        return !is_null($this->status);
    }

    /**
     * @return array
     */
    public function getStatusFilter(): array
    {
        return ['status[' . $this->statusOperator . ']' => $this->status];
    }

    /**
     * @return bool
     */
    public function hasUpdatedAtFilter(): bool
    {
        return !is_null($this->updatedAt);
    }

    /**
     * @return array
     */
    public function getUpdatedAtFilter(): array
    {
        return ['updatedAt[' . $this->updatedAtOperator . ']' => $this->updatedAt];
    }

    /**
     * @return bool
     */
    public function hasIdFilter(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @return array
     */
    public function getIdFilter(): array
    {
        return ['id[' . $this->idOperator . ']' => $this->id];
    }

    /**
     * @return bool
     */
    public function hasNameFilter(): bool
    {
        return !is_null($this->name);
    }

    /**
     * @return array
     */
    public function getNameFilter(): array
    {
        return ['name[' . $this->nameOperator . ']' => $this->name];
    }

    /**
     * @return bool
     */
    public function hasPriceFilter(): bool
    {
        return !is_null($this->price);
    }

    /**
     * @return array
     */
    public function getPriceFilter(): array
    {
        return ['price[' . $this->price . ']' => $this->price];
    }

    /**
     * @return bool
     */
    public function hasPeriodFilter(): bool
    {
        return !is_null($this->period);
    }

    /**
     * @return array
     */
    public function getPeriodFilter(): array
    {
        return ['period[' . $this->periodOperator . ']' => $this->period];
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return bool
     */
    public function hasLimit(): bool
    {
        return !is_null($this->limit);
    }

    /**
     * @param int $limit
     *
     * @return ListAll
     */
    public function setLimit(int $limit): ListAll
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return string
     */
    public function getOffset(): string
    {
        return $this->offset;
    }

    /**
     * @return bool
     */
    public function hasOffset(): bool
    {
        return !is_null($this->offset);
    }

    /**
     * @param string $offset
     *
     * @return ListAll
     */
    public function setOffset(string $offset): ListAll
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return array
     */
    public function getResult(): array
    {
        $result = ['nextOffset' => $this->result->nextOffset(), 'list' => []];

        /** @var ChargeBee_Result $entry */
        foreach ($this->result as $entry) {
            /** @var ChargeBee_Plan $plan */
            $plan = $entry->plan();
            $result['list'][] = $plan->getValues();
        }

        return $result;
    }
}
