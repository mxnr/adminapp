<?php

namespace ChargeBeeBundle\Packet\Plans;

use ChargeBee_Plan;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Delete.
 */
class Delete extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * id
     * A unique ID for your system to identify the plan.
     * string, max chars=100.
     *
     * @var string
     * @Assert\Type(type="string")s
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     */
    private $id;

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(['id']);
    }

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $this->result = ChargeBee_Plan::delete($this->hasId() ? $this->getId() : null, $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Delete
     */
    public function setId(string $id): Delete
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Plan $plan */
        $plan = $this->result->plan();

        $planAsArray = $plan->getValues();
        if (!is_array($planAsArray)) {
            throw  new \RuntimeException('got unexpected response from api');
        }

        return $planAsArray;
    }
}
