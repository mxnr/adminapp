<?php

namespace ChargeBeeBundle\Packet;

use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Service\EnvironmentService;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class Base.
 */
abstract class BasePacket
{
    /**
     * @var EnvironmentService
     */
    protected $environmentService;

    /**
     * @var ValidatorInterface
     */
    protected $validatorService;

    /**
     * BasePacket constructor.
     *
     * @param EnvironmentService $environmentService
     * @param ValidatorInterface $validatorService
     */
    public function __construct(EnvironmentService $environmentService, ValidatorInterface $validatorService)
    {
        $this->environmentService = $environmentService;
        $this->validatorService = $validatorService;
    }

    /**
     * @return EnvironmentService
     */
    public function getEnvironmentService(): EnvironmentService
    {
        return $this->environmentService;
    }

    /**
     * @return bool
     */
    public function hasEnvironmentService(): bool
    {
        return !is_null($this->environmentService);
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidatorService(): ValidatorInterface
    {
        return $this->validatorService;
    }

    /**
     * @return bool
     */
    public function hasValidatorService(): bool
    {
        return !is_null($this->validatorService);
    }

    /**
     * @param PacketMethodInterface $packetMethod
     *
     * @return array
     */
    protected function processPacketMethod(PacketMethodInterface $packetMethod): array
    {
        $this->validatePacketMethodAndThrow($packetMethod);

        return $packetMethod->setEnvironment($this->environmentService)->execute()->getResult();
    }

    /**
     * @param PacketMethodInterface $packetMethod
     *
     * @return void
     *
     * @throws ValidatorException
     */
    protected function validatePacketMethodAndThrow(PacketMethodInterface $packetMethod)
    {
        $constraintViolations = $this->validatorService->validate($packetMethod);
        if ($constraintViolations->count() > 0) {
            throw new ValidatorException((string) $constraintViolations);
        }
    }
}
