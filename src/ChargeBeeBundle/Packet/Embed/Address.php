<?php

namespace ChargeBeeBundle\Packet\Embed;

use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class Address implements PacketMethodAttributesInteface
{
    use AttributeHelperTrait;

    /**
     * firstName First name.
     * optional, string, max chars=150
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $firstName;

    /**
     * lastName
     * Last name.
     * optional, string, max chars=150
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $lastName;

    /**
     * email
     * Email.
     * optional, string, max chars=70
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="70")
     */
    private $email;

    /**
     * company
     * Company name.
     * optional, string, max chars=250
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="250")
     */
    private $company;

    /**
     * phone
     * Phone number.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $phone;

    /**
     * line1
     * Address line 1.
     * optional, string, max chars=150
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $line1;

    /**
     * line2
     * Address line 2.
     * optional, string, max chars=150
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $line2;

    /**
     * line3
     * Address line 3.
     * optional, string, max chars=150
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="150")
     */
    private $line3;

    /**
     * city
     * City.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $city;

    /**
     * stateCode
     * The ISO 3166-2 state/province code. The recommended way of passing the state/province information. Supported for
     * US, Canada and India now. Further if this is specified, 'state' attribute should not be specified as it will be
     * set automatically. optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $stateCode;

    /**
     * state
     * The state/province name. Use this to pass the state/province information for cases where 'state_code' is not
     * supported or cannot be passed. optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $state;

    /**
     * zip
     * Zip or Postal code.
     * optional, string, max chars=20
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="20")
     */
    private $zip;

    /**
     * country
     * 2-letter ISO 3166 alpha-2 country code.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     * @Assert\Country()
     */
    private $country;

    /**
     * validationStatus
     * The address verification status.
     * optional, enumerated string, default=not_validated
     * Possible values are
     * not_validated
     * Address is not yet validated.
     * valid
     * Address was validated successfully.
     * partially_valid
     * Address is verified but only partially.
     * invalid
     * Address is invalid.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"not_validated","valid","partially_valid","invalid"})
     */
    private $validationStatus = 'not_validated';

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'validationStatus',
                'country',
                'zip',
                'state',
                'stateCode',
                'city',
                'line3',
                'line2',
                'line1',
                'phone',
                'company',
                'email',
                'lastName',
                'firstName',
            ]
        );
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return bool
     */
    public function hasFirstName(): bool
    {
        return !is_null($this->firstName);
    }

    /**
     * @param string $firstName
     *
     * @return Address
     */
    public function setFirstName(string $firstName): Address
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return bool
     */
    public function hasLastName(): bool
    {
        return !is_null($this->lastName);
    }

    /**
     * @param string $lastName
     *
     * @return Address
     */
    public function setLastName(string $lastName): Address
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !is_null($this->email);
    }

    /**
     * @param string $email
     *
     * @return Address
     */
    public function setEmail(string $email): Address
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @return bool
     */
    public function hasCompany(): bool
    {
        return !is_null($this->company);
    }

    /**
     * @param string $company
     *
     * @return Address
     */
    public function setCompany(string $company): Address
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return bool
     */
    public function hasPhone(): bool
    {
        return !is_null($this->phone);
    }

    /**
     * @param string $phone
     *
     * @return Address
     */
    public function setPhone(string $phone): Address
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getLine1(): string
    {
        return $this->line1;
    }

    /**
     * @return bool
     */
    public function hasLine1(): bool
    {
        return !is_null($this->line1);
    }

    /**
     * @param string $line1
     *
     * @return Address
     */
    public function setLine1(string $line1): Address
    {
        $this->line1 = $line1;

        return $this;
    }

    /**
     * @return string
     */
    public function getLine2(): string
    {
        return $this->line2;
    }

    /**
     * @return bool
     */
    public function hasLine2(): bool
    {
        return !is_null($this->line2);
    }

    /**
     * @param string $line2
     *
     * @return Address
     */
    public function setLine2(string $line2): Address
    {
        $this->line2 = $line2;

        return $this;
    }

    /**
     * @return string
     */
    public function getLine3(): string
    {
        return $this->line3;
    }

    /**
     * @return bool
     */
    public function hasLine3(): bool
    {
        return !is_null($this->line3);
    }

    /**
     * @param string $line3
     *
     * @return Address
     */
    public function setLine3(string $line3): Address
    {
        $this->line3 = $line3;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return bool
     */
    public function hasCity(): bool
    {
        return !is_null($this->city);
    }

    /**
     * @param string $city
     *
     * @return Address
     */
    public function setCity(string $city): Address
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getStateCode(): string
    {
        return $this->stateCode;
    }

    /**
     * @return bool
     */
    public function hasStateCode(): bool
    {
        return !is_null($this->stateCode);
    }

    /**
     * @param string $stateCode
     *
     * @return Address
     */
    public function setStateCode(string $stateCode): Address
    {
        $this->stateCode = $stateCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @return bool
     */
    public function hasState(): bool
    {
        return !is_null($this->state);
    }

    /**
     * @param string $state
     *
     * @return Address
     */
    public function setState(string $state): Address
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @return bool
     */
    public function hasZip(): bool
    {
        return !is_null($this->zip);
    }

    /**
     * @param string $zip
     *
     * @return Address
     */
    public function setZip(string $zip): Address
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return bool
     */
    public function hasCountry(): bool
    {
        return !is_null($this->country);
    }

    /**
     * @param string $country
     *
     * @return Address
     */
    public function setCountry(string $country): Address
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getValidationStatus(): string
    {
        return $this->validationStatus;
    }

    /**
     * @return bool
     */
    public function hasValidationStatus(): bool
    {
        return !is_null($this->validationStatus);
    }

    /**
     * @param string $validationStatus
     *
     * @return Address
     */
    public function setValidationStatus(string $validationStatus): Address
    {
        $this->validationStatus = $validationStatus;

        return $this;
    }
}
