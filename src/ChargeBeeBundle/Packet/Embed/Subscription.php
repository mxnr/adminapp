<?php

namespace ChargeBeeBundle\Packet\Embed;

use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * {@inheritDoc}
 */
class Subscription implements PacketMethodAttributesInteface
{
    use AttributeHelperTrait;

    /**
     * @var string
     *
     * @Assert\Length(max="50")
     * @Assert\Type(type="string")
     */
    private $coupon;

    /**
     * id
     * Id for the new subscription. If not given, this will be auto-generated.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * planId
     * Identifier of the plan for this subscription.
     * required, string, max chars=100
     *
     * @var string
     * @Assert\Length(max="100")
     */
    private $planId;

    /**
     * planQuantity
     * Plan quantity for this subscription.
     * optional, integer, default=1, min=1
     *
     * @var int
     * @Assert\Range(max="1")
     */
    private $planQuantity = 1;

    /**
     * planUnitPrice
     * Amount that will override the Plan's default price.
     * optional, in cents, min=0
     *
     * @var int
     * @Assert\Range(min="0")
     */
    private $planUnitPrice;

    /**
     * setupFee
     * Amount that will override the default setup fee.
     * optional, in cents, min=0
     *
     * @var int
     * @Assert\Range(min="0")
     */
    private $setupFee;

    /**
     * startDate
     * Allows you to specify a future date or a past date on which the subscription starts.Past dates can be entered in
     * case the subscription has already started. Any past date entered must be within the current billing cycle/plan
     * term. The subscription will start immediately if this parameter is not passed. optional, timestamp(UTC) in
     * seconds
     *
     * @var int
     * @Assert\Range(min="1")
     */
    private $startDate;

    /**
     * trialEnd
     * The time at which the trial ends for this subscription. Can be specified to override the default trial period.If
     * '0' is passed, the subscription will be activated immediately. optional, timestamp(UTC) in seconds
     *
     * @var int
     * @Assert\Range(min="0")
     */
    private $trialEnd;

    /**
     * invoiceNotes
     * Invoice Notes for this resource. Read More.
     * optional, string, max chars=1000
     *
     * @var string
     * @Assert\Length(max="1000")
     */
    private $invoiceNotes;

    /**
     * {@inheritDoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'invoiceNotes',
                'coupon',
                'trialEnd',
                'startDate',
                'setupFee',
                'planUnitPrice',
                'planQuantity',
                'planId',
                'id',
            ]
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Subscription
     */
    public function setId(string $id): Subscription
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlanId(): string
    {
        return $this->planId;
    }

    /**
     * @return bool
     */
    public function hasPlanId(): bool
    {
        return !is_null($this->planId);
    }

    /**
     * @param string $planId
     *
     * @return Subscription
     */
    public function setPlanId(string $planId): Subscription
    {
        $this->planId = $planId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPlanQuantity(): int
    {
        return $this->planQuantity;
    }

    /**
     * @return bool
     */
    public function hasPlanQuantity(): bool
    {
        return !is_null($this->planQuantity);
    }

    /**
     * @param int $planQuantity
     *
     * @return Subscription
     */
    public function setPlanQuantity(int $planQuantity): Subscription
    {
        $this->planQuantity = $planQuantity;

        return $this;
    }

    /**
     * @return int
     */
    public function getPlanUnitPrice(): int
    {
        return $this->planUnitPrice;
    }

    /**
     * @return bool
     */
    public function hasPlanUnitPrice(): bool
    {
        return !is_null($this->planUnitPrice);
    }

    /**
     * @param int $planUnitPrice
     *
     * @return Subscription
     */
    public function setPlanUnitPrice(int $planUnitPrice): Subscription
    {
        $this->planUnitPrice = $planUnitPrice;

        return $this;
    }

    /**
     * @return int
     */
    public function getSetupFee(): int
    {
        return $this->setupFee;
    }

    /**
     * @return bool
     */
    public function hasSetupFee(): bool
    {
        return !is_null($this->setupFee);
    }

    /**
     * @param int $setupFee
     *
     * @return Subscription
     */
    public function setSetupFee(int $setupFee): Subscription
    {
        $this->setupFee = $setupFee;

        return $this;
    }

    /**
     * @return int
     */
    public function getStartDate(): int
    {
        return $this->startDate;
    }

    /**
     * @return bool
     */
    public function hasStartDate(): bool
    {
        return !is_null($this->startDate);
    }

    /**
     * @param int $startDate
     *
     * @return Subscription
     */
    public function setStartDate(int $startDate): Subscription
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return int
     */
    public function getTrialEnd(): int
    {
        return $this->trialEnd;
    }

    /**
     * @return bool
     */
    public function hasTrialEnd(): bool
    {
        return !is_null($this->trialEnd);
    }

    /**
     * @param int $trialEnd
     *
     * @return Subscription
     */
    public function setTrialEnd(int $trialEnd): Subscription
    {
        $this->trialEnd = $trialEnd;

        return $this;
    }

    /**
     * @return string
     */
    public function getCoupon(): string
    {
        return $this->coupon;
    }

    /**
     * @return bool
     */
    public function hasCoupon(): bool
    {
        return !is_null($this->coupon);
    }

    /**
     * @param string $coupon
     *
     * @return Subscription
     */
    public function setCoupon(string $coupon): Subscription
    {
        $this->coupon = $coupon;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNotes(): string
    {
        return $this->invoiceNotes;
    }

    /**
     * @return bool
     */
    public function hasInvoiceNotes(): bool
    {
        return !is_null($this->invoiceNotes);
    }

    /**
     * @param string $invoiceNotes
     *
     * @return Subscription
     */
    public function setInvoiceNotes(string $invoiceNotes): Subscription
    {
        $this->invoiceNotes = $invoiceNotes;

        return $this;
    }
}
