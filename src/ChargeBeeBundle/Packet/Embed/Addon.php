<?php

namespace ChargeBeeBundle\Packet\Embed;

use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * {@inheritDoc}
 */
class Addon implements PacketMethodAttributesInteface
{
    use AttributeHelperTrait;

    /**
     * id
     * Identifier of the addon. Multiple addons can be passed.
     * optional, string, max chars=100
     *
     * @var string
     * @Assert\Length(max="100")
     */
    private $id;

    /**
     * quantity
     * Addon quantity. Applicable only for the quantity based addons. Should be passed with the same index as that of
     * associated addon id.
     * optional, integer, default=1, min=1
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $quantity = 1;

    /**
     * unitPrice
     * Amount that will override the Addon's default price. The Plan's billing frequency will not be considered for
     * overriding. E.g. If the Plan's billing frequency is every 3 months, and if the price override amount is $10, $10
     * will be used, and not $30 (i.e $10*3).
     * optional, in cents, min=0
     *
     * @var int
     * @Assert\Type(type="int")
     * @Assert\Range(min="0")
     */
    private $unitPrice;

    /**
     * trialEnd
     * The time at which the trial ends for the addon. In order to use this param please contact support@chargebee.com.
     * optional, timestamp(UTC) in seconds
     *
     * @var int
     *
     * @Assert\Type(type="integer")
     * @Assert\Range(min="0")
     */
    private $trialEnd;

    /**
     * {@inheritDoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                'id',
                'quantity',
                'unitPrice',
                'trialEnd',
            ]
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Addon
     */
    public function setId(string $id): Addon
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return bool
     */
    public function hasQuantity(): bool
    {
        return !is_null($this->quantity);
    }

    /**
     * @param int $quantity
     *
     * @return Addon
     */
    public function setQuantity(int $quantity): Addon
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return int
     */
    public function getUnitPrice(): int
    {
        return $this->unitPrice;
    }

    /**
     * @return bool
     */
    public function hasUnitPrice(): bool
    {
        return !is_null($this->unitPrice);
    }

    /**
     * @param int $unitPrice
     *
     * @return Addon
     */
    public function setUnitPrice(int $unitPrice): Addon
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    /**
     * @return int
     */
    public function getTrialEnd(): int
    {
        return $this->trialEnd;
    }

    /**
     * @return bool
     */
    public function hasTrialEnd(): bool
    {
        return !is_null($this->trialEnd);
    }

    /**
     * @param int $trialEnd
     *
     * @return Addon
     */
    public function setTrialEnd(int $trialEnd): Addon
    {
        $this->trialEnd = $trialEnd;

        return $this;
    }
}
