<?php

namespace ChargeBeeBundle\Packet\Subscriptions;

use ChargeBee_Card;
use ChargeBee_Customer;
use ChargeBee_Invoice;
use ChargeBee_Subscription;
use ChargeBee_UnbilledCharge;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Embed\Addon;
use ChargeBeeBundle\Packet\Embed\Address;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class Create extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * id
     * Id for the customer.
     * If not given, this will be auto-generated.
     * optional, string, max chars=50.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     * @Assert\NotNull()
     */
    private $customerId;

    /**
     * @var ArrayCollection
     * @Assert\Valid()
     */
    private $addons;

    /**
     * @var Address
     *
     * @Assert\Type(type="\ChargeBeeBundle\Packet\Embed\Address")
     * @Assert\Valid()
     */
    private $shippingAddress;

    /**
     * id
     * Id for the new subscription. If not given, this will be auto-generated.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Length(min="50")
     */
    private $id;

    /**
     * planId
     * Identifier of the plan for this subscription.
     * required, string, max chars=100
     *
     * @var string
     * @Assert\Length(max="100")
     * @Assert\NotNull()
     */
    private $planId;

    /**
     * planQuantity
     * Plan quantity for this subscription.
     * optional, integer, default=1, min=1
     *
     * @var int
     * @Assert\Range(max="1")
     */
    private $planQuantity = 1;

    /**
     * planUnitPrice
     * Amount that will override the Plan's default price.
     * optional, in cents, min=0
     *
     * @var int
     * @Assert\Range(min="0")
     */
    private $planUnitPrice;

    /**
     * setupFee
     * Amount that will override the default setup fee.
     * optional, in cents, min=0
     *
     * @var int
     * @Assert\Range(min="0")
     */
    private $setupFee;

    /**
     * startDate
     * Allows you to specify a future date or a past date on which the subscription starts.Past dates can be entered in
     * case the subscription has already started. Any past date entered must be within the current billing cycle/plan
     * term. The subscription will start immediately if this parameter is not passed. optional, timestamp(UTC) in
     * seconds
     *
     * @var int
     * @Assert\Range(min="1")
     */
    private $startDate;

    /**
     * trialEnd
     * The time at which the trial ends for this subscription. Can be specified to override the default trial period.If
     * '0' is passed, the subscription will be activated immediately. optional, timestamp(UTC) in seconds
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="0")
     */
    private $trialEnd;

    /**
     * billingCycles
     * Number of cycles(plan interval) this subscription should be charged. After the billing cycles exhausted, the
     * subscription will be cancelled.
     * optional, integer, min=0
     *
     * @var int
     * @Assert\Range(min="0")
     */
    private $billingCycles;

    /**
     * autoCollection
     * Defines whether payments need to be collected automatically for this subscription. Overrides customer's
     * auto-collection property.
     * optional, enumerated string
     * Possible values are
     * on Whenever an invoice is created for
     * this subscription, an automatic charge will be attempted on the payment method available.
     * off Automatic
     * collection of charges will not be made for this subscription. All payments must be recorded offline.
     *
     * @var string
     * @Assert\Choice(choices={"on","off"})
     */
    private $autoCollection;

    /**
     * termsToCharge
     * The number of future renewals for which advance invoicing is done. However, if a new term gets started in this
     * operation, the specified terms_to_charge will be inclusive of this new term.
     * optional, integer, min=1
     *
     * @var int
     * @Assert\Range(min="1")
     */
    private $termsToCharge;

    /**
     * billingAlignmentMode
     * Applicable when calendar billing is enabled and subscription is getting created in active / non_renewing states.
     * Unless specified the configured default value will be used.
     * optional, enumerated string Possible values are
     * immediate Subscription period will be aligned with the configured billing date immediately. delayed Subscription
     * period will be aligned with the configured billing date at a later date (subsequent renewals).
     *
     * @var string
     * @Assert\Choice(choices={"immediate","delayed"})
     */
    private $billingAlignmentMode;

    /**
     * poNumber
     * Purchase Order Number for this subscription.
     * optional, string, max chars=100
     *
     * @var string
     * @Assert\Length(max="100")
     */
    private $poNUmber;

    /**
     * couponIds
     * Identifier of the coupon as a List. Coupon Codes can also be passed.
     * optional, list of string
     *
     * @var array
     * @Assert\Type(type="array")
     */
    private $couponIds;

    /**
     * paymentSourceId
     * Unique identifier of the payment source to be attached to this subscription.
     * optional, string, max chars=40
     *
     * @var string
     * @Assert\Length(max="40")
     */
    private $paymentSourceId;

    /**
     * invoiceNotes
     * Invoice Notes for this resource. Read More.
     * optional, string, max chars=1000
     *
     * @var string
     * @Assert\Length(max="1000")
     */
    private $invoiceNotes;

    /**
     * metaData
     * Additional data about this resource can be stored here in the JSON Format. Learn more.
     * optional, jsonobject
     *
     * @var string
     * @Assert\Type(type="json")
     */
    private $metaData;

    /**
     * invoiceImmediately
     * Applicable when charges gets added during this operation. If specified as false, these charges will be added to
     * the unbilled charges instead of getting invoiced immediately. optional, boolean, default=true
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $invoiceImmediately = true;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $customerId = $this->hasCustomerId() ? $this->getCustomerId() : null;
        $this->result = ChargeBee_Subscription::createForCustomer(
            $customerId,
            $this->getAttributes(),
            $this->environment
        );

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @return bool
     */
    public function hasCustomerId(): bool
    {
        return !is_null($this->customerId);
    }

    /**
     * @param string $customerId
     *
     * @return Create
     */
    public function setCustomerId(string $customerId): Create
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        $attributes = $this->getAttributesByMap(
            [
                'id',
                'planId',
                'planQuantity',
                'planUnitPrice',
                'setupFee',
                'startDate',
                'trialEnd',
                'billingCycles',
                'autoCollection',
                'termsToCharge',
                'billingAlignmentMode',
                'poNumber',
                'couponIds',
                'paymentSourceId',
                'invoiceNotes',
                'metaData',
                [
                    'get' => 'isInvoiceImmediately',
                    'has' => 'hasInvoiceImmediately',
                ],
                'shippingAddress',
            ]
        );

        if ($this->hasAddons()) {
            /** @var Addon $addon */
            foreach ($this->getAddons() as $addon) {
                $attributes['ADDONS'][] = $addon->getAttributes();
            }
        }

        return $attributes;
    }

    /**
     * @return ArrayCollection
     */
    public function getAddons(): ArrayCollection
    {
        return $this->addons;
    }

    /**
     * @return bool
     */
    public function hasAddons(): bool
    {
        return !is_null($this->addons);
    }

    /**
     * @param ArrayCollection $addons
     *
     * @return Create
     */
    public function setAddons(ArrayCollection $addons): Create
    {
        $this->addons = $addons;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Create
     */
    public function setId(string $id): Create
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlanId(): string
    {
        return $this->planId;
    }

    /**
     * @return bool
     */
    public function hasPlanId(): bool
    {
        return !is_null($this->planId);
    }

    /**
     * @param string $planId
     *
     * @return Create
     */
    public function setPlanId(string $planId): Create
    {
        $this->planId = $planId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPlanQuantity(): int
    {
        return $this->planQuantity;
    }

    /**
     * @return bool
     */
    public function hasPlanQuantity(): bool
    {
        return !is_null($this->planQuantity);
    }

    /**
     * @param int $planQuantity
     *
     * @return Create
     */
    public function setPlanQuantity(int $planQuantity): Create
    {
        $this->planQuantity = $planQuantity;

        return $this;
    }

    /**
     * @return int
     */
    public function getPlanUnitPrice(): int
    {
        return $this->planUnitPrice;
    }

    /**
     * @return bool
     */
    public function hasPlanUnitPrice(): bool
    {
        return !is_null($this->planUnitPrice);
    }

    /**
     * @param int $planUnitPrice
     *
     * @return Create
     */
    public function setPlanUnitPrice(int $planUnitPrice): Create
    {
        $this->planUnitPrice = $planUnitPrice;

        return $this;
    }

    /**
     * @return int
     */
    public function getSetupFee(): int
    {
        return $this->setupFee;
    }

    /**
     * @return bool
     */
    public function hasSetupFee(): bool
    {
        return !is_null($this->setupFee);
    }

    /**
     * @param int $setupFee
     *
     * @return Create
     */
    public function setSetupFee(int $setupFee): Create
    {
        $this->setupFee = $setupFee;

        return $this;
    }

    /**
     * @return int
     */
    public function getStartDate(): int
    {
        return $this->startDate;
    }

    /**
     * @return bool
     */
    public function hasStartDate(): bool
    {
        return !is_null($this->startDate);
    }

    /**
     * @param int $startDate
     *
     * @return Create
     */
    public function setStartDate(int $startDate): Create
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return int
     */
    public function getTrialEnd(): int
    {
        return $this->trialEnd;
    }

    /**
     * @return bool
     */
    public function hasTrialEnd(): bool
    {
        return !is_null($this->trialEnd);
    }

    /**
     * @param int $trialEnd
     *
     * @return Create
     */
    public function setTrialEnd(int $trialEnd): Create
    {
        $this->trialEnd = $trialEnd;

        return $this;
    }

    /**
     * @return int
     */
    public function getBillingCycles(): int
    {
        return $this->billingCycles;
    }

    /**
     * @return bool
     */
    public function hasBillingCycles(): bool
    {
        return !is_null($this->billingCycles);
    }

    /**
     * @param int $billingCycles
     *
     * @return Create
     */
    public function setBillingCycles(int $billingCycles): Create
    {
        $this->billingCycles = $billingCycles;

        return $this;
    }

    /**
     * @return string
     */
    public function getAutoCollection(): string
    {
        return $this->autoCollection;
    }

    /**
     * @return bool
     */
    public function hasAutoCollection(): bool
    {
        return !is_null($this->autoCollection);
    }

    /**
     * @param string $autoCollection
     *
     * @return Create
     */
    public function setAutoCollection(string $autoCollection): Create
    {
        $this->autoCollection = $autoCollection;

        return $this;
    }

    /**
     * @return int
     */
    public function getTermsToCharge(): int
    {
        return $this->termsToCharge;
    }

    /**
     * @return bool
     */
    public function hasTermsToCharge(): bool
    {
        return !is_null($this->termsToCharge);
    }

    /**
     * @param int $termsToCharge
     *
     * @return Create
     */
    public function setTermsToCharge(int $termsToCharge): Create
    {
        $this->termsToCharge = $termsToCharge;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAlignmentMode(): string
    {
        return $this->billingAlignmentMode;
    }

    /**
     * @return bool
     */
    public function hasBillingAlignmentMode(): bool
    {
        return !is_null($this->billingAlignmentMode);
    }

    /**
     * @param string $billingAlignmentMode
     *
     * @return Create
     */
    public function setBillingAlignmentMode(string $billingAlignmentMode): Create
    {
        $this->billingAlignmentMode = $billingAlignmentMode;

        return $this;
    }

    /**
     * @return string
     */
    public function getPoNUmber(): string
    {
        return $this->poNUmber;
    }

    /**
     * @return bool
     */
    public function hasPoNUmber(): bool
    {
        return !is_null($this->poNUmber);
    }

    /**
     * @param string $poNUmber
     *
     * @return Create
     */
    public function setPoNUmber(string $poNUmber): Create
    {
        $this->poNUmber = $poNUmber;

        return $this;
    }

    /**
     * @return array
     */
    public function getCouponIds(): array
    {
        return $this->couponIds;
    }

    /**
     * @return bool
     */
    public function hasCouponIds(): bool
    {
        return !is_null($this->couponIds);
    }

    /**
     * @param array $couponIds
     *
     * @return Create
     */
    public function setCouponIds(array $couponIds): Create
    {
        $this->couponIds = $couponIds;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentSourceId(): string
    {
        return $this->paymentSourceId;
    }

    /**
     * @return bool
     */
    public function hasPaymentSourceId(): bool
    {
        return !is_null($this->paymentSourceId);
    }

    /**
     * @param string $paymentSourceId
     *
     * @return Create
     */
    public function setPaymentSourceId(string $paymentSourceId): Create
    {
        $this->paymentSourceId = $paymentSourceId;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceNotes(): string
    {
        return $this->invoiceNotes;
    }

    /**
     * @return bool
     */
    public function hasInvoiceNotes(): bool
    {
        return !is_null($this->invoiceNotes);
    }

    /**
     * @param string $invoiceNotes
     *
     * @return Create
     */
    public function setInvoiceNotes(string $invoiceNotes): Create
    {
        $this->invoiceNotes = $invoiceNotes;

        return $this;
    }

    /**
     * @return string
     */
    public function getMetaData(): string
    {
        return $this->metaData;
    }

    /**
     * @return bool
     */
    public function hasMetaData(): bool
    {
        return !is_null($this->metaData);
    }

    /**
     * @param string $metaData
     *
     * @return Create
     */
    public function setMetaData(string $metaData): Create
    {
        $this->metaData = $metaData;

        return $this;
    }

    /**
     * @return bool
     */
    public function isInvoiceImmediately(): bool
    {
        return $this->invoiceImmediately;
    }

    /**
     * @return bool
     */
    public function hasInvoiceImmediately(): bool
    {
        return !is_null($this->invoiceImmediately);
    }

    /**
     * @param bool $invoiceImmediately
     *
     * @return Create
     */
    public function setInvoiceImmediately(bool $invoiceImmediately): Create
    {
        $this->invoiceImmediately = $invoiceImmediately;

        return $this;
    }

    /**
     * @return Address
     */
    public function getShippingAddress(): Address
    {
        return $this->shippingAddress;
    }

    /**
     * @return bool
     */
    public function hasShippingAddress(): bool
    {
        return !is_null($this->shippingAddress);
    }

    /**
     * @param Address $shippingAddress
     *
     * @return Create
     */
    public function setShippingAddress(Address $shippingAddress): Create
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Subscription $subscription */
        $subscription = $this->result->subscription();
        /** @var ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var ChargeBee_Card $card */
        $card = $this->result->card();
        /** @var ChargeBee_Invoice $invoice */
        $invoice = $this->result->invoice();
        /** @var ChargeBee_UnbilledCharge $unbilledCharge */
        $unbilledCharge = $this->result->unbilledCharge();

        return [
            'subscription' => $subscription ? $subscription->getValues() : null,
            'customer' => $customer ? $customer->getValues() : null,
            'card' => $card ? $card->getValues() : null,
            'invoice' => $invoice ? $invoice->getValues() : null,
            'unbilledCharge' => $unbilledCharge ? $unbilledCharge->getValues() : null,
        ];
    }
}
