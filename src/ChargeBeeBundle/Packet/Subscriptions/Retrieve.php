<?php

namespace ChargeBeeBundle\Packet\Subscriptions;

use ChargeBee_Subscription;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use Symfony\Component\Validator\Constraints as Assert;

class Retrieve extends BasePacketMethod
{
    /**
     * id
     * Id for the new subscription. If not given, this will be auto-generated.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * {@inheritDoc}
     */
    public function getAttributes(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;
        $this->result = ChargeBee_Subscription::retrieve($id, $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Retrieve
     */
    public function setId(string $id): Retrieve
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Subscription $subscription */
        $subscription = $this->result->subscription();
        /** @var \ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var \ChargeBee_Card $card */
        $card = $this->result->card();

        return [
            'subscription' => $subscription ? $subscription->getValues() : null,
            'customer' => $customer ? $customer->getValues() : null,
            'card' => $card ? $card->getValues() : null,
        ];
    }
}
