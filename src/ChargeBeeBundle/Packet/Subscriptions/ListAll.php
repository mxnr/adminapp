<?php

namespace ChargeBeeBundle\Packet\Subscriptions;

use ChargeBee_Subscription;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * {@inheritDoc}
 */
class ListAll extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * @var \ChargeBee_ListResult
     */
    protected $result;

    /**
     * limit
     * Limits the number of resources to be returned.
     * optional, integer, default=10, min=1, max=100
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\NotNull()
     * @Assert\Range(max="100", min="1")
     */
    private $limit = 10;

    /**
     * offset
     * Allows you to fetch the next set of resources. The value used for this parameter must be the value returned for
     * next_offset parameter in the previous API call. optional, string, max chars=1000
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="1000")
     */
    private $offset;

    /**
     * includeDeleted
     * If set to true, includes the deleted resources in the response. For the deleted resources in the response, the
     * 'deleted' attribute will be 'true'. optional, boolean, default=false
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $includeDeleted = false;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"created_at"})
     */
    private $sortBy;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"asc","desc"})
     */
    private $sortOrder;

    /**
     * id[<operator>]
     * To filter based on Subscription Id.
     * Supported operators : is, isNot, startsWith, in, notIn
     *
     * Example → "id[is]" => "8gsnbYfsMLds"
     * optional, string filter
     *
     * @var string
     * @Assert\Choice(choices={"is","isNot","startsWith","in","notIn"})
     */
    private $idOperator;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     */
    private $id;

    /**
     * customerId[<operator>]
     * To filter based on Subscription Customer Id.
     * Supported operators : is, isNot, startsWith, in, notIn
     *
     * Example → "customerId[is]" => "8gsnbYfsMLds"
     * optional, string filter
     *
     * @var string
     * @Assert\Choice(choices={"is","isNot","startsWith","in","notIn"})
     */
    private $customerIdOperator;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     */
    private $customerId;

    /**
     * planId[<operator>]
     * To filter based on Subscription Plan Id.
     * Supported operators : is, isNot, startsWith, in, notIn
     *
     * Example → "planId[isNot]" => "basic"
     * optional, string filter
     *
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","startsWith","in","notIn"})
     */
    private $planIdOperator;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $planId;

    /**
     * status[<operator>]
     * To filter based on Subscription State. Possible values are : future, in_trial, active, non_renewing, cancelled.
     * Supported operators : is, isNot, in, notIn
     *
     * Example → "status[is]" => "active"
     * optional, enumerated string filter
     *
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","in","notIn"})
     */
    private $statusOperator;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"future","in_trial","active","non_renewing","cancelled"})
     */
    private $status;

    /**
     * cancelReason[<operator>]
     * To filter based on Subscription Cancel Reason. Possible values are : not_paid, no_card, fraud_review_failed,
     * non_compliant_eu_customer, tax_calculation_failed, currency_incompatible_with_gateway, non_compliant_customer.
     * Supported operators : is, isNot, in, notIn, isPresent
     *
     * Example → "cancelReason[isNot]" => "not_paid"
     * optional, enumerated string filter
     *
     * @var string
     * @Assert\Choice(choices={"is","isNot","in","notIn","isPresent"})
     */
    private $cancelReasonOperator;

    /**
     * @var string
     *
     * @Assert\Choice(choices={
     *     "not_paid",
     *     "no_card",
     *     "fraud_review_failed",
     *     "non_compliant_eu_customer",
     *     "tax_calculation_failed",
     *     "currency_incompatible_with_gateway",
     *     "non_compliant_customer"
     * })
     */
    private $cancelReason;

    /**
     * remainingBillingCycles[<operator>]
     * To filter based on Subscription Remaining Billing Cycles.
     * Supported operators : is, isNot, lt, lte, gt, gte, between, isPresent
     *
     * Example → "remainingBillingCycles[isNot]" => "3"
     * optional, integer filter
     *
     * @var string
     * @Assert\Choice(choices={
     *     "is",
     *     "isNot",
     *     "lt",
     *     "lte",
     *     "gt",
     *     "gte",
     *     "between",
     *     "isPresent"
     *     })
     */
    private $remainingBillingCyclesOperator;

    /**
     * @var int
     * @Assert\Type(type="integer")
     */
    private $remainingBillingCycles;

    /**
     * createdAt[<operator>]
     * To filter based on Subscription Created At.
     * Supported operators : after, before, on, between
     *
     * Example → "createdAt[on]" => "1435054328"
     * optional, timestamp(UTC) in seconds filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"after","before","on","between"})
     */
    private $createdAtOperator;

    /**
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $createdAt;

    /**
     * activatedAt[<operator>]
     * To filter based on Subscription Activated At.
     * Supported operators : after, before, on, between, isPresent
     *
     * Example → "activatedAt[before]" => "1435054328"
     * optional, timestamp(UTC) in seconds filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"after","before","on","between"})
     */
    private $activatedAtOperator;

    /**
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $activatedAt;

    /**
     * updatedAt[<operator>]
     * To filter based on updated at. This attribute will be present only if the resource has been updated after
     * 2016-09-28. Supported operators : after, before, on, between
     *
     * Example → "updatedAt[after]" => "1243545465"
     * optional, timestamp(UTC) in seconds filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"after","before","on","between"})
     */
    private $updatedAtOperator;

    /**
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $updatedAt;

    /**
     * nextBillingAt[<operator>]
     * To filter based on Subscription Next Billing At.
     * Supported operators : after, before, on, between
     *
     * Example → "nextBillingAt[before]" => "1435054328"
     * optional, timestamp(UTC) in seconds filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"after","before","on","between"})
     */
    private $nextBillingAtOperator;

    /**
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $nextBillingAt;

    /**
     * cancelledAt[<operator>]
     * To filter based on Subscription Cancelled At.
     * Supported operators : after, before, on, between
     *
     * Example → "cancelledAt[after]" => "1435054328"
     * optional, timestamp(UTC) in seconds filter
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Choice(choices={"after","before","on","between"})
     */
    private $cancelledAtOperator;

    /**
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $cancelledAt;

    /**
     * hasScheduledChanges[<operator>]
     * To filter based on Subscription Has Scheduled Changes. Possible values are : true, false
     * Supported operators : is
     *
     * Example → "hasScheduledChanges[is]" => "true"
     * optional, boolean filter
     *
     * @var string
     * @Assert\Choice(choices={"is"})
     */
    private $hasScheduledChangesOperator;

    /**
     * @var bool
     * @Assert\Type(type="boolean")
     */
    private $hasScheduledChanges;

    /**
     * @param string $operator
     * @param bool   $hasScheduledChanges
     *
     * @return ListAll
     */
    public function setHasScheduledChangesFilter(string $operator, bool $hasScheduledChanges): ListAll
    {
        $this->hasScheduledChangesOperator = $operator;
        $this->hasScheduledChanges = $hasScheduledChanges;

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $cancelReason
     *
     * @return ListAll
     */
    public function setCancelReasonFilter(string $operator, array $cancelReason): ListAll
    {
        $this->cancelReasonOperator = $operator;

        if (in_array($operator, ['in', 'notIn'])) {
            $this->cancelReason = $cancelReason;
        } else {
            $this->cancelReason = array_pop($cancelReason);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $id
     *
     * @return ListAll
     */
    public function setIdFilter(string $operator, array $id): ListAll
    {
        $this->idOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->id = $id;
        } else {
            $this->id = array_pop($id);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $customerId
     *
     * @return ListAll
     */
    public function setCustomerIdFilter(string $operator, array $customerId): ListAll
    {
        $this->customerIdOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->customerId = $customerId;
        } else {
            $this->customerId = array_pop($customerId);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $plainId
     *
     * @return ListAll
     */
    public function setPlanIdFilter(string $operator, array $plainId): ListAll
    {
        $this->planIdOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->planId = $plainId;
        } else {
            $this->planId = array_pop($plainId);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $status
     *
     * @return ListAll
     */
    public function setStatusFilter(string $operator, array $status): ListAll
    {
        $this->statusOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->status = $status;
        } else {
            $this->status = array_pop($status);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $createdAt
     *
     * @return ListAll
     */
    public function setCreatedAtFilter(string $operator, array $createdAt): ListAll
    {
        $this->createdAtOperator = $operator;
        if (in_array($operator, ['between'])) {
            $this->createdAt = $createdAt;
        } else {
            $this->createdAt = array_pop($createdAt);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $nextBillingAt
     *
     * @return ListAll
     */
    public function setNextBillingAtFilter(string $operator, array $nextBillingAt): ListAll
    {
        $this->nextBillingAtOperator = $operator;

        if (in_array($operator, ['between'])) {
            $this->nextBillingAt = $nextBillingAt;
        } else {
            $this->nextBillingAt = array_pop($nextBillingAt);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $cancelledAt
     *
     * @return ListAll
     */
    public function setCancelledAtFilter(string $operator, array $cancelledAt): ListAll
    {
        $this->cancelledAtOperator = $operator;

        if (in_array($operator, ['between'])) {
            $this->cancelledAt = $cancelledAt;
        } else {
            $this->cancelledAt = array_pop($cancelledAt);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $activatedAt
     *
     * @return ListAll
     */
    public function setActivatedAtFilter(string $operator, array $activatedAt): ListAll
    {
        $this->activatedAtOperator = $operator;

        if (in_array($operator, ['between'])) {
            $this->activatedAt = $activatedAt;
        } else {
            $this->activatedAt = array_pop($activatedAt);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $updatedAt
     *
     * @return ListAll
     */
    public function setUpdatedAtFilter(string $operator, array $updatedAt): ListAll
    {
        $this->updatedAtOperator = $operator;

        if (in_array($operator, ['between'])) {
            $this->updatedAt = $updatedAt;
        } else {
            $this->updatedAt = array_pop($updatedAt);
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function execute(): PacketMethodInterface
    {
        $this->result = ChargeBee_Subscription::all($this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getAttributes(): array
    {
        $attributes = $this->getAttributesByMap(
            [
                'sortBy',
                'idFilter',
                'customerIdFilter',
                'planIdFilter',
                'statusFilter',
                'cancelReasonFilter',
                'remainingBillingCyclesFilter',
                'createdAtFilter',
                'activatedAtFilter',
                'nextBillingAtFilter',
                'cancelledAtFilter',
                'hasScheduledChangesFilter',
                'updatedAtFilter',
            ],
            [],
            true
        );

        $attributes = $this->getAttributesByMap(
            [
                'limit',
                'offset',
                [
                    'get' => 'isIncludeDeleted',
                    'has' => 'hasIncludeDeleted',
                ],
            ],
            $attributes
        );

        return $attributes;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return bool
     */
    public function hasLimit(): bool
    {
        return !is_null($this->limit);
    }

    /**
     * @param int $limit
     *
     * @return ListAll
     */
    public function setLimit(int $limit): ListAll
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return string
     */
    public function getOffset(): string
    {
        return $this->offset;
    }

    /**
     * @return bool
     */
    public function hasOffset(): bool
    {
        return !is_null($this->offset);
    }

    /**
     * @param string $offset
     *
     * @return ListAll
     */
    public function setOffset(string $offset): ListAll
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return bool
     */
    public function isIncludeDeleted(): bool
    {
        return $this->includeDeleted;
    }

    /**
     * @return bool
     */
    public function hasIncludeDeleted(): bool
    {
        return !is_null($this->includeDeleted);
    }

    /**
     * @param bool $includeDeleted
     *
     * @return ListAll
     */
    public function setIncludeDeleted(bool $includeDeleted): ListAll
    {
        $this->includeDeleted = $includeDeleted;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasSortBy(): bool
    {
        return !is_null($this->sortBy);
    }

    /**
     * @return array
     */
    public function getSortBy(): array
    {
        return ['sortBy[' . $this->sortOrder . ']' => $this->sortBy];
    }

    /**
     * @param string $by
     * @param string $order
     *
     * @return ListAll
     */
    public function setSortBy(string $by, string $order): ListAll
    {
        $this->sortBy = $by;
        $this->sortOrder = $order;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasIdFilter(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @return array
     */
    public function getIdFilter(): array
    {
        return ['id[' . $this->idOperator . ']' => $this->id];
    }

    /**
     * @return bool
     */
    public function hasCustomerIdFilter(): bool
    {
        return !is_null($this->customerId);
    }

    /**
     * @return array
     */
    public function getCustomerIdFilter(): array
    {
        return ['customerId[' . $this->customerIdOperator . ']' => $this->customerId];
    }

    /**
     * @return bool
     */
    public function hasPlanIdFilter(): bool
    {
        return !is_null($this->planId);
    }

    /**
     * @return array
     */
    public function getPlanIdFilter(): array
    {
        return ['planId[' . $this->planIdOperator . ']' => $this->planId];
    }

    /**
     * @return bool
     */
    public function hasStatusFilter(): bool
    {
        return !is_null($this->status);
    }

    /**
     * @return array
     */
    public function getStatusFilter(): array
    {
        return ['status[' . $this->statusOperator . ']' => $this->status];
    }

    /**
     * @return bool
     */
    public function hasCancelReasonFilter(): bool
    {
        return !is_null($this->cancelReason);
    }

    /**
     * @return array
     */
    public function getCancelReasonFilter(): array
    {
        return ['cancelReason[' . $this->cancelReasonOperator . ']' => $this->cancelReason];
    }

    /**
     * @return bool
     */
    public function hasRemainingBillingCyclesFilter(): bool
    {
        return !is_null($this->remainingBillingCycles);
    }

    /**
     * @return array
     */
    public function getRemainingBillingCyclesFilter(): array
    {
        return [
            'remainingBillingCycles[' . $this->remainingBillingCyclesOperator . ']' => $this->remainingBillingCycles,
        ];
    }

    /**
     * @param string $operator
     * @param array  $remainingBillingCycles
     *
     * @return ListAll
     */
    public function setRemainingBillingCycles(string $operator, array $remainingBillingCycles): ListAll
    {
        $this->remainingBillingCyclesOperator = $operator;

        if (in_array($operator, ['between'])) {
            $this->remainingBillingCycles = $remainingBillingCycles;
        } else {
            $this->remainingBillingCycles = array_pop($remainingBillingCycles);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function hasCreatedAtFilter(): bool
    {
        return !is_null($this->createdAt);
    }

    /**
     * @return array
     */
    public function getCreatedAtFilter(): array
    {
        return ['createdAt[' . $this->createdAtOperator . ']' => $this->createdAt];
    }

    /**
     * @return bool
     */
    public function hasActivatedAtFilter(): bool
    {
        return !is_null($this->activatedAt);
    }

    /**
     * @return array
     */
    public function getActivatedAtFilter(): array
    {
        return ['activatedAt[' . $this->activatedAtOperator . ']' => $this->activatedAt];
    }

    /**
     * @return bool
     */
    public function hasNextBillingAtFilter(): bool
    {
        return !is_null($this->nextBillingAt);
    }

    /**
     * @return array
     */
    public function getNextBillingAtFilter(): array
    {
        return ['nextBillingAt[' . $this->nextBillingAtOperator . ']' => $this->nextBillingAt];
    }

    /**
     * @return bool
     */
    public function hasCancelledAtFilter(): bool
    {
        return !is_null($this->cancelledAt);
    }

    /**
     * @return array
     */
    public function getCancelledAtFilter(): array
    {
        return ['nextBillingAt[' . $this->cancelledAtOperator . ']' => $this->cancelledAt];
    }

    /**
     * @return bool
     */
    public function hasHasScheduledChangesFilter(): bool
    {
        return !is_null($this->hasScheduledChanges);
    }

    /**
     * @return array
     */
    public function getHasScheduledChangesFilter(): array
    {
        return ['hasScheduledChanges[' . $this->hasScheduledChangesOperator . ']' => $this->hasScheduledChanges];
    }

    /**
     * @return bool
     */
    public function hasUpdatedAtFilter(): bool
    {
        return !is_null($this->updatedAt);
    }

    /**
     * @return array
     */
    public function getUpdatedAtFilter(): array
    {
        return ['updatedAt[' . $this->updatedAtOperator . ']' => $this->updatedAt];
    }

    /**
     * {@inheritDoc}
     */
    public function getResult(): array
    {
        $result = ['nextOffset' => $this->result->nextOffset(), 'list' => []];

        /** @var \ChargeBee_Result $entry */
        foreach ($this->result as $entry) {

            /** @var ChargeBee_Subscription $subscription */
            $subscription = $entry->subscription();
            /** @var \ChargeBee_Customer $customer */
            $customer = $entry->customer();
            /** @var \ChargeBee_Card $card */
            $card = $entry->card();

            $result['list'][] = [
                'subscription' => $subscription ? $subscription->getValues() : null,
                'customer' => $customer ? $customer->getValues() : null,
                'card' => $card ? $card->getValues() : null,
            ];
        }


        return $result;
    }
}
