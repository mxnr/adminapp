<?php

namespace ChargeBeeBundle\Packet\Subscriptions;

use ChargeBee_Subscription;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class Cancel extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $id;

    /**
     * endOfTerm
     * You can specify when you want to cancel the subscription.
     * optional, boolean, default=false
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $endOfTerm = false;

    /**
     * creditOptionForCurrentTermCharges
     * Specify this if you want to issue credits against the current term charges. Not applicable when 'end_of_term' is
     * true.
     * optional, enumerated string
     * Possible values are
     * none No credits will be issued against current term charges.
     * prorate Prorated credits for the unused period will be issued against current term charges.
     * full Credits will be issued for all the current term charges.
     *
     * @var string
     * @Assert\Choice(choices={"none","prorate","full"})
     */
    private $creditOptionForCurrentTermCharges;

    /**
     * unbilledChargesOption
     * Applicable when unbilled charges are present for this subscription.
     * If specified as delete, these charges will be deleted instead of getting invoiced immediately.
     *
     * Note: On the invoice raised, an automatic charge will be attempted on the payment method available if customer's
     * auto-collection property is ‘ON’.
     * Not applicable when 'end_of_term' is true.
     *
     * optional, enumerated string
     *
     * Possible values are
     * invoice Unbilled charges will be raised as an invoice.
     * delete Unbilled charges will be deleted.
     *
     * @var string
     * @Assert\Choice(choices={"invoice", "delete"})
     */
    private $unbilledChargesOption;

    /**
     * accountReceivablesHandling
     * Applicable when the subscription has past due invoices. Specify this if you want to close the due invoices of
     * the subscription. If specified as schedule_payment_collection/write_off, the due invoices of the subscription
     * will be qualified for the selected operation after the remaining refundable credits and excess payments are
     * applied. Note: The payment collection attempt will be asynchronous. Not applicable when 'end_of_term' is true.
     * optional, enumerated string Possible values are no_action No action is taken.
     * schedule_payment_collection
     * An automatic charge for the due amount of the past invoices will be attempted on the payment method available,
     * if customer's auto-collection property is ‘ON’. write_off The amount due in the invoices will be written-off.
     *
     * @var string
     * @Assert\Choice(choices={"no_action","schedule_payment_collection","write_off"})
     */
    private $accountReceivablesHandling;

    /**
     * refundableCreditsHandling
     * Applicable when the customer has remaining refundable credits(issued against online payments). If specified as
     * schedule_refund, the refund will be initiated for these credits after they are applied against the
     * subscription’s past due invoices if any. Note: The refunds initiated will be asynchronous. Not applicable when
     * 'end_of_term' is true. optional, enumerated string Possible values are no_action No action is taken. .
     * schedule_refund Initiates refund of the remaining credits.
     *
     * @var string
     * @Assert\Choice(choices={"no_action", "schedule_refund"})
     */
    private $refundableCreditsHandling;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;

        $this->result = ChargeBee_Subscription::cancel($id, $this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Cancel
     */
    public function setId(string $id): Cancel
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(
            [
                [
                    'has' => 'hasEndOfTerm',
                    'get' => 'isEndOfTerm',
                ],
                'creditOptionForCurrentTermCharges',
                'unbilledChargesOption',
                'accountReceivablesHandling',
                'refundableCreditsHandling',
            ]
        );
    }

    /**
     * @return bool
     */
    public function isEndOfTerm(): bool
    {
        return $this->endOfTerm;
    }

    /**
     * @return bool
     */
    public function hasEndOfTerm(): bool
    {
        return !is_null($this->endOfTerm);
    }

    /**
     * @param bool $endOfTerm
     *
     * @return Cancel
     */
    public function setEndOfTerm(bool $endOfTerm): Cancel
    {
        $this->endOfTerm = $endOfTerm;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreditOptionForCurrentTermCharges(): string
    {
        return $this->creditOptionForCurrentTermCharges;
    }

    /**
     * @return bool
     */
    public function hasCreditOptionForCurrentTermCharges(): bool
    {
        return !is_null($this->creditOptionForCurrentTermCharges);
    }

    /**
     * @param string $creditOptionForCurrentTermCharges
     *
     * @return Cancel
     */
    public function setCreditOptionForCurrentTermCharges(string $creditOptionForCurrentTermCharges): Cancel
    {
        $this->creditOptionForCurrentTermCharges = $creditOptionForCurrentTermCharges;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnbilledChargesOption(): string
    {
        return $this->unbilledChargesOption;
    }

    /**
     * @return bool
     */
    public function hasUnbilledChargesOption(): bool
    {
        return !is_null($this->unbilledChargesOption);
    }

    /**
     * @param string $unbilledChargesOption
     *
     * @return Cancel
     */
    public function setUnbilledChargesOption(string $unbilledChargesOption): Cancel
    {
        $this->unbilledChargesOption = $unbilledChargesOption;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccountReceivablesHandling(): string
    {
        return $this->accountReceivablesHandling;
    }

    /**
     * @return bool
     */
    public function hasAccountReceivablesHandling(): bool
    {
        return !is_null($this->accountReceivablesHandling);
    }

    /**
     * @param string $accountReceivablesHandling
     *
     * @return Cancel
     */
    public function setAccountReceivablesHandling(string $accountReceivablesHandling): Cancel
    {
        $this->accountReceivablesHandling = $accountReceivablesHandling;

        return $this;
    }

    /**
     * @return string
     */
    public function getRefundableCreditsHandling(): string
    {
        return $this->refundableCreditsHandling;
    }

    /**
     * @return bool
     */
    public function hasRefundableCreditsHandling(): bool
    {
        return !is_null($this->refundableCreditsHandling);
    }

    /**
     * @param string $refundableCreditsHandling
     *
     * @return Cancel
     */
    public function setRefundableCreditsHandling(string $refundableCreditsHandling): Cancel
    {
        $this->refundableCreditsHandling = $refundableCreditsHandling;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /** @var ChargeBee_Subscription $subscription */
        $subscription = $this->result->subscription();
        /** @var \ChargeBee_Customer $customer */
        $customer = $this->result->customer();
        /** @var \ChargeBee_Card $card */
        $card = $this->result->card();
        /** @var \ChargeBee_Invoice $invoice */
        $invoice = $this->result->invoice();
        /** @var \ChargeBee_UnbilledCharge $unbilledCharges */
        $unbilledCharges = $this->result->unbilledCharges();
        /** @var \ChargeBee_CreditNote $creditNotes */
        $creditNotes = $this->result->creditNotes();

        return [
            'subscription' => $subscription ? $subscription->getValues() : null,
            'customer' => $customer ? $customer->getValues() : null,
            'card' => $card ? $card->getValues() : null,
            'invoice' => $invoice ? $invoice->getValues() : null,
            'unbilledCharges' => $unbilledCharges ? $unbilledCharges->getValues() : null,
            'creaditNotes' => $creditNotes ? $creditNotes->getValues() : null,
        ];
    }
}
