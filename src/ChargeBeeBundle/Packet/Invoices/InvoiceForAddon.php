<?php

namespace ChargeBeeBundle\Packet\Invoices;

use ChargeBee_Invoice;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

class InvoiceForAddon extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * Customer Id
     *
     * Identifier of the customer for which this invoice needs to be created.
     * @var string
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     */
    private $customerId;

    /**
     * Addon Id
     *
     * The ID of the non-recurring addon to be charged.
     * @var string
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     */
    private $addonId;

    /**
     * Addon Unit Price
     *
     * Amount that will override the Addon's default price.
     * @var integer
     * @Assert\Type(type="integer")
     * @Assert\NotBlank()
     */
    private $addonUnitPrice;

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $this->result = ChargeBee_Invoice::chargeAddon($this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        $attributes = $this->getAttributesByMap(
            [
                'customerId',
                'addonId',
                'addonUnitPrice',
            ]
        );

        return $attributes;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        /**@var ChargeBee_Invoice $invoice*/
        $invoice = $this->result->invoice();

        return $invoice->getValues();
    }

    /**
     * @return string
     */
    public function getCustomerId(): string
    {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     * @return InvoiceForAddon
     */
    public function setCustomerId(string $customerId): self
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasCustomerId(): bool
    {
        return !is_null($this->customerId);
    }

    /**
     * @return string
     */
    public function getAddonId(): string
    {
        return $this->addonId;
    }

    /**
     * @param string $addonId
     * @return InvoiceForAddon
     */
    public function setAddonId(string $addonId): self
    {
        $this->addonId = $addonId;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasAddonId(): bool
    {
        return !is_null($this->addonId);
    }

    /**
     * @return int
     */
    public function getAddonUnitPrice(): int
    {
        return $this->addonUnitPrice;
    }

    /**
     * @param int $addonUnitPrice
     * @return InvoiceForAddon
     */
    public function setAddonUnitPrice(int $addonUnitPrice): self
    {
        $this->addonUnitPrice = $addonUnitPrice;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasAddonUnitPrice(): bool
    {
        return !is_null($this->addonUnitPrice);
    }
}
