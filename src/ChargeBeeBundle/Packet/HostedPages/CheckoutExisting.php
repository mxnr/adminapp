<?php

namespace ChargeBeeBundle\Packet\HostedPages;

use ChargeBee_HostedPage;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Embed\Addon;
use ChargeBeeBundle\Packet\Embed\Subscription;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * {@inheritDoc}
 */
class CheckoutExisting extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * billingCycles
     * Number of cycles(plan interval) this subscription should be charged. After the billing cycles exhausted, the
     * subscription will be cancelled. optional, integer, min=0
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(max="0")
     */
    private $billingCycles;

    /**
     * replaceAddonList
     * Should be true if the existing addons should be replaced with the ones that are being passed.
     * optional, boolean, default=false
     *
     * @var bool
     *
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $replaceAddonList = false;

    /**
     * termsToCharge
     * The number of future renewals for which advance invoicing is done. However, if a new term gets started in this
     * operation, the specified terms_to_charge will be inclusive of this new term.
     * optional, integer, min=1
     *
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $termsToCharge;

    /**
     * reactivateFrom
     * The time from which this subscription should be reactivated.
     * optional, timestamp(UTC) in seconds
     *
     * @var int
     *
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $reactivateFrom;

    /**
     * billingAlignmentMode
     * Applicable when calendar billing is enabled and a new term gets started during this operation. Unless specified
     * the configured default value will be used. optional, enumerated string Possible values are immediate
     * Subscription period will be aligned with the configured billing date immediately. delayed Subscription period
     * will be aligned with the configured billing date at a later date (subsequent renewals).
     *
     * @var string
     * @Assert\Choice(choices={"immediate","delayed"})
     */
    private $billingAlignmentMode;

    /**
     * reactivate
     * Applicable only for canceled subscriptions. Once this is passed as true, canceled subscription will become
     * active; otherwise subscription changes will be made but the the subscription state will remain canceled. If not
     * passed, subscription will be activated only if there is any change in subscription data. optional, boolean
     *
     * @var bool
     * @Assert\Type(type="boolean")
     */
    private $reactivate;

    /**
     * forceTermReset
     * Applicable for 'Active' & 'Non Renewing' states alone. Generally, subscription's term will be reset (i.e current
     * term is ended and a new term starts immediately) when a new plan having different billing frequency is specified
     * in the input. For all the other cases, the subscription's term will remain intact. Now for this later scenario,
     * if you want to force a term reset you can specify this param as 'true'. Note: Specifying this value as 'false'
     * has no impact on the default behaviour. optional, boolean, default=false
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $forceTermReset = false;

    /**
     * redirectUrl
     * The customers will be redirected to this URL upon successful checkout. The hosted page id and state will be
     * passed as parameters to this URL. This parameter is not applicable for iframe messaging. Note : Redirect URL
     * configured in Settings > Hosted Pages Settings would be overriden by this redirect URL. Eg :
     * http://yoursite.com?id=<hosted_page_id>&state=succeeded. optional, string, max chars=250
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="250")
     */
    private $redirectUrl;

    /**
     * cancelUrl
     * The customers will be redirected to this URL upon canceling checkout. The hosted page id and state will be
     * passed as parameters to this URL. This parameter is not applicable for iframe messaging. Note : Cancel URL
     * configured in Settings > Hosted Pages Settings would be overriden by this cancel URL. Eg :
     * http://yoursite.com?id=<hosted_page_id>&state=cancelled. optional, string, max chars=250
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="250")
     */
    private $cancelUrl;

    /**
     * passThruContent
     * You can pass through any content specific to the hosted page request and get it back after user had submitted
     * the hosted page. optional, string, max chars=2048
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="2048")
     */
    private $passThruContent;

    /**
     * embed
     * If true then hosted page formatted to be shown in in-app iframe embed.If false, it is formatted to be shown as a
     * separate page . optional, boolean, default=true
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $embed = true;

    /**
     * iframeMessaging
     * If true then iframe will communicate with the parent window. Applicable only for embedded(iframe) hosted pages.
     * If you're using iframe_messaging you need to implement onSuccess & onCancel callbacks. To know more about
     * iframe_messaging please refer to this tutorial. optional, boolean, default=false
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $iframeMessaging = false;

    /**
     * VAT/ Tax registration number of the customer. Learn more.
     * optional, string, max chars=20
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="20")
     */
    private $vatNumber;

    /**
     * gatewayAccountId
     * The gateway account in which this payment source is stored.
     * optional, string, max chars=50
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $gatewayAccountId;

    /**
     * @var Subscription
     * @Assert\Valid()
     */
    private $subscription;

    /**
     * @var ArrayCollection
     * @Assert\Valid()
     */
    private $addons;

    /**
     * {@inheritDoc}
     */
    public function execute(): PacketMethodInterface
    {
        $this->result = ChargeBee_HostedPage::checkoutExisting($this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getAttributes(): array
    {
        $attributes = $this->getAttributesByMap(
            [
                'billingCycles',
                [
                    'get' => 'isReplaceAddonList',
                    'has' => 'hasReplaceAddonList',
                ],
                'termsToCharge',
                'reactivateFrom',
                'billingAlignmentMode',
                [
                    'has' => 'hasReactivate',
                    'get' => 'isReactivate',
                ],
                [
                    'has' => 'hasForceTermReset',
                    'get' => 'isForceTermReset',
                ],
                'redirectUrl',
                'cancelUrl',
                'passThruContent',
                [
                    'has' => 'hasEmbed',
                    'get' => 'isEmbed',
                ],
                [
                    'has' => 'hasIframeMessaging',
                    'get' => 'isIframeMessaging',
                ],
                'subscription',
            ]
        );

        if ($this->hasVatNumber()) {
            $attributes['CUSTOMER']['vatNumber'] = $this->getVatNumber();
        }

        if ($this->hasGatewayAccountId()) {
            $attributes['CARD']['gatewayAccountId'] = $this->getGatewayAccountId();
        }

        if ($this->hasAddons()) {
            /** @var Addon $addon */
            foreach ($this->getAddons() as $addon) {
                $attributes['ADDONS'][] = $addon->getAttributes();
            }
        }

        return $attributes;
    }

    /**
     * @return string
     */
    public function getVatNumber(): string
    {
        return $this->vatNumber;
    }

    /**
     * @return bool
     */
    public function hasVatNumber(): bool
    {
        return !is_null($this->vatNumber);
    }

    /**
     * @param string $vatNumber
     *
     * @return CheckoutExisting
     */
    public function setVatNumber(string $vatNumber): CheckoutExisting
    {
        $this->vatNumber = $vatNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getGatewayAccountId(): string
    {
        return $this->gatewayAccountId;
    }

    /**
     * @return bool
     */
    public function hasGatewayAccountId(): bool
    {
        return !is_null($this->gatewayAccountId);
    }

    /**
     * @param string $gatewayAccountId
     *
     * @return CheckoutExisting
     */
    public function setGatewayAccountId(string $gatewayAccountId): CheckoutExisting
    {
        $this->gatewayAccountId = $gatewayAccountId;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAddons(): ArrayCollection
    {
        return $this->addons;
    }

    /**
     * @return bool
     */
    public function hasAddons(): bool
    {
        return !is_null($this->addons);
    }

    /**
     * @param ArrayCollection $addons
     *
     * @return CheckoutExisting
     */
    public function setAddons(ArrayCollection $addons): CheckoutExisting
    {
        $this->addons = $addons;

        return $this;
    }

    /**
     * @return Subscription
     */
    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }

    /**
     * @return bool
     */
    public function hasSubscription(): bool
    {
        return !is_null($this->subscription);
    }

    /**
     * @param Subscription $subscription
     *
     * @return CheckoutExisting
     */
    public function setSubscription(Subscription $subscription): CheckoutExisting
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * @return int
     */
    public function getBillingCycles(): int
    {
        return $this->billingCycles;
    }

    /**
     * @return bool
     */
    public function hasBillingCycles(): bool
    {
        return !is_null($this->billingCycles);
    }

    /**
     * @param int $billingCycles
     *
     * @return CheckoutExisting
     */
    public function setBillingCycles(int $billingCycles): CheckoutExisting
    {
        $this->billingCycles = $billingCycles;

        return $this;
    }

    /**
     * @return bool
     */
    public function isReplaceAddonList(): bool
    {
        return $this->replaceAddonList;
    }

    /**
     * @return bool
     */
    public function hasReplaceAddonList(): bool
    {
        return !is_null($this->replaceAddonList);
    }

    /**
     * @param bool $replaceAddonList
     *
     * @return CheckoutExisting
     */
    public function setReplaceAddonList(bool $replaceAddonList): CheckoutExisting
    {
        $this->replaceAddonList = $replaceAddonList;

        return $this;
    }

    /**
     * @return int
     */
    public function getTermsToCharge(): int
    {
        return $this->termsToCharge;
    }

    /**
     * @return bool
     */
    public function hasTermsToCharge(): bool
    {
        return !is_null($this->termsToCharge);
    }

    /**
     * @param int $termsToCharge
     *
     * @return CheckoutExisting
     */
    public function setTermsToCharge(int $termsToCharge): CheckoutExisting
    {
        $this->termsToCharge = $termsToCharge;

        return $this;
    }

    /**
     * @return int
     */
    public function getReactivateFrom(): int
    {
        return $this->reactivateFrom;
    }

    /**
     * @return bool
     */
    public function hasReactivateFrom(): bool
    {
        return !is_null($this->reactivateFrom);
    }

    /**
     * @param int $reactivateFrom
     *
     * @return CheckoutExisting
     */
    public function setReactivateFrom(int $reactivateFrom): CheckoutExisting
    {
        $this->reactivateFrom = $reactivateFrom;

        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAlignmentMode(): string
    {
        return $this->billingAlignmentMode;
    }

    /**
     * @return bool
     */
    public function hasBillingAlignmentMode(): bool
    {
        return !is_null($this->billingAlignmentMode);
    }

    /**
     * @param string $billingAlignmentMode
     *
     * @return CheckoutExisting
     */
    public function setBillingAlignmentMode(string $billingAlignmentMode): CheckoutExisting
    {
        $this->billingAlignmentMode = $billingAlignmentMode;

        return $this;
    }

    /**
     * @return bool
     */
    public function isReactivate(): bool
    {
        return $this->reactivate;
    }

    /**
     * @return bool
     */
    public function hasReactivate(): bool
    {
        return !is_null($this->reactivate);
    }

    /**
     * @param bool $reactivate
     *
     * @return CheckoutExisting
     */
    public function setReactivate(bool $reactivate): CheckoutExisting
    {
        $this->reactivate = $reactivate;

        return $this;
    }

    /**
     * @return bool
     */
    public function isForceTermReset(): bool
    {
        return $this->forceTermReset;
    }

    /**
     * @return bool
     */
    public function hasForceTermReset(): bool
    {
        return !is_null($this->forceTermReset);
    }

    /**
     * @param bool $forceTermReset
     *
     * @return CheckoutExisting
     */
    public function setForceTermReset(bool $forceTermReset): CheckoutExisting
    {
        $this->forceTermReset = $forceTermReset;

        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }

    /**
     * @return bool
     */
    public function hasRedirectUrl(): bool
    {
        return !is_null($this->redirectUrl);
    }

    /**
     * @param string $redirectUrl
     *
     * @return CheckoutExisting
     */
    public function setRedirectUrl(string $redirectUrl): CheckoutExisting
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getCancelUrl(): string
    {
        return $this->cancelUrl;
    }

    /**
     * @return bool
     */
    public function hasCancelUrl(): bool
    {
        return !is_null($this->cancelUrl);
    }

    /**
     * @param string $cancelUrl
     *
     * @return CheckoutExisting
     */
    public function setCancelUrl(string $cancelUrl): CheckoutExisting
    {
        $this->cancelUrl = $cancelUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassThruContent(): string
    {
        return $this->passThruContent;
    }

    /**
     * @return bool
     */
    public function hasPassThruContent(): bool
    {
        return !is_null($this->passThruContent);
    }

    /**
     * @param string $passThruContent
     *
     * @return CheckoutExisting
     */
    public function setPassThruContent(string $passThruContent): CheckoutExisting
    {
        $this->passThruContent = $passThruContent;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmbed(): bool
    {
        return $this->embed;
    }

    /**
     * @return bool
     */
    public function hasEmbed(): bool
    {
        return !is_null($this->embed);
    }

    /**
     * @param bool $embed
     *
     * @return CheckoutExisting
     */
    public function setEmbed(bool $embed): CheckoutExisting
    {
        $this->embed = $embed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isIframeMessaging(): bool
    {
        return $this->iframeMessaging;
    }

    /**
     * @return bool
     */
    public function hasIframeMessaging(): bool
    {
        return !is_null($this->iframeMessaging);
    }

    /**
     * @param bool $iframeMessaging
     *
     * @return CheckoutExisting
     */
    public function setIframeMessaging(bool $iframeMessaging): CheckoutExisting
    {
        $this->iframeMessaging = $iframeMessaging;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getResult(): array
    {
        return $this->result->hostedPage()->getValues();
    }
}
