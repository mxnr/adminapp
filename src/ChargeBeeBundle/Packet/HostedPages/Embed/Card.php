<?php

namespace ChargeBeeBundle\Packet\HostedPages\Embed;

use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Card.
 */
class Card implements PacketMethodAttributesInteface
{
    use AttributeHelperTrait;

    /**
     * The gateway account in which this payment source is stored.
     * optional, string, max chars=50.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $gatewayAccountId;

    /**
     * {@inheritDoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(['gatewayAccountId']);
    }

    /**
     * @return string
     */
    public function getGatewayAccountId(): string
    {
        return $this->gatewayAccountId;
    }

    /**
     * @return bool
     */
    public function hasGatewayAccountId(): bool
    {
        return !is_null($this->gatewayAccountId);
    }

    /**
     * @param string|null $gatewayAccountId
     *
     * @return Card
     */
    public function setGatewayAccountId(string $gatewayAccountId = null): Card
    {
        $this->gatewayAccountId = $gatewayAccountId;

        return $this;
    }
}
