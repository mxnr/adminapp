<?php

namespace ChargeBeeBundle\Packet\HostedPages\Embed;

use ChargeBeeBundle\Contract\PacketMethodAttributesInteface;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Customer.
 */
class Customer implements PacketMethodAttributesInteface
{
    use AttributeHelperTrait;

    /**
     * Identifier of the customer.
     * required, string, max chars=50.
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     * @Assert\NotNull()
     */
    private $id;

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        return $this->getAttributesByMap(['id']);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string|null $id
     *
     * @return Customer
     */
    public function setId(string $id = null): Customer
    {
        $this->id = $id;

        return $this;
    }
}
