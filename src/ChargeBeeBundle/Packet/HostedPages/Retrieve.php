<?php

namespace ChargeBeeBundle\Packet\HostedPages;

use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * {@inheritDoc}
 */
class Retrieve extends BasePacketMethod
{
    /**
     * Unique identifier generated for each hosted page requested.
     * optional, string, max chars=70
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="70")
     */
    private $id;

    /**
     * {@inheritDoc}
     */
    public function getAttributes(): array
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;
        $this->result = \ChargeBee_HostedPage::retrieve($id, $this->environment);

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @param string $id
     *
     * @return Retrieve
     */
    public function setId(string $id): Retrieve
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getResult(): array
    {
        return $this->result->hostedPage()->getValues();
    }
}
