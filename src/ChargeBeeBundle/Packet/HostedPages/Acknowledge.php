<?php

namespace ChargeBeeBundle\Packet\HostedPages;

use ChargeBee_HostedPage;
use ChargeBeeBundle\Contract\PacketMethodInterface;

/**
 * {@inheritDoc}
 */
class Acknowledge extends Retrieve
{
    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $id = $this->hasId() ? $this->getId() : null;
        $this->result = ChargeBee_HostedPage::acknowledge($id, $this->environment);

        return $this;
    }
}
