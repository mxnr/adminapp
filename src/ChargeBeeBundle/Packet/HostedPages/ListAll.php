<?php

namespace ChargeBeeBundle\Packet\HostedPages;

use ChargeBee_HostedPage;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * {@inheritDoc}
 */
class ListAll extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * @var \ChargeBee_ListResult
     */
    protected $result;

    /**
     *
     * limit
     * Limits the number of resources to be returned.
     * optional, integer, default=10, min=1, max=100
     *
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1", max="100")
     */
    private $limit = 10;

    /**
     * offset
     * Allows you to fetch the next set of resources. The value used for this parameter must be the value returned for
     * next_offset parameter in the previous API call.
     * optional, string, max chars=1000 Filter Params
     *
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="1000")
     */
    private $offset;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"is","isNot","startsWith","in","notIn"})
     */
    private $idOperator;

    /**
     * @var string
     */
    private $id;

    /**
     * type[<operator>]
     * To filter based on HostedPage Type. Possible values are : checkout_new, checkout_existing,
     * update_payment_method, manage_payment_sources, collect_now. Supported operators : is, isNot, in, notIn
     *
     * Example → "type[is]" => "checkout_new"
     * optional, enumerated string filter
     *
     * @var string
     * @Assert\Choice(choices={"is","isNot","in","notIn"})
     */
    private $typeOperator;

    /**
     * @var string
     * @Assert\Choice(choices={"checkout_new","checkout_existing"})
     */
    private $type;

    /**
     * state[<operator>]
     * To filter based on HostedPage State. Possible values are : created, requested, succeeded, cancelled,
     * acknowledged. Supported operators : is, isNot, in, notIn
     *
     * Example → "state[is]" => "succeeded"
     * optional, enumerated string filter
     *
     * @var string
     * @Assert\Choice(choices={"is","isNot","in","notIn"})
     */
    private $stateOperator;

    /**
     * @var string
     *
     * @Assert\Choice(choices={"created","requested","succeeded","cancelled"})
     */
    private $state;

    /**
     * updatedAt[<operator>]
     * To filter based on Updated At.
     * Supported operators : after, before, on, between
     *
     * Example → "updatedAt[on]" => "1490784813"
     * optional, timestamp(UTC) in seconds filter
     *
     * @var string
     *
     * @Assert\Choice(choices={"after","before","on","between"})
     */
    private $updatedAtOperator;

    /**
     * @var int
     *
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    private $updatedAt;

    /**
     * @param string $operator
     * @param array  $updatedAt
     *
     * @return ListAll
     */
    public function setUpdatedAtFilter(string $operator, array $updatedAt): ListAll
    {
        $this->updatedAtOperator = $operator;
        if (in_array($operator, ["between"])) {
            $this->updatedAt = $updatedAt;
        } else {
            $this->updatedAt = array_pop($updatedAt);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $state
     *
     * @return ListAll
     */
    public function setStateFilter(string $operator, array $state): ListAll
    {
        $this->stateOperator = $operator;
        if (in_array($operator, ["in", "notIn"])) {
            $this->state = $state;
        } else {
            $this->state = array_pop($state);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $type
     *
     * @return ListAll
     */
    public function setTypeFilter(string $operator, array $type): ListAll
    {
        $this->typeOperator = $operator;

        if (in_array($operator, ["in", "notIn"])) {
            $this->type = $type;
        } else {
            $this->type = array_pop($type);
        }

        return $this;
    }

    /**
     * @param string $operator
     * @param array  $id
     *
     * @return ListAll
     */
    public function setIdFilter(string $operator, array $id): ListAll
    {
        $this->idOperator = $operator;
        if (in_array($operator, ['in', 'notIn'])) {
            $this->id = $id;
        } else {
            $this->id = array_pop($id);
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function execute(): PacketMethodInterface
    {
        $this->result = ChargeBee_HostedPage::all($this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getAttributes(): array
    {
        $attributes = $this->getAttributesByMap(['limit', 'offset']);
        $attributes = $this->getAttributesByMap(
            ['idFilter', 'typeFilter', 'stateFilter', 'updatedAtFilter'],
            $attributes,
            true
        );

        return $attributes;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return bool
     */
    public function hasLimit(): bool
    {
        return !is_null($this->limit);
    }

    /**
     * @param int $limit
     *
     * @return ListAll
     */
    public function setLimit(int $limit): ListAll
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return string
     */
    public function getOffset(): string
    {
        return $this->offset;
    }

    /**
     * @return bool
     */
    public function hasOffset(): bool
    {
        return !is_null($this->offset);
    }

    /**
     * @param string $offset
     *
     * @return ListAll
     */
    public function setOffset(string $offset): ListAll
    {
        $this->offset = $offset;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasIdFilter(): bool
    {
        return !is_null($this->id);
    }

    /**
     * @return array
     */
    public function getIdFilter(): array
    {
        return ['id[' . $this->idOperator . ']' => $this->id];
    }

    /**
     * @return bool
     */
    public function hasTypeFilter(): bool
    {
        return !is_null($this->type);
    }

    /**
     * @return array
     */
    public function getTypeFilter(): array
    {
        return ['type[' . $this->typeOperator . ']' => $this->type];
    }

    /**
     * @return bool
     */
    public function hasStateFilter(): bool
    {
        return !is_null($this->state);
    }

    /**
     * @return array
     */
    public function getStateFilter(): array
    {
        return ['state[' . $this->stateOperator . ']' => $this->state];
    }

    /**
     * @return bool
     */
    public function hasUpdatedAtFilter(): bool
    {
        return !is_null($this->updatedAt);
    }

    /**
     * @return array
     */
    public function getUpdatedAtFilter(): array
    {
        return ['updatedAt[' . $this->updatedAtOperator . ']' => $this->updatedAt];
    }

    /**
     * {@inheritDoc}
     */
    public function getResult(): array
    {
        $list = [];

        /** @var ChargeBee_HostedPage $item */
        foreach ($this->result as $item) {
            $list[] = $item->getValues();
        }

        return [
            'nextOffset' => $this->result->nextOffset(),
            'list' => $list,
        ];
    }
}
