<?php

namespace ChargeBeeBundle\Packet\HostedPages;

use ChargeBee_HostedPage;
use ChargeBeeBundle\Contract\PacketMethodInterface;
use ChargeBeeBundle\Packet\BasePacketMethod;
use ChargeBeeBundle\Packet\Helper\AttributeHelperTrait;
use ChargeBeeBundle\Packet\HostedPages\Embed\Card;
use ChargeBeeBundle\Packet\HostedPages\Embed\Customer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UpdatePaymentMethod.
 */
class UpdatePaymentMethod extends BasePacketMethod
{
    use AttributeHelperTrait;

    /**
     * redirectUrl
     * The customers will be redirected to this URL upon successful checkout. The hosted page id and state will be
     * passed as parameters to this URL. This parameter is not applicable for iframe messaging. Note : Redirect URL
     * configured in Settings > Hosted Pages Settings would be overriden by this redirect URL. Eg :
     * http://yoursite.com?id=<hosted_page_id>&state=succeeded. optional, string, max chars=250
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="250")
     */
    private $redirectUrl;

    /**
     * cancelUrl
     * The customers will be redirected to this URL upon canceling checkout. The hosted page id and state will be
     * passed as parameters to this URL. This parameter is not applicable for iframe messaging. Note : Cancel URL
     * configured in Settings > Hosted Pages Settings would be overriden by this cancel URL. Eg :
     * http://yoursite.com?id=<hosted_page_id>&state=cancelled. optional, string, max chars=250
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="250")
     */
    private $cancelUrl;

    /**
     * passThruContent
     * You can pass through any content specific to the hosted page request and get it back after user had submitted
     * the hosted page. optional, string, max chars=2048
     *
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="2048")
     */
    private $passThruContent;

    /**
     * embed
     * If true then hosted page formatted to be shown in in-app iframe embed.If false, it is formatted to be shown as a
     * separate page . optional, boolean, default=true
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $embed = true;

    /**
     * iframeMessaging
     * If true then iframe will communicate with the parent window. Applicable only for embedded(iframe) hosted pages.
     * If you're using iframe_messaging you need to implement onSuccess & onCancel callbacks. To know more about
     * iframe_messaging please refer to this tutorial. optional, boolean, default=false
     *
     * @var bool
     * @Assert\Type(type="boolean")
     * @Assert\NotNull()
     */
    private $iframeMessaging = false;

    /**
     * @var Customer
     *
     * @Assert\Valid()
     */
    private $customer;

    /**
     * @var Card
     *
     * @Assert\Valid()
     */
    private $card;

    /**
     * @return Card
     */
    public function getCard(): Card
    {
        return $this->card;
    }

    /**
     * @return bool
     */
    public function hasCard(): bool
    {
        return !is_null($this->card);
    }

    /**
     * @param Card|null $card
     *
     * @return UpdatePaymentMethod
     */
    public function setCard(Card $card = null): UpdatePaymentMethod
    {
        $this->card = $card;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(): PacketMethodInterface
    {
        $this->result = ChargeBee_HostedPage::updatePaymentMethod($this->getAttributes(), $this->environment);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttributes(): array
    {
        $attributes = $this->getAttributesByMap(
            [
                'redirectUrl',
                'cancelUrl',
                'passThruContent',
                [
                    'get' => 'isEmbed',
                    'has' => 'hasEmbed',
                ],
                [
                    'get' => 'isIframeMessaging',
                    'has' => 'hasIframeMessaging',
                ],
                'customer',
            ]
        );

        return $attributes;
    }

    /**
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }

    /**
     * @return bool
     */
    public function hasRedirectUrl(): bool
    {
        return !is_null($this->redirectUrl);
    }

    /**
     * @param string|null $redirectUrl
     *
     * @return UpdatePaymentMethod
     */
    public function setRedirectUrl(string $redirectUrl = null): UpdatePaymentMethod
    {
        $this->redirectUrl = $redirectUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getCancelUrl(): string
    {
        return $this->cancelUrl;
    }

    /**
     * @return bool
     */
    public function hasCancelUrl(): bool
    {
        return !is_null($this->cancelUrl);
    }

    /**
     * @param string|null $cancelUrl
     *
     * @return UpdatePaymentMethod
     */
    public function setCancelUrl(string $cancelUrl = null): UpdatePaymentMethod
    {
        $this->cancelUrl = $cancelUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassThruContent(): string
    {
        return $this->passThruContent;
    }

    /**
     * @return bool
     */
    public function hasPassThruContent(): bool
    {
        return !is_null($this->passThruContent);
    }

    /**
     * @param string|null $passThruContent
     *
     * @return UpdatePaymentMethod
     */
    public function setPassThruContent(string $passThruContent = null): UpdatePaymentMethod
    {
        $this->passThruContent = $passThruContent;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmbed(): bool
    {
        return $this->embed;
    }

    /**
     * @return bool
     */
    public function hasEmbed(): bool
    {
        return !is_null($this->embed);
    }

    /**
     * @param bool|null $embed
     *
     * @return UpdatePaymentMethod
     */
    public function setEmbed(bool $embed = null): UpdatePaymentMethod
    {
        $this->embed = $embed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isIframeMessaging(): bool
    {
        return $this->iframeMessaging;
    }

    /**
     * @return bool
     */
    public function hasIframeMessaging(): bool
    {
        return !is_null($this->iframeMessaging);
    }

    /**
     * @param bool|null $iframeMessaging
     *
     * @return UpdatePaymentMethod
     */
    public function setIframeMessaging(bool $iframeMessaging = null): UpdatePaymentMethod
    {
        $this->iframeMessaging = $iframeMessaging;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return bool
     */
    public function hasCustomer(): bool
    {
        return !is_null($this->customer);
    }

    /**
     * @param Customer|null $customer
     *
     * @return UpdatePaymentMethod
     */
    public function setCustomer(Customer $customer = null): UpdatePaymentMethod
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getResult(): array
    {
        return [
            'hostedPage' => $this->result->hostedPage()->getValues(),
        ];
    }
}
