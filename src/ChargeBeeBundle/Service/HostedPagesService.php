<?php

namespace ChargeBeeBundle\Service;

use ChargeBeeBundle\Packet\BasePacket;
use ChargeBeeBundle\Packet\HostedPages\Acknowledge;
use ChargeBeeBundle\Packet\HostedPages\CheckoutExisting;
use ChargeBeeBundle\Packet\HostedPages\ListAll;
use ChargeBeeBundle\Packet\HostedPages\Retrieve;
use ChargeBeeBundle\Packet\HostedPages\UpdatePaymentMethod;

/**
 * Class HostedPagesService.
 */
class HostedPagesService extends BasePacket
{
    /**
     * @param Acknowledge $acknowledgeMethod
     *
     * @return array
     */
    public function acknowledge(Acknowledge $acknowledgeMethod): array
    {
        return $this->processPacketMethod($acknowledgeMethod);
    }

    /**
     * @param CheckoutExisting $checkoutExistingMethod
     *
     * @return array
     */
    public function checkoutExisting(CheckoutExisting $checkoutExistingMethod): array
    {
        return $this->processPacketMethod($checkoutExistingMethod);
    }

    /**
     * @param ListAll $listAllMethod
     *
     * @return array
     */
    public function listAll(ListAll $listAllMethod): array
    {
        return $this->processPacketMethod($listAllMethod);
    }

    /**
     * @param Retrieve $retrieveMethod
     *
     * @return array
     */
    public function retrieve(Retrieve $retrieveMethod): array
    {
        return $this->processPacketMethod($retrieveMethod);
    }

    /**
     * @param UpdatePaymentMethod $updatePaymentMethod
     *
     * @return array
     */
    public function updatePaymentMethod(UpdatePaymentMethod $updatePaymentMethod): array
    {
        return $this->processPacketMethod($updatePaymentMethod);
    }
}
