<?php

namespace ChargeBeeBundle\Service;

use ChargeBee_Environment;

/**
 * Class EnvironmentService.
 */
class EnvironmentService
{
    /**
     * @var ChargeBee_Environment
     */
    protected $evnInstance = null;

    /**
     * Environment constructor.
     *
     * @param string $site
     * @param string $key
     */
    public function __construct(string $site, string $key)
    {
        $this->evnInstance = new ChargeBee_Environment($site, $key);
    }

    /**
     * @return ChargeBee_Environment
     */
    public function getEnvInstance(): ChargeBee_Environment
    {
        return $this->evnInstance;
    }
}
