<?php

namespace ChargeBeeBundle\Service;

use ChargeBeeBundle\Packet\BasePacket;
use ChargeBeeBundle\Packet\Invoices\InvoiceForAddon;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class InvoicesService extends BasePacket
{
    /**
     * @var string
     */
    private $buyDomainAddonId;

    /**
     * InvoicesService constructor.
     * @param EnvironmentService $environmentService
     * @param ValidatorInterface $validatorService
     * @param string $buyDomainAddonId
     */
    public function __construct(
        EnvironmentService $environmentService,
        ValidatorInterface $validatorService,
        string $buyDomainAddonId
    )
    {
        parent::__construct($environmentService, $validatorService);
        $this->buyDomainAddonId = $buyDomainAddonId;
    }

    /**
     * @param string $customerId
     * @param int $price
     * @return array
     */
    public function chargeAddon(string $customerId, int $price): array
    {
        $invoiceForAddonMethod = $this->getAddonInvoice($customerId, $this->buyDomainAddonId, $price);

        return $this->processPacketMethod($invoiceForAddonMethod);
    }

    /**
     * @param string $customerId
     * @param string $addonId
     * @param int $price
     * @return InvoiceForAddon
     */
    private function getAddonInvoice(string $customerId, string $addonId, int $price)
    {

        return (new InvoiceForAddon())
            ->setCustomerId($customerId)
            ->setAddonId($addonId)
            ->setAddonUnitPrice($price);
    }
}
