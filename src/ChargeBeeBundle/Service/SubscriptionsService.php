<?php

namespace ChargeBeeBundle\Service;

use ChargeBeeBundle\Packet\BasePacket;
use ChargeBeeBundle\Packet\Subscriptions\Cancel;
use ChargeBeeBundle\Packet\Subscriptions\Create;
use ChargeBeeBundle\Packet\Subscriptions\ListAll;
use ChargeBeeBundle\Packet\Subscriptions\Retrieve;

/**
 * Class SubscriptionsService.
 */
class SubscriptionsService extends BasePacket
{
    /**
     * @param Create $createMethod
     *
     * @return array
     */
    public function create(Create $createMethod): array
    {
        return $this->processPacketMethod($createMethod);
    }

    /**
     * @param ListAll $listAllMethod
     *
     * @return array
     */
    public function listAll(ListAll $listAllMethod): array
    {
        return $this->processPacketMethod($listAllMethod);
    }

    /**
     * @param Cancel $cancelMethod
     *
     * @return array
     */
    public function cancel(Cancel $cancelMethod): array
    {
        return $this->processPacketMethod($cancelMethod);
    }

    /**
     * @param Retrieve $retrieveMethod
     *
     * @return array
     */
    public function retrieve(Retrieve $retrieveMethod): array
    {
        return $this->processPacketMethod($retrieveMethod);
    }
}
