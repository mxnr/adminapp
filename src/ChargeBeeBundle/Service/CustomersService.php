<?php

namespace ChargeBeeBundle\Service;

use ChargeBeeBundle\Packet\BasePacket;
use ChargeBeeBundle\Packet\Customers\AddContacts;
use ChargeBeeBundle\Packet\Customers\AssignPaymentRole;
use ChargeBeeBundle\Packet\Customers\CollectPayment;
use ChargeBeeBundle\Packet\Customers\Create;
use ChargeBeeBundle\Packet\Customers\Delete;
use ChargeBeeBundle\Packet\Customers\DeleteContacts;
use ChargeBeeBundle\Packet\Customers\ListAll;
use ChargeBeeBundle\Packet\Customers\RecordAnExcessPayment;
use ChargeBeeBundle\Packet\Customers\Retrieve;
use ChargeBeeBundle\Packet\Customers\Update;
use ChargeBeeBundle\Packet\Customers\UpdateBillingInfo;
use ChargeBeeBundle\Packet\Customers\UpdateContancts;
use ChargeBeeBundle\Packet\Customers\UpdatePaymentMethod;

/**
 * Class CustomersService.
 */
class CustomersService extends BasePacket
{
    /**
     * @param Create $createMethod
     *
     * @return array
     */
    public function create(Create $createMethod): array
    {
        return $this->processPacketMethod($createMethod);
    }

    /**
     * @param ListAll $listAllMethod
     *
     * @return array
     */
    public function listAll(ListAll $listAllMethod): array
    {
        return $this->processPacketMethod($listAllMethod);
    }

    /**
     * @param Retrieve $retrieveMethod
     *
     * @return array
     */
    public function retrieve(Retrieve $retrieveMethod): array
    {
        return $this->processPacketMethod($retrieveMethod);
    }

    /**
     * @param Update $updateMethod
     *
     * @return array
     */
    public function update(Update $updateMethod): array
    {
        return $this->processPacketMethod($updateMethod);
    }

    /**
     * @param UpdatePaymentMethod $updatePaymentMethodMethod
     *
     * @return array
     */
    public function updatePaymentMethod(UpdatePaymentMethod $updatePaymentMethodMethod): array
    {
        return $this->processPacketMethod($updatePaymentMethodMethod);
    }

    /**
     * @param UpdateBillingInfo $updateBillingInfoMethod
     *
     * @return array
     */
    public function updateBillingInfo(UpdateBillingInfo $updateBillingInfoMethod): array
    {
        return $this->processPacketMethod($updateBillingInfoMethod);
    }

    /**
     * @param AssignPaymentRole $assignPaymentRoleMethod
     *
     * @return array
     */
    public function assignPaymentRole(AssignPaymentRole $assignPaymentRoleMethod): array
    {
        return $this->processPacketMethod($assignPaymentRoleMethod);
    }

    /**
     * @param AddContacts $addContactsMethod
     *
     * @return array
     */
    public function addContacts(AddContacts $addContactsMethod): array
    {
        return $this->processPacketMethod($addContactsMethod);
    }

    /**
     * @param UpdateContancts $updateContanctsMethod
     *
     * @return array
     */
    public function updateContacts(UpdateContancts $updateContanctsMethod): array
    {
        return $this->processPacketMethod($updateContanctsMethod);
    }

    /**
     * @param DeleteContacts $deleteContactsMethod
     *
     * @return array
     */
    public function deleteContacts(DeleteContacts $deleteContactsMethod): array
    {
        return $this->processPacketMethod($deleteContactsMethod);
    }

    /**
     * @param RecordAnExcessPayment $recordAnExcessPaymentMethod
     *
     * @return array
     */
    public function recordAnExpressPayment(RecordAnExcessPayment $recordAnExcessPaymentMethod): array
    {
        return $this->processPacketMethod($recordAnExcessPaymentMethod);
    }

    /**
     * @param CollectPayment $collectPaymentMethod
     *
     * @return array
     */
    public function collectPayment(CollectPayment $collectPaymentMethod): array
    {
        return $this->processPacketMethod($collectPaymentMethod);
    }

    /**
     * @param Delete $deleteMethod
     *
     * @return array
     */
    public function delete(Delete $deleteMethod): array
    {
        return $this->processPacketMethod($deleteMethod);
    }
}
