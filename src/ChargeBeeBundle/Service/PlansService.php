<?php

namespace ChargeBeeBundle\Service;

use ChargeBeeBundle\Packet\BasePacket;
use ChargeBeeBundle\Packet\Plans\Create;
use ChargeBeeBundle\Packet\Plans\Delete;
use ChargeBeeBundle\Packet\Plans\ListAll;
use ChargeBeeBundle\Packet\Plans\Update;

/**
 * Class PlansService.
 */
class PlansService extends BasePacket
{
    /**
     * @param Create $createMethod
     *
     * @return array
     */
    public function create(Create $createMethod): array
    {
        return $this->processPacketMethod($createMethod);
    }

    /**
     * @param Update $updateMethod
     *
     * @return array
     */
    public function update(Update $updateMethod): array
    {
        return $this->processPacketMethod($updateMethod);
    }

    /**
     * @param ListAll $listAllMethod
     *
     * @return array
     */
    public function listAll(ListAll $listAllMethod): array
    {
        return $this->processPacketMethod($listAllMethod);
    }

    /**
     * @param Delete $deleteMethod
     *
     * @return array
     */
    public function delete(Delete $deleteMethod): array
    {
        return $this->processPacketMethod($deleteMethod);
    }
}
