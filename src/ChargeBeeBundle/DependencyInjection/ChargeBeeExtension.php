<?php

namespace ChargeBeeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

/**
 * Class ChargeBeeExtension.
 */
class ChargeBeeExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $configuration = new Configuration();

        //reading config
        $config = $processor->processConfiguration($configuration, $configs);

        //preparing bundle service params
        $container->setParameter('charge_bee_site', $config['site']);
        $container->setParameter('charge_bee_key', $config['key']);
        $container->setParameter('charge_bee_addon_buy_domain_id', $config['addon_buy_domain_id']);
    }
}
