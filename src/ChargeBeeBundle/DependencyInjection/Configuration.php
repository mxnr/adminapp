<?php

namespace ChargeBeeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('charge_bee');

        $rootNode
            ->children()
            ->scalarNode('site')->defaultValue('')->end()
            ->scalarNode('key')->defaultValue('')->end()
            ->scalarNode('addon_buy_domain_id')->defaultValue('')->end();

        return $treeBuilder;
    }
}
