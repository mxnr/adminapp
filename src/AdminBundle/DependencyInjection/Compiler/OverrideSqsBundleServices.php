<?php

namespace AdminBundle\DependencyInjection\Compiler;

use AdminBundle\Service\SQS\BatchWorker;
use AdminBundle\Service\SQS\QueueFactory;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class OverrideServiceCompilerPass.
 */
class OverrideSqsBundleServices implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $container->getDefinition('tritran.sqs_queue.queue_worker')
            ->setClass(BatchWorker::class)
            ->addMethodCall('setLogger', [new Reference('logger')]);

        $container->getDefinition('tritran.sqs_queue.queue_factory')
            ->setClass(QueueFactory::class);
    }
}
