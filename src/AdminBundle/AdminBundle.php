<?php

namespace AdminBundle;

use AdminBundle\DependencyInjection\Compiler\OverrideSqsBundleServices;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AdminBundle.
 */
class AdminBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideSqsBundleServices());
    }
}
