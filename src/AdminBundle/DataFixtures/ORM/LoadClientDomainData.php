<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;
use ThreeWebOneEntityBundle\Entity\User;

class LoadClientDomainData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $userConfigList = $this->getUserConfig();

        foreach ($userConfigList as &$item) {

            $this->prepareRow($item);
            /**@var User $user*/
            $user = $this->getReference($item['reference']);

            $this->createSubDomain($user);
        }

        $this->manager->flush();
    }

    /**
     * Create Sub Domain
     *
     * @param User $user
     */
    private function createSubDomain(User $user)
    {
        $clientDomain = (new ClientDomain())
            ->setActive(ClientDomain::ACTIVE)
            ->setDomainType(ClientDomain::SUB_DOMAIN_TYPE)
            ->setOwner($user)
            ->setDomain(str_replace("_", "-", $user->getUsername() . '.' . ClientDomain::CLIENT_TLD))
            ->setStatus(ClientDomain::ADDED_MANUALLY);

        $this->manager->persist($clientDomain);
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 11;
    }
}
