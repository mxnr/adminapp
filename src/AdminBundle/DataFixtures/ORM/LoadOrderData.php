<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\User;

class LoadOrderData extends AbstractDataFixture
{
    const ORDERS_COUNT = 2;

    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     * @return bool
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $userConfigList = $this->getUserConfig();
        $ordersConfigList = $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/orders.csv');

        foreach ($userConfigList as &$userConfig) {
            $this->prepareRow($userConfig);

            /**@var User $user */
            $user = $this->getReference($userConfig['reference']);

            foreach ($ordersConfigList as &$orderConfig) {
                $this->prepareRow($orderConfig);

                $this->createSellingOrders($user, $orderConfig, $userConfig['orders']);
                $this->createBuybackRepairOrders($user, $orderConfig, $userConfig['orders']);
            }
        }
        $this->manager->flush();
    }

    /**
     * Create Selling Orders
     *
     * @param User $user
     * @param array $orderConfig
     * @param String $orderType
     */
    private function createSellingOrders(User $user, array $orderConfig, String $orderType)
    {
        $inventoryItems = $user->getInventoryItems();

        /**@var InventoryItem $inventoryItem */
        foreach ($inventoryItems as $key => $inventoryItem) {
            $inventoryItemSiteType = $inventoryItem->getPriceType()->getSite();

            if ($inventoryItemSiteType == SiteTypeInterface::SALE && in_array($orderType, self::ORDER_BUYBACK_REPAIR_LIST)) {
                $order = $this->createOrder($orderConfig, $user, SiteTypeInterface::SALE);

                $inventoryItem->addOrder($order);
                $inventoryItem->setQuantity(rand(1, 10));
                $this->manager->persist($order);

                $this->getContainer()->get('admin.inventory_item_service')
                    ->generateBarcodes($inventoryItem, $order, $user);
            }
        }
    }

    /**
     * Create Order
     *
     * @param array $orderConfig
     * @param User $user
     * @param int $siteType
     * @return Order
     */
    private function createOrder(array $orderConfig, User $user, int $siteType): Order
    {
        return (new Order())
            ->setFirstName($orderConfig['first_name'])
            ->setLastName($orderConfig['last_name'])
            ->setPhone($orderConfig['phone'])
            ->setEmail($orderConfig['email'])
            ->setSiteType($siteType)
            ->setStatus(Order::NEW_ORDER)
            ->setDeliveryType(Order::PICK_UP)
            ->setPaymentType(Order::CASH)
            ->setTermsChecked(true)
            ->setOwner($user);
    }

    /**
     * Create Buyback or Repair Orders
     *
     * @param User $user
     * @param array $orderConfig
     * @param String $orderType
     */
    private function createBuybackRepairOrders(User $user, array $orderConfig, String $orderType)
    {
        $userPrices = $user->getPrices();
        $buybackOrdersCount = 0;
        $repairOrdersCount = 0;

        /**@var Price $userPrice */
        foreach ($userPrices as $key => $userPrice) {
            $siteType = $userPrice->getPriceType()->getSite();
            if ($this->isAbleCreateBuybackOrder($siteType, $orderType) && $buybackOrdersCount < self::ORDERS_COUNT) {
                $this->createBuybackRepairOrder($user, $orderConfig, $siteType, $userPrice);
                $buybackOrdersCount++;
            }

            if ($this->isAbleCreateRepairOrder($siteType, $orderType) && $repairOrdersCount < self::ORDERS_COUNT) {
                $this->createBuybackRepairOrder($user, $orderConfig, $siteType, $userPrice);
                $repairOrdersCount++;
            }
        }
    }

    /**
     * Is Able To Create Buyback Order
     *
     * @param int $siteType
     * @param String $orderType
     * @return bool
     */
    private function isAbleCreateBuybackOrder(int $siteType, String $orderType)
    {
        return $siteType == SiteTypeInterface::BUYBACK && in_array($orderType, self::ORDER_BUYBACK_REPAIR_LIST);
    }

    /**
     * Create Buyback or Repair Order
     *
     * @param User $user
     * @param array $orderConfig
     * @param int $siteType
     * @param Price $price
     */
    private function createBuybackRepairOrder(User $user, array $orderConfig, int $siteType, Price $price)
    {
        $order = $this->createOrder($orderConfig, $user, $siteType);

        $orderItem = new InventoryItem();
        $orderItem
            ->setPurchasePrice($price->getValue())
            ->setTitle($price->getModel()->getTitle())
            ->setOwner($user)
            ->setModel($price->getModel());

        $this->manager->persist($orderItem);
        $order->addItem($orderItem);
        $this->manager->persist($order);
    }

    /**
     * Is Able To Create Repair Order
     *
     * @param int $siteType
     * @param String $orderType
     * @return bool
     */
    private function isAbleCreateRepairOrder(int $siteType, String $orderType)
    {
        return $siteType == SiteTypeInterface::REPAIR && in_array($orderType, self::ORDER_BUYBACK_REPAIR_LIST);
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 120;
    }
}
