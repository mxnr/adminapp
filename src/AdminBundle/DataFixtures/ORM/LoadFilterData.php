<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\FilterImage;
use ThreeWebOneEntityBundle\Entity\User;

class LoadFilterData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $resources = [
            '@AdminBundle/Resources/fixtures/capacity.csv',
            '@AdminBundle/Resources/fixtures/providers.csv',
            '@AdminBundle/Resources/fixtures/color.csv',
            '@AdminBundle/Resources/fixtures/brands.csv',
        ];

        $users = $this->getUserReferencesBySyncType();

        foreach ($resources as $resource) {
            $filterArray = $this->loadDataFileAsArray($resource);
            $this->createAdminFilters($filterArray, $users['sync']);
            $this->createUserFilters($filterArray, $users['all']);
        }
        $this->manager->flush();
    }

    /**
     * Creates Admin Filters
     *
     * @param $filterArray
     * @param $syncUsers
     */
    public function createAdminFilters($filterArray, $syncUsers)
    {
        foreach ($filterArray as &$item) {
            $this->prepareRow($item);
            if (!($this->hasReference(self::SUPER_ADMIN_LINK))) {
                break;
            }
            $filter = (new Filter())
                ->setTitle($item['title'])
                ->setStatus($item['status'])
                ->setType($item['type'])
                ->setOwner($this->getReference(self::SUPER_ADMIN_LINK))
                ->addUser($this->getReference(self::SUPER_ADMIN_LINK));

            foreach ($syncUsers as $userLink) {
                if (!($this->hasReference($userLink))) {
                    break;
                }
                $filter->addUser($this->getReference($userLink));
            }

            if (!empty($item['image'])) {
                $this->createEntityImage($item['image'], $filter, $this->getReference(self::SUPER_ADMIN_LINK));
            }
            $this->setReference($item['reference_id'], $filter);

            if ($item['parent_id'] != 'null') {
                $filter->setParent($this->getReference($item['parent_id']));
            }
            $this->manager->persist($filter);
        }
    }

    /**
     * Creates FilterImage Entity
     *
     * @param string $name
     * @param Filter $filter
     * @param User $user
     */
    public function createEntityImage($name, $filter, $user)
    {
        $image = (new FilterImage())
            ->setImageName($name)
            ->setEntity($filter)
            ->setOwner($user)
            ->addUser($user);
        $this->manager->persist($image);
    }

    /**
     * Creates User Filters
     *
     * @param filterArray
     * @param $hostUsers
     */
    public function createUserFilters($filterArray, $hostUsers)
    {
        foreach ($hostUsers as $userLink) {
            if (!($this->hasReference($userLink))) {
                break;
            }
            $user = $this->getReference($userLink);

            foreach ($filterArray as &$item) {
                $this->prepareRow($item);
                $filter = (new Filter())
                    ->setTitle($user->getUsername() . "'s " . $item['title'])
                    ->setStatus($item['status'])
                    ->setType($item['type'])
                    ->setOwner($user)
                    ->addUser($user);

                if (!empty($item['image'])) {
                    $this->createEntityImage($item['image'], $filter, $user);
                }
                $this->setReference($userLink . '_' . $item['reference_id'], $filter);

                if ($item['parent_id'] != 'null') {
                    $filter->setParent($this->getReference($item['parent_id']));
                }
                $this->manager->persist($filter);
            }
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 30;
    }
}
