<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\CategoryImage;
use ThreeWebOneEntityBundle\Entity\User;

class LoadProductData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $users = $this->getUserReferencesBySyncType();
        $productArray = $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/products.csv');

        $this->createAdminProducts($productArray, $users['sync']);
        $this->createUserProducts($productArray, $users['all']);

        $this->manager->flush();
    }

    /**
     * Creates Admin Products
     *
     * @param $productArray
     * @param $syncUsers
     */
    public function createAdminProducts($productArray, $syncUsers)
    {
        foreach ($productArray as &$item) {
            if (!($this->hasReference(self::SUPER_ADMIN_LINK))) {
                break;
            }
            $this->prepareRow($item);
            $product = (new Category())
                ->setTitle($item['title'])
                ->setStatus($item['status'])
                ->setType(Category::TYPE_PRODUCT)
                ->setOwner($this->getReference(self::SUPER_ADMIN_LINK))
                ->addUser($this->getReference(self::SUPER_ADMIN_LINK));

            foreach ($syncUsers as $userLink) {
                if (!($this->hasReference($userLink))) {
                    break;
                }
                $product->addUser($this->getReference($userLink));
            }

            if (!empty($item['image'])) {
                $this->createEntityImage($item['image'], $product, $this->getReference(self::SUPER_ADMIN_LINK));
            }

            $this->setReference($item['reference_id'], $product);
            $this->manager->persist($product);
        }
    }

    /**
     * Creates Product's Image
     *
     * @param string $name
     * @param Category $product
     * @param User $user
     */
    public function createEntityImage($name, $product, $user)
    {
        $image = new CategoryImage();
        $image->setImageName($name)
            ->setEntity($product)
            ->setOwner($user)
            ->addUser($user);
        $this->manager->persist($image);
    }

    /**
     * Creates User Products
     *
     * @param $productArray
     * @param $hostUsers
     */
    public function createUserProducts($productArray, $hostUsers)
    {
        foreach ($hostUsers as $userLink) {
            if (!($this->hasReference($userLink))) {
                break;
            }
            $user = $this->getReference($userLink);

            foreach ($productArray as &$item) {
                $this->prepareRow($item);
                $product = new Category();
                $product->setTitle($user->getUsername() . "'s " . $item['title'])
                    ->setStatus($item['status'])
                    ->setType(Category::TYPE_PRODUCT)
                    ->setOwner($user)
                    ->addUser($user);

                if (!empty($item['image'])) {
                    $this->createEntityImage($item['image'], $product, $user);
                }
                $this->setReference($userLink . '_' . $item['reference_id'], $product);
                $this->manager->persist($product);
            }
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 80;
    }
}
