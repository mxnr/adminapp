<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractDataFixture extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface, FixtureInterface
{
    use ContainerAwareTrait;

    const USERS_PASSWORD = 'password';
    const SUPER_ADMIN_LINK = 'u0';

    /**
     * Synchronization types
     */
    const SYNCHRONIZATION_TYPE_NO = 0;
    const SYNCHRONIZATION_TYPE_ALL = 1;
    const SYNCHRONIZATION_TYPE_SELLING = 2;
    const SYNCHRONIZATION_TYPE_BUYBACK = 3;
    const SYNCHRONIZATION_TYPE_REPAIR = 4;

    /**
     * Billing types
     */
    const BILLING_TYPE_NO = 0;
    const BILLING_TYPE_ALL = 1;
    const BILLING_TYPE_SELLING_BUYBACK = 2;
    const BILLING_TYPE_SELLING_REPAIR = 3;
    const BILLING_TYPE_BUYBACK_REPAIR = 4;
    const BILLING_TYPE_SELLING = 5;
    const BILLING_TYPE_BUYBACK = 6;
    const BILLING_TYPE_REPAIR = 7;
    const BILLING_TYPE_SUPER_ADMIN = 100;

    const BILLING_ITEMS = [
        self::BILLING_TYPE_NO => ['sell' => false, 'buy' => false, 'repair' => false],
        self::BILLING_TYPE_ALL => ['sell' => true, 'buy' => true, 'repair' => true],
        self::BILLING_TYPE_SELLING_BUYBACK => ['sell' => true, 'buy' => true, 'repair' => false],
        self::BILLING_TYPE_SELLING_REPAIR => ['sell' => true, 'buy' => false, 'repair' => true],
        self::BILLING_TYPE_BUYBACK_REPAIR => ['sell' => false, 'buy' => true, 'repair' => true],
        self::BILLING_TYPE_SELLING => ['sell' => true, 'buy' => false, 'repair' => false],
        self::BILLING_TYPE_BUYBACK => ['sell' => false, 'buy' => true, 'repair' => false],
        self::BILLING_TYPE_REPAIR => ['sell' => false, 'buy' => false, 'repair' => true],
        self::BILLING_TYPE_SUPER_ADMIN => ['sell' => true, 'buy' => true, 'repair' => true],
    ];

    const BILLING_TYPE_PLAN = [
        self::BILLING_TYPE_NO => 'p1',
        self::BILLING_TYPE_ALL => 'p4',
        self::BILLING_TYPE_SELLING_BUYBACK => 'p3',
        self::BILLING_TYPE_SELLING_REPAIR => 'p3',
        self::BILLING_TYPE_BUYBACK_REPAIR => 'p3',
        self::BILLING_TYPE_SELLING => 'p2',
        self::BILLING_TYPE_BUYBACK => 'p2',
        self::BILLING_TYPE_REPAIR => 'p2',
        self::BILLING_TYPE_SUPER_ADMIN => 'p1',
    ];

    /**
     * Site type prices
     */
    const WITH_SELLING_DATA = 1;
    const WITHOUT_SELLING_DATA = 0;

    const WITH_BUYBACK_DATA = 1;
    const WITHOUT_BUYBACK_DATA = 0;

    const WITH_REPAIR_DATA = 1;
    const WITHOUT_REPAIR_DATA = 0;

    const WITHOUT_LOCATION_DATA = 0;
    const WITH_LOCATION_DATA = 1;

    /**
     * Sync types
     */
    const WITHOUT_SYNC = 0;
    const SYNC_ALL = 1;
    const SYNC_SELLING = 2;
    const SYNC_BAYBACK = 3;
    const SYNC_REPAIR = 4;

    const SYNC_SELLING_LIST = [
        self::SYNC_ALL,
        self::SYNC_SELLING,
    ];

    const SYNC_BUYBACK_REPAIR_LIST = [
        self::SYNC_ALL,
        self::SYNC_BAYBACK,
        self::SYNC_REPAIR,
    ];

    /**
     * Order types
     */
    const ORDER_TYPE_NO = 0;
    const ORDER_TYPE_ALL = 1;
    const ORDER_TYPE_SELLING = 2;
    const ORDER_TYPE_BUYBACK = 3;
    const ORDER_TYPE_REPAIR = 4;
    const ORDER_TYPE_SELLING_BUYBACK = 5;
    const ORDER_TYPE_SELLING_REPAIR = 6;
    const ORDER_TYPE_BUYBACK_REPAIR = 7;

    const ORDER_SELLING_LIST = [
        self::ORDER_TYPE_ALL,
        self::SYNC_SELLING,
        self::ORDER_TYPE_SELLING_BUYBACK,
        self::ORDER_TYPE_SELLING_REPAIR,
    ];

    const ORDER_BUYBACK_REPAIR_LIST = [
        self::ORDER_TYPE_ALL,
        self::ORDER_TYPE_BUYBACK,
        self::ORDER_TYPE_REPAIR,
        self::ORDER_TYPE_SELLING_BUYBACK,
        self::ORDER_TYPE_SELLING_REPAIR,
        self::ORDER_TYPE_BUYBACK_REPAIR,
    ];

    /**
     * Get Super Admin
     *
     * @return null|object
     */
    public function getSuperAdmin()
    {
        if (!($this->hasReference(self::SUPER_ADMIN_LINK))) {
            return null;
        }

        return $this->getReference(self::SUPER_ADMIN_LINK);
    }

    /**
     * Get User References
     *
     * @return array
     */
    protected function getUserReferencesBySyncType(): array
    {
        $userConfigList = $this->getUserConfig();

        $result = ['sync' => [], 'host' => [], 'all' => []];
        foreach ($userConfigList as &$userConfig) {
            $this->prepareRow($userConfig);

            if ($userConfig['reference'] === self::SUPER_ADMIN_LINK) {
                continue;
            }

            if ((integer)$userConfig['synchronization_type'] === self::WITHOUT_SYNC) {
                $result['host'][] = $userConfig['reference'];
            } else {
                $result['sync'][] = $userConfig['reference'];
            }

            $result['all'][] = $userConfig['reference'];
        }

        return $result;
    }

    /**
     * Get User Config
     *
     * @return array
     */
    protected function getUserConfig(): array
    {
        return $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/users.csv');
    }

    /**
     * Load Data File As Array
     *
     * @param string $dataFilePath
     * @param string $delimiter
     * @return array
     */
    protected function loadDataFileAsArray(string $dataFilePath, string $delimiter = ','): array
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder($delimiter)]);
        $dataFilePath = $this->getContainer()->get('kernel')->locateResource($dataFilePath);

        return $serializer->decode(file_get_contents($dataFilePath), 'csv');
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * Prepare Row
     *
     * @param array $row
     */
    protected function prepareRow(array &$row)
    {
        foreach ($row as $column => $value) {
            $trimmedColumn = trim($column);
            $value = trim($value);
            unset($row[$column]);
            $row[$trimmedColumn] = $value == "NULL" ? null : $value;
        }
    }

    /**
     * Get Sync Users Config
     *
     * @return array
     */
    protected function getSyncUsersConfig()
    {
        $userConfigList = $this->getUserConfig();

        $result = [];
        foreach ($userConfigList as &$userConfig) {
            $this->prepareRow($userConfig);
            if ((integer)$userConfig['synchronization_type'] !== self::WITHOUT_SYNC) {
                $result[] = $userConfig;
            }
        }

        return $result;
    }

    /**
     * Has Container
     *
     * @return bool
     */
    protected function hasContainer(): bool
    {
        return !is_null($this->container);
    }

    /**
     * Get Customer Reference
     *
     * @param String $userReference
     * @return String
     */
    protected function getCustomerReference(String $userReference): String
    {
        return $userReference . '_c';
    }

    /**
     * Get Subscription Reference
     *
     * @param String $userReference
     * @return String
     */
    protected function getSubscriptionReference(String $userReference): String
    {
        return $userReference . '_s';
    }

    /**
     * Get User CategoryvReference
     *
     * @param String $userLink
     * @param String $categoryLink
     * @return String
     */
    protected function getUserCategoryReference(String $userLink, String $categoryLink): String
    {
        return $userLink . '_' . $categoryLink;
    }
}
