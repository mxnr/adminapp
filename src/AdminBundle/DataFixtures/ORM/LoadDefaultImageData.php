<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use ThreeWebOneEntityBundle\Entity\UserConfig\DefaultImage;

class LoadDefaultImageData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $defaultImages = $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/default-images.csv', '|');

        foreach ($defaultImages as $item) {
            $theme = (new DefaultImage())
                ->setTitle($item['title'])
                ->setContent($item['content']);

            $this->manager->persist($theme);
        }

        $this->manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 70;
    }
}
