<?php

namespace AdminBundle\DataFixtures\ORM;

use DateTime;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use RuntimeException;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Billing\CustomerInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Plan;
use ThreeWebOneEntityBundle\Entity\Billing\PlanInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Entity\Billing\SubscriptionInterface;

/**
 * Class LoadBillingData.
 */
class LoadBillingData extends AbstractDataFixture
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @return ReferenceRepository
     */
    public function getReferenceRepository(): ReferenceRepository
    {
        return $this->referenceRepository;
    }

    /**
     * @return bool
     */
    public function hasReferenceRepository(): bool
    {
        return !is_null($this->referenceRepository);
    }

    /**
     * @param ReferenceRepository|null $referenceRepository
     *
     * @return LoadBillingData
     */
    public function setReferenceRepository(ReferenceRepository $referenceRepository = null): LoadBillingData
    {
        $this->referenceRepository = $referenceRepository;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        if ($this->hasContainer() === false) {
            throw new RuntimeException('container unavailable');
        }
        $this->importPlans($this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/billing_plan.csv'));
        $this->importCustomers();
        $this->importSubscriptions();

        $this->manager->flush();
    }

    /**
     * @param array $data
     */
    protected function importPlans(array $data)
    {
        foreach ($data as &$row) {
            $this->prepareRow($row);
            $plan = new Plan();

            $plan->setChargeBeeId($row['charge_bee_id'])
                ->setName($row['name'])
                ->setDescription($row['description'])
                ->setPrice($row['price'])
                ->setActive($row['active'])
                ->setCurrencyCode($row['currency_code'])
                ->setChargeInterval($row['charge_interval'])
                ->setChargeIntervalUnit($row['charge_interval_unit'])
                ->setSetupFee($row['setup_fee'])
                ->setMetaData(json_decode($row['meta_data'], true))
                ->setHasTrial($row['has_trial'])
                ->setTrialInterval($row['trial_interval'])
                ->setTrialIntervalUnit($row['trial_interval_unit']);

            $this->getManager()->persist($plan);

            $this->setReference($row['reference_id'], $plan);
        }

        $this->getManager()->flush();
    }

    /**
     * @return ObjectManager
     */
    public function getManager(): ObjectManager
    {
        return $this->manager;
    }


    protected function importCustomers()
    {
        $userConfigList = $this->getUserConfig();

        foreach ($userConfigList as &$userConfig) {
            $this->prepareRow($userConfig);

            if ($userConfig['reference'] === self::SUPER_ADMIN_LINK) {
                continue;
            }

            $customer = new Customer();
            $customer->setUser($this->getReference($userConfig['reference']))
                ->setChargeBeeId($userConfig['charge_bee_id'])
                ->setCardStatus(0)
                ->setTrialStartedAt(new DateTime())
                ->setTrialEndedAt((new DateTime())->modify('+10 years'));

            $this->getManager()->persist($customer);

            $this->setReference($this->getCustomerReference($userConfig['reference']), $customer);
        }
        $this->getManager()->flush();
    }

    protected function importSubscriptions()
    {
        $userConfigList = $this->getUserConfig();

        foreach ($userConfigList as &$userConfig) {
            $this->prepareRow($userConfig);

            if ($userConfig['reference'] === self::SUPER_ADMIN_LINK) {
                continue;
            }

            $subscription = new Subscription();
            $subscription
                ->setPlan($this->getReference(self::BILLING_TYPE_PLAN[$userConfig['billing_type']]))
                ->setCustomer($this->getReference($this->getCustomerReference($userConfig['reference'])))
                ->setChargeBeeId($userConfig['charge_bee_id'])
                ->setItems(self::BILLING_ITEMS[$userConfig['billing_type']])
                ->setStatus(SubscriptionInterface::STATUS_ACTIVE)
                ->setChargeInterval(1)
                ->setChargeIntervalUnit(PlanInterface::PERIOD_YEAR)
                ->setNextChargeAt((new DateTime())->modify('+1 year'))
                ->setHasCard(CustomerInterface::CARD_STATUS_NONE)
                ->setTrialStart(new DateTime())
                ->setTrialEnd((new DateTime())->modify('+10 years'))
                ->setCurrentPeriodStart(new DateTime())
                ->setCurrentPeriodEnd((new DateTime())->modify('+10 years'))
                ->setCreatedAt(new DateTime());

            $this->getManager()->persist($subscription);
        }

        $this->getManager()->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 130;
    }

    /**
     * @param null|string $date
     *
     * @return DateTime|null
     */
    protected function getDataFromString(?string $date): ?DateTime
    {
        return !is_null($date) ? DateTime::createFromFormat('Y-m-d H:i:s', $date) : null;
    }
}
