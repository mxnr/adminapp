<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\CategoryImage;
use ThreeWebOneEntityBundle\Entity\User;

class LoadCategoryData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $resources = [
            '@AdminBundle/Resources/fixtures/categories.csv',
            '@AdminBundle/Resources/fixtures/products.csv',
            '@AdminBundle/Resources/fixtures/providers.csv',
        ];

        $users = $this->getUserReferencesBySyncType();

        foreach ($resources as $resource) {
            $categoryArray = $this->loadDataFileAsArray($resource);
            $this->createAdminCategories($categoryArray, $users['sync']);
            $this->createUserCategories($categoryArray, $users['all']);
        }
        $this->manager->flush();
    }

    /**
     * Creates Admin Categories
     *
     * @param $categoryArray
     * @param $syncUsers
     */
    public function createAdminCategories($categoryArray, $syncUsers)
    {
        foreach ($categoryArray as &$item) {
            if (!($this->hasReference(self::SUPER_ADMIN_LINK))) {
                break;
            }

            $this->prepareRow($item);
            $category = (new Category())
                ->setTitle($item['title'])
                ->setStatus($item['status'])
                ->setType(!empty($item['category_type']) ? $item['category_type'] : $item['type'])
                ->setOwner($this->getReference(self::SUPER_ADMIN_LINK))
                ->addUser($this->getReference(self::SUPER_ADMIN_LINK));

            foreach ($syncUsers as $userLink) {
                if (!($this->hasReference($userLink))) {
                    break;
                }
                $category->addUser($this->getReference($userLink));
            }

            if (!empty($item['image'])) {
                $this->createEntityImage($item['image'], $category, $this->getReference(self::SUPER_ADMIN_LINK));
            }
            $this->setReference(
                (!empty($item['category_type']) ? 'category_' : '') . $item['reference_id'],
                $category
            );

            if (!empty($item['parent_id']) && $item['parent_id'] != 'null') {
                $category->setParent($this->getReference((
                    !empty($item['category_type']) ? 'category_' : '') . $item['parent_id']
                ));
            }
            $this->manager->persist($category);
        }
    }

    /**
     * Creates CategoryImage Entity
     *
     * @param string $name
     * @param Category $category
     * @param User $user
     */
    public function createEntityImage($name, $category, $user)
    {
        $image = (new CategoryImage())
            ->setImageName($name)
            ->setEntity($category)
            ->setOwner($user)
            ->addUser($user);
        $this->manager->persist($image);
    }

    /**
     * Creates User Categories
     *
     * @param $categoryArray
     * @param $hostUsers
     */
    public function createUserCategories($categoryArray, $hostUsers)
    {
        foreach ($hostUsers as $userLink) {
            if (!($this->hasReference($userLink))) {
                break;
            }
            $user = $this->getReference($userLink);
            foreach ($categoryArray as &$item) {
                $this->prepareRow($item);
                $category = (new Category())
                    ->setTitle($user->getUsername() . "'s " . $item['title'])
                    ->setStatus($item['status'])
                    ->setType($item['type'])
                    ->setOwner($user)
                    ->addUser($user);

                if (!empty($item['image'])) {
                    $this->createEntityImage($item['image'], $category, $user);
                }

                $this->setReference($this->getUserCategoryReference($userLink, $item['reference_id']), $category);

                if (!empty($item['parent_id']) && $item['parent_id'] != 'null') {
                    $category->setParent($this->getReference($this->getUserCategoryReference($userLink, $item['parent_id'])));
                }
                $this->manager->persist($category);
            }
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 20;
    }
}
