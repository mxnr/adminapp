<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\ModelImage;
use ThreeWebOneEntityBundle\Entity\User;

class LoadModelData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        // Switched off SQLLogger because of memory leak reasons
        $this->manager->getConnection()->getConfiguration()->setSQLLogger(null);

        $modelArray = $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/models.csv');
        $users = $this->getUserReferencesBySyncType();
        $this->createAdminModels($modelArray, $users['sync']);
        $this->createUserModels($modelArray, $users['all']);
        $this->manager->flush();
    }

    /**
     * Creates Admin Models
     *
     * @param $modelArray
     * @param $syncUsers
     */
    public function createAdminModels($modelArray, $syncUsers)
    {
        foreach ($modelArray as &$item) {
            if (!($this->hasReference(self::SUPER_ADMIN_LINK))) {
                break;
            }
            $this->prepareRow($item);
            $model = (new Model())
                ->setTitle($item['title'])
                ->setStatus($item['status'])
                ->setOwner($this->getReference(self::SUPER_ADMIN_LINK))
                ->addUser($this->getReference(self::SUPER_ADMIN_LINK));

            $productItem = $this->getProductItem($item['product_id']);
            if (is_array($productItem) && !empty($productItem['description'])) {
                $model->setDescription($productItem['description']);
            }

            if (!empty($item['product_id']) && $this->hasReference('product' . $item['product_id'])) {
                $this->updateProduct($item, $model);
            }
            $this->updateFilter($item, $model);

            foreach ($syncUsers as $userLink) {
                if (!($this->hasReference($userLink))) {
                    break;
                }
                $model->addUser($this->getReference($userLink));
            }

            if (!empty($item['image'])) {
                $this->createImage($item['image'], $model, $this->getReference(self::SUPER_ADMIN_LINK));
            }
            $this->setReference($item['title'], $model);
            $this->manager->persist($model);
        }
    }

    /**
     * @param $productReference
     *
     * @return array|null
     */
    private function getProductItem($productReference)
    {
        $productsArray = $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/products.csv');

        $productReference = 'product' . $productReference;
        foreach ($productsArray as &$item) {
            $this->prepareRow($item);
            if ($item['reference_id'] === $productReference) {
                return $item;
            }
        }

        return null;
    }

    /**
     * Updates Model's Product
     *
     * @param array $item
     * @param Model $model
     * @param string $userLink
     */
    public function updateProduct($item, $model, $userLink = '')
    {
        if ($userLink != '') {
            $userLink .= '_';
        }

        $product = $this->getReference($userLink . 'product' . $item['product_id']);
        $model->addCategory($product);

        if (!empty($item['provider_id']) && $this->hasReference(
                $userLink . 'category_provider' . $item['provider_id']
            )) {
            $model->addCategory($this->getReference($userLink . 'category_provider' . $item['provider_id']));
        }

        if (!empty($item['category_id']) && $this->hasReference(
                $userLink . 'cat' . $item['category_id']
            )) {
            $model->addCategory($this->getReference($userLink . 'cat' . $item['category_id']));
        }
        $this->manager->persist($product);
    }

    /**
     * Updates Model's Filter
     *
     * @param array $item
     * @param Model $model
     * @param string $userLink
     */
    public function updateFilter($item, $model, $userLink = '')
    {
        if ($userLink != '') {
            $userLink .= '_';
        }

        if (!empty($item['capacity_id']) && $this->hasReference(
                $userLink . 'cap' . $item['capacity_id']
            )) {
            $model->addFilter($this->getReference($userLink . 'cap' . $item['capacity_id']));
        }

        if (!empty($item['provider_id']) && $this->hasReference(
                $userLink . 'provider' . $item['provider_id']
            )) {
            $model->addFilter($this->getReference($userLink . 'provider' . $item['provider_id']));
        }

        if (!empty($item['color_id']) && $this->hasReference(
                $userLink . 'col' . $item['color_id']
            )) {
            $model->addFilter($this->getReference($userLink . 'col' . $item['color_id']));
        }
    }

    /**
     * Creates Model Image
     *
     * @param string $name
     * @param Model $model
     * @param User $user
     */
    public function createImage($name, $model, $user)
    {
        $image = (new ModelImage())
            ->setImageName($name)
            ->setEntity($model)
            ->setOwner($user)
            ->addUser($user);
        $this->manager->persist($image);
    }

    /**
     * Creates User Models
     *
     * @param modelArray
     * @param $hostUsers
     */
    public function createUserModels($modelArray, $hostUsers)
    {
        foreach ($hostUsers as $userLink) {
            if (!($this->hasReference($userLink))) {
                break;
            }
            $user = $this->getReference($userLink);

            foreach ($modelArray as &$item) {
                $this->prepareRow($item);

                $model = new Model();
                $model->setTitle($user->getUsername() . "'s " . $item['title'])
                    ->setStatus($item['status'])
                    ->setOwner($user)
                    ->addUser($user);

                $productItem = $this->getProductItem($item['product_id']);
                if (is_array($productItem) && !empty($productItem['description'])) {
                    $model->setDescription($productItem['description']);
                }

                if (!empty($item['product_id']) && $this->hasReference(
                        $userLink . '_' . 'product' . $item['product_id']
                    )) {
                    $this->updateProduct($item, $model, $userLink);
                }
                $this->updateFilter($item, $model, $userLink);

                if (!empty($item['image'])) {
                    $this->createImage($item['image'], $model, $user);
                }
                $this->setReference($userLink . '_' . $item['title'], $model);
                $this->manager->persist($model);
            }
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 90;
    }
}
