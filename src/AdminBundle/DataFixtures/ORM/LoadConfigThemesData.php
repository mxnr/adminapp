<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use ThreeWebOneEntityBundle\Entity\UserConfig\Theme;

class LoadConfigThemesData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $resources = [
            '@AdminBundle/Resources/fixtures/themes.csv',
        ];

        foreach ($resources as $resource) {
            $themesArray = $this->loadDataFileAsArray($resource);

            foreach ($themesArray as &$item) {
                $this->prepareRow($item);

                $theme = (new Theme())
                    ->setTitle($item['title'])
                    ->setType($item['type']);

                $this->manager->persist($theme);
            }
        }

        $this->manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 50;
    }
}
