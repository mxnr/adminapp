<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserInterface;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\User;

class LoadPriceData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $this->createPrices();
    }

    public function createPrices()
    {
        $priceTypesList = $this->getPriceTypesConfig();
        $userConfigList = $this->getUserConfig();

        foreach ($userConfigList as &$userConfig) {
            $this->prepareRow($userConfig);

            /**@var User $user */
            $user = $this->getReference($userConfig['reference']);

            $categories = $this->manager->getRepository(Category::class)
                ->getUserParentCategoriesQuery(Category::TYPE_CATEGORY, $user)
                ->getQuery()->getResult();

            /**@var Category $category */
            foreach ($categories as $catKey => $category) {
                $subCategories = $category->getChildren();

                if (count($subCategories) > 0) {
                    /**@var Category $subCategory */
                    foreach ($subCategories as $subCategory) {
                        $models = $subCategory->getModels();
                        $this->loadModels($models, $priceTypesList, $userConfig, $user, $subCategory);
                    }
                } else {
                    $models = $category->getModels();
                    $this->loadModels($models, $priceTypesList, $userConfig, $user, $category);
                }
            }
        }
    }

    /**
     * Get Price Types Config
     *
     * @return array
     */
    private function getPriceTypesConfig()
    {
        return $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/price-types.csv');
    }

    /**
     * Load Models
     *
     * @param $models
     * @param $priceTypesList
     * @param $userConfig
     * @param User $user
     * @param Category $category
     */
    private function loadModels($models, $priceTypesList, $userConfig, User $user, Category $category)
    {
        if ($user->hasRole(UserInterface::ROLE_SUPER_ADMIN)) {
            $syncUserConfigList = $this->getSyncUsersConfig();
        }

        foreach ($models as $key => $model) {

            // Load only 3 model for all kind of category
            if ($key === 2) {
                break;
            }

            foreach ($priceTypesList as &$priceTypeConfig) {
                $this->prepareRow($priceTypeConfig);

                if (isset($syncUserConfigList)) {
                    foreach ($syncUserConfigList as $syncUserConfig) {
                        $syncUser = $this->getReference($syncUserConfig['reference']);

                        if ($this->isAbleSyncSellingPrice($priceTypeConfig['site_type'], $syncUserConfig['synchronization_type'])) {
                            $priceType = $this->getReference($priceTypeConfig['reference']);
                            $this->createSellingPrice($syncUser, $model, $category, $priceType);
                        }

                        if ($this->isAbleSyncBuybackRepairPrice($priceTypeConfig['site_type'], $syncUserConfig['synchronization_type'])) {
                            $priceType = $this->getReference($priceTypeConfig['reference']);
                            $this->createBuybackRepairPrice($syncUser, $model, $priceType);
                        }
                    }
                }

                if ($this->isAbleCreateSellingPrice($priceTypeConfig['site_type'], $userConfig['selling_data'])) {
                    $priceType = $this->getReference($priceTypeConfig['reference']);
                    $this->createSellingPrice($user, $model, $category, $priceType);
                }

                if ($this->isAbleCreateBuybackPrice($priceTypeConfig['site_type'], $userConfig['buyBack_data'])) {
                    $priceType = $this->getReference($priceTypeConfig['reference']);
                    $this->createBuybackRepairPrice($user, $model, $priceType);
                }

                if ($this->isAbleCreateRepairPrice($priceTypeConfig['site_type'], $userConfig['repair_data'])) {
                    $priceType = $this->getReference($priceTypeConfig['reference']);
                    $this->createBuybackRepairPrice($user, $model, $priceType);
                }
            }

            $this->manager->flush();
        }
    }

    /**
     * Is Able To Synchronization Selling Price
     *
     * @param String $priceType
     * @param String $userSyncType
     * @return bool
     */
    private function isAbleSyncSellingPrice(String $priceType, String $userSyncType)
    {
        return $priceType == SiteTypeInterface::SALE && in_array($userSyncType, self::SYNC_SELLING_LIST);
    }

    /**
     * Create Selling Price
     *
     * @param User $user
     * @param Model $model
     * @param Category $category
     * @param PriceType $priceType
     */
    public function createSellingPrice(User $user, Model $model, Category $category, PriceType $priceType)
    {
        $this->addUserIfNecessary($user, $priceType);
        $inventoryItem = (new InventoryItem())
            ->setCategory($category)
            ->setModel($model)
            ->setTitle($model->getTitle())
            ->setPriceType($priceType)
            ->setOwner($user)
            ->setStatus(1)
            ->setQuantity(rand(1, 10))
            ->setPriceValue(rand(1, 9) * 100)
            ->setPurchasePrice(rand(1, 9) * 100)
            ->setType(InventoryItem::SALE);

        $this->manager->persist($inventoryItem);

        $price = (new Price())
            ->setPriceType($priceType)
            ->setOwner($user)
            ->setValue($inventoryItem->getPriceValue())
            ->setModel($model)
            ->addInventoryItem($inventoryItem);

        $this->addUserIfNecessary($user, $price);

        $this->manager->persist($price);
    }

    /**
     * @param $user
     * @param $item
     */
    private function addUserIfNecessary($user, $item)
    {
        if (!in_array($user, $item->getUsers()->toArray())) {
            $item->addUser($user);
        }
    }

    /**
     * Is Able To Synchronization Buyback Or Repair Price
     *
     * @param String $priceType
     * @param String $userSyncType
     * @return bool
     */
    private function isAbleSyncBuybackRepairPrice(String $priceType, String $userSyncType)
    {
        return in_array($priceType, [SiteTypeInterface::BUYBACK, SiteTypeInterface::REPAIR]) &&
            in_array($userSyncType, self::SYNC_BUYBACK_REPAIR_LIST);
    }

    /**
     * Create Buyback Or Repair Price
     *
     * @param User $user
     * @param Model $model
     * @param PriceType $priceType
     */
    public function createBuybackRepairPrice(User $user, Model $model, PriceType $priceType)
    {
        $price = (new Price())
            ->setValue(rand(1, 9) * 100)
            ->setPriceType($priceType)
            ->setOwner($user)
            ->addUser($user);

        $model->addPrice($price);

        $this->manager->persist($price);
    }

    /**
     * Is Able To Create Selling Price
     *
     * @param String $priceType
     * @param String $userType
     * @return bool
     */
    private function isAbleCreateSellingPrice(String $priceType, String $userType)
    {
        return $priceType == SiteTypeInterface::SALE && $userType == self::WITH_SELLING_DATA;
    }

    /**
     * Is Able To Create Buyback Price
     *
     * @param String $priceType
     * @param String $userType
     * @return bool
     */
    private function isAbleCreateBuybackPrice(String $priceType, String $userType)
    {
        return $priceType == SiteTypeInterface::BUYBACK && $userType == self::WITH_BUYBACK_DATA;
    }

    /**
     * Is Able To Create Repair Price
     *
     * @param String $priceType
     * @param String $userType
     * @return bool
     */
    private function isAbleCreateRepairPrice(String $priceType, String $userType)
    {
        return $priceType == SiteTypeInterface::REPAIR && $userType == self::WITH_REPAIR_DATA;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 100;
    }
}
