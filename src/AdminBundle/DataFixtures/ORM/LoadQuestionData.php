<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use ThreeWebOneEntityBundle\Entity\UserConfig\Question;

class LoadQuestionData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $questionsArray = $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/questions.csv');

        foreach ($questionsArray as &$item) {
            if (!($this->hasReference(self::SUPER_ADMIN_LINK))) {
                break;
            }
            $this->prepareRow($item);

            $question = (new Question())
                ->setCategory($item['category'])
                ->setQuestion($item['question'])
                ->setAnswer($item['answer'])
                ->setOwner($this->getReference(self::SUPER_ADMIN_LINK))
                ->addUser($this->getReference(self::SUPER_ADMIN_LINK));

            $this->manager->persist($question);
        }

        $this->manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 60;
    }
}
