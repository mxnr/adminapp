<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\UserConfig\Location;

class LoadUserLocationData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $userConfigList = $this->getUserConfig();

        foreach ($userConfigList as &$userConfig) {
            $this->prepareRow($userConfig);
            if ((integer)$userConfig['locations'] === self::WITH_LOCATION_DATA) {
                $this->loadLocations($this->getReference($userConfig['reference']));
            }
        }

        $this->manager->flush();
    }

    /**
     * @param User $owner
     */
    private function loadLocations(User $owner)
    {
        $locations = $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/locations.csv');
        foreach ($locations as &$item) {
            $this->prepareRow($item);
            $location = (new Location())
                ->setName($item['name'])
                ->setDescription($item['description'])
                ->setHours($item['hours'])
                ->setServices(json_decode($item['services'], true))
                ->setPhone($item['phone'])
                ->setOwner($owner);

            $this->setReference($item['ref'], $location);
            $this->manager->persist($location);
        }
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 110;
    }
}
