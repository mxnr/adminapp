<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use ThreeWebOneEntityBundle\Entity\Inventory\Inventory;
use ThreeWebOneEntityBundle\Entity\Page\Page;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\UserConfig\Config;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogo;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigOfferImage;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSlider;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSliderImage;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSocialLink;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigTexts;

class LoadUserData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->passwordEncoder = $this->getContainer()->get('security.password_encoder');
        $user_list = $this->getUserConfig();

        foreach ($user_list as &$item) {
            $this->prepareRow($item);
            $user = $this->createUser($item);

            $config = $this->createConfig($user);
            $this->createPrivacyAndTermsPages($user);

            $user->setConfig($config);
            $this->createInventories($user);
        }

        $this->manager->flush();
    }

    /**
     * @param array $userData
     * @return User
     */
    private function createUser($userData): User
    {
        $user = new User();
        $password = $this->passwordEncoder->encodePassword($user, self::USERS_PASSWORD);
        $user->setEmail($userData['username'] . '@3web1.com')
            ->setEnabled(true)
            ->setStoreName($userData['username'] . ' store')
            ->setUsername($userData['username'])
            ->setPassword($password)
            ->setRoles([$userData['role']]);
        $this->setReference($userData['reference'], $user);

        return $user;
    }

    /**
     * @param $user
     *
     * @return Config
     */
    private function createConfig($user)
    {
        $config = new Config();
        $configLogo = new ConfigLogo();
        $configSlider = new ConfigSlider();
        $configText = new ConfigTexts();
        $configSocial = new ConfigSocialLink();
        $config
            ->setConfigLogo($configLogo)
            ->setConfigTexts($configText)
            ->setConfigSocial($configSocial);
        $this->manager->persist($configLogo);
        $this->manager->persist($configText);
        $this->manager->persist($configSocial);

        $offerImages = $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/offer-images.csv');
        foreach ($offerImages as &$image) {
            $this->prepareRow($image);
            $offerImage = (new ConfigOfferImage())
                ->setImageName($image['name'])
                ->setEntity($config)
                ->setOwner($user);
            $offerImage
                ->getSetting()
                ->setTitle($image['title'])
                ->setDescription($image['description']);
            $this->manager->persist($offerImage);
            $config->addOfferImage($offerImage);
        }

        $sliderImages = $this->loadDataFileAsArray('@AdminBundle/Resources/fixtures/slider-images.csv');
        foreach ($sliderImages as &$image) {
            $this->prepareRow($image);
            $sliderImage = (new ConfigSliderImage())
                ->setImageName($image['name'])
                ->setEntity($configSlider)
                ->setOwner($user);
            $configSlider->addSliderImage($sliderImage);
        }

        $this->manager->persist($configSlider);
        $config->setConfigSlider($configSlider);
        $this->manager->persist($config);

        return $config;
    }

    /**
     * @param User $user
     */
    private function createPrivacyAndTermsPages($user)
    {
        $path = $this->getContainer()->get('kernel')->locateResource('@AdminBundle/Resources/fixtures/page/pages.csv');
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder(';')]);
        $pagesArray = $serializer->decode(file_get_contents($path), 'csv');
        foreach ($pagesArray as $pageArray) {
            $page = new Page();
            $page->setPageType($pageArray['page_type'])
                ->setTitle($pageArray['title'])
                ->setDescription($pageArray['description'])
                ->setStatus(1)
                ->setOwner($user);

            $this->manager->persist($page);
        }
    }

    /**
     * Create Inventories
     * @param $user
     */
    private function createInventories($user)
    {
        $inventory_types = [Inventory::SALE, Inventory::BUYBACK, Inventory::REPAIR];

        foreach ($inventory_types as $type) {
            $inventory = (new Inventory())->setType($type)->setStatus(Inventory::STATUS_ACTIVE)->setOwner($user);
            $this->manager->persist($inventory);
        }
        $this->manager->persist($user);
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 10;
    }
}
