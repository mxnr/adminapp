<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use ThreeWebOneEntityBundle\Entity\PriceType;

class LoadPriceTypeData extends AbstractDataFixture
{
    /**
     * @var ObjectManager;
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $resources = [
            '@AdminBundle/Resources/fixtures/price-types.csv',
        ];

        $users = $this->getUserReferencesBySyncType();

        foreach ($resources as $resource) {
            $priceTypeArray = $this->loadDataFileAsArray($resource);
            $this->createAdminPriceType($priceTypeArray, $users['sync']);
            $this->createUserPriceType($priceTypeArray, $users['all']);
        }

        $this->manager->flush();
    }

    /**
     * Create Admin PriceTypes
     *
     * @param $priceTypeArray
     * @param $syncUsers
     */
    public function createAdminPriceType($priceTypeArray, $syncUsers)
    {
        foreach ($priceTypeArray as &$item) {
            if (!($this->hasReference(self::SUPER_ADMIN_LINK))) {
                break;
            }

            $this->prepareRow($item);
            $priceType = (new PriceType())
                ->setTitle($item['title'])
                ->setStatus($item['status'])
                ->setSite($item['site_type'])
                ->setDescription($item['description'])
                ->setOwner($this->getReference(self::SUPER_ADMIN_LINK))
                ->addUser($this->getReference(self::SUPER_ADMIN_LINK));
            $this->setReference($item['reference'], $priceType);

            foreach ($syncUsers as $userLink) {
                if (!($this->hasReference($userLink))) {
                    break;
                }
                $priceType->addUser($this->getReference($userLink));
            }

            $this->manager->persist($priceType);
        }
    }

    /**
     * Create User PriceTypes
     *
     * @param array $priceTypeArray
     * @param array $hostUsers
     */
    public function createUserPriceType($priceTypeArray, $hostUsers)
    {
        foreach ($hostUsers as $userLink) {
            if (!($this->hasReference($userLink))) {
                break;
            }
            $user = $this->getReference($userLink);

            foreach ($priceTypeArray as &$item) {
                $this->prepareRow($item);
                $priceType = (new PriceType())
                    ->setTitle($user->getUsername() . "'s " . $item['title'])
                    ->setStatus($item['status'])
                    ->setSite($item['site_type'])
                    ->setDescription($item['description'])
                    ->setOwner($user)
                    ->addUser($user);

                $this->manager->persist($priceType);
            }
        }
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 40;
    }
}
