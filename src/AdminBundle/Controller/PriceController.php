<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\Type\PriceType;
use AdminBundle\Security\Voter\EntityVoter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType as EntityPriceType;

class PriceController extends Controller
{
    /**
     * @param Price $price
     *
     * @return RedirectResponse
     */
    public function synchronizePriceAction(Price $price)
    {
        $this->denyAccessUnlessGranted(EntityVoter::VIEW, $price);
        $this->get('admin.clone_service')->synchronizeObject($price, $this->getUser());

        return $this->redirectToRoute(
            'admin_model_show',
            [
                'id' => $price->getModel()->getId()
            ]
        );
    }

    /**
     * @param Model $model
     * @param Request $request
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createPriceAction(Model $model, Request $request)
    {
        $this->denyAccessUnlessGranted(EntityVoter::VIEW, $model);
        $price = new Price();
        $em = $this->getDoctrine()->getManager();
        $priceTypes = $em->getRepository(EntityPriceType::class)
            ->getUserPriceTypesBuybackAndRepair($this->getUser());
        $form = $this->createForm(PriceType::class, $price, ['choices' => $priceTypes]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $price->setOwner($this->getUser())
                ->addUser($this->getUser());
            $model->addPrice($price);
            $em->persist($price);
            $em->flush();

            $this->addFlash(
                'sonata_flash_success',
                'Price created!'
            );

            return $this->redirectToRoute(
                'admin_model_show',
                [
                    'id' => $model->getId()
                ]
            );
        }

        return $this->render(
            '@Admin/Price/edit_price.html.twig',
            [
                'form' => $form->createView(),
                'model' => $model,
                'price_action' => 'create'
            ]
        );
    }

    /**
     * @param Price $price
     * @param Request $request
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editPriceAction(Price $price, Request $request)
    {
        $this->denyAccessUnlessGranted(EntityVoter::EDIT, $price);
        $em = $this->getDoctrine()->getManager();
        $priceTypes = $em->getRepository(EntityPriceType::class)
            ->getUserPriceTypesBuybackAndRepair($this->getUser());
        $form = $this->createForm(PriceType::class, $price, ['choices' => $priceTypes]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($price);
            $em->flush();

            $this->addFlash(
                'sonata_flash_success',
                'Price edited!'
            );

            return $this->redirectToRoute(
                'admin_model_show',
                [
                    'id' => $price->getModel()->getId(),
                ]
            );
        }

        return $this->render(
            '@Admin/Price/edit_price.html.twig',
            [
                'form' => $form->createView(),
                'model' => $price->getModel(),
                'price_action' => 'edit'
            ]
        );
    }

    /**
     * @param Price $price
     * @return RedirectResponse
     */
    public function deletePriceAction(Price $price)
    {
        $this->denyAccessUnlessGranted(EntityVoter::EDIT, $price);
        $em = $this->getDoctrine()->getManager();
        $em->remove($price);
        $em->flush();

        $this->addFlash(
            'sonata_flash_success',
            'Price deleted!'
        );

        return $this->redirectToRoute(
            'admin_model_show',
            [
                'id' => $price->getModel()->getId()
            ]
        );
    }
}
