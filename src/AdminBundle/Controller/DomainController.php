<?php

namespace AdminBundle\Controller;

use AdminBundle\Exception\DomainNotFoundException;
use AdminBundle\Form\Type\Domain\ClientDomainType;
use AdminBundle\Form\Type\Domain\DomainContactsType;
use AdminBundle\Security\Voter\SubscriptionRestrictVoter;
use AdminBundle\Service\AccessControlService;
use AdminBundle\Service\ClientDomainService;
use AdminBundle\Service\DomainName\AcmService;
use AdminBundle\Service\FlashNotificationService;
use AdminBundle\Service\UserGroupsService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Intl;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;

class DomainController extends BaseDomainController
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var UserGroupsService
     */
    private $userGroupsService;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var AccessControlService
     */
    private $accessControlService;

    /**
     * @var String
     */
    private $cloudFrontDomainName;

    /**
     * @var ClientDomainService
     */
    private $clientDomainService;

    /**
     * @var FlashNotificationService
     */
    private $flashNotificationService;

    /**
     * @var AcmService
     */
    private $acmService;

    /**
     * DomainController constructor.
     * @param \Twig_Environment $twig
     * @param FormFactory $formFactory
     * @param EntityManager $entityManager
     * @param UserGroupsService $userGroupsService
     * @param Router $router
     * @param AccessControlService $accessControlService
     * @param String $cloudFrontDomainName
     * @param ClientDomainService $clientDomainService
     * @param FlashNotificationService $flashNotificationService
     * @param AcmService $acmService
     */
    public function __construct(
        \Twig_Environment $twig,
        FormFactory $formFactory,
        EntityManager $entityManager,
        UserGroupsService $userGroupsService,
        Router $router,
        AccessControlService $accessControlService,
        String $cloudFrontDomainName,
        ClientDomainService $clientDomainService,
        FlashNotificationService $flashNotificationService,
        AcmService $acmService
    ) {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->userGroupsService = $userGroupsService;
        $this->router = $router;
        $this->accessControlService = $accessControlService;
        $this->cloudFrontDomainName = $cloudFrontDomainName;
        $this->clientDomainService = $clientDomainService;
        $this->flashNotificationService = $flashNotificationService;
        $this->acmService = $acmService;
        $this->accessControlService->denyAccessUnlessGranted(SubscriptionRestrictVoter::DOMAIN_RESTRICT);
    }

    /**
     * Domains Page Action
     *
     * @return Response
     */
    public function domainsPageAction(Request $request): Response
    {
        $user = $this->userGroupsService->getUserOrUserSupervisor();
        $clientDomain = new ClientDomain();
        $clientDomain->setOwner($user);

        $form = $this->formFactory->create(ClientDomainType::class, $clientDomain);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $this->clientDomainService->createClientDomain($form->getData());

            // Reset form
            $form = $this->formFactory->create(ClientDomainType::class);
        }

        $clientDomains = $this->clientDomainService->getClientDomains();
        return new Response($this->twig->render('AdminBundle:Domain:domains_info_page.html.twig', [
            'form' => $form->createView(),
            'clientDomains' => $clientDomains,
            'countries' => Intl::getRegionBundle()->getCountryNames(),
            'cloudFrontDomainName' => $this->cloudFrontDomainName,
            'formIsValid' => $form->isSubmitted() && !$form->isValid(),
            'activeDomains' => $this->clientDomainService->getCountActiveDomains(),
            'domainList' => $this->clientDomainService->getDomainsList($clientDomains)
        ]));
    }

    /**
     * @param Request $request
     * @param ClientDomain $clientDomain
     * @return Response
     */
    public function registerDomainAction(Request $request, ClientDomain $clientDomain)
    {
        if ($clientDomain->getStatus() === ClientDomain::PAYED_WITH_DOMAIN) {
            $this->flashNotificationService->addFlashMessage('success', 'Domain has been added!');

            return new RedirectResponse($this->router->generate('admin_domain'));
        }

        if ($clientDomain->getStatus() !== ClientDomain::PAYED_WITHOUT_DOMAIN) {
            $this->flashNotificationService->addFlashMessage('error', 'Domain not payed');

            return new RedirectResponse($this->router->generate('admin_domain'));
        }

        $form = $this->formFactory->create(DomainContactsType::class);
        $form->handleRequest($request);

        // Set form data when user go to the contacts page first time
        if (!$form->isSubmitted()) {
            $contacts = $clientDomain->getContacts();
            $form->setData($contacts['owner']);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            if ($clientDomain->getStatus() === ClientDomain::PAYED_WITHOUT_DOMAIN) {
                $ownerContacts = $this->clientDomainService->getContacts($form->getData());

                if (
                    $this->clientDomainService->registerNewDomain(
                        $clientDomain,
                        $ownerContacts,
                        $this->getContacts($ownerContacts)
                    )
                ) {
                    return new RedirectResponse($this->router->generate('admin_domain'));
                };
            }
        }

        return new Response($this->twig->render('AdminBundle:Domain:contact_page.html.twig', [
            'form' => $form->createView(),
            'clientDomain' => $clientDomain->getId()
        ]));
    }

    /**
     * @param ClientDomain $clientDomain
     * @return RedirectResponse
     */
    public function changeActiveStatusAction(clientDomain $clientDomain)
    {
        if ($clientDomain->isActive()) {
            $clientDomain->setActive(clientDomain::INACTIVE);
            $this->flashNotificationService->addFlashMessage('success', 'Domain status was changed to inactive');
        } else {
            $clientDomain->setActive(clientDomain::ACTIVE);
            $this->flashNotificationService->addFlashMessage('success', 'Domain status was changed to active');
        }

        $this->entityManager->persist($clientDomain);
        $this->entityManager->flush();

        return new RedirectResponse($this->router->generate('admin_domain'));
    }

    /**
     * @param ClientDomain $clientDomain
     * @return RedirectResponse
     */
    public function deleteDomainAction(clientDomain $clientDomain)
    {
        $this->flashNotificationService->addFlashMessage('success', 'Domain was deleted');

        $this->entityManager->remove($clientDomain);
        $this->entityManager->flush();

        return new RedirectResponse($this->router->generate('admin_domain'));
    }

    /**
     * @param ClientDomain $clientDomain
     * @return RedirectResponse
     */
    public function checkDomainAction(clientDomain $clientDomain)
    {
        try {
            $status = $this->acmService->getStatus($clientDomain->getDomain());

            if ($status === AcmService::ISSUED && $clientDomain->getStatus() === clientDomain::ADDED_MANUALLY_APPROVED) {
                $this->flashNotificationService->addFlashMessage('success', 'All checks is done');
            }

            if ($status === AcmService::ISSUED && $clientDomain->getStatus() === clientDomain::ADDED_MANUALLY) {
                $this->clientDomainService->domainOwnershipApproved($clientDomain);
            }

            if ($status !== AcmService::ISSUED && $clientDomain->getStatus() === clientDomain::ADDED_MANUALLY) {
                $this->clientDomainService->addDomainOwnershipInstructionToFlash($clientDomain);
            }

        } catch (DomainNotFoundException $e) {
            $this->flashNotificationService->addFlashMessage('error', 'Domain not found');
        }

        return new RedirectResponse($this->router->generate('admin_domain'));
    }
}
