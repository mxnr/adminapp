<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ThreeWebOneEntityBundle\Entity\UserConfig\DefaultImage;

class DefaultImageController extends Controller
{
    public function indexAction()
    {
        return $this->render(
            'AdminBundle:Config:default_images.html.twig',
            [
                'images' => $this->getDoctrine()->getRepository(DefaultImage::class)->findAll(),
            ]
        );
    }
}
