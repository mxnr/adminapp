<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\Type\CustomerType;
use AdminBundle\Security\Voter\EntityVoter;
use Doctrine\ORM\QueryBuilder;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Column\DateTimeColumn;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Column\TwigColumn;
use Omines\DataTablesBundle\Controller\DataTablesTrait;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Order\Order;

/**
 * Class CustomerController
 */
class CustomerController extends Controller
{
    use DataTablesTrait;

    public function detailsAction($id, Request $request)
    {
        $userService = $this->get('admin.user_group_service');
        $owner = $userService->getUserOrUserSupervisor();
        $customer = $this->getDoctrine()->getRepository(Customer::class)->findBy([
            'storeId' => $id,
            'owner' => $owner,
        ]);
        $customer = $customer[0] ?? null;
        if (!$customer || $customer->getOwner() != $owner) {
            throw new NotFoundHttpException(
                sprintf(
                    'unable to find the object with id : %s',
                    $id
                )
            );
        }

        $tableOrders = $this->createDataTable()
            ->add('createdAt', DateTimeColumn::class, ['label' => 'Date', 'format' => 'F d-Y H:i'])
            ->add(
                'id',
                TwigColumn::class,
                [
                    'label' => 'Order #',
                    'template' => '@Admin/datatable/order_id_column.html.twig',
                    'propertyPath' => 'store_id'
                ]
            )
            ->add(
                'siteType',
                TextColumn::class,
                [
                    'label' => 'Type',
                    'data' => function($value) {
                        switch ($value->getSiteType()) {
                            case Order::BUYBACK:
                                return 'buyback';
                            case Order::SALE:
                                return 'selling';
                            case Order::REPAIR:
                                return 'repair';
                            default:
                                return 'undefined';
                        }
                    }
                ]
            )
            ->add(
                'status',
                TextColumn::class,
                [
                    'label' => 'Status',
                    'data' => function($value) {
                        switch ($value->getStatus()) {
                            case Order::NEW_ORDER:
                                return 'unfilled';
                            case Order::IN_PROGRESS:
                                return 'unpaid';
                            case Order::COMPLETED:
                                return 'completed';
                            case Order::REJECTED:
                                return 'rejected';
                            default:
                                return 'undefined';
                        }
                    }
                ]
            )
            ->add(
                'total',
                TextColumn::class,
                [
                    'label' => 'Total',
                    'data' => function($value) {
                        $result = 0;
                        /** @var InventoryItem $item */
                        foreach ($value->getItems() as $item) {
                            if ($value->getSiteType() == Order::BUYBACK) {
                                /** @var InventoryItemBarcode $barcode */
                                foreach ($item->getInventoryItemBarcodes() as $barcode) {
                                    $result += (int) $barcode->getPurchasePrice();
                                }
                            } else {
                                /** @var InventoryItemBarcode $barcode */
                                foreach ($item->getInventoryItemBarcodes() as $barcode) {
                                    $result += (int) $barcode->getSellPrice();
                                }
                            }
                        }

                        return $result / 100 . "$";
                    }
                ]
            )
            ->createAdapter(ORMAdapter::class, [
                'entity' => Order::class,
                'query' => function (QueryBuilder $builder) use ($customer) {
                    $builder
                        ->select('o')
                        ->from(Order::class, 'o')
                        ->where('o.customer = :customer')
                        ->setParameter('customer', $customer)
                    ;
                },
            ])
            ->setName('orders')
            ->handleRequest($request);

        $tableItems = $this->createDataTable()
            ->add('createdAt', DateTimeColumn::class, ['label' => 'Date', 'format' => 'F d-Y H:i'])
            ->add(
                'id',
                TwigColumn::class,
                [
                    'label' => 'Item #',
                    'template' => '@Admin/datatable/item_id_column.html.twig',
                    'propertyPath' => 'store_id'
                ]
            )
            ->add('title', TextColumn::class, ['label' => 'Product'])
            ->add('quantity', TextColumn::class, ['label' => 'Quantity'])
            ->add(
                'total',
                TextColumn::class,
                [
                    'label' => 'Total',
                    'data' => function($value) {
                        $result = 0;
                        foreach ($value->getOrders() as $order) {
                            if ($order->getSiteType() == Order::BUYBACK) {
                                foreach ($value->getInventoryItemBarcodes() as $barcode) {
                                    $result += (int) $barcode->getPurchasePrice();
                                }
                            } else {
                                foreach ($value->getInventoryItemBarcodes() as $barcode) {
                                    $result += (int) $barcode->getPurchasePrice();
                                }
                            }
                        }

                        return $result / 100 . "$";
                    }
                ]
            )
            ->createAdapter(ORMAdapter::class, [
                'entity' => InventoryItem::class,
                'query' => function (QueryBuilder $builder) use ($customer) {
                    $builder
                        ->select('ii')
                        ->from(InventoryItem::class, 'ii')
                        ->distinct()
                        ->innerJoin('ii.orders', 'o')
                        ->where('o.customer = :customer')
                        ->setParameter('customer', $customer)
                    ;
                },
            ])
            ->setName('items')
            ->handleRequest($request);

        if ($tableOrders->isCallback()) {
            return $tableOrders->getResponse();
        }
        if ($tableItems->isCallback()) {
            return $tableItems->getResponse();
        }

        $customerForm = $this->createForm(CustomerType::class, $customer, ['action' => $this->generateUrl('admin_customer_details', ['id' => $customer->getId()])]);
        $customerForm->handleRequest($request);
        if ($customerForm->isSubmitted() && $customerForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();
            $this->addFlash('success', 'Customer updated');
        }

        return $this->render(
            '@Admin/customer/customer_details.html.twig',
            [
                'customer' => $customer,
                'customerForm' => $customerForm->createView(),
                'datatableOrders' => $tableOrders,
                'datatableItems' => $tableItems,
            ]
        );
    }

    /**
     * @param Customer $customer
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteCustomerAction(Customer $customer)
    {
        $this->denyAccessUnlessGranted(EntityVoter::DELETE, $customer);
        $em = $this->getDoctrine()->getManager();
        $em->remove($customer);
        $em->flush();

        return $this->redirectToRoute('admin_customer_list');
    }
}
