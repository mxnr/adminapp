<?php

namespace AdminBundle\Controller\Billing;

use ChargeBeeBundle\Packet\HostedPages\Acknowledge;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Workflow\Exception\LogicException;
use ThreeWebOneEntityBundle\Entity\Billing\HostedPage;
use ThreeWebOneEntityBundle\Entity\Billing\HostedPageInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Repository\Billing\HostedPageRepository;

/**
 * Class CallbackController.
 */
class CallbackController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function proceedAction(Request $request): Response
    {
        $pageId = $request->query->get('id', null);
        $operationSuccess = $request->query->get('state', 'cancelled');

        if ($operationSuccess === 'succeeded') {
            $operationSuccess = true;
        } elseif ($operationSuccess === 'cancelled') {
            $operationSuccess = false;
        } else {
            throw new BadRequestHttpException(
                'illegal value for state parameter - <![CDATA[' . $operationSuccess . ']]>'
            );
        }

        $em = $this->get('doctrine.orm.entity_manager');
        $subscriptionService = $this->get('admin.charge_bee.subscription');
        $hostedPagesService = $this->get('charge_bee.hosted_pages');
        $customerService = $this->get('admin.charge_bee.customer');

        /** @var HostedPageRepository $hostedPageRepo */
        $hostedPageRepo = $em->getRepository(HostedPage::class);

        /** @var HostedPage $hostedPage */
        $hostedPage = $hostedPageRepo->findOneBy(['chargeBeeId' => $pageId]);

        if (true === is_null($hostedPage)) {
            throw new NotFoundHttpException('unknown value for id parameter - <![CDATA[' . $pageId . ']]>');
        }

        if ($operationSuccess) {
            if ($hostedPage->getType() === HostedPageInterface::TYPE_CHECKOUT_SUBSCRIPTION) {
                $subscriptionId = $hostedPage->getEntityId();
                $subscriptionRepo = $em->getRepository(Subscription::class);
                $subscription = $subscriptionRepo->find($subscriptionId);
                if (true === is_null($subscription)) {
                    throw new LogicException('no subscription but hosted page found');
                }
                $subscriptionExternalId = $subscription->getChargeBeeId();

                $subscriptionService->synchronize($subscriptionExternalId);
            }

            $customerService->synchronize($hostedPage->getCustomer()->getChargeBeeId());

            //ack that we already accepted changes too
            $hostedPagesService->acknowledge(
                (new Acknowledge())->setId($hostedPage->getChargeBeeId())
            );
        }

        return new RedirectResponse($this->generateUrl('admin_billing_subscriptions_list'));
    }
}
