<?php

namespace AdminBundle\Controller\Billing;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class SubscriptionController.
 */
class SubscriptionController extends CRUDController
{
    /**
     * {@inheritdoc}
     */
    public function createAction(): Response
    {
        /** @var User $me */
        $me = $this->getUser();
        if ($me->hasRole('ROLE_SUPER_ADMIN')) {
            //preventing subscription creation for admin user
            $this->addFlash(
                'sonata_flash_success',
                'This action does not have sense for admin.'
            );

            return new RedirectResponse(
                $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
            );
        }

        return parent::createAction();
    }

    /**
     * @param Subscription $subscription
     *
     * @return Response
     */
    public function cancelAction(Subscription $subscription): Response
    {
        $subscriptionService = $this->get("admin.charge_bee.subscription");

        $subscriptionService->cancel($subscription);

        return new RedirectResponse(
            $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
        );
    }
}
