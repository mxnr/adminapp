<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\Type\RegistrationFormType;
use AdminBundle\Form\Type\UpgradeFormType;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ThreeWebOneEntityBundle\Entity\Billing\Embed\Upgrade;
use ThreeWebOneEntityBundle\Entity\Billing\PlanInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\Inventory;
use ThreeWebOneEntityBundle\Entity\Page\Page;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\UserConfig\Config;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogo;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigOfferImage;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSlider;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSliderImage;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigSocialLink;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigTexts;

class RegistrationController extends Controller
{
    /**
     * Default offer images
     */
    const OFFER_IMAGES = [
        [
            "name" => "offer_icon1.png",
            "title" => "Great Value",
            "description" => "We offer competitive prices on many popular products.",
        ],
        [
            "name" => "offer_icon2.png",
            "title" => "Worldwide Delivery",
            "description" => "Need it shipped. Fast, easy checkout through our eBay or Amazon store.",
        ],
        [
            "name" => "offer_icon3.png",
            "title" => "Safe Payment",
            "description" => "Pay with the world's most popular and secure payment methods.",
        ],
        [
            "name" => "offer_icon4.png",
            "title" => "Shop with Confidence",
            "description" => "Our Buyer Protection covers your purchase from click to delivery.",
        ],
        [
            "name" => "offer_icon5.png",
            "title" => "Help Center",
            "description" => "Round-the-clock assistance for a smooth shopping experience.",
        ],
    ];

    /**
     * Default slider images
     */
    const SLIDER_IMAGES = [
        ["name" => "1494220282-106.jpg"],
        ["name" => "1494220292-108.jpg"],
        ["name" => "1494220301-109.jpg"],
        ["name" => "1494220309-111.jpg"],
    ];

    /**
     * Register new site's admin user
     *
     * @param Request $request
     *
     * @return null|RedirectResponse|Response
     */
    public function registerAction(Request $request)
    {
        $upgradeService = $this->get('admin.upgrade');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $upgrade = new Upgrade();
        /** @var User $user */
        $user = $userManager->createUser();
        $user->setEnabled(true);
        $user->setRoles(['ROLE_ADMIN']);

        $this->createInventory($user);
        $config = $this->createConfig($user);
        $user->setConfig($config);
        $this->createPages($user);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $upgradeForm = $this->createForm(UpgradeFormType::class, $upgrade);
        $upgradeForm->handleRequest($request);

        $registrationForm = $this->createForm(RegistrationFormType::class, $user);

        if ($upgradeForm->isSubmitted()) {
            $registrationForm->submit($upgrade->getUser(), false);
            if ($upgradeForm->isValid() && $registrationForm->isValid()) {
                $event = new FormEvent($registrationForm, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $user->setUsername($user->getEmail());
                $userManager->updateUser($user);

                if (null === ($response = $event->getResponse())) {
                    $url = $this->generateUrl('sonata_admin_dashboard');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(
                    FOSUserEvents::REGISTRATION_COMPLETED,
                    new FilterUserResponseEvent($user, $request, $response)
                );

                return $response;
            }

            $event = new FormEvent($registrationForm, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);
        }

        $plansList = $upgradeService->getAvailablePlansWithTrialList();

        return $this->render(
            '@Admin/Registration/register.html.twig',
            [
                'form' => $upgradeForm->createView(),
                'formRegistration' => $registrationForm->createView(),
                'monthlyPlans' => $plansList[PlanInterface::PERIOD_MONTH],
                'yearlyPlans' => $plansList[PlanInterface::PERIOD_YEAR],
            ]
        );
    }

    /**
     * Register new site's admin user
     *
     * @param Request $request
     *
     * @return null|RedirectResponse|Response
     */
    public function registerApiAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $upgrade = new Upgrade();
        /** @var User $user */
        $user = $userManager->createUser();
        $user->setEnabled(true);
        $user->setRoles(['ROLE_ADMIN']);

        $this->createInventory($user);
        $config = $this->createConfig($user);
        $user->setConfig($config);
        $this->createPages($user);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $upgradeForm = $this->createForm(UpgradeFormType::class, $upgrade, ['csrf_protection' => false]);
        $upgradeForm->submit($data);

        $registrationForm = $this->createForm(RegistrationFormType::class, $user);
        $registrationForm->submit($upgrade->getUser(), false);
        if ($upgradeForm->isValid() && $registrationForm->isValid()) {
            $event = new FormEvent($registrationForm, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
            $user->setUsername($user->getEmail());
            $userManager->updateUser($user);

            if (null === ($response = $event->getResponse())) {
                $url = $this->generateUrl('sonata_admin_dashboard');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(
                FOSUserEvents::REGISTRATION_COMPLETED,
                new FilterUserResponseEvent($user, $request, $response)
            );

            return new JsonResponse([], Response::HTTP_CREATED);
        }

        foreach ($registrationForm as $fieldName => $formField) {
            if (isset($formField->getErrors()[0])) {
                $errors[$fieldName] = $formField->getErrors()[0]->getMessage();
            }
        }

        return new JsonResponse($errors, Response::HTTP_BAD_REQUEST);
    }

    /**
     * Create and link inventories
     *
     * @param User $user
     */
    private function createInventory(User $user)
    {
        /** @var $dispatcher EntityManager */
        $em = $this->get('doctrine.orm.entity_manager');
        $inventoryTypes = [
            Inventory::SALE,
            Inventory::BUYBACK,
            Inventory::REPAIR,
        ];

        foreach ($inventoryTypes as $type) {
            $inventory = (new Inventory())
                ->setType($type)
                ->setStatus(Inventory::STATUS_ACTIVE)
                ->setOwner($user);

            $em->persist($inventory);
        }
    }

    /**
     * Create user config
     *
     * @param User $user
     *
     * @return Config
     */
    private function createConfig(User $user)
    {
        /** @var $dispatcher EntityManager */
        $em = $this->get('doctrine.orm.entity_manager');
        $config = new Config();
        $configLogo = new ConfigLogo();
        $configSlider = new ConfigSlider();
        $configText = new ConfigTexts();
        $configSocial = new ConfigSocialLink();
        $config
            ->setConfigLogo($configLogo)
            ->setConfigTexts($configText)
            ->setConfigSocial($configSocial);
        $em->persist($configLogo);
        $em->persist($configText);
        $em->persist($configSocial);

        foreach (self::OFFER_IMAGES as $image) {
            $offerImage = (new ConfigOfferImage())
                ->setImageName($image['name'])
                ->setEntity($config)
                ->setOwner($user);
            $offerImage
                ->getSetting()
                ->setTitle($image['title'])
                ->setDescription($image['description']);
            $em->persist($offerImage);
            $config->addOfferImage($offerImage);
        }

        foreach (self::SLIDER_IMAGES as $image) {
            $sliderImage = (new ConfigSliderImage())
                ->setImageName($image['name'])
                ->setEntity($configSlider)
                ->setOwner($user);
            $configSlider->addSliderImage($sliderImage);
        }

        $em->persist($configSlider);
        $config->setConfigSlider($configSlider);
        $em->persist($config);

        return $config;
    }

    /**
     * @param User $user
     */
    public function createPages(User $user)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        foreach (array_keys(Page::PAGES_ARRAY) as $type) {
            $page = (new Page())
                ->setOwner($user)
                ->setPageType($type)
                ->setStatus(Page::STATUS_INACTIVE);
            $em->persist($page);
        }
    }
}
