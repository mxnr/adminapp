<?php

namespace AdminBundle\Controller;

use AdminBundle\Admin\ModelAdmin;
use AdminBundle\Security\Voter\EntityVoter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Page\PageTypeInterface;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\Queue\Message\SynchronizeTask;

class CrudController extends Controller
{
    /**
     * Get current admin subject
     *
     * @return CloneableInterface
     */
    private function getSubject()
    {
        $object = $this->admin->getSubject();
        if (!$object || $object->getOwner()->getUsername() != 'admin') {
            throw new NotFoundHttpException(
                sprintf(
                    'unable to find the object with id : %s',
                    $this->getRequest()->get($this->admin->getIdParameter())
                )
            );
        }

        return $object;
    }

    /**
     * @return RedirectResponse
     */
    public function cloneAction()
    {
        $object = $this->getSubject();
        $this->get('admin.clone_service')->processObject($object, $this->getUser());

        return new RedirectResponse($this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()]));
    }

    /**
     * @return RedirectResponse
     */
    public function synchronizeAction()
    {
        $object = $this->getSubject();
        $this->get('admin.clone_service')->synchronizeObject($object, $this->getUser());

        return new RedirectResponse($this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()]));
    }

    /**
     * @param null $id
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id = null)
    {
        $this->denyAccessUnlessGranted(EntityVoter::EDIT, $this->admin->getSubject());

        return parent::editAction($id);
    }

    /**
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id = null)
    {
        $this->denyAccessUnlessGranted(EntityVoter::VIEW, $this->admin->getSubject());
        return parent::showAction($id);
    }

    /**
     * @param null $id
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id = null)
    {
        $this->denyAccessUnlessGranted(EntityVoter::DELETE, $this->admin->getSubject());
        return parent::deleteAction($id);
    }

    /**
     * {@inheritDoc}
     */
    public function batchActionDelete(ProxyQueryInterface $query)
    {
        return parent::batchActionDelete($query);
    }

    /**
     * Batch synchronize action
     *
     * @param ProxyQuery $query
     *
     * @return Response
     */
    public function batchActionSynchronize(ProxyQuery $query): Response
    {
        $queryParams = $query->getParameters();
        $queryParamsArray = [];

        /** @var Parameter $param */
        foreach ($queryParams as $param) {
            $queryParamsArray[$param->getName()] = $param->getValue();
        }

        $synchronizeTask = new SynchronizeTask();
        $synchronizeTask
            ->setEntityClass($this->admin->getClass())
            ->setSynchronizeWith($this->getUser()->getId())
            ->setQuery($query->getDql())
            ->setQueryParams($queryParamsArray)
            ->setTask('synchronize');

        $violations = $this->get('validator')->validate($synchronizeTask);

        if ($violations->count() == 0) {
            $this->get('admin.sqs.synchronize_task_producer')->publish((string) $synchronizeTask);
            $this->get('session')->getFlashBag()->add(
                'sonata_flash_success',
                'Objects synchronization scheduled.'
            );
        } else {
            $this->get('session')
                ->getFlashBag()
                ->add(
                    'sonata_flash_error',
                    'Objects synchronization failed.'
                );
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
        );
    }

    /**
     * Batch desynchronize action
     *
     * @param ProxyQuery $query
     *
     * @return Response
     */
    public function batchActionDesynchronize(ProxyQuery $query): Response
    {
        $queryParams = $query->getParameters();
        $queryParamsArray = [];

        /** @var Parameter $param */
        foreach ($queryParams as $param) {
            $queryParamsArray[$param->getName()] = $param->getValue();
        }

        $synchronizeTask = new SynchronizeTask();
        $synchronizeTask
            ->setEntityClass($this->admin->getClass())
            ->setSynchronizeWith($this->getUser()->getId())
            ->setQuery($query->getDql())
            ->setQueryParams($queryParamsArray)
            ->setTask('desynchronize');

        $violations = $this->get('validator')->validate($synchronizeTask);

        if ($violations->count() == 0) {
            $this->get('admin.sqs.synchronize_task_producer')->publish((string) $synchronizeTask);
            $this->get('session')->getFlashBag()->add(
                'sonata_flash_success',
                'Objects desynchronization scheduled.'
            );
        } else {
            $this->get('session')->getFlashBag()->add(
                'sonata_flash_error',
                'Objects desynchronization failed.'
            );
        }

        return new RedirectResponse(
            $this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()])
        );
    }

    /**
     * Print invoice action
     *
     * @return Response
     */
    public function printAction()
    {
        $this->denyAccessUnlessGranted(EntityVoter::VIEW, $this->admin->getSubject());
        /** @var Order $order */
        $order = $this->admin->getSubject();

        $location = $order->getPickupLocation();
        if (null === $location) {
            $locations = $this->getUser()->getLocations();
            $location = $locations[0] ?? null;
            foreach ($locations as $item) {
                if ($item->getIsMain()) {
                    $location = $item;
                }
            }
        }

        $paymentData = $this->get('admin.order_service')->getOrderTotalPrice($order);
        $user = $order->getOwner();
        $disclaimerType = array_flip(PageTypeInterface::ORDER_DISCLAIMER_CONNECTOR)[$order->getSiteType()];

        return $this->render(
            '@Admin/Order/print_invoice.html.twig',
            [
                'order' => $order,
                'subtotal' => $paymentData['subtotal'],
                'tax' => $paymentData['tax'],
                'total' => $paymentData['total'],
                'amountPaid' => $paymentData['amountPaid'],
                'balanceDue' => $paymentData['balanceDue'],
                'location' => $location,
                'disclaimer' => $this->get('admin.disclaimer_service')->getDisclaimer($user, $disclaimerType),
                'user' => $user,
            ]
        );
    }

    /**
     * eBay template of given Inventory Item
     */
    public function templateAction()
    {
        $this->denyAccessUnlessGranted(EntityVoter::VIEW, $this->admin->getSubject());

        $multiple = [];
        /** @var Filter $entity */
        foreach ($this->admin->getSubject()->getFilters() as $entity) {
            if ($entity->getParent() && $entity->getParent()->getIsMultiSelect()) {
                if (isset($multiple[$entity->getParent()->getId()])) {
                    array_push($multiple[$entity->getParent()->getId()]['filters'], $entity);
                } else {
                    $multiple[$entity->getParent()->getId()]['filters'] = [$entity];
                    $multiple[$entity->getParent()->getId()]['parent'] = $entity->getParent();
                }
            }
        }

        $ebayTemplate = $this->get('twig')->render(
            'AdminBundle:EbayTemplates/templates:shop_model.html.twig',
            [
                'inventoryItem' => $this->admin->getSubject(),
                'multiple' => $multiple,
            ]
        );

        return new Response(
            $this->render(
                'AdminBundle:EbayTemplates:create_template_page.html.twig',
                [
                    'ebayTemplate' => $ebayTemplate
                ]
            )
        );
    }
}
