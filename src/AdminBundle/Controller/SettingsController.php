<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;

class SettingsController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function defaultAction()
    {
        $user = $this->getUser();

        if ($user->hasBillingCustomer()) {
            $billingCustomer = $user->getBillingCustomer();
        } else {
            $billingCustomer = new Customer();
        }

        $billingAddressId = null;
        if ($billingCustomer->hasAddress() &&
            (
                $user->hasRole('ROLE_ADMIN') ||
                $user->hasRole('ROLE_SUPER_ADMIN') ||
                ($user->hasRole('ROLE_USER') && $user->hasRole('ROLE_ADMIN_BILLING_ADDRESS_EDIT'))
            )
        ) {
            $billingAddressId = $billingCustomer->getAddress()->getId();
        }

        $paymentMethodDetailsUrl = null;
        if ($billingCustomer->hasChargeBeeId() &&
            (
                $user->hasRole('ROLE_ADMIN') ||
                $user->hasRole('ROLE_SUPER_ADMIN') ||
                ($user->hasRole('ROLE_USER') && $user->hasRole('ROLE_ADMIN_BILLING_PAYMENT_INFO_ALL'))
            )
        ) {
            $paymentMethodDetailsUrl = $this->get('admin.charge_bee.hosted_page')->paymentMethod($billingCustomer)['url'];
        }

        return $this->render(
            '@Admin/Settings/settings_default.html.twig',
            [
                'billingAddressId' => $billingAddressId,
                'paymentMethodDetailsUrl' => $paymentMethodDetailsUrl,
            ]
        );
    }
}
