<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Order\Order;

class SearchController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $queryString = $request->query->get('q');
        if (null === $queryString) {
            return $this->redirectToRoute('sonata_admin_dashboard');
        }
        $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();
        $em = $this->getDoctrine()->getManager();

        return $this->render(
            '@Admin/Search/search.html.twig',
            [
                'models' => $em->getRepository(Model::class)->globalSearchUserModel($user, $queryString),
                'customers' => $em->getRepository(Customer::class)
                    ->globalSearchUserCustomers($user, $queryString),
                'inventoryItems' => $em->getRepository(InventoryItem::class)
                    ->globalSearchUserInventoryItems($user, $queryString),
                'orders' => $em->getRepository(Order::class)->globalSearchUserOrders($user, $queryString),
            ]
        );
    }
}
