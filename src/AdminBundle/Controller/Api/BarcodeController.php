<?php

namespace AdminBundle\Controller\Api;

use AdminBundle\Security\Voter\EntityVoter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;

class BarcodeController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function getActiveBarcodesAction()
    {
        $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();

        $barcodes = $this->getDoctrine()->getManager()->getRepository(InventoryItemBarcode::class)
            ->getUserActiveBarcodesQuery($user)->getResult();
        $result = [];
        /** @var InventoryItemBarcode $barcode */
        foreach ($barcodes as $barcode) {
            $result[] = [
                'id' => $barcode->getId(),
                'name' => (string) $barcode,
            ];
        }

        return new JsonResponse(
            $result,
            200
        );
    }

    /**
     * @return JsonResponse
     */
    public function getBarcodeAction(InventoryItemBarcode $barcode)
    {
        $this->denyAccessUnlessGranted(EntityVoter::EDIT, $barcode->getInventoryItem());

        return new JsonResponse(
            [
                'barcode' => $barcode->getBarcode(),
                'model' => (string) $barcode->getInventoryItem()->getModel() ?? '',
                'priceType' => (string) $barcode->getInventoryItem()->getPrice()->getPriceType(),
                'price' => (string) $barcode->getInventoryItem()->getPrice()->getValue() / 100,
            ],
            200
        );
    }
}
