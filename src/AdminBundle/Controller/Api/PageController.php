<?php

namespace AdminBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ThreeWebOneEntityBundle\Entity\Page\Page;
use ThreeWebOneEntityBundle\Entity\Page\PageTypeInterface;

class PageController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function editPageAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $type = $data['type'] ?? 0;
        $description = $data['content'] ?? 0;

        if (!in_array($type, array_keys(PageTypeInterface::PAGES_ARRAY)) || !$description) {
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
        $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();
        $page = $user->getPageByType($type);
        if (!$page) {
            $page = (new Page())
                ->setPageType($type)
                ->setTitle(PageTypeInterface::PAGES_ARRAY[$type])
                ->setOwner($user);
        }

        $page->setDescription($description);
        $em = $this->getDoctrine()->getManager();
        $em->persist($page);
        $em->flush();

        return new JsonResponse([], Response::HTTP_OK);
    }
}
