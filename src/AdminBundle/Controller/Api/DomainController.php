<?php

namespace AdminBundle\Controller\Api;

use AdminBundle\Controller\BaseDomainController;
use AdminBundle\Form\Type\Domain\ClientDomainType;
use AdminBundle\Security\Voter\SubscriptionRestrictVoter;
use AdminBundle\Service\AccessControlService;
use AdminBundle\Service\ApiResponseService;
use AdminBundle\Service\UserGroupsService;
use ChargeBeeBundle\Service\InvoicesService;
use Doctrine\ORM\EntityManager;
use DomainResellerBundle\Service\OpenSRSService;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;

class DomainController extends BaseDomainController
{
    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var UserGroupsService
     */
    private $userGroupsService;

    /**
     * @var OpenSRSService
     */
    private $openSRSService;

    /**
     * @var InvoicesService
     */
    private $invoiceService;

    /**
     * @var AccessControlService
     */
    private $accessControlService;

    /**
     * @var ApiResponseService
     */
    private $apiResponseService;

    /**
     * DomainController constructor.
     *
     * @param FormFactory $formFactory
     * @param Session $session
     * @param EntityManager $entityManager
     * @param UserGroupsService $userGroupsService
     * @param OpenSRSService $openSRSService
     * @param InvoicesService $invoiceService
     * @param Router $router
     * @param AccessControlService $accessControlService
     * @param ApiResponseService $apiResponseService
     */
    public function __construct(
        FormFactory $formFactory,
        Session $session,
        EntityManager $entityManager,
        UserGroupsService $userGroupsService,
        OpenSRSService $openSRSService,
        InvoicesService $invoiceService,
        Router $router,
        AccessControlService $accessControlService,
        ApiResponseService $apiResponseService
    ) {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->userGroupsService = $userGroupsService;
        $this->openSRSService = $openSRSService;
        $this->invoiceService = $invoiceService;
        $this->accessControlService = $accessControlService;
        $this->apiResponseService = $apiResponseService;

        $this->accessControlService->denyAccessUnlessGranted(SubscriptionRestrictVoter::DOMAIN_RESTRICT);
    }

    /**
     * Get Suggestion Domain Action
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getSuggestionDomainAction(Request $request): JsonResponse
    {
        $rawData = json_decode($request->getContent(), true);

        if (!array_key_exists('domain', $rawData) || !array_key_exists('tld', $rawData)) {
            return $this->apiResponseService->getErrorResponse('Bad Request');
        }

        $suggestedDomains = $this->openSRSService->getSuggestDomainNames($rawData['domain'], $rawData['tld']);

        if ($suggestedDomains['responseCode'] !== OpenSRSService::SUCCESS_REQUEST) {
            return $this->apiResponseService->getErrorResponse($suggestedDomains['responseText']);
        }

        return $this->apiResponseService->getSuccessResponse($suggestedDomains['content']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getDomainPriceAction(Request $request): JsonResponse
    {
        $rawData = json_decode($request->getContent(), true);

        if (!array_key_exists('domain', $rawData)) {
            return $this->apiResponseService->getErrorResponse('Domain name is required');
        }

        $domainPrice = $this->openSRSService->getPrice($rawData['domain']);

        if ($domainPrice['responseCode'] !== OpenSRSService::SUCCESS_REQUEST) {
            return $this->apiResponseService->getErrorResponse($domainPrice['responseText']);
        }

        return $this->apiResponseService->getSuccessResponse($this->getDomainPrice(floatval($domainPrice['content'])));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function buyDomainAction(Request $request): JsonResponse
    {
        $rawData = json_decode($request->getContent(), true);

        if (!array_key_exists('domain', $rawData)) {
            return $this->apiResponseService->getErrorResponse('Domain is required');
        }

        $domainPrice = $this->openSRSService->getPrice($rawData['domain']);

        if ($domainPrice['responseCode'] !== OpenSRSService::SUCCESS_REQUEST) {
            return $this->apiResponseService->getErrorResponse($domainPrice['responseText']);
        }

        $user = $this->userGroupsService->getUserOrUserSupervisor();
        $customer = $user->getBillingCustomer();

        $clientDomainRepo = $this->entityManager->getRepository(clientDomain::class);
        $clientDomain = $clientDomainRepo->findOneBy(['owner' => $user, 'domain' => $rawData['domain']]);

        if (!is_null($clientDomain)) {
            return $this->apiResponseService->getErrorResponse('This domain already used');
        }

        try {
            $this->invoiceService->chargeAddon($customer->getChargeBeeId(),
                $this->getDomainPrice(floatval($domainPrice['content']))
            );

            $contacts = [
                'owner' => $rawData['ownerInfo'],
            ];

            $form = $this->formFactory->create(ClientDomainType::class);
            $form->submit(['domain' => $rawData['domain']]);
            $user = $this->userGroupsService->getUserOrUserSupervisor();

            /* @var ClientDomain $clientDomain */
            $clientDomain = $form->getData()
                ->setOwner($user)
                ->setActive(ClientDomain::ACTIVE)
                ->setDomainType(ClientDomain::CLIENT_DOMAIN_TYPE)
                ->setStatus(ClientDomain::PAYED_WITHOUT_DOMAIN)
                ->setContacts($contacts);

            $this->entityManager->persist($clientDomain);
            $this->entityManager->flush();

            return $this->apiResponseService->getSuccessResponse($clientDomain->getId());
        } catch (\ChargeBee_PaymentException $e) {
            return $this->apiResponseService->getErrorResponse('Not enough money');
        }
    }

    /**
     * Get Expire Date
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getExpireDateAction(Request $request): JsonResponse
    {
        $rawData = json_decode($request->getContent(), true);

        if (!array_key_exists('domain', $rawData)) {
            return $this->apiResponseService->getErrorResponse('Domain is required');
        }

        $result = $this->openSRSService->getDomainExpireDate($rawData['domain']);

        if ($result['responseCode'] !== openSRSService::SUCCESS_REQUEST) {
            return $this->apiResponseService->getErrorResponse($result['responseText'], $result['responseCode']);
        }

        return $this->apiResponseService->getSuccessResponse($result['content']);
    }
}
