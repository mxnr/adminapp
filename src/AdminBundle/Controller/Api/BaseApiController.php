<?php

namespace AdminBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;

class BaseApiController extends Controller
{
    /**
     * @param string $class
     * @param string $method
     * @param string $property
     *
     * @return JsonResponse
     */
    public function getJsonResponseForTypeahead(string $class, string $method, string $property)
    {
        $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();

        $items = $this->getDoctrine()->getManager()->getRepository($class)
            ->$method($user);

        $result = [];
        foreach ($items as $item) {
            $result[] = $item[$property];
        }

        return new JsonResponse(
            $result,
            Response::HTTP_OK
        );
    }
}
