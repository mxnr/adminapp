<?php

namespace AdminBundle\Controller\Api;

use AdminBundle\Security\Voter\EntityVoter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class PriceController extends Controller
{
    /**
     * @param Model $model
     * @param PriceType $priceType
     * @param Request $request
     *
     * @ParamConverter("model", options={"id" = "model_id"})
     * @ParamConverter("priceType", options={"id" = "price_type_id"})
     *
     * @return JsonResponse
     */
    public function editPriceAction(Model $model, PriceType $priceType, Request $request)
    {
        $priceService = $this->get('admin.price_service');
        $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();
        $admin = $this->get('admin.admin_user')->getAdminUser();
        $value = $request->request->get('value', 0);
        $priceService->managePrice($model, $priceType, $value, $this->getUser());
        $adminPrice = $priceService->getPriceByType($model, $priceType->getId(), $admin->getId());
        $synchronized = $priceService->isPriceSynchronized($model, $priceType->getId(), $admin->getId(), $user->getId());

        return new JsonResponse(
            $this->render(
            '@Admin/partials/editable_admin_price.html.twig',
                [
                    'tdWrap' => true,
                    'userPrice' => number_format($value, 2, '.', ''),
                    'priceTypeId' => $priceType->getId(),
                    'priceTypeTitle' => $priceType->getTitle(),
                    'modelId' => $model->getId(),
                    'adminPrice' => $adminPrice ? $adminPrice->getValue()/100 : $adminPrice,
                    'synchronized' => $synchronized
                ]
            )->getContent(),
            200
        );
    }

    /**
     * @param Model $model
     * @param PriceType $priceType
     *
     * @ParamConverter("model", options={"id" = "model_id"})
     * @ParamConverter("priceType", options={"id" = "price_type_id"})
     *
     * @return JsonResponse
     */
    public function synchronizePriceAction(Model $model, PriceType $priceType)
    {
        $priceService = $this->get('admin.price_service');
        $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();
        $admin = $this->get('admin.admin_user')->getAdminUser();
        $adminPrice = $priceService->getPriceByType($model, $priceType->getId(), $admin->getId());
        $userPrice = $priceService->getPriceByType($model, $priceType->getId(), $user->getId());
        $priceService->synchronizePrice($user, $userPrice, $adminPrice);

        return new JsonResponse(
            $this->render(
                '@Admin/partials/editable_admin_price.html.twig',
                [
                    'tdWrap' => true,
                    'userPrice' => 0,
                    'priceTypeId' => $priceType->getId(),
                    'priceTypeTitle' => $priceType->getTitle(),
                    'modelId' => $model->getId(),
                    'adminPrice' => $adminPrice ? $adminPrice->getValue()/100 : $adminPrice,
                    'synchronized' => true
                ]
            )->getContent(),
            200
        );
    }
}
