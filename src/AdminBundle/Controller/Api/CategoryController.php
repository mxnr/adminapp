<?php

namespace AdminBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\CategoryImage;
use ThreeWebOneEntityBundle\Status\StatusInterface;

class CategoryController extends Controller
{
    /**
     * @param string $type
     *
     * @return JsonResponse
     */
    public function getCategoriesAction(string $type)
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)
            ->getUserCategoryByType($this->getUser(), $type);

        $result = [];
        /** @var Category $category */
        foreach ($categories as $category) {
            $result[] = [
                'id' => $category->getId(),
                'name' => $category->getTitle()
            ];
        }

        return new JsonResponse(
            $result,
            200
        );
    }

    /**
     * @param string $type
     *
     * @return JsonResponse
     */
    public function createCategoryAction(string $type, Request $request)
    {
        $rawData = json_decode($request->getContent(), true);
        if (empty($rawData['data']) || !in_array($type, array_keys(Category::TYPES_ARRAY))) {
            return new JsonResponse('bad request', 400);
        }
        $category = (new Category())
            ->setTitle($rawData['data'])
            ->setType($type)
            ->setOwner($this->getUser())
            ->addUser($this->getUser())
            ->setStatus(StatusInterface::STATUS_ACTIVE);

        $em = $this->getDoctrine()->getManager();
        if (!empty($rawData['image'])) {
            $imageFile = $this->get('admin.image_service')
                ->createUploadedFileFormString($rawData['image']);
            $image = (new CategoryImage())
                ->setImageName($imageFile->getClientOriginalName())
                ->setImageFile($imageFile)
                ->setEntity($category)
                ->setOwner($this->getUser())
                ->addUser($this->getUser());
            $em->persist($image);
        }

        $em->persist($category);
        $em->flush($category);
        $em->refresh($category);

        return new JsonResponse(
            [
                'id' => $category->getId(),
                'name' => $category->getTitle(),
            ],
            200
        );
    }
}
