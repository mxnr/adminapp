<?php

namespace AdminBundle\Controller\Api;

use AdminBundle\Service\StatsService;
use AdminBundle\Service\UserGroupsService;
use dateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use ThreeWebOneEntityBundle\Entity\User;

class BusinessPerformanceController
{
    /**
     * @var StatsService
     */
    private $statsService;

    /**
     * @var UserGroupsService
     */
    private $userGroupsService;


    /**
     * BusinessPerformanceController constructor.
     *
     * @param StatsService $statsService
     * @param UserGroupsService $userGroupsService
     */
    public function __construct(StatsService $statsService, UserGroupsService $userGroupsService)
    {
        $this->statsService = $statsService;
        $this->userGroupsService = $userGroupsService;
    }

    /**
     * Get Selling Stats Info Action
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getSellingStatsInfoAction(Request $request): JsonResponse
    {
        $user = $this->userGroupsService->getUserOrUserSupervisor();
        $dateFrom = $request->query->get('dateFrom');
        $dateTo = $request->query->get('dateTo');

        if (!$user instanceof User) {
            return new JsonResponse('Unauthorized', 401);
        }

        if ($dateFrom === '' || $dateTo === '') {
            return new JsonResponse('Bad Request', 400);
        }

        $statsInfo = $this->statsService->getSellingStatsInfo($user, new dateTime($dateFrom), new dateTime($dateTo));

        return new JsonResponse(json_encode($statsInfo), 200);
    }
}
