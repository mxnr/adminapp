<?php

namespace AdminBundle\Controller\Api;

use AdminBundle\Form\Type\CustomerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use Exception;

class CustomerController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function getCustomersAction()
    {
        $result = [];
        /** @var Customer $customer */
        foreach ($this->getUser()->getCustomers() as $customer) {
            $result[] = [
                'id' => $customer->getId(),
                'email' => $customer->getEmail(),
                'firstName' => $customer->getFirstname(),
                'lastName' => $customer->getLastname(),
                'business' => $customer->getCompany()
            ];
        }

        return new JsonResponse(
            $result,
            200
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createCustomerAction(Request $request)
    {
        try {
            $data = json_decode($request->getContent(), true);
            $customer = new Customer();
            $form = $this->createForm(CustomerType::class, $customer);
            $form->submit($data);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();
                $customer->setOwner($user)
                    ->setPassword('qwerty12'); //need to add password generation and send mail to customer
                $em->persist($customer);
                $em->flush();
            }

            return new JsonResponse([], Response::HTTP_OK);
        } catch (Exception $exception) {

            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }
    }
}
