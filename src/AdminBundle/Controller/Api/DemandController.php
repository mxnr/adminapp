<?php

namespace AdminBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class DemandController.
 */
class DemandController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function synchronizePoolAction(Request $request)
    {
        return $this->runSqsWorker('synchronize_pool', $this->get('kernel'), $request->get('timeLimit'));
    }

    /**
     * @param string $worker
     * @param KernelInterface $kernel
     * @param int $timePeriod
     *
     * @return Response
     * @throws \Exception
     */
    protected function runSqsWorker(string $worker, KernelInterface $kernel, int $timePeriod = 10): Response
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(
            array(
                'command' => 'admin:sqs_worker:batch',
                'name' => $worker,
                'prefetchCount' => 10,
                'timePeriod' => $timePeriod,
            )
        );

        $output = new BufferedOutput();
        $application->run($input, $output);

        return new Response($output->fetch());
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function synchronizeTaskAction(Request $request): Response
    {
        return $this->runSqsWorker('synchronize_task', $this->get('kernel'), $request->get('timeLimit'));
    }

    public function chargeBeeSyncSubscriptionsAction(): Response
    {
        return $this->runChargeBeeCron('subscriptions', $this->get('kernel'));
    }

    protected function runChargeBeeCron(string $task, KernelInterface $kernel): Response
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);

        $input = new ArrayInput(['command' => 'admin:billing:update:' . $task]);

        $output = new BufferedOutput();
        $application->run($input, $output);

        return new Response($output->fetch());
    }

    /**
     * @return Response
     */
    public function chargeBeeSyncCustomers(): Response
    {
        return $this->runChargeBeeCron('customers', $this->get('kernel'));
    }

    /**
     * @return Response
     */
    public function chargeBeeSyncPlansAction(): Response
    {
        return $this->runChargeBeeCron('plans', $this->get('kernel'));
    }
}
