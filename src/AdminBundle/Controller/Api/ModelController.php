<?php

namespace AdminBundle\Controller\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use ThreeWebOneEntityBundle\Entity\Model;

class ModelController extends BaseApiController
{
    /**
     * @return JsonResponse
     */
    public function getModelsAction()
    {
        $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();

        $models = $this->getDoctrine()->getManager()->getRepository(Model::class)
            ->getUserModelsQuery($user)->getQuery()->getResult();
        $result = [];
        /** @var Model $model */
        foreach ($models as $model) {
            $result[] = [
                'id' => $model->getId(),
                'name' => (string) $model,
            ];
        }

        return new JsonResponse(
            $result,
            200
        );
    }

    /**
     * @return JsonResponse
     */
    public function getModelsTitlesAction()
    {
        return $this->getJsonResponseForTypeahead(Model::class, 'getModelTitlesForUser', 'title');
    }
}
