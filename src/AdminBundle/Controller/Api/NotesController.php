<?php

namespace AdminBundle\Controller\Api;

use AdminBundle\Security\Voter\EntityVoter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ThreeWebOneEntityBundle\Entity\ClientNotes\ClientNotes;

class NotesController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function notesAction()
    {
        $clientNotesRepo = $this->getDoctrine()->getManager()->getRepository(ClientNotes::class);
        $notes = $clientNotesRepo->getAllClientNotes($this->getUser());
        $response = [];
        /** @var ClientNotes $item */
        foreach ($notes as $item) {
            $response[] = [
                'id' => $item->getId(),
                'note' => $item->getContent(),
                'createdAt' => $item->getCreatedAt()->format('F d-Y H:i'),
                'owner' => $item->getOwner()->getUsername(),
            ];
        }

        return new JsonResponse($response);
    }

    /**
     * @param ClientNotes $clientNote
     * @return JsonResponse
     */
    public function deleteAction(ClientNotes $clientNote)
    {
        $this->denyAccessUnlessGranted(EntityVoter::DELETE, $clientNote);
        $em = $this->getDoctrine()->getManager();
        $em->remove($clientNote);
        $em->flush();

        return new JsonResponse([]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $rawData = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        /** @var ClientNotes $clientNotes*/
        $clientNotes = (new ClientNotes())
            ->setContent($rawData['content'])
            ->setOwner($user);
        $em->persist($clientNotes);
        $em->flush();

        return new JsonResponse([]);
    }
}
