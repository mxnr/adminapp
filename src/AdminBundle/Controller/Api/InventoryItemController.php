<?php

namespace AdminBundle\Controller\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;

class InventoryItemController extends BaseApiController
{
    const TYPE_INVENTORY = 'inventory';
    const TYPE_ORDER = 'order';

    /**
     * @param string $type
     *
     * @return JsonResponse
     */
    public function getInventoryItemsTitlesAction(string $type)
    {
        $method = null;
        if ($type === self::TYPE_INVENTORY) {
            $method = 'getInventoryItemsTitlesForUser';
        }
        if ($type === self::TYPE_ORDER) {
            $method = 'getOrderInventoryItemsTitlesForUser';
        }

        if (!$method) {
            return new JsonResponse([], Response::HTTP_OK);
        }

        return $this->getJsonResponseForTypeahead(
            InventoryItem::class,
            $method,
            'title'
        );
    }

    /**
     * @param string $type
     *
     * @return JsonResponse
     */
    public function getInventoryItemsBarcodesAction(string $type)
    {
        $method = null;
        if ($type === self::TYPE_INVENTORY) {
            $method = 'getInventoryItemsBarcodesForUser';
        }
        if ($type === self::TYPE_ORDER) {
            $method = 'getOrdersBarcodesForUser';
        }

        if (!$method) {
            return new JsonResponse([], Response::HTTP_OK);
        }

        return $this->getJsonResponseForTypeahead(
            InventoryItemBarcode::class,
            $method,
            'barcode'
        );
    }
}
