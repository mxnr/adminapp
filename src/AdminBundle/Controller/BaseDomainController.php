<?php

namespace AdminBundle\Controller;

abstract class BaseDomainController
{
    const APP_TAX = 0;
    const WHOIS_PRIVACY_PURCHASE = 300; //3$

    const CONTACTS_TYPE_OWNER_NAME = 'Owner';
    const CONTACTS_TYPE_BILLING_NAME = 'Billing';
    const CONTACTS_TYPE_ADMIN_NAME = 'Admin';
    const CONTACTS_TYPE_TECH_NAME = 'Tech';

    const ADMIN_CONTACT_INFO = [
        "country"=> "US",
        "org_name"=> "3web1",
        "phone"=> "+1.7865430565",
        "state"=>"Florida",
        "email"=>"admin@3web1.com",
        "city"=>"Miami",
        "postal_code"=>"33169",
        "address1"=>"21135 nw 14th pl unit 257",
        "first_name"=>"andre Van Veen",
        "last_name"=> "andre Van Veen"
    ];

    /**
     * @param float $domainPrice
     * @return int
     */
    protected function getDomainPrice(float $domainPrice): int
    {
        return intval($domainPrice * 100) + self::APP_TAX + self::WHOIS_PRIVACY_PURCHASE;
    }

    /**
     * @param array $ownerContacts
     * @return array
     */
    protected function getContacts(array $ownerContacts): array
    {
        $contacts['owner'] = $ownerContacts;
        $contacts['billing'] = $ownerContacts;
        $contacts['admin'] = self::ADMIN_CONTACT_INFO;
        $contacts['tech'] = self::ADMIN_CONTACT_INFO;

        $contacts['owner']['address3'] = self::CONTACTS_TYPE_OWNER_NAME;
        $contacts['admin']['address3'] = self::CONTACTS_TYPE_ADMIN_NAME;
        $contacts['billing']['address3'] = self::CONTACTS_TYPE_BILLING_NAME;
        $contacts['tech']['address3'] = self::CONTACTS_TYPE_TECH_NAME;

        return $contacts;
    }
}
