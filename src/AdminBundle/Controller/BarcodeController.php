<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;

class BarcodeController extends Controller
{
    public function displayBarcodesAction(InventoryItem $inventoryItem)
    {
        $barcode = $this->get('hackzilla_barcode');
        $barcodes = [];
        foreach ($inventoryItem->getInventoryItemBarcodes() as $item) {
            $barcodes[] = [
                'barcode' => $barcode->outputHtml($item->getBarcode()),
                'code' => $item->getBarcode(),
            ];
        }

        return $this->render(
            '@Admin/Barcode/list.html.twig',
            [
                'barcodes' => $barcodes,
                'inventoryItem' => $inventoryItem,
            ]
        );
    }
}
