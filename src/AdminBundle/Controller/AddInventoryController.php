<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\Type\AddInventoryType;
use AdminBundle\Form\Type\CustomerType;
use AdminBundle\Form\Type\DisclaimerType;
use AdminBundle\Form\Type\OrderType;
use AdminBundle\Security\Voter\EntityVoter;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;
use ThreeWebOneEntityBundle\Entity\Order\OrderItem;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Page\Page;

class AddInventoryController extends Controller
{
    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function createAction(Request $request)
    {
        $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();
        $form = $this->createForm(
            AddInventoryType::class,
            new Order(),
            [
                'user' => $user,
                'action' => $this->generateUrl('admin_add_inventory_items'),
                'method' => 'POST',
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $order = $this->get('admin.order_service')->createAddInventoryOrder($form, $user);
            $this->addFlash('success', 'Order created');

            $inventoryItemService = $this->get('admin.inventory_item_service');
            /** @var InventoryItem $item */
            foreach ($order->getItems() as $item) {
                $item->setStatus(InventoryItem::STATUS_INACTIVE);
                $inventoryItemService->generateBarcodes($item, $order);
            }

            return $this->redirectToRoute('admin_inventory_item_list');
        }

        return $this->render(
            '@Admin/inventory/add_inventory.html.twig',
            [
                'form' => $form->createView(),
                'customerForm' => $this->createForm(CustomerType::class, new Customer())->createView(),
                'disclaimer' => $this->get('admin.disclaimer_service')->getDisclaimer($user, Page::DISCLAIMER_BUYBACK),
                'disclaimerForm' => $this->createForm(DisclaimerType::class)->createView(),
            ]
        );
    }
}
