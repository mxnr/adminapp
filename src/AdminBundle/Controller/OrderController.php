<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\Type\CustomerType;
use AdminBundle\Form\Type\DisclaimerType;
use AdminBundle\Form\Type\OrderType;
use AdminBundle\Security\Voter\EntityVoter;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use AdminBundle\Service\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;
use ThreeWebOneEntityBundle\Entity\Order\OrderItem;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Page\Page;
use ThreeWebOneEntityBundle\Entity\Page\PageTypeInterface;

class OrderController extends Controller
{
    /**
     * @param int $type
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function createAction(int $type, Request $request)
    {
        if (!in_array($type, array_keys(SiteSplitService::ORDER_TYPE_CONNECTOR))) {
            throw new NotFoundHttpException($type . ' type doesn\'t exist');
        }
        $this->denyAccessUnlessGranted(SiteSplitService::ORDER_TYPE_CONNECTOR[$type], $this->get('admin.charge_bee.site_split'));

        $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();
        $form = $this->createForm(
            OrderType::class,
            (new Order())->setSiteType($type),
            [
                'user' => $user,
                'action' => $this->generateUrl('admin_create_order', ['type' => $type]),
                'method' => 'POST',
            ]
        );

        $disclaimerType = array_flip(PageTypeInterface::ORDER_DISCLAIMER_CONNECTOR)[$type];
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->getConfig()->setTax($form->get('tax')->getData());
            $order = $this->get('admin.order_service')->createOrder($form, $user, $type);
            $this->addFlash('success', 'Order created');

            return $this->redirectToRoute('admin_edit_order', ['id' => $order->getStoreId()]);
        }

        return $this->render(
            '@Admin/Order/order_edit.html.twig',
            [
                'form' => $form->createView(),
                'customerForm' => $this->createForm(CustomerType::class, new Customer())->createView(),
                'status' => 0,
                'type' => $type,
                'disclaimer' => $this->get('admin.disclaimer_service')->getDisclaimer($user, $disclaimerType),
                'disclaimerForm' => $this->createForm(DisclaimerType::class)->createView(),
            ]
        );
    }

    /**
     * @param int $id
     * @param Request $request
     *
     * @return Response
     */
    public function editAction($id, Request $request)
    {
        $user = $this->get('admin.user_group_service')->getUserOrUserSupervisor();
        $order = $this->getDoctrine()->getRepository(Order::class)->findBy([
            'storeId' => $id,
            'owner' => $user
        ])[0] ?? null;
        if (!$order) {
            throw new NotFoundHttpException(sprintf('Order with id %s don\'t exist', $id));
        }
        $type = $order->getSiteType();
        if (!in_array($type, array_keys(SiteSplitService::ORDER_TYPE_CONNECTOR))) {
            throw new NotFoundHttpException($type . ' type don\'t exist');
        }
        $this->denyAccessUnlessGranted(
            SiteSplitService::ORDER_TYPE_CONNECTOR[$type],
            $this->get('admin.charge_bee.site_split')
        );
        $this->denyAccessUnlessGranted(EntityVoter::EDIT, $order);

        $orderService = $this->get('admin.order_service');

        $orderItems = [];
        foreach ($order->getPurchaseBarcodes() as $barcode) {
            $orderItems[] = $barcode->getorderItem();
        }
        $oldInventoryItems = $order->getItems()->toArray();
        $form = $this->createForm(
            OrderType::class,
            $order,
            [
                'user' => $user,
                'orderItems' => $orderItems,
                'action' => $this->generateUrl('admin_edit_order', ['id' => $order->getStoreId()]),
                'method' => 'POST',
            ]
        );
        $disclaimerType = array_flip(PageTypeInterface::ORDER_DISCLAIMER_CONNECTOR)[$type];

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->getConfig()->setTax($form->get('tax')->getData());
            if ($request->request->has('btn_complete')) {
                $order->setStatus(Order::COMPLETED);
                $this->addFlash('success', 'Order completed');
            }
            if ($request->request->has('btn_reject')) {
                $order->setStatus(Order::REJECTED);
                $this->addFlash('success', 'Order rejected');
            }
            $order = $orderService->processOrder($form, $order, $oldInventoryItems, $orderItems);
        }

        return $this->render(
            '@Admin/Order/order_edit.html.twig',
            [
                'form' => $form->createView(),
                'status' => $order->getStatus(),
                'type' => $type,
                'updatedAt' => $order->getUpdatedAt(),
                'originalOrder' => $orderService->prepareOriginalOrder($order),
                'order' => $order,
                'customerForm' => $this->createForm(CustomerType::class, new Customer())->createView(),
                'disclaimer' => $this->get('admin.disclaimer_service')->getDisclaimer($user, $disclaimerType),
                'disclaimerForm' => $this->createForm(DisclaimerType::class)->createView(),
            ]
        );
    }
}
