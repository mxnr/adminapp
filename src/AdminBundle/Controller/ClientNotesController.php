<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ThreeWebOneEntityBundle\Entity\ClientNotes\ClientNotes;
use AdminBundle\Form\Type\ClientNotesType;
use AdminBundle\Security\Voter\EntityVoter;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ClientNotesController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function createAction(Request $request)
    {
        $form = $this->createForm(ClientNotesType::class, new ClientNotes());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            /** @var ClientNotes $clientNotes*/
            $clientNotes = $form->getData();
            $clientNotes->setOwner($user);
            $em->persist($clientNotes);
            $em->flush();

            $this->addFlash('sonata_flash_success', 'Added new notice ');
        } else {
            $this->addFlash('sonata_flash_error', 'Error: Can\'t add new notice');
        }

        return $this->redirectToRoute('sonata_admin_dashboard');
    }

    /**
     * @param ClientNotes $clientNote
     * @return RedirectResponse
     */
    public function deleteAction(ClientNotes $clientNote)
    {
        $this->denyAccessUnlessGranted(EntityVoter::DELETE, $clientNote);
        $em = $this->getDoctrine()->getManager();
        $em->remove($clientNote);
        $em->flush();

        $this->addFlash(
            'sonata_flash_success',
            'Note deleted!'
        );

        return $this->redirectToRoute('sonata_admin_dashboard');
    }
}
