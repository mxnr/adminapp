<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

class ProfitCalculatorController
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * ProfitCalculatorController constructor.
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Profit Calculator Page Action
     * @return Response
     */
    public function profitCalculatorPageAction()
    {
        return new Response($this->twig->render('AdminBundle:ProfitCalculator:profit_calculator_page.html.twig'));
    }
}
