<?php

namespace AdminBundle\Controller;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Response;
use Twig_Environment;

class BusinessPerformanceController
{
    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * BusinessPerformanceController constructor.
     *
     * @param Twig_Environment $twig
     * @param FormFactory $formFactory
     */
    public function __construct(Twig_Environment $twig, FormFactory $formFactory)
    {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
    }

    /**
     * Business Performance Page Action
     *
     * @return Response
     */
    public function businessPerformancePageAction(): Response
    {
        return new Response($this->twig->render('AdminBundle:BusinessPerformance:business_performance_page.html.twig'));
    }
}
