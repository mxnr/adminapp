<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\Type\EbayTemplatesType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use Twig_Environment;

class EbayTemplatesController
{
    /**
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * EbayTemplatesController constructor.
     * @param Twig_Environment $twig
     * @param FormFactory $formFactory
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        Twig_Environment $twig,
        FormFactory $formFactory,
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage
    ) {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->token = $tokenStorage;
    }

    /**
     * Create Template Page Action
     *
     * @return Response
     */
    public function createTemplatePageAction(Request $request): Response
    {
        $user = $this->token->getToken()->getUser();
        $inventoryItemsRepo = $this->em->getRepository(InventoryItem::class);
        $ebayTemplate = '';

        $inventoryItems = $inventoryItemsRepo->findAllUserActiveItems($user);
        $form = $this->formFactory->create(EbayTemplatesType::class, null, ['inventoryItems' => $inventoryItems]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $ebayTemplate = $this->twig->render(
                'AdminBundle:EbayTemplates/templates:shop_model.html.twig',
                [
                    'inventoryItem' => $formData['inventoryItem']
                ]
            );
        }

        return new Response(
            $this->twig->render(
            'AdminBundle:EbayTemplates:create_template_page.html.twig',
            [
                'form' => $form->createView(),
                'ebayTemplate' => $ebayTemplate
            ]
        )
        );
    }
}
