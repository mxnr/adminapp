<?php

namespace AdminBundle\Repository;

use AdminBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

/**
 * InventoryRepository
 */
class InventoryRepository extends EntityRepository
{
    /**
     * Gets User Inventories
     *
     * @param \AdminBundle\Entity\User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUserInventoriesQuery(User $user)
    {
        $qb = $this->createQueryBuilder('i');
        $qb->andWhere('i.user = :user');
        $qb->setParameter('user', $user);
        $qb->getQuery()->useResultCache(true, 36000)->useQueryCache(true);

        return $qb;
    }
}
