<?php

namespace AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * FilterRepository
 */
class FilterRepository extends EntityRepository
{
    /**
     * Gets Parent Filters
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getParentFiltersForm()
    {
        $qb = $this->createQueryBuilder('f');
        $qb->andWhere('f.parent IS NULL');
        $qb->getQuery()->useResultCache(true, 36000)->useQueryCache(true);

        return $qb;
    }
}
