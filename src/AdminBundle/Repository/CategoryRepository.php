<?php

namespace AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CategoryRepository
 */
class CategoryRepository extends EntityRepository
{
    /**
     * Gets Parent Categories
     *
     * @param integer $type
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getParentCategoriesForm($type)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->andWhere('c.parent IS NULL AND c.type = :category_type');
        $qb->setParameter('category_type', $type);
        $qb->getQuery()->useResultCache(true, 36000)->useQueryCache(true);

        return $qb;
    }
}
