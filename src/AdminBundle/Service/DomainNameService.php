<?php

namespace AdminBundle\Service;

use AdminBundle\Event\DnsEvent;
use Aws\CloudFront\CloudFrontClient;
use Aws\Exception\AwsException;
use Aws\Route53\Route53Client;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use ThreeWebOneEntityBundle\Entity\Domain\Zone;

class DomainNameService
{
    const CLOUFDRONT_TYPE_CF = 'cf';

    const CLOUDFRONT_TYPE_EC2 = 'ec2';

    const CLOUDFRONT_TYPE_LIST = [
        self::CLOUFDRONT_TYPE_CF,
        self::CLOUDFRONT_TYPE_EC2,
    ];

    const EVENT_DNS_UNLINK = 'dns.unlink';

    const EVENT_DNS_LINK = 'dns.link';

    const RECORD_TYPE_LIST = [
        'SOA' => true,
        'A' => true,
        'TXT' => true,
        'NS' => true,
        'CNAME' => true,
        'MX' => true,
        'NAPTR' => true,
        'PTR' => true,
        'SRV' => true,
        'SPF' => true,
        'AAAA' => true,
        'CAA' => true,
    ];

    /**
     * @var Route53Client
     */
    private $route53;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var CloudFrontClient
     */
    private $cloudFront;

    /**
     * @var string
     */
    private $cloudFrontDistributionId;

    /**
     * @var string
     */
    private $cloudFrontDistributionHost;

    /**
     * @var
     */
    private $cloudFrontServiceHost;

    /**
     * @var string
     */
    private $cloudFrontDistributionZoneId;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var bool
     */
    private $serviceEnabledFlag = true;

    /**
     * @var string
     */
    private $cloudFrontType;

    /**
     * DomainNameService constructor.
     *
     * @param Route53Client          $route53Client
     * @param CloudFrontClient       $cloudFrontClient
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface        $logger
     * @param string                 $cloudFrontDistributionId
     * @param string                 $cloudFrontDistributionHost
     * @param string                 $cloudFrontDistributionZoneId
     * @param string                 $cloudFrontServiceHost
     * @param string                 $cloudFrontType
     */
    public function __construct(
        Route53Client $route53Client,
        CloudFrontClient $cloudFrontClient,
        EntityManagerInterface $entityManager,
        LoggerInterface $logger,
        string $cloudFrontDistributionId,
        string $cloudFrontDistributionHost,
        string $cloudFrontDistributionZoneId,
        string $cloudFrontServiceHost,
        string $cloudFrontType = self::CLOUFDRONT_TYPE_CF
    ) {
        $this->route53 = $route53Client;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->cloudFront = $cloudFrontClient;
        $this->cloudFrontDistributionId = $cloudFrontDistributionId;
        $this->cloudFrontDistributionHost = $cloudFrontDistributionHost;
        $this->cloudFrontDistributionZoneId = $cloudFrontDistributionZoneId;
        $this->cloudFrontServiceHost = $cloudFrontServiceHost;

        $this->setCloudFrontType($cloudFrontType);

        if (
            empty($this->cloudFrontDistributionId) ||
            empty($this->cloudFrontDistributionHost) ||
            empty($this->cloudFrontDistributionZoneId)
        ) {
            $logger->notice('DomainNameService is not configured, may cause some failures');
            $this->setServiceEnabledFlag(false);
        } else {
            $this->setServiceEnabledFlag(true);
        }
    }

    /**
     * @return mixed|null
     */
    public function getCloudFrontServiceHost()
    {
        return $this->cloudFrontServiceHost;
    }

    /**
     * @return bool
     */
    public function hasCloudFrontServiceHost()
    {
        return !is_null($this->cloudFrontServiceHost);
    }

    /**
     * @param mixed|null $cloudFrontServiceHost
     *
     * @return DomainNameService
     */
    public function setCloudFrontServiceHost($cloudFrontServiceHost = null)
    {
        $this->cloudFrontServiceHost = $cloudFrontServiceHost;

        return $this;
    }

    /**
     * @param DnsEvent $event
     */
    public function onEventUnlink(DnsEvent $event)
    {
        if (!$event->hasZone()) {
            throw new InvalidArgumentException('zone is required for this event');
        }

        $zone = $event->getZone();

        if (!$zone->hasOwner()) {
            throw new InvalidArgumentException('zone must have owner to continue');
        }

        if (!$zone->hasDomain()) {
            throw new InvalidArgumentException('zone must have domain to continue');
        }

        if ($this->isServiceEnabledFlag()) {
            $zone->getOwner()->setDomainZone(null);

            if ($this->getCloudFrontType() === self::CLOUFDRONT_TYPE_CF) {
                $this->removeCname($zone->getDomain());
            }
            $this->clearRecordsForZone($zone);
            $this->unlinkZone($zone);

            $this->getEntityManager()->remove($zone);
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return bool
     */
    public function isServiceEnabledFlag(): bool
    {
        return $this->serviceEnabledFlag;
    }

    /**
     * @param bool $serviceEnabledFlag
     *
     * @return DomainNameService
     */
    public function setServiceEnabledFlag(bool $serviceEnabledFlag): DomainNameService
    {
        $this->serviceEnabledFlag = $serviceEnabledFlag;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCloudFrontType(): ?string
    {
        return $this->cloudFrontType;
    }

    /**
     * @return bool
     */
    public function hasCloudFrontType(): bool
    {
        return !is_null($this->cloudFrontType);
    }

    /**
     * @param string|null $cloudFrontType
     *
     * @return DomainNameService
     */
    public function setCloudFrontType(string $cloudFrontType = null): DomainNameService
    {
        if (in_array($cloudFrontType, self::CLOUDFRONT_TYPE_LIST) === false) {
            throw new InvalidArgumentException('Unknown cloudFront type <![CDATA[' . $cloudFrontType . ']]>');
        }

        $this->cloudFrontType = $cloudFrontType;

        return $this;
    }

    /**
     * @param string $domain
     *
     * @return bool
     */
    public function removeCname(string $domain): bool
    {
        try {
            $cloudFront = $this->getCloudFront();

            $cloudFrontConfig = $cloudFront->getDistribution(
                ['Id' => $this->getCloudFrontDistributionId()]
            );

            if (isset($cloudFrontConfig['Distribution']['DistributionConfig'])) {
                $distributionConfig = $cloudFrontConfig['Distribution']['DistributionConfig'];
            }

            $needToUpdate = false;

            if (isset($distributionConfig['Aliases']['Items'])) {
                foreach ($distributionConfig['Aliases']['Items'] as $id => $alias) {
                    if ($alias == $domain) {
                        $needToUpdate = true;
                        $distributionConfig['Aliases']['Quantity']--;
                        unset($distributionConfig['Aliases']['Items'][$id]);
                    }
                }
            }

            if ($needToUpdate) {
                $updatedDistribution['DistributionConfig'] = $distributionConfig;
                $updatedDistribution['Id'] = $this->getCloudFrontDistributionId();
                $updatedDistribution['IfMatch'] = $cloudFrontConfig['ETag'];

                $cloudFront->updateDistribution($updatedDistribution);

                return true;
            }

            return false;
        } catch (AwsException $e) {
            $this->logAwsException($e, ['method' => __METHOD__]);

            return false;
        }
    }

    /**
     * @return CloudFrontClient|null
     */
    public function getCloudFront(): ?CloudFrontClient
    {
        return $this->cloudFront;
    }

    /**
     * @return bool
     */
    public function hasCloudFront(): bool
    {
        return !is_null($this->cloudFront);
    }

    /**
     * @return string|null
     */
    public function getCloudFrontDistributionId(): ?string
    {
        return $this->cloudFrontDistributionId;
    }

    /**
     * @return bool
     */
    public function hasCloudFrontDistributionId(): bool
    {
        return !is_null($this->cloudFrontDistributionId);
    }

    /**
     * @param AwsException $awsException
     * @param array        $context
     */
    private function logAwsException(AwsException $awsException, array $context)
    {
        $this->getLogger()->error('Got aws error: <![CDATA[' . $awsException->getMessage() . ']]>', $context);
    }

    /**
     * @return LoggerInterface|null
     */
    public function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }

    /**
     * @return bool
     */
    public function hasLogger(): bool
    {
        return !is_null($this->logger);
    }

    /**
     * @param Zone  $zone
     *
     * @param array $include
     * @param array $except
     *
     * @return bool
     */
    public function clearRecordsForZone(Zone $zone, array $include = [], array $except = []): bool
    {
        $route53 = $this->getRoute53();

        $except['NS'] = true;
        $except['SOA'] = true;

        $result = $route53->listResourceRecordSets(
            [
                'HostedZoneId' => $zone->getZoneId(),
            ]
        );

        try {
            $changes = [];
            if (isset($result['ResourceRecordSets'])) {
                foreach ($result['ResourceRecordSets'] as $record) {
                    if ((empty($include) || isset($include[$record['Type']])) && !isset($except[$record['Type']])) {
                        $changes[] = [
                            'Action' => 'DELETE',
                            'ResourceRecordSet' => $record,
                        ];
                    }
                }
            }

            if (!empty($changes)) {
                $route53 = $this->getRoute53();
                $route53->changeResourceRecordSets(
                    [
                        'ChangeBatch' => [
                            'Changes' => $changes,
                        ],
                        'HostedZoneId' => $zone->getZoneId(),
                    ]
                );

                return true;
            }

            return false;
        } catch (AwsException $e) {
            $this->logAwsException($e, ['method' => __METHOD__]);

            return false;
        }
    }

    /**
     * @return Route53Client|null
     */
    public function getRoute53(): ?Route53Client
    {
        return $this->route53;
    }

    /**
     * @return bool
     */
    public function hasRoute53(): bool
    {
        return !is_null($this->route53);
    }

    /**
     * @param Route53Client|null $route53
     *
     * @return DomainNameService
     */
    public function setRoute53(Route53Client $route53 = null): DomainNameService
    {
        $this->route53 = $route53;

        return $this;
    }

    /**
     * @param Zone $zone
     *
     * @return bool
     */
    public function unlinkZone(Zone $zone): bool
    {
        if ($zone->hasZoneId() === false) {
            throw new \InvalidArgumentException('zone must have zoneId to continue');
        }

        try {
            $this->getRoute53()->deleteHostedZone(['Id' => $zone->getZoneId()]);

            return true;
        } catch (AwsException $e) {
            $this->logAwsException($e, ['method' => __METHOD__]);

            return false;
        }
    }

    /**
     * @return EntityManager|null
     */
    public function getEntityManager(): ?EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return bool
     */
    public function hasEntityManager(): bool
    {
        return !is_null($this->entityManager);
    }

    /**
     * @param EntityManagerInterface|null $entityManager
     *
     * @return DomainNameService
     */
    public function setEntityManager(EntityManagerInterface $entityManager = null): DomainNameService
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @param DnsEvent $event
     */
    public function onEventLink(DnsEvent $event)
    {
        if (!$event->hasZone()) {
            throw new InvalidArgumentException('event must have zone to continue');
        }

        $zone = $event->getZone();

        if (!$zone->hasDomain()) {
            throw new InvalidArgumentException('zone must have domain to continue');
        }

        if ($this->isServiceEnabledFlag()) {
            $this->linkZone($zone);
            if ($this->getCloudFrontType() === self::CLOUFDRONT_TYPE_CF) {
                $this->addCname($zone->getDomain());
            } else {
                $this->addRecord(
                    $zone,
                    'A',
                    $this->getCloudFrontDistributionHost(),
                    true
                );
            }

            $this->getEntityManager()->persist($zone);
            $this->getEntityManager()->flush();

            $this->addCloudFrontAlias($zone);
        }
    }

    /**
     * @param string $domain
     *
     * @return bool
     */
    public function addCname(string $domain): bool
    {
        try {
            $cloudFront = $this->getCloudFront();

            $cloudFrontConfig = $cloudFront->getDistribution(
                ['Id' => $this->getCloudFrontDistributionId()]
            );

            $distributionConfig = null;
            if (isset($cloudFrontConfig['Distribution']['DistributionConfig'])) {
                $distributionConfig = $cloudFrontConfig['Distribution']['DistributionConfig'];
            } else {
                return false;
            }

            $needToAddDomain = true;
            if (isset($distributionConfig['Aliases']['Items'])) {
                foreach ($distributionConfig['Aliases']['Items'] as $alias) {
                    if ($alias == $domain) {
                        $needToAddDomain = false;
                    }
                }
            }

            if ($needToAddDomain) {
                $distributionConfig['Aliases']['Quantity']++;
                $distributionConfig['Aliases']['Items'][] = $domain;

                $updatedDistribution['DistributionConfig'] = $distributionConfig;
                $updatedDistribution['Id'] = $this->getCloudFrontDistributionId();
                $updatedDistribution['IfMatch'] = $cloudFrontConfig['ETag'];

                $cloudFront->updateDistribution($updatedDistribution);

                return true;
            }

            return false;
        } catch (AwsException $e) {
            $this->logAwsException($e, ['method' => __METHOD__]);

            return false;
        }
    }

    /**
     * @param Zone $zone
     *
     * @return bool
     */
    public function linkZone(Zone $zone): bool
    {
        if ($zone->hasOwner() === false) {
            throw new InvalidArgumentException('zone must already have owner to continue');
        }

        if ($zone->hasDomain() === false) {
            throw new InvalidArgumentException('zone must already have domain to continue');
        }

        try {
            $route53 = $this->getRoute53();

            $user = $zone->getOwner();
            $domain = $zone->getDomain();

            $result = $route53->createHostedZone(
                [
                    'CallerReference' => md5($user->getId() . $domain . time()),
                    'HostedZoneConfig' => [
                        'Comment' => 'Zone for "' . $user->getStoreName() . '" store.',
                        'PrivateZone' => false,
                    ],
                    'Name' => $domain,
                ]
            );

            $user->setDomainZone($zone);
            $zone
                ->setZoneId($result['HostedZone']['Id'])
                ->setNsList($result['DelegationSet']['NameServers'])
                ->setDistributionId($this->getCloudFrontDistributionId());

            return true;
        } catch (AwsException $e) {
            $this->logAwsException($e, ['method' => __METHOD__]);

            return false;
        }
    }

    /**
     * @param Zone $zone
     *
     * @return bool
     */
    public function addCloudFrontAlias(Zone $zone): bool
    {
        try {
            $route53 = $this->getRoute53();
            $route53->changeResourceRecordSets(
                [
                    'ChangeBatch' => [
                        'Changes' => [
                            [
                                'Action' => 'CREATE',
                                'ResourceRecordSet' => [
                                    'AliasTarget' => [
                                        'DNSName' => $this->getCloudFrontDistributionHost(),
                                        'EvaluateTargetHealth' => false,
                                        'HostedZoneId' => $this->getCloudFrontDistributionZoneId(),
                                    ],
                                    'Name' => $zone->getDomain(),
                                    'Type' => 'A',
                                ],
                            ],
                        ],
                        'Comment' => 'Alias record for ' . $zone->getZoneId() . ' zone',
                    ],
                    'HostedZoneId' => $zone->getZoneId(),
                ]
            );

            return true;
        } catch (AwsException $e) {
            $this->logAwsException($e, ['method' => __METHOD__]);

            return false;
        }
    }

    /**
     * @return string|null
     */
    public function getCloudFrontDistributionHost(): ?string
    {
        return $this->cloudFrontDistributionHost;
    }

    /**
     * @return bool
     */
    public function hasCloudFrontDistributionHost(): bool
    {
        return !is_null($this->cloudFrontDistributionHost);
    }

    /**
     * @return string|null
     */
    public function getCloudFrontDistributionZoneId(): ?string
    {
        return $this->cloudFrontDistributionZoneId;
    }

    /**
     * @return bool
     */
    public function hasCloudFrontDistributionZoneId(): bool
    {
        return !is_null($this->cloudFrontDistributionZoneId);
    }

    /**
     * @param Zone   $zone
     * @param string $type
     * @param string $ip
     * @param bool   $withWWW
     *
     * @return bool
     */
    public function addRecord(Zone $zone, string $type, string $ip, bool $withWWW = false): bool
    {
        if (!isset(self::RECORD_TYPE_LIST[$type])) {
            throw new InvalidArgumentException('wrong $type');
        }

        try {
            $changes[] = [
                'Action' => 'CREATE',
                'ResourceRecordSet' => [
                    'Name' => $zone->getDomain(),
                    'ResourceRecords' => [
                        [
                            'Value' => $ip,
                        ],
                    ],
                    'TTL' => 300,
                    'Type' => $type,
                ],
            ];

            if ($withWWW) {
                $changes[] = [
                    'Action' => 'CREATE',
                    'ResourceRecordSet' => [
                        'Name' => 'www.' . $zone->getDomain(),
                        'ResourceRecords' => [
                            [
                                'Value' => $ip,
                            ],
                        ],
                        'TTL' => 300,
                        'Type' => $type,
                    ],
                ];
            }

            $route53 = $this->getRoute53();
            $route53->changeResourceRecordSets(
                [
                    'ChangeBatch' => [
                        'Changes' => $changes,
                        'Comment' => $type . ' record for ' . $zone->getZoneId() . ' zone',
                    ],
                    'HostedZoneId' => $zone->getZoneId(),
                ]
            );

            return true;
        } catch (AwsException $e) {
            $this->logAwsException($e, ['method' => __METHOD__]);

            return false;
        }
    }
}
