<?php

namespace AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;
use ThreeWebOneEntityBundle\Entity\Order\OrderItem;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\User;

class OrderService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var InventoryItemService
     */
    protected $inventoryItemService;

    /**
     * @var ImageService
     */
    protected $imageService;

    /**
     * OrderService constructor.
     *
     * @param EntityManager $entityManager
     * @param InventoryItemService $inventoryItemService
     * @param ImageService $imageService
     */
    public function __construct(
        EntityManager $entityManager,
        InventoryItemService $inventoryItemService,
        ImageService $imageService
    ) {
        $this->em = $entityManager;
        $this->inventoryItemService = $inventoryItemService;
        $this->imageService = $imageService;
    }

    /**
     * @param Order $order
     * @param FormInterface $orderItems
     * @param array $items
     *
     * @return Order
     * @throws \Doctrine\ORM\ORMException
     */
    public function manageSaleOrder(Order $order, FormInterface $orderItems, array $items = [])
    {
        foreach ($orderItems as $form) {
            $item = $form->getData();
            if ($items && !in_array($item, $items)) {
                continue;
            }
            $this->em->persist($item);
            $order->addPurchaseBarcode($item->getBarcode());
            $item->getBarcode()->setSellPrice($form->get('sellPrice')->getData());
        }
        if ($order->getStatus() === Order::COMPLETED) {
            foreach ($order->getPurchaseBarcodes() as $barcode) {
                $barcode->setStatus(InventoryItemBarcode::SOLD);
            }
        }
        if ($order->getStatus() === Order::REJECTED) {
            foreach ($order->getPurchaseBarcodes() as $barcode) {
                $order->removePurchaseBarcode($barcode);
                $this->em->remove($barcode->getOrderItem());
            }
        }

        return $order;
    }

    /**
     * @param FormInterface $form
     * @param array $oldItems
     */
    public function manageBuybackOrRepairOrder(FormInterface $form, array $oldItems = [])
    {
        $newInventoryItems = $form->get('items')->getData()->toArray();
        $itemsToRemove = array_udiff($oldItems, $newInventoryItems, function (InventoryItem $a, InventoryItem $b) {
            return $a->getId() - $b->getId();
        });
        $itemsToAdd = array_udiff($newInventoryItems, $oldItems, function (InventoryItem $a, InventoryItem $b) {
            return $a->getId() - $b->getId();
        });
        foreach ($itemsToRemove as $item) {
            $this->em->remove($item);
        }
        /** @var Order $order */
        $order = $form->getData();
        if ($order->getStatus() !== Order::REJECTED) {
            foreach ($form->get('items') as $itemForm) {
                /** @var InventoryItem $inventoryItem */
                $inventoryItem = $itemForm->getData();
                $inventoryItem->setOwner($order->getOwner())
                    ->setStatus(InventoryItem::STATUS_PREPARED);
                $searchModelData = $itemForm->get('searchModel')->getData();
                if ($searchModelData instanceof Model) {
                    $inventoryItem->setModel($searchModelData);
                    $inventoryItem->setTitle($searchModelData->getTitle());
                }
                if (is_string($searchModelData)) {
                    $inventoryItem->setTitle($searchModelData);
                }
                if (in_array($inventoryItem, $itemsToAdd)) {
                    $orderItem = (new OrderItem())
                        ->setDescription($itemForm->get('description')->getData())
                        ->setAdminNote($itemForm->get('adminNote')->getData())
                        ->setInventoryItem($inventoryItem);

                    $this->em->persist($orderItem);
                    $this->em->persist($inventoryItem);
                } else {
                    $inventoryItem->getOrderItem()
                        ->setDescription($itemForm->get('description')->getData())
                        ->setAdminNote($itemForm->get('adminNote')->getData());
                }
            }
        } else {
            foreach ($order->getItems() as $item) {
                $order->removeItem($item);
                $this->em->remove($item);
            }
        }
    }

    /**
     * @param Order $order
     *
     * @return Order
     */
    public function managePurchaseOrder(Order $order, User $user)
    {
        $oldOrder = $this->em->getUnitOfWork()->getOriginalEntityData($order);
        if (
            $order->getStatus() !== $oldOrder['status'] && $order->getStatus() === Order::COMPLETED
        ) {
            foreach ($order->getItems() as $item) {
                $item->setOwner($user);
                $this->inventoryItemService->createPrice($item, $user);
            }
        }

        return $order;
    }

    /**
     * @param Order $order
     *
     * @return array
     */
    public function getOrderTotalPrice(Order $order)
    {
        $subtotal = 0;
        if ($order->getSiteType() === SiteTypeInterface::SALE) {
            /** @var InventoryItemBarcode $item */
            foreach ($order->getPurchaseBarcodes() as $item) {
                $inventoryItem = $item->getInventoryItem();
                $subtotal += $inventoryItem->getPrice()->getValue();
                $subtotal += $inventoryItem->getShippingPrice();
            }
        } else {
            /** @var InventoryItem $item */
            foreach ($order->getItems() as $item) {
                $subtotal += $item->getPurchasePrice() * $item->getQuantity();
            }
        }
        $tax = $order->getOwner()->getConfig()->getTax() ?? 1;
        $total = $subtotal * $tax / 10000 + $subtotal;
        $amountPaid =  $order->getAmountPaid() ?? 0;
        $balanceDue = $total - $amountPaid;

        return [
            'subtotal' => $subtotal,
            'tax' => $order->getOwner()->getConfig()->getTax() ?? 0,
            'total' => $total,
            'amountPaid' => $amountPaid,
            'balanceDue' => $balanceDue,
        ];
    }

    /**
     * @param Order $order
     */
    public function manageCustomer(Order $order, Customer $customer = null)
    {
        if ($customer) {
            $order->setCustomer($customer);
        }
    }

    /**
     * @param Order $order
     *
     * @return array|null
     */
    public function prepareOriginalOrder(Order $order)
    {
        $askedQuantity = $order->getAskedQuantity();
        if (!$askedQuantity) {

            return null;
        }

        $originalOrder = [];
        foreach ($askedQuantity as $id => $quantity)  {
            $price = $this->em->getRepository(Price::class)->find($id);
            if ($price) {
                if ($order->getSiteType() === Order::SALE) {
                    $inventoryItem = $price->getInventoryItem();
                    $originalOrder[$quantity] = (string) $inventoryItem->getModel() ?? $inventoryItem->getTitle();
                } else {
                    $originalOrder[$quantity] = (string) $price->getModel();
                }
            }
        }

        return $originalOrder;
    }

    /**
     * @param FormInterface $form
     * @param User $user
     * @param int $type
     *
     * @return Order
     */
    public function createOrder(FormInterface $form, User $user, int $type)
    {
        $order = $form->getData()
            ->setOwner($user)
            ->setStatus(Order::IN_PROGRESS)
            ->setSiteType($type)
            ->setTermsChecked(true);

        $this->manageCustomer($order, $form->get('customer')->getData());

        if (in_array($type, [Order::BUYBACK, Order::REPAIR])) {
            $this->manageBuybackOrRepairOrder($form);
        }
        if ($type === Order::SALE) {
            $this->manageSaleOrder($order, $form->get('orderItems'));
        }
        $signatureData = $form->get('signatureImage')->getData();
        if ($signatureData) {
            $this->inventoryItemService->setSignatureImage($order, $signatureData);
        }
        $this->em->persist($order);
        $this->em->flush();
        $this->em->refresh($order);

        return $order;
    }

    /**
     * @param FormInterface $form
     * @param Order $order
     * @param array $oldInventoryItems
     * @param array $orderItems
     *
     * @return Order
     */
    public function processOrder(FormInterface $form, Order $order, array $oldInventoryItems, array $orderItems)
    {
        $type = $order->getSiteType();
        $signatureData = $form->get('signatureImage')->getData();
        if ($signatureData) {
            $this->setSignatureImage($order, $signatureData);
        }
        $this->manageCustomer($order, $form->get('customer')->getData());
        if ($type === Order::SALE) {
            $newOrderItems = $form->get('orderItems')->getData();
            $itemsToRemove = array_udiff($orderItems, $newOrderItems, function (OrderItem $a, OrderItem $b) {
                return $a->getId() - $b->getId();
            });
            $itemsToAdd = array_udiff($newOrderItems, $orderItems, function (OrderItem $a, OrderItem $b) {
                return $a->getId() - $b->getId();
            });
            /** @var OrderItem $item */
            foreach ($itemsToRemove as $item) {
                $order->removePurchaseBarcode($item->getBarcode());
                $this->em->remove($item);
            }
            $this->manageSaleOrder($order, $form->get('orderItems'), $itemsToAdd);
        }
        if (in_array($type, [Order::BUYBACK, Order::REPAIR])) {
            $this->manageBuybackOrRepairOrder($form, $oldInventoryItems);
        }
        $this->em->persist($order);
        $this->em->flush();

        if ($order->getStatus() === Order::COMPLETED && in_array($type, [Order::BUYBACK, Order::REPAIR])) {
            foreach ($order->getItems() as $item) {
                $item->setStatus(InventoryItem::STATUS_INACTIVE);
                $this->inventoryItemService->generateBarcodes($item, $order);
                if ($type === Order::REPAIR) {
                    $item->setStatus(InventoryItem::STATUS_COMPLETED_HIDDEN);
                    foreach ($item->getInventoryItemBarcodes() as $barcode) {
                        $barcode->setStatus(InventoryItemBarcode::SOLD);
                        $barcode->setSellPrice($item->getPriceValue());
                        $this->em->flush();
                    }
                }
            }
        }

        return $order;
    }

    /**
     * @param FormInterface $form
     * @param $user
     *
     * @return Order
     */
    public function createAddInventoryOrder(FormInterface $form, $user)
    {
        $order = $this->prepareIncomeOrder($form->getData(), $user);
        $signatureData = $form->get('signatureImage')->getData();
        if ($signatureData) {
            $this->setSignatureImage($order, $signatureData);
        }
        $inventoryItemsForm = $form->get('items');
        foreach ($inventoryItemsForm as $form) {
            $this->inventoryItemService->manageModelSelectData($form, $user);
        }

        $this->em->persist($order);
        $this->em->flush();
        $this->em->refresh($order);

        return $order;
    }

    /**
     * @param Order $order
     * @param User $user
     *
     * @return Order
     */
    private function prepareIncomeOrder(Order $order, User $user)
    {

        return $order
            ->setSiteType(Order::BUYBACK)
            ->setStatus(Order::COMPLETED)
            ->setOwner($user)
            ->setDeliveryType(Order::PICK_UP)
            ->setPaymentType(Order::CASH)
            ->setTermsChecked(true);
    }

    /**
     * @param Order $order
     * @param string $content
     */
    private function setSignatureImage(Order $order, string $content)
    {
        $imageFile = $this->imageService
            ->createUploadedFileFormString($content);
        if ($imageFile instanceof UploadedFile) {
            $order->setImageFile($imageFile);
            $order->refreshUpdated();
        }
    }
}
