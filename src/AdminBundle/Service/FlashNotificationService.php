<?php

namespace AdminBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;

class FlashNotificationService
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * FlashNotificationService constructor.
     * @param Session $session
     * @param TranslatorInterface $translator
     */
    public function __construct(
        Session $session,
        TranslatorInterface $translator
    ) {
        $this->session = $session;
        $this->translator = $translator;
    }

    /**
     * Add Flash Message
     *
     * @param string $type
     * @param string $message
     * @param array $parameters
     */
    public function addFlashMessage(string $type, string $message, array $parameters = [])
    {
        $this->session->getFlashBag()->add($type, $this->translator->trans($message, $parameters));
    }
}
