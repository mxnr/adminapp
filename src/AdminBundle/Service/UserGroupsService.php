<?php

namespace AdminBundle\Service;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use ThreeWebOneEntityBundle\Entity\User;

class UserGroupsService
{
    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * UserGroupsService constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->token = $tokenStorage;
    }

    /**
     * Get user or users supervisor
     *
     * @param User $user
     * @return User
     */
    public function getUserOrUserSupervisor(User $user = null) : User
    {
        if (!$user) {
            $user = $this->token->getToken()->getUser();
        }

        if ($user->hasRole('ROLE_ADMIN') || $user->hasRole('ROLE_SUPER_ADMIN')) {
            return $user;
        }

        return $user->getSupervisor();
    }
}
