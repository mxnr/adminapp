<?php

namespace AdminBundle\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

class ApiResponseService
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * ApiResponseService constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param $content
     * @param int $status
     * @return JsonResponse
     */
    public function getErrorResponse($content, $status = Response::HTTP_BAD_REQUEST): JsonResponse
    {
        return new JsonResponse(
            [
                'status' => 'error',
                'content' => $this->translator->trans($content)
            ],
            $status
        );
    }

    /**
     * @param $content
     * @return JsonResponse
     */
    public function getSuccessResponse($content): JsonResponse
    {
        return new JsonResponse(
            [
                'status' => 'ok',
                'content' => $content
            ]
        );
    }
}