<?php

namespace AdminBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use Hshn\Base64EncodedFile\HttpFoundation\File\Base64EncodedFile;

class ImageService
{
    /**
     * Creates new UploadedFile from base64 encoded image
     */
    public function createUploadedFileFormString(string $content)
    {
        $data = substr($content, strpos($content, ',') + 1);
        $file =  new Base64EncodedFile($data);
        $name = sprintf('%s.%s', uniqid(), explode('/', $file->getMimeType())[1]);

        return new UploadedFile($file->getRealPath(), $name);
    }
}
