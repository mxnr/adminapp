<?php

namespace AdminBundle\Service;

use ThreeWebOneEntityBundle\Entity\IncomeCustomer;
use ThreeWebOneEntityBundle\Entity\Page\Page;
use ThreeWebOneEntityBundle\Entity\User;

class DisclaimerService
{
    /**
     * @param User $user
     * @param int $type
     *
     * @return Page
     */
    public function getDisclaimer(User $user, int $type) {
        $disclaimer = $user->getPageByType($type);
        if (!$disclaimer) {
            $disclaimer = (new Page())
                ->setPageType($type);
        }

        return $disclaimer;
    }
}
