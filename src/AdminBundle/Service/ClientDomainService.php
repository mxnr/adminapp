<?php

namespace AdminBundle\Service;

use AdminBundle\Event\DomainEvent;
use AdminBundle\Service\DomainName\AcmService;
use AdminBundle\Service\DomainName\DomainQueueService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use DomainResellerBundle\Service\OpenSRSService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomainQueue;

class ClientDomainService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var UserGroupsService
     */
    private $userGroupsService;

    /**
     * @var FlashNotificationService
     */
    private $flashNotificationService;

    /**
     * @var OpenSRSService
     */
    private $openSRSService;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var AcmService
     */
    private $acmService;

    /**
     * @var DomainQueueService
     */
    private $domainQueueService;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var String
     */
    private $loadBalancerDomainName;

    /**
     * ClientDomainService constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserGroupsService $userGroupsService
     * @param FlashNotificationService $flashNotificationService
     * @param OpenSRSService $openSRSService
     * @param EventDispatcherInterface $eventDispatcher
     * @param AcmService $acmService
     * @param DomainQueueService $domainQueueService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserGroupsService $userGroupsService,
        FlashNotificationService $flashNotificationService,
        OpenSRSService $openSRSService,
        EventDispatcherInterface $eventDispatcher,
        AcmService $acmService,
        DomainQueueService $domainQueueService,
        \Twig_Environment $twig,
        String $loadBalancerDomainName
    ) {
        $this->entityManager = $entityManager;
        $this->userGroupsService = $userGroupsService;
        $this->flashNotificationService = $flashNotificationService;
        $this->openSRSService = $openSRSService;
        $this->eventDispatcher = $eventDispatcher;
        $this->acmService = $acmService;
        $this->domainQueueService = $domainQueueService;
        $this->domainQueueService = $domainQueueService;
        $this->twig = $twig;
        $this->loadBalancerDomainName = $loadBalancerDomainName;
    }

    /**
     * @param ClientDomain $clientDomain
     */
    public function createClientDomain(ClientDomain $clientDomain)
    {
        $user = $this->userGroupsService->getUserOrUserSupervisor();

        $clientDomain
            ->setOwner($user)
            ->setActive(ClientDomain::INACTIVE)
            ->setDomainType(ClientDomain::CLIENT_DOMAIN_TYPE)
            ->setStatus(ClientDomain::ADDED_MANUALLY);

        $this->entityManager->persist($clientDomain);
        $this->entityManager->flush();

        $this->flashNotificationService->addFlashMessage('success','Domain was added');
        $this->acmService->requestCertificate($clientDomain->getDomain());
    }

    /**
     * @param array $contacts
     * @return array
     */
    public function getContacts(array $contacts): array
    {
        return array_filter($contacts, function ($value) {
            return !is_null($value);
        });
    }

    /**
     * @param ClientDomain $clientDomain
     * @param array $ownerContacts
     * @param array $contacts
     * @return bool
     */
    public function registerNewDomain(ClientDomain $clientDomain, array $ownerContacts, array $contacts): bool
    {
        $response = $this->openSRSService->registerNewDomain(
            $clientDomain->getDomain(),
            $ownerContacts['login'],
            $ownerContacts['password'],
            $contacts
        );

        if ($response['responseCode'] === openSRSService::SUCCESS_REQUEST) {
            $this->successRegistration($clientDomain);

            return true;
        }

        $this->failedRegistration($response);

        return false;
    }

    /**
     * @param ClientDomain $clientDomain
     */
    private function successRegistration(ClientDomain $clientDomain)
    {
        $this->flashNotificationService->addFlashMessage(
            'success',
            'Congratulations! You bought a new domain!'
        );

        $this->flashNotificationService->addFlashMessage(
            'success',
            'Please wait until DNS server refresh own data, this may take up to 24 hours'
        );

        $this->addDomainForwarding($clientDomain);
        $this->setToInactiveDuplicateDomains($clientDomain);

        $clientDomain->setStatus(ClientDomain::PAYED_WITH_DOMAIN);
        $clientDomain = $this->setDomainActiveStatus($clientDomain);

        $this->entityManager->persist($clientDomain);
        $this->entityManager->flush();

        $clientDomainEvent = new DomainEvent();
        $clientDomainEvent->setDomainClient($clientDomain);
        $this->eventDispatcher->dispatch(DomainEvent::DOMAIN_CREATE, $clientDomainEvent);
    }

    /**
     * @param ClientDomain $clientDomain
     * @return bool
     */
    private function addDomainForwarding(ClientDomain $clientDomain): bool
    {
        $response = $this->openSRSService->createDomainForwarding($clientDomain->getDomain());

        if ($response['responseCode'] === openSRSService::SUCCESS_REQUEST) {
            $response = $this->openSRSService->setDomainForwarding($clientDomain->getDomain());
            if ($response['responseCode'] === openSRSService::SUCCESS_REQUEST) {
                return true;
            }
        }

        $this->flashNotificationService->addFlashMessage(
            'error',
            'Domain not linked with your shop please send to support for details'
        );

        return false;
    }

    /**
     * @param ClientDomain $clientDomain
     */
    private function setToInactiveDuplicateDomains(ClientDomain $clientDomain)
    {
        $domains = $this->getClientDomainRepo()->getDuplicateDomains(
            $clientDomain->getOwner(),
            $clientDomain->getDomain()
        );

        /**@var ClientDomain $domain */
        foreach ($domains as $domain) {
            $domain->setActive(ClientDomain::INACTIVE);
            $domain->setStatus(ClientDomain::DUPLICATE);
            $this->entityManager->persist($domain);
        }
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository|\Doctrine\ORM\EntityRepository|\ThreeWebOneEntityBundle\Repository\Domain\ClientDomainRepository
     */
    public function getClientDomainRepo()
    {
        return $this->entityManager->getRepository(ClientDomain::class);
    }

    /**
     * @param ClientDomain $clientDomain
     * @return ClientDomain
     */
    public function setDomainActiveStatus(ClientDomain $clientDomain): ClientDomain
    {
        $activeDomains = $this->getCountActiveDomains();

        if ($activeDomains > ClientDomain::ACTIVE_LIMIT) {
            $clientDomain->setActive(ClientDomain::INACTIVE);
        }

        return $clientDomain;
    }

    /**
     * @return int
     */
    public function getCountActiveDomains()
    {
        $clientDomains = $this->getClientDomains();

        $activeDomains = 0;
        /**@var ClientDomain $domain */
        foreach ($clientDomains as $domain) {
            if (
                $domain->isActive() &&
                $domain->getDomainType() === ClientDomain::CLIENT_DOMAIN_TYPE &&
                (
                    $domain->getStatus() === ClientDomain::PAYED_WITH_DOMAIN or
                    $domain->getStatus() === ClientDomain::ADDED_MANUALLY_APPROVED
                )
            ) {
                $activeDomains++;
            }
        }

        return $activeDomains;
    }

    /**
     * @return ClientDomain[]
     */
    public function getClientDomains()
    {
        $user = $this->userGroupsService->getUserOrUserSupervisor();

        return $this->getClientDomainRepo()->getAllUserDomains($user);
    }

    /**
     * @param ClientDomain $clientDomain
     * @param String $domain
     * @return ClientDomain
     */
    public function getSubDomain(ClientDomain $clientDomain, String $domain = 'www'): ClientDomain
    {
        $subDomain = (new ClientDomain())
            ->setDomain(sprintf('%s.%s', $domain, $clientDomain->getDomain()))
            ->setOwner($clientDomain->getOwner())
            ->setActive(ClientDomain::ACTIVE)
            ->setDomainType(ClientDomain::SUB_DOMAIN_TYPE)
            ->setStatus(ClientDomain::PAYED_WITH_DOMAIN);

        return $subDomain;
    }

    /**
     * @param array $response
     */
    private function failedRegistration(array $response)
    {
        $this->flashNotificationService->addFlashMessage('error', $response['responseText']);

        if (array_key_exists('item', $response)) {
            $this->flashNotificationService->addFlashMessage('error', $response['item']);
        }
    }

    /**
     * @param String $domain
     * @return String
     */
    public function convertStringToSubDomain(String $domain): String
    {
        $domain = strtolower($domain);
        $domain = trim($domain);
        $domain = trim($domain, '-');
        $domain = preg_replace('/[^A-Za-z0-9\-]/', '', $domain);
        $domain = preg_replace('/-+/', '-', $domain);

        return $domain . '.' . ClientDomain::CLIENT_TLD;
    }

    /**
     * @param array $clientDomains
     * @return array
     */
    public function getDomainsList(array $clientDomains)
    {
        $result = [];

        foreach ($clientDomains as $clientDomain) {
            $result[] = [
                'domain' => $clientDomain->getDomain()
            ];
        }

        return $result;
    }

    /**
     * @param ClientDomain $clientDomain
     */
    public function domainOwnershipApproved(clientDomain $clientDomain)
    {
        $clientDomain->setStatus(clientDomain::ADDED_MANUALLY_APPROVED);
        $this->entityManager->persist($clientDomain);
        $this->entityManager->flush();

        $this->domainQueueService->addDomain($clientDomain->getDomain(), ClientDomainQueue::OWNERSHIP_EXTERNAL_REGISTERER);
        $this->flashNotificationService->addFlashMessage('success', 'All checks are done! The work with your domain will fifteen minutes');
    }

    /**
     * @param ClientDomain $clientDomain
     */
    public function addDomainOwnershipInstructionToFlash(clientDomain $clientDomain)
    {
        $resourceRecord = $this->acmService->getSSLDomainOwnershipRecords($clientDomain->getDomain());

        $template = $this->twig->render('AdminBundle:Domain/partials:create_domain_info.html.twig', [
            'loadBalancerDomainName' => $this->loadBalancerDomainName,
            'domainOptions' => $resourceRecord
        ]);

        $this->flashNotificationService->addFlashMessage('warning', $template);
    }

}
