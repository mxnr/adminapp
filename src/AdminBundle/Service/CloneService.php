<?php

namespace AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\Translator;
use ThreeWebOneEntityBundle\Entity\BaseEntity;
use ThreeWebOneEntityBundle\Entity\Helper\SynchronizeableInterface;
use ThreeWebOneEntityBundle\Entity\User;

class CloneService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var UserGroupsService
     */
    private $userGroupService;

    /**
     * CloneService constructor.
     *
     * @param EntityManager $entityManager
     * @param Session $session
     * @param Translator $translator
     * @param UserGroupsService $userGroupsService
     */
    public function __construct(
        EntityManager $entityManager,
        Session $session,
        Translator $translator,
        UserGroupsService $userGroupsService
    ) {
        $this->em = $entityManager;
        $this->session = $session;
        $this->translator = $translator;
        $this->userGroupService = $userGroupsService;
    }

    /**
     * @param BaseEntity $object
     * @param User $user
     */
    public function processObject(BaseEntity $object, User $user)
    {
        $user = $this->userGroupService->getUserOrUserSupervisor($user);
        $newObj = clone $object;
        $newObj->setOwner($user);
        $newObj->addUser($user);

        $this->session->getFlashBag()->add(
            'sonata_flash_success',
            $this->translator->trans('Cloned successfully')
        );
        $this->em->persist($newObj);
        $this->em->flush();
    }

    /**
     * @param SynchronizeableInterface $object
     * @param User $user
     */
    public function synchronizeObject(SynchronizeableInterface $object, User $user)
    {
        $user = $this->userGroupService->getUserOrUserSupervisor($user);
        if (!in_array($user, $object->getUsers()->toArray())) {
            $object->addUser($user);
            $this->session->getFlashBag()->add('sonata_flash_success', 'Object synchronized');
        } else {
            $object->removeUser($user);
            $this->session->getFlashBag()->add('sonata_flash_success', 'Object desynchronized');
        }

        $this->em->persist($object);
        $this->em->flush();
    }
}
