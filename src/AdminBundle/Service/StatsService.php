<?php

namespace AdminBundle\Service;

use DateTime;
use Doctrine\ORM\EntityManager;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\IncomeCustomer;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\User;

class StatsService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * StatsService constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Get all aggregated stats data
     *
     * @return array
     */
    public function getDashboardStatsInfo(User $owner): array
    {
        return [
            'newCustomers' => $this->getNewCustomers($owner),
            'inventoryItems' => $this->getInventoryItemsInfo($owner),
            'inventoryItemsSold' => $this->getInventoryItemsSoldInfo($owner),
            'inventoryUnsoldCost' => $this->getInventoryUnsoldCost($owner),
            'inventoryItemsPriceOnSite' => $this->getInventoryItemsPriceOnSite($owner)
        ];
    }

    /**
     * Get count of customers registered last month
     *
     * @param User $owner
     * @return int
     */
    private function getNewCustomers(User $owner): int
    {
        $customerRepo = $this->em->getRepository(Customer::class);

        return $customerRepo->getAggregatedCustomersCount($owner);
    }

    /**
     * Get count and sum of inventories items created last month
     *
     * @return array
     */
    private function getInventoryItemsInfo(User $owner): array
    {
        $inventoryItemBarcodesRepo = $this->em->getRepository(InventoryItemBarcode::class);

        return $inventoryItemBarcodesRepo->getAggregatedInventoryInfo($owner);
    }

    /**
     * Get inventory items sold count
     *
     * @param User $owner
     * @return array
     */
    private function getInventoryItemsSoldInfo(User $owner): array
    {
        $inventoryItemBarcodesRepo = $this->em->getRepository(InventoryItemBarcode::class);

        return $inventoryItemBarcodesRepo->getAggregatedSoldItems($owner);
    }

    /**
     * Get inventory unsold cost
     *
     * @param User $owner
     * @return int
     */
    private function getInventoryUnsoldCost(User $owner): int
    {
        $inventoryItemBarcodesRepo = $this->em->getRepository(InventoryItemBarcode::class);

        return $inventoryItemBarcodesRepo->getAggregatedUnsoldInventoryCost($owner);
    }

    /**
     * Get inventory items price on site
     *
     * @param User $owner
     * @return int
     */
    private function getInventoryItemsPriceOnSite(User $owner): int
    {
        $inventoryItemBarcodesRepo = $this->em->getRepository(InventoryItemBarcode::class);

        return $inventoryItemBarcodesRepo->getAggregatedInventoryPriceActiveInWebSite($owner);
    }

    /**
     * Get Selling Stats Info
     *
     * @param User $owner
     * @param DateTime $from
     * @param DateTime $to
     * @return array
     */
    public function getSellingStatsInfo(User $owner, DateTime $from, DateTime $to): array
    {
        $orderRepo = $this->em->getRepository(Order::class);
        $ordersInfo = $orderRepo->getAggregatedUserSoldOrdersDuringPeriod($owner, $from, $to);

        $result = [
            'totalInventoryInvoiced' => 0,
            'totalPaymentsCollected' => 0,
            'ordersInfo' => $ordersInfo
        ];

        foreach ($ordersInfo as $item) {
            $result['totalInventoryInvoiced'] += $item['itemsCount'];
            $result['totalPaymentsCollected'] += $item['itemsPurchasePrice'];
        }

        return $result;
    }

    /**
     * Get BuyBack Stats Info
     *
     * @param User $owner
     * @param DateTime $from
     * @param DateTime $to
     * @return array
     */
    public function getBuybackStatsInfo(User $owner, DateTime $from, DateTime $to): array
    {
        $orderRepo = $this->em->getRepository(Order::class);

        return $orderRepo->getAggregatedUserBuybackInventoryItemsDuringPeriod($owner, $from, $to);
    }
}
