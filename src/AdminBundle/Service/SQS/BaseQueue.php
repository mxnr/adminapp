<?php

namespace AdminBundle\Service\SQS;

use Aws\Exception\AwsException;
use Aws\Result;
use Aws\Sqs\SqsClient;
use InvalidArgumentException;
use TriTran\SqsQueueBundle\Service\Message;
use TriTran\SqsQueueBundle\Service\MessageCollection;
use TriTran\SqsQueueBundle\Service\Worker\AbstractWorker;

/**
 * Class BaseQueue.
 */
class BaseQueue
{
    /**
     * @var SqsClient
     */
    private $client;

    /**
     * @var string
     */
    private $queueUrl;

    /**
     * @var string
     */
    private $queueName;

    /**
     * @var AbstractWorker
     */
    private $queueWorker;

    /**
     * @var array
     */
    private $attributes;

    /**
     * BaseQueue constructor.
     *
     * @param SqsClient              $client
     * @param string                 $queueName
     * @param string                 $queueUrl
     * @param BatchConsumerInterface $queueWorker
     * @param array                  $options
     */
    public function __construct(
        SqsClient $client,
        string $queueName,
        string $queueUrl,
        BatchConsumerInterface $queueWorker,
        array $options = []
    ) {
        $this->client = $client;
        $this->queueUrl = $queueUrl;
        $this->queueName = $queueName;
        $this->queueWorker = $queueWorker;
        $this->attributes = $options;
    }

    /**
     * @return string
     */
    public function ping()
    {
        $message = (new Message())->setBody('ping');

        return $this->sendMessage($message);
    }

    /**
     * @param Message $message
     * @param int     $delay
     *
     * @return string
     */
    public function sendMessage(Message $message, int $delay = 0)
    {
        $params = [
            'DelaySeconds' => $delay,
            'MessageAttributes' => $message->getAttributes(),
            'MessageBody' => $message->getBody(),
            'QueueUrl' => $this->queueUrl,
        ];
        try {
            $result = $this->client->sendMessage($params);
            $messageId = $result->get('MessageId');
        } catch (AwsException $e) {
            throw new InvalidArgumentException($e->getAwsErrorMessage());
        }

        return $messageId;
    }

    /**
     * @param MessageCollection $collection
     * @param int               $delay
     *
     * @return Result
     */
    public function sendMessageBatch(MessageCollection $collection, int $delay = 0): Result
    {
        if ($collection->count() > 10) {
            throw new \InvalidArgumentException('maximum messages amount per batch is 10');
        }

        $messages = [];
        /** @var Message $message */
        foreach ($collection as $message) {
            $messages[md5($message->getBody())] =
                [
                    'Id' => md5($message->getBody()),
                    'DelaySeconds' => $delay,
                    'MessageAttributes' => $message->getAttributes(),
                    'MessageBody' => $message->getBody(),
                ];
        }

        $params = [
            'Entries' => array_values($messages),
            'QueueUrl' => $this->queueUrl,
        ];

        return $this->client->sendMessageBatch($params);
    }

    /**
     * Retrieves one or more messages (up to 10), from the specified queue.
     *
     * @param int $limit
     *
     * @return MessageCollection|Message[]
     */
    public function receiveMessage(int $limit = 10)
    {
        $collection = new MessageCollection([]);

        try {
            $result = $this->client->receiveMessage(
                [
                    'AttributeNames' => ['SentTimestamp'],
                    'MaxNumberOfMessages' => $limit,
                    'MessageAttributeNames' => ['All'],
                    'QueueUrl' => $this->queueUrl,
                    'WaitTimeSeconds' => $this->attributes['ReceiveMessageWaitTimeSeconds'] ?? 0,
                ]
            );

            $messages = $result->get('Messages') ?? [];
            foreach ($messages as $message) {
                $collection->append(
                    (new Message())
                        ->setId($message['MessageId'])
                        ->setBody($message['Body'])
                        ->setReceiptHandle($message['ReceiptHandle'])
                        ->setAttributes($message['Attributes'])
                );
            }
        } catch (AwsException $e) {
            throw new InvalidArgumentException($e->getAwsErrorMessage());
        }

        return $collection;
    }

    /**
     * Deletes the specified message from the specified queue.
     *
     * @param Message $message
     *
     * @return bool
     */
    public function deleteMessage(Message $message)
    {
        try {
            $this->client->deleteMessage(
                [
                    'QueueUrl' => $this->queueUrl,
                    'ReceiptHandle' => $message->getReceiptHandle(),
                ]
            );

            return true;
        } catch (AwsException $e) {
            throw new InvalidArgumentException($e->getAwsErrorMessage());
        }
    }

    /**
     * @param MessageCollection $collection
     *
     * @return bool
     */
    public function deleteMessageBatch(MessageCollection $collection)
    {
        try {
            if ($collection->count() > 10) {
                throw new \InvalidArgumentException('maximum messages amount per batch is 10');
            }

            $messages = [];
            /** @var Message $message */
            foreach ($collection as $message) {
                $messages[md5($message->getBody())] = [
                    'Id' => md5($message->getBody()),
                    'ReceiptHandle' => $message->getReceiptHandle(),
                ];
            }

            $this->client->deleteMessageBatch(
                [
                    'QueueUrl' => $this->queueUrl,
                    'Entries' => array_values($messages),
                ]
            );

            return true;
        } catch (AwsException $e) {
            throw new InvalidArgumentException($e->getAwsErrorMessage());
        }
    }

    /**
     * Releases a message back to the queue, making it visible again.
     *
     * @param Message $message
     *
     * @return bool
     */
    public function releaseMessage(Message $message)
    {
        try {
            $this->client->changeMessageVisibility(
                [
                    'QueueUrl' => $this->queueUrl,
                    'ReceiptHandle' => $message->getReceiptHandle(),
                    'VisibilityTimeout' => 0,
                ]
            );

            return true;
        } catch (AwsException $e) {
            throw new InvalidArgumentException($e->getAwsErrorMessage());
        }
    }

    /**
     * @param MessageCollection $collection
     *
     * @return bool
     */
    public function releaseMessageBatch(MessageCollection $collection)
    {
        try {
            if ($collection->count() > 10) {
                throw new \InvalidArgumentException('maximum messages amount per batch is 10');
            }

            $messages = [];
            /** @var Message $message */
            foreach ($collection as $message) {
                $messages[md5($message->getBody())] = [
                    'Id' => md5($message->getBody()),
                    'ReceiptHandle' => $message->getReceiptHandle(),
                    'VisibilityTimeout' => 0,
                ];
            }

            $this->client->changeMessageVisibilityBatch(
                [
                    'QueueUrl' => $this->queueUrl,
                    'Entries' => array_values($messages),
                ]
            );

            return true;
        } catch (AwsException $e) {
            throw new InvalidArgumentException($e->getAwsErrorMessage());
        }
    }

    /**
     * Deletes the messages in a queue.
     * When you use the this action, you can't retrieve a message deleted from a queue.
     *
     * @return bool
     */
    public function purge()
    {
        try {
            $this->client->purgeQueue(
                [
                    'QueueUrl' => $this->queueUrl,
                ]
            );

            return true;
        } catch (AwsException $e) {
            throw new InvalidArgumentException($e->getAwsErrorMessage());
        }
    }

    /**
     * @return string
     */
    public function getQueueUrl(): string
    {
        return $this->queueUrl;
    }

    /**
     * @param string $queueUrl
     *
     * @return $this
     */
    public function setQueueUrl(string $queueUrl)
    {
        $this->queueUrl = $queueUrl;

        return $this;
    }

    /**
     * @return BatchConsumerInterface
     */
    public function getQueueWorker(): BatchConsumerInterface
    {
        return $this->queueWorker;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     *
     * @return $this
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * @return SqsClient
     */
    public function getClient(): SqsClient
    {
        return $this->client;
    }
}
