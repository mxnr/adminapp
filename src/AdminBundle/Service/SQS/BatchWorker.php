<?php

namespace AdminBundle\Service\SQS;

use Psr\Log\LoggerAwareTrait;
use TriTran\SqsQueueBundle\Service\Message;
use TriTran\SqsQueueBundle\Service\MessageCollection;

/**
 * Class BatchWorker.
 */
class BatchWorker
{
    use LoggerAwareTrait;

    /**
     * @param BaseQueue $queue
     * @param int       $limit     Zero is all
     * @param int       $timeLimit execution time limit, if 0 no limit
     */
    public function start(BaseQueue $queue, int $limit = 10, int $timeLimit = 0)
    {
        $this->consume($queue, $limit, $timeLimit);
    }

    /**
     * @param BaseQueue $queue
     * @param int       $limit     Zero is all
     * @param int       $timeLimit execution time limit, if 0 no limit
     */
    private function consume(BaseQueue $queue, int $limit = 10, int $timeLimit = 0)
    {
        $started = time();
        $shouldEnd = $started + $timeLimit;
        $limitExceeded = false;

        while ($limitExceeded === false) {
            $iterationStart = time();
            $this->fetchMessage($queue, $limit);
            $iterationTime = time() - $iterationStart;
            $currentTime = time();

            if ($timeLimit > 0) {
                if ($iterationTime > $timeLimit) {
                    $limitExceeded = true;
                }
                if (($shouldEnd - $currentTime) <= $iterationTime) {
                    $limitExceeded = true;
                }
            }
        }
    }

    /**
     * @param BaseQueue $queue
     * @param int       $limit
     */
    private function fetchMessage(BaseQueue $queue, int $limit = 10)
    {
        $consumer = $queue->getQueueWorker();

        /** @var MessageCollection $result */
        $messages = $queue->receiveMessage($limit);
        $messages->rewind();

        $result = $consumer->process($messages);

        $messagesAckList = [];
        $messagesRequeueList = [];

        /** @var Message $message */
        foreach ($messages as $message) {
            $messageId = $message->getReceiptHandle();
            $processingResult = isset($result[$messageId]) ? true === $result[$messageId] : false;

            if ($processingResult) {
                $messagesAckList[] = $message;
            } else {
                $messagesRequeueList[] = $message;
            }
        }

        do {
            $chunkRequeue = array_splice($messagesRequeueList, 0, $limit);
            $chunkAck = array_splice($messagesAckList, 0, $limit);

            if (!empty($chunkAck)) {
                $ackCollection = new MessageCollection();
                foreach ($chunkAck as $message) {
                    $ackCollection->append($message);
                }
                $queue->deleteMessageBatch($ackCollection);
            }

            if (!empty($chunkRequeue)) {
                $requeueCollection = new MessageCollection();
                foreach ($chunkRequeue as $message) {
                    $requeueCollection->append($message);
                }
                $queue->releaseMessageBatch($requeueCollection);
            }
        } while (!empty($chunkAck) || !empty($chunkRequeue));
    }
}
