<?php

namespace AdminBundle\Service\SQS;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use RuntimeException;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\Queue\Message\Synchronize;
use ThreeWebOneEntityBundle\Entity\Queue\Message\SynchronizeTask;
use TriTran\SqsQueueBundle\Service\Message;
use TriTran\SqsQueueBundle\Service\MessageCollection;

/**
 * Class SynchronizeTaskConsumerService.
 */
class SynchronizeTaskConsumerService extends BatchConsumer
{
    const TASK_SYNCHRONIZE = 'synchronize';
    const TASK_DESYNCHRONIZE = 'desynchronize';

    const TASK_LIST = [self::TASK_SYNCHRONIZE, self::TASK_DESYNCHRONIZE];

    /**
     * {@inheritdoc}
     */
    public function process(MessageCollection $collection): array
    {
        $this->checkServicesAndThrow();

        $return = [];

        /** @var Message $message */
        foreach ($collection as $message) {
            $messageId = $message->getReceiptHandle();
            $messageBody = json_decode($message->getBody(), true);

            try {
                /** @var SynchronizeTask $synchronizeTask */
                $synchronizeTask = $this->fillUpEntityWithData(new SynchronizeTask(), $messageBody);
                $violations = $this->getValidatorService()->validate($synchronizeTask);

                if ($violations->count() > 0) {
                    throw new RuntimeException('got invalid message');
                }

                $this->enqueueMessages(
                    $this->getMessagesForTask(
                        $synchronizeTask,
                        //TODO: POTENTIAL MEMORY PROBLEM, have to use iterator at least, but for now doctrine does
                        //TODO: not allow to iterate on queries with joins, so need to think about it
                        $this->createQueryFromMessage($synchronizeTask)->execute()
                    )
                );

                $return[$messageId] = true;
            } catch (\Throwable $t) {
                $this->logger->error(
                    '<![CDATA[' . $t->getMessage() . ']]> got error while processing <![CDATA['
                    . print_r($messageBody, 1) .
                    ']]>'
                );

                $return[$messageId] = false;
            }
        }

        return $return;
    }

    /**
     * Checks service dependencies and throws exceptions if something wrong
     *
     * @throws RuntimeException
     */
    public function checkServicesAndThrow()
    {
        if (false === $this->hasProducer()) {
            throw new RuntimeException('for properly work, requires producer.* service');
        }

        if (false === $this->hasAdminUserService()) {
            throw new RuntimeException('for properly work, requires admin_user service');
        }

        if (false === $this->hasValidatorService()) {
            throw new RuntimeException('for properly work, requires validator service');
        };
    }

    /**
     * @param array $messages
     *
     * @return void
     */
    public function enqueueMessages(array $messages)
    {
        do {
            $chunk = array_splice($messages, 0, 10);
            if (!empty($chunk)) {
                $this->getProducer()->publishBatch($chunk);
            }
        } while (!empty($chunk));
    }

    /**
     * @param SynchronizeTask $task
     * @param array           $entitiesData
     *
     * @return array
     */
    public function getMessagesForTask(SynchronizeTask $task, array $entitiesData): array
    {
        $messages = [];
        /** @var OwnerInterface $item */
        foreach ($entitiesData as $item) {
            if ($item->getOwner()->getId() != $task->getSynchronizeWith()) {
                $messages[] = (string) $this->buildSynchronize($item->getId(), $task);
            }
        }

        return $messages;
    }

    /**
     * @param string          $id
     * @param SynchronizeTask $task
     *
     * @return Synchronize
     */
    public function buildSynchronize(string $id, SynchronizeTask $task): Synchronize
    {
        $synchronize = new Synchronize();

        $synchronize
            ->setSynchronizeWith($task->getSynchronizeWith())
            ->setEntityClass($task->getEntityClass())
            ->setTask($task->getTask())
            ->setEntityId($id);

        $violations = $this->getValidatorService()->validate($task);

        if ($violations->count() > 0) {
            throw new RuntimeException('got error while building message');
        }

        return $synchronize;
    }

    /**
     * Creates query based on message params.
     *
     * @param SynchronizeTask $task
     *
     * @return Query
     */
    private function createQueryFromMessage(SynchronizeTask $task): Query
    {
        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository($task->getEntityClass());
        $queryBuilder = $repository->createQueryBuilder('o');
        $query = $queryBuilder->getQuery()->setDQL($task->getQuery());

        foreach ($task->getQueryParams() as $queryParamName => $queryParam) {
            $query->setParameter($queryParamName, $queryParam);
        }

        return $query;
    }
}
