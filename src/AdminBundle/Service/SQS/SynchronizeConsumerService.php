<?php

namespace AdminBundle\Service\SQS;

use Doctrine\ORM\EntityRepository;
use ThreeWebOneEntityBundle\Entity\Helper\SynchronizeableInterface;
use ThreeWebOneEntityBundle\Entity\Queue\Message\Synchronize;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\UserConfig\Question;
use TriTran\SqsQueueBundle\Service\Message;
use TriTran\SqsQueueBundle\Service\MessageCollection;

/**
 * Class SynchronizeConsumerService.
 */
class SynchronizeConsumerService extends BatchConsumer
{
    /**
     * {@inheritdoc}
     */
    public function process(MessageCollection $collection): array
    {
        if (false === $this->hasValidatorService()) {
            throw new \RuntimeException('Service was not properly configured, please see BatchConsumer setters!');
        }

        try {
            $result = [];
            $aggregatedMessagesList = $this->getAggregatedMessagesList($collection);

            foreach ($aggregatedMessagesList as $toUserId => $entities) {
                $toUser = $this->getUserById($toUserId);
                foreach ($entities as $entityClass => $commands) {
                    foreach ($commands as $command => $entityMessages) {
                        $result += $this->processEntityMessagesForUser(
                            $toUser,
                            $entityMessages,
                            $this->getEntitiesMap($this->getEntityData($entityMessages))
                        );
                    }
                }
            }

            return $result;
        } catch (\Throwable $t) {
            $this->logger->critical($t->getMessage() . ' got at: ' . $t->getFile() . ':' . $t->getLine());

            return [];
        }
    }

    /**
     * @param MessageCollection $collection
     *
     * @return array
     */
    public function getAggregatedMessagesList(MessageCollection $collection): array
    {
        $aggregated = [];
        /** @var Message $message */
        foreach ($collection as $message) {
            $messageId = $message->getReceiptHandle();
            $messageBody = json_decode($message->getBody(), true);

            /** @var Synchronize $synchronize */
            $synchronize = $this->fillUpEntityWithData(new Synchronize(), $messageBody);
            $violations = $this->getValidatorService()->validate($synchronize);

            if ($violations->count() > 0) {
                $result[$messageId] = false;
            } else {
                $toUserId = $synchronize->getSynchronizeWith();
                $entityClass = $synchronize->getEntityClass();
                $command = $synchronize->getTask();

                $aggregated[$toUserId][$entityClass][$command][$messageId] = $synchronize;
            }
        }

        return $aggregated;
    }

    /**
     * @param User  $user
     * @param array $entityMessages
     * @param array $entitiesMap
     *
     * @return array
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function processEntityMessagesForUser(User $user, array $entityMessages, array $entitiesMap): array
    {
        /**
         * @var int
         * @var Synchronize $entityMessage
         */
        foreach ($entityMessages as $messageId => $entityMessage) {
            $entityId = $entityMessage->getEntityId();
            if (isset($entitiesMap[$entityId])) {
                call_user_func_array(
                    [$this, $entityMessage->getTask()],
                    [$entitiesMap[$entityId], $user]
                );
                $result[$messageId] = true;
            } else {
                $result[$messageId] = false;
            }
        }

        $this->entityManager->flush();

        return $result;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function getEntitiesMap(array $data): array
    {
        $entitiesMap = [];
        foreach ($data as $entity) {
            if (false === method_exists($entity, 'getId')) {
                $this->logger->error(
                    '<![CDATA[' . get_class($entity) . ']]> must have getId method to continue'
                );
            } else {
                $entitiesMap[$entity->getId()] = $entity;
            }
        }

        return $entitiesMap;
    }

    /**
     * @param array $entityMessages
     *
     * @return array
     */
    public function getEntityData(array $entityMessages): array
    {
        $entitiesId = [];
        /**
         * @var int
         * @var Synchronize $entityMessage
         */
        foreach ($entityMessages as $messageId => $entityMessage) {
            if (empty($entityRepository)) {
                /** @var EntityRepository $entityRepository */
                $entityRepository = $this->entityManager->getRepository($entityMessage->getEntityClass());
            }
            $entitiesId[$messageId] = $entityMessage->getEntityId();
        }
        $entitiesData = $entityRepository->findBy(['id' => $entitiesId]);

        return $entitiesData;
    }

    /**
     * @param SynchronizeableInterface $entity
     * @param User                     $user
     */
    public function desynchronize(SynchronizeableInterface $entity, User $user)
    {
        if (in_array($user, $entity->getUsers()->toArray())) {
            $entity->removeUser($user);

            $this->entityManager->persist($entity);
        }
    }

    /**
     * @param SynchronizeableInterface $entity
     * @param User                     $user
     */
    public function synchronize(SynchronizeableInterface $entity, User $user)
    {
        if ($entity instanceof Question) {
            $entity = (clone $entity)
                ->setOwner($user);
        } else {
            if (!in_array($user, $entity->getUsers()->toArray())) {
                $entity->addUser($user);
            }
        }

        $this->entityManager->persist($entity);
    }
}
