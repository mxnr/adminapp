<?php

namespace AdminBundle\Service\SQS;

use Aws\Result;
use TriTran\SqsQueueBundle\Service\Message;
use TriTran\SqsQueueBundle\Service\MessageCollection;

/**
 * Class SynchronizeTaskProducer.
 */
class SynchronizeTaskProducer implements ProducerInterface
{
    /**
     * @var BaseQueue
     */
    protected $queue;

    /**
     * SynchronizeTaskProducer constructor.
     *
     * @param BaseQueue $queue
     */
    public function __construct(BaseQueue $queue)
    {
        $this->queue = $queue;
    }

    /**
     * {@inheritdoc}
     */
    public function publish(string $message, int $delay = 0): string
    {
        return $this->queue->sendMessage((new Message())->setBody($message), $delay);
    }

    /**
     * {@inheritdoc}
     */
    public function publishBatch(array $messages, int $delay = 0): Result
    {
        $collection = new MessageCollection();

        foreach ($messages as $message) {
            $collection->append((new Message())->setBody($message));
        }

        return $this->queue->sendMessageBatch($collection, $delay);
    }
}
