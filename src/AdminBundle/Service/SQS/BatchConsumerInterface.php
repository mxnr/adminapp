<?php

namespace AdminBundle\Service\SQS;

use TriTran\SqsQueueBundle\Service\MessageCollection;

/**
 * Interface BatchConsumerInterface.
 */
interface BatchConsumerInterface
{
    /**
     * @param MessageCollection $collection
     *
     * @return array
     */
    public function process(MessageCollection $collection): array;
}
