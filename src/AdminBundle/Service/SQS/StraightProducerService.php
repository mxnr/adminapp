<?php

namespace AdminBundle\Service\SQS;

use Aws\Result;
use TriTran\SqsQueueBundle\Service\Message;
use TriTran\SqsQueueBundle\Service\MessageCollection;

/**
 * Class StraightProducerService.
 */
class StraightProducerService implements ProducerInterface
{
    /**
     * @var BatchConsumerInterface
     */
    private $consumer;

    /**
     * StraightProducerService constructor.
     *
     * @param BatchConsumerInterface $batchConsumer
     */
    public function __construct(BatchConsumerInterface $batchConsumer)
    {
        $this->consumer = $batchConsumer;
    }

    /**
     * {@inheritdoc}
     */
    public function publish(string $message, int $delay = 0): string
    {
        $collection = new MessageCollection();

        $collection->append((new Message())->setReceiptHandle(time())->setBody($message));

        $this->consumer->process($collection);

        return md5($message);
    }

    /**
     * {@inheritdoc}
     */
    public function publishBatch(array $messages, int $delay = 0): Result
    {
        $collection = new MessageCollection();

        $id = time();
        foreach ($messages as $message) {
            $collection->append((new Message())->setReceiptHandle($id)->setBody($message));
            $id++;
        }

        $this->consumer->process($collection);

        return new Result();
    }
}
