<?php

namespace AdminBundle\Service\SQS;

/**
 * Class ProducerServiceFactory.
 */
class ProducerServiceFactory
{
    /**
     * @param int               $useRealQueues
     * @param ProducerInterface $realProducer
     * @param ProducerInterface $fakeProducer
     *
     * @return ProducerInterface
     */
    public static function createProducer(
        int $useRealQueues,
        ProducerInterface $realProducer,
        ProducerInterface $fakeProducer
    ): ProducerInterface {
        return 1 === $useRealQueues ? $realProducer : $fakeProducer;
    }
}
