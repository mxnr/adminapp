<?php

namespace AdminBundle\Service\SQS;

use Aws\Result;

/**
 * Interface ProducerInterface.
 */
interface ProducerInterface
{
    /**
     * @param string $message
     * @param int    $delay
     *
     * @return string
     */
    public function publish(string $message, int $delay = 0): string;

    /**
     * @param array $messages
     * @param int   $delay
     *
     * @return Result
     */
    public function publishBatch(array $messages, int $delay = 0): Result;
}
