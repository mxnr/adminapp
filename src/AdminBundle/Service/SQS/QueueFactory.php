<?php

namespace AdminBundle\Service\SQS;

use Aws\Sqs\SqsClient;

/**
 * Class QueueFactory.
 */
class QueueFactory
{
    /**
     * @var BaseQueue[]
     */
    private $queues;

    /**
     * @var SqsClient
     */
    private $client;

    /**
     * QueueFactory constructor.
     *
     * @param SqsClient $client
     */
    public function __construct(SqsClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param SqsClient              $client
     * @param string                 $queueName
     * @param string                 $queueUrl
     * @param BatchConsumerInterface $queueWorker
     * @param array                  $options
     *
     * @return BaseQueue
     */
    public static function createQueue(
        SqsClient $client,
        string $queueName,
        string $queueUrl,
        BatchConsumerInterface $queueWorker,
        array $options = []
    ) {
        $instance = new self($client);

        return $instance->create($queueName, $queueUrl, $queueWorker, $options);
    }

    /**
     * @param string                 $queueName
     * @param string                 $queueUrl
     * @param BatchConsumerInterface $queueWorker
     * @param array                  $options
     *
     * @return BaseQueue
     */
    public function create(
        string $queueName,
        string $queueUrl,
        BatchConsumerInterface $queueWorker,
        array $options = []
    ) {
        if (null === $this->queues) {
            $this->queues = [];
        }

        if (isset($this->queues[$queueUrl])) {
            return $this->queues[$queueUrl];
        }

        $queue = new BaseQueue($this->client, $queueName, $queueUrl, $queueWorker, $options);
        $this->queues[$queueUrl] = $queue;

        return $queue;
    }

    /**
     * @return SqsClient
     */
    public function getClient(): SqsClient
    {
        return $this->client;
    }
}
