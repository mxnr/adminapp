<?php

namespace AdminBundle\Service\SQS;

use AdminBundle\Service\AdminUserService;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Repository\UserRepository;

/**
 * Class BatchConsumer.
 */
abstract class BatchConsumer implements BatchConsumerInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var ProducerInterface
     */
    protected $producer;

    /**
     * @var AdminUserService
     */
    protected $adminUserService;

    /**
     * @var RecursiveValidator
     */
    protected $validatorService;

    /**
     * BatchConsumer constructor.
     *
     * @param EntityManager   $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManager $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @return RecursiveValidator
     */
    public function getValidatorService(): RecursiveValidator
    {
        return $this->validatorService;
    }

    /**
     * @return bool
     */
    public function hasValidatorService(): bool
    {
        return !empty($this->validatorService);
    }

    /**
     * @param RecursiveValidator $validatorService
     *
     * @return BatchConsumer
     */
    public function setValidatorService(RecursiveValidator $validatorService): self
    {
        $this->validatorService = $validatorService;

        return $this;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    /**
     * @return bool
     */
    public function hasLogger(): bool
    {
        return !empty($this->logger);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @return bool
     */
    public function hasEntityManager(): bool
    {
        return !empty($this->entityManager);
    }

    /**
     * @return ProducerInterface
     */
    public function getProducer(): ProducerInterface
    {
        return $this->producer;
    }

    /**
     * @return bool
     */
    public function hasProducer(): bool
    {
        return !empty($this->producer);
    }

    /**
     * @param ProducerInterface $producer
     */
    public function setProducer(ProducerInterface $producer)
    {
        $this->producer = $producer;
    }

    /**
     * @return AdminUserService
     */
    public function getAdminUserService(): AdminUserService
    {
        return $this->adminUserService;
    }

    /**
     * @return bool
     */
    public function hasAdminUserService(): bool
    {
        return !empty($this->adminUserService);
    }

    /**
     * @param AdminUserService $adminUserService
     */
    public function setAdminUserService(AdminUserService $adminUserService)
    {
        $this->adminUserService = $adminUserService;
    }

    /**
     * @param int $id
     *
     * @return User
     */
    protected function getUserById(int $id): User
    {
        /** @var UserRepository $user */
        $user = $this->entityManager->getRepository(User::class);

        return $user->find($id);
    }

    /**
     * @param mixed $entity
     * @param array $data
     *
     * @return mixed
     */
    protected function fillUpEntityWithData($entity, array $data = [])
    {
        if (!is_object($entity)) {
            throw new \InvalidArgumentException('$entity must be type of object');
        }

        foreach ($data as $fieldName => $fieldValue) {
            $methodName = 'set' . ucfirst($fieldName);
            if (!is_null($fieldValue) && method_exists($entity, $methodName)) {
                call_user_func_array([$entity, $methodName], [$fieldValue]);
            }
        }

        return $entity;
    }
}
