<?php

namespace AdminBundle\Service\DomainName;

use Doctrine\ORM\EntityManager;
use Exception;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomainQueue;
use ThreeWebOneEntityBundle\Repository\Domain\ClientDomainQueueRepository;

class DomainQueueService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * DomainQueueService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param String $domainName
     * @param int $domainOwnershipType
     */
    public function addDomain(String $domainName, int $domainOwnershipType = ClientDomainQueue::OWNERSHIP_OPEN_SRS)
    {
        $domain = (new ClientDomainQueue())
            ->setDomain($domainName)
            ->setDomainOwnershipType($domainOwnershipType);

        $this->em->persist($domain);
        $this->em->flush();
    }

    /**
     * @param $domainName
     */
    public function removeDomain($domainName)
    {
        $domainRepository = $this->em->getRepository(ClientDomainQueue::class);

        if ($domain = $domainRepository->getByDomainName($domainName)) {
            $this->em->remove($domain);
            $this->em->flush();
        }
    }

    /**
     * @return null|ClientDomainQueue
     * @throws Exception
     */
    public function getDomain(): ?ClientDomainQueue
    {
        $result = null;

        /** @var ClientDomainQueueRepository $domainRepository */
        $domainRepository = $this->em->getRepository(ClientDomainQueue::class);

        $this->em->getConnection()->beginTransaction();
        try {
            if ($domain = $domainRepository->getNextDomain()) {
                $domain->setIsLock(true)
                ->setUpdatedAt(new \DateTime());
                $this->em->persist($domain);
                $this->em->flush();
                $result = $domain;
            }
            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollback();
            throw $e;
        }

        return $result;
    }

    /**
     * @param $domainName
     */
    public function unlockDomain($domainName)
    {
        /** @var ClientDomainQueueRepository $domainRepository */
        $domainRepository = $this->em->getRepository(ClientDomainQueue::class);

        if ($domain = $domainRepository->getByDomainName($domainName)) {
            $domain->setIsLock(false);
            $this->em->persist($domain);
            $this->em->flush();
        }
    }

    public function unlockAllDomains()
    {
        /** @var ClientDomainQueueRepository $domainRepository */
        $domainRepository = $this->em->getRepository(ClientDomainQueue::class);
        if ($domains = $domainRepository->getDomainForUnlock()) {
            /** @var ClientDomainQueue $domain */
            foreach ($domains as $domain) {
                $domain
                    ->setIsLock(false)
                    ->setUpdatedAt(new \DateTime());
                $this->em->persist($domain);
            }
        }
        $this->em->flush();
    }
}
