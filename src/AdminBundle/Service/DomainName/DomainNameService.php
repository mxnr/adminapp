<?php

namespace AdminBundle\Service\DomainName;

use AdminBundle\Exception\AcmRequestCertificateException;
use AdminBundle\Exception\DomainNotFoundException;
use Aws\Exception\AwsException;
use DomainResellerBundle\Service\OpenSRSService;
use Psr\Log\LoggerInterface;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomainQueue;

class DomainNameService
{
    /**
     * @var AcmService
     */
    protected $acmService;

    /**
     * @var ElasticLoadBalancingService
     */
    protected $elbService;

    /**
     * @var OpenSRSService
     */
    protected $openSRSService;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    protected $listenerArn;

    /**
     * @var DomainQueueService
     */
    protected $domainQueueService;

    /**
     * DomainNameService constructor.
     * @param AcmService $acmService
     * @param ElasticLoadBalancingService $elbService
     * @param OpenSRSService $openSRSService
     * @param LoggerInterface $logger
     * @param DomainQueueService $domainQueueService
     * @param string $listenerArn
     */
    public function __construct(
        AcmService $acmService,
        ElasticLoadBalancingService $elbService,
        OpenSRSService $openSRSService,
        LoggerInterface $logger,
        DomainQueueService $domainQueueService,
        string $listenerArn
    ) {
        $this->acmService = $acmService;
        $this->elbService = $elbService;
        $this->openSRSService = $openSRSService;
        $this->logger = $logger;
        $this->domainQueueService = $domainQueueService;
        $this->listenerArn = $listenerArn;
    }

    /**
     * @return bool
     */
    public function process()
    {
        /**@var ClientDomainQueue $domain*/
        while ($domain = $this->domainQueueService->getDomain()) {
            try {
                $this->processAcm($domain);
            } catch (AwsException $e) {
                $this->logger->error('Got aws error: ' . $e->getMessage(), $e->getTrace());
            }
            $this->domainQueueService->unlockDomain($domain->getDomain());
        }
    }

    /**
     * @param ClientDomainQueue $domain
     * @throws AcmRequestCertificateException
     */
    protected function processAcm(ClientDomainQueue $domain)
    {
        try {
            $status = $this->acmService->getStatus($domain->getDomain());
        } catch (DomainNotFoundException $e) {
            $this->acmService->requestCertificate($domain->getDomain());
            return;
        }

        switch ($status) {
            case AcmService::ISSUED:
                $arnCertificate = $this->acmService->getCertificate($domain->getDomain());
                $this->processElb($arnCertificate);
                $this->removeDomainFromQueue($domain->getDomain());
                break;
            case AcmService::PENDING_VALIDATION:
                $this->processDomainReseller($domain->getDomain(), $domain->getDomainOwnershipType());
                break;
            case AcmService::EXPIRED:
            case AcmService::FAILED:
            case AcmService::INACTIVE:
            case AcmService::REVOKED:
                throw new AcmRequestCertificateException('Acm certificate status is ' . $status);
                break;
        }
    }

    /**
     * @param string $certificateArn
     */
    protected function processElb(string $certificateArn)
    {
        $this->elbService->addListener($this->listenerArn, $certificateArn);
    }

    /**
     * @param $domainName
     */
    protected function removeDomainFromQueue($domainName)
    {
        $this->domainQueueService->removeDomain($domainName);
    }

    /**
     * @param string $domainName
     * @param int $domainOwnershipType
     */
    protected function processDomainReseller(string $domainName, int $domainOwnershipType )
    {
        $options = $this->acmService->getValidationOptions($domainName);

        if (
            $domainOwnershipType === ClientDomainQueue::OWNERSHIP_OPEN_SRS &&
            !$this->openSRSService->checkSSLDomainOwnership($options)
        ) {
            $this->openSRSService->setSSLDomainOwnership($options);
        }
    }
}