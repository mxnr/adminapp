<?php

namespace AdminBundle\Service\DomainName;

use Aws\ElasticLoadBalancingV2\ElasticLoadBalancingV2Client;

class ElasticLoadBalancingService
{
    /**
     * @var ElasticLoadBalancingV2Client
     */
    protected $elbClient;

    /**
     * ElasticLoadBalancingService constructor.
     * @param ElasticLoadBalancingV2Client $elbClient
     */
    public function __construct(ElasticLoadBalancingV2Client $elbClient)
    {
        $this->elbClient = $elbClient;
    }

    /**
     * @param string $listenerArn
     * @param string $certificateArn
     */
    public function addListener(string $listenerArn, string $certificateArn)
    {
        $config = [
            'Certificates' => [
                [
                    'CertificateArn' => $certificateArn
                ],
            ],
            'ListenerArn' => $listenerArn,
        ];
        $this->elbClient->addListenerCertificates($config);
    }
}