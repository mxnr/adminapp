<?php

namespace AdminBundle\Service\DomainName;

use AdminBundle\Exception\DomainNotFoundException;
use Aws\Acm\AcmClient;
use Aws\Exception\AwsException;
use Psr\Log\LoggerInterface;

class AcmService
{
    const PENDING_VALIDATION = 'PENDING_VALIDATION';

    const ISSUED = 'ISSUED';

    const INACTIVE = 'INACTIVE';

    const EXPIRED = 'EXPIRED';

    const VALIDATION_TIMED_OUT = 'VALIDATION_TIMED_OUT';

    const REVOKED = 'REVOKED';

    const FAILED = 'FAILED';

    /**
     * @var AcmClient
     */
    protected $acmClient;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * AcmService constructor.
     * @param AcmClient $acmClient
     */
    public function __construct(AcmClient $acmClient, LoggerInterface $logger)
    {
        $this->acmClient = $acmClient;
        $this->logger = $logger;
    }

    /**
     * @param $domainName
     * @return bool
     */
    public function requestCertificate($domainName): bool
    {
        try {
            if ($this->getCertificate($domainName)) {
                return true;
            }
            $domainName = $this->prepareDomainName($domainName);

            $certificate = [
                'DomainName' => $domainName,
                'ValidationMethod' => 'DNS',
                'SubjectAlternativeNames' => ['www.' . $domainName],
            ];
            $result = $this->acmClient->requestCertificate($certificate);

            return (bool)$result['CertificateArn'];
        } catch (AwsException $e) {
            $this->logger->error($e->getMessage());
        }

        return false;
    }

    /**
     * @param $domainName
     * @return array
     */
    public function getSSLDomainOwnershipRecords($domainName)
    {
        return $this->getValidationOptions($domainName);
    }

    /**
     * @param $domainName
     * @return null|string
     * @throws DomainNotFoundException
     */
    public function getCertificate($domainName): ?string
    {
        $domainName = $this->prepareDomainName($domainName);
        $certificates = $this->acmClient->listCertificates()['CertificateSummaryList'];

        foreach ($certificates as $certificate) {
            if ($domainName == $certificate['DomainName']) {
                return $certificate['CertificateArn'];
            }
        }

        return null;
    }

    /**
     * @param string $domainName
     * @return string
     */
    protected function prepareDomainName(string $domainName): string
    {
        return (string)str_replace('www.', '', $domainName);
    }

    /**
     * @param string $domainName
     * @return string
     * @throws DomainNotFoundException
     */
    public function getStatus(string $domainName): string
    {
        if ($certificateArn = $this->getCertificate($domainName)) {
            $result = $this->acmClient->describeCertificate([
                'CertificateArn' => $certificateArn,
            ]);

            return $result['Certificate']['Status'];
        }

        throw new DomainNotFoundException();
    }

    /**
     * @param $domainName
     * @return bool
     * @throws DomainNotFoundException
     */
    public function checkValidation($domainName): bool
    {
        if ($validationOptions = $this->getValidationOptions($domainName)) {
            foreach ($validationOptions as $option) {
                if ($option['ValidationStatus'] <> 'SUCCESS') {
                    return false;
                }
            }

            return true;
        }

        throw new DomainNotFoundException();
    }

    /**
     * @param $domainName
     *
     * @return array [
     *      [
     *          'DomainName' => '3web1-i-shop.com',
     *          'ValidationStatus' => 'SUCCESS',
     *          'ResourceRecord' => [
     *                  'Name' => '_038085e7327ceb4f4a40ebc9fd33f946.3web1-i-shop.com.',
     *                  'Type' => 'CNAME',
     *                  'Value' => '_d5f53f0eadc14ded3c328795d7cb329e.tljzshvwok.acm-validations.aws.',
     *          ],
     *          'ValidationMethod' => 'DNS',
     *      ],
     * ]|null
     * @throws DomainNotFoundException
     */
    public function getValidationOptions($domainName): array
    {
        if ($certificateArn = $this->getCertificate($domainName)) {
            $result = $this->acmClient->describeCertificate(
                [
                    'CertificateArn' => $certificateArn,
                ]
            );

            return $result['Certificate']['DomainValidationOptions'];
        }

        throw new DomainNotFoundException();
    }

    /**
     * @param string $domainName
     * @return bool
     * @throws DomainNotFoundException
     */
    public function deleteCertificate(string $domainName): bool
    {
        if ($certificateArn = $this->getCertificate($domainName)) {
            $this->acmClient->deleteCertificate(
                [
                    'CertificateArn' => $certificateArn,
                ]
            );

            return true;
        }

        throw new DomainNotFoundException();
    }
}