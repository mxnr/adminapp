<?php

namespace AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use RuntimeException;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class AdminUserService.
 */
class AdminUserService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * AdminUserService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Returns administrator user.
     *
     * @return User
     *
     * @throws RuntimeException
     */
    public function getAdminUser(): User
    {
        $user = $this->em->getRepository(User::class)->findOneBy(['username' => 'admin']);

        if (is_null($user)) {
            throw new RuntimeException('Admin user was not found!');
        }

        return $user;
    }
}
