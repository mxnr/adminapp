<?php

namespace AdminBundle\Service;

use AdminBundle\Service\SQS\ProducerInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Parameter;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use ThreeWebOneEntityBundle\Entity\Accessory;
use ThreeWebOneEntityBundle\Entity\Category as CategoryEntity;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\Product;
use ThreeWebOneEntityBundle\Entity\Queue\Message\SynchronizeTask;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\UserConfig\Question;
use ThreeWebOneEntityBundle\Repository\ModelRepository;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * Class InitialSynchronizationService.
 */
class InitialSynchronizationService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var AdminUserService
     */
    private $adminUserService;

    /**
     * @var ProducerInterface
     */
    private $producer;

    /**
     * @var array
     */
    private $entitiesToSynchronize = [
        Model::class,
        [CategoryEntity::class => ["paramParentCategoryId" => 1]], //category
        [CategoryEntity::class => ["paramParentCategoryId" => 2]], //product
        [CategoryEntity::class => ["paramParentCategoryId" => 3]], //provider
        Filter::class,
        PriceType::class,
        Accessory::class,
        Question::class,
        Price::class,
    ];

    /**
     * @var RecursiveValidator
     */
    private $validatorService;

    /**
     * InitialSynchronizationService constructor.
     *
     * @param EntityManager      $entityManager
     * @param AdminUserService   $adminUserService
     * @param ProducerInterface  $producer
     * @param RecursiveValidator $validator
     */
    public function __construct(
        EntityManager $entityManager,
        AdminUserService $adminUserService,
        ProducerInterface $producer,
        RecursiveValidator $validator
    ) {
        $this->entityManager = $entityManager;
        $this->adminUserService = $adminUserService;
        $this->producer = $producer;
        $this->validatorService = $validator;

        $this->validateEntitiesAndThrow();
    }

    private function validateEntitiesAndThrow()
    {
        if ($this->hasEntitiesToSynchronize()) {
            foreach ($this->getEntitiesToSynchronize() as $entityClass => $entity) {
                if (is_string($entity)) {
                    $entityClass = $entity;
                } else {
                    $entityClass = key($entity);
                }

                if (false === class_exists($entityClass)) {
                    throw new \InvalidArgumentException('unable to load entity: <![CDATA[' . $entityClass . ']]>');
                }

                $repository = $this->entityManager->getRepository($entityClass);

                if (false === ($repository instanceof AdminQueryInterface)) {
                    throw new \InvalidArgumentException(
                        'unable to use entity: <![CDATA[' . $entityClass . ']]> because interface ' .
                        'ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface not implemented '
                    );
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getEntitiesToSynchronize(): array
    {
        return $this->entitiesToSynchronize;
    }

    /**
     * @return bool
     */
    public function hasEntitiesToSynchronize(): bool
    {
        return !empty($this->entitiesToSynchronize);
    }

    /**
     * @param User $user
     */
    public function scheduleSynchronizationFor(User $user)
    {
        if ($this->hasEntitiesToSynchronize()) {
            $adminUser = $this->getAdminUserService()->getAdminUser();
            foreach ($this->getEntitiesToSynchronize() as $entityClass => $entity) {
                $additionalParams = [];
                if (is_string($entity)) {
                    $entityClass = $entity;
                } else {
                    $entityClass = key($entity);
                    $additionalParams = $entity[$entityClass];
                }

                /** @var AdminQueryInterface $repository */
                $repository = $this->getEntityManager()->getRepository($entityClass);
                $queryParams = [];
                foreach ($repository->getParamsList() as $param) {
                    switch ($param) {
                        case AdminQueryInterface::PARAM_ADMIN_USER_ID:
                            $queryParams[AdminQueryInterface::PARAM_ADMIN_USER_ID] = $adminUser->getId();
                            break;
                        case AdminQueryInterface::PARAM_OWNER_USER_ID:
                            $queryParams[AdminQueryInterface::PARAM_OWNER_USER_ID] = $user->getId();
                            break;
                        case AdminQueryInterface::PARAM_PARENT_CATEGORY_ID:
                            $queryParams[AdminQueryInterface::PARAM_PARENT_CATEGORY_ID] =
                                $additionalParams[AdminQueryInterface::PARAM_PARENT_CATEGORY_ID];
                            break;
                        case AdminQueryInterface::PARAM_SITES:
                            $queryParams[AdminQueryInterface::PARAM_SITES] = [
                                PriceType::REPAIR,
                                PriceType::BUYBACK,
                                PriceType::SALE,
                            ];
                            break;
                        case AdminQueryInterface::PARAM_SITES_PRICE:
                            $queryParams[AdminQueryInterface::PARAM_SITES_PRICE] = [
                                PriceType::REPAIR,
                                PriceType::BUYBACK,
                            ];
                            break;
                        case ModelRepository::PARAM_SWITCH_QUERY:
                            $queryParams[ModelRepository::PARAM_SWITCH_QUERY] = ModelRepository::QUERY_MODEL;
                            break;
                        default:
                            throw new \RuntimeException(
                                'param have to be processed properly, but unknown: <![CDATA[' . $param . ']]>'
                            );
                    }
                }
                $query = $repository->getAdminQuery($queryParams);

                $queryParams = $query->getParameters();
                $queryParamsArray = [];

                /** @var Parameter $param */
                foreach ($queryParams as $param) {
                    $queryParamsArray[$param->getName()] = $param->getValue();
                }

                $synchronizeTask = (new SynchronizeTask())
                    ->setEntityClass($entityClass)
                    ->setSynchronizeWith($user->getId())
                    ->setQuery($query->getDQL())
                    ->setQueryParams($queryParamsArray)
                    ->setTask('synchronize');

                $violations = $this->getValidatorService()->validate($synchronizeTask);

                if (0 == $violations->count()) {
                    $this->getProducer()->publish((string) $synchronizeTask);
                }
            }
        }
    }

    /**
     * @return AdminUserService
     */
    public function getAdminUserService(): AdminUserService
    {
        return $this->adminUserService;
    }

    /**
     * @return bool
     */
    public function hasAdminUserService(): bool
    {
        return !empty($this->adminUserService);
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @return bool
     */
    public function hasEntityManager(): bool
    {
        return !empty($this->entityManager);
    }

    /**
     * @return RecursiveValidator
     */
    public function getValidatorService(): RecursiveValidator
    {
        return $this->validatorService;
    }

    /**
     * @return bool
     */
    public function hasValidatorService(): bool
    {
        return !empty($this->validatorService);
    }

    /**
     * @return ProducerInterface
     */
    public function getProducer(): ProducerInterface
    {
        return $this->producer;
    }

    /**
     * @return bool
     */
    public function hasProducer(): bool
    {
        return !empty($this->producer);
    }
}
