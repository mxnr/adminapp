<?php

namespace AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemImage;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\User;

class InventoryItemService
{
    /**
     * 13 digets needed for EAN-13 standard
     */
    const FIRST_BARCODE_CODE = '1000000000000';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var UserGroupsService
     */
    private $userGroupService;

    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * @var string
     */
    private $lastBarcode;

    /**
     * InventoryItemService constructor.
     *
     * @param EntityManager $entityManager
     * @param UserGroupsService $userGroupsService
     * @param ImageService $imageService
     */
    public function __construct(
        EntityManager $entityManager,
        UserGroupsService $userGroupsService,
        ImageService $imageService
    ) {
        $this->em = $entityManager;
        $this->userGroupService = $userGroupsService;
        $this->imageService = $imageService;
    }

    /**
     * Generate barcodes based on quantity
     *
     * @param InventoryItem $inventoryItem
     * @param Order $order
     * @param User $user
     * @param Bool $reserveBarcode
     *
     * @return InventoryItem
     */
    public function generateBarcodes(InventoryItem $inventoryItem, Order $order, User $user = null)
    {
        $limit = $inventoryItem->getEnteredQuantity();
        $this->em->getConnection()->beginTransaction();
        try {
            if (!$this->lastBarcode) {
                $lastBarcode = $this->em->getRepository(InventoryItemBarcode::class)
                    ->getUserLastBarcodesCode($this->userGroupService->getUserOrUserSupervisor($user));
                if ($lastBarcode) {
                    $this->lastBarcode = $lastBarcode->getBarcode();
                } else {
                    $this->lastBarcode = self::FIRST_BARCODE_CODE;
                }
            }
            for ($i = 0; $i < $limit; $i++) {
                $barcode = new InventoryItemBarcode();
                $barcode->setBarcode(++$this->lastBarcode)
                    ->setPurchasePrice($inventoryItem->getPurchasePrice())
                    ->setSaleOrder($order);
                $inventoryItem->addInventoryItemBarcode($barcode);
                $this->em->persist($barcode);
            }
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
        }

        return $inventoryItem;
    }

    /**
     * @param Order $order
     * @param string $content
     */
    public function setSignatureImage(Order $order, string $content)
    {
        $imageFile = $this->imageService
            ->createUploadedFileFormString($content);
        if ($imageFile instanceof UploadedFile) {
            $order->setImageFile($imageFile);
            $order->refreshUpdated();
        }
    }

    /**
     * @param InventoryItem $inventoryItem
     * @param User $user
     */
    public function createPrice(InventoryItem $inventoryItem, User $user)
    {
        $price = (new Price())
            ->setOwner($user)
            ->addInventoryItem($inventoryItem)
            ->setValue($inventoryItem->getPriceValue())
            ->setPriceType($inventoryItem->getPriceType())
            ->setModel($inventoryItem->getModel());

        $this->em->persist($price);
    }

    /**
     * @param InventoryItem $inventoryItem
     */
    public function updatePrice(InventoryItem $inventoryItem)
    {
        $inventoryItem->getPrice()
            ->addInventoryItem($inventoryItem)
            ->setValue($inventoryItem->getPriceValue())
            ->setPriceType($inventoryItem->getPriceType())
            ->setModel($inventoryItem->getModel());
    }

    /**
     * @param FormInterface $form
     * @param User $user
     * @throws \Doctrine\ORM\ORMException
     */
    public function manageModelSelectData(FormInterface $form, User $user) {
        $inventoryItem = $form->getData()
            ->setOwner($user);
        $searchModelData = $form->get('searchModel')->getData();
        if ($searchModelData instanceof Model) {
            $inventoryItem->setModel($searchModelData)
                ->setTitle($searchModelData->getTitle())
                ->setCategory($searchModelData->getCategoryByType(Category::TYPE_CATEGORY));

            foreach ($searchModelData->getImages() as $image) {
                $image = (new InventoryItemImage())
                    ->setOwner($user)
                    ->setImageName($image->getImageName());
                $inventoryItem->addImage($image);
                $this->em->persist($image);
            }
        }
        if (is_string($searchModelData)) {
            $inventoryItem->setTitle($searchModelData);
        }
    }
}
