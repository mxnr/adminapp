<?php

namespace AdminBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Router;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\User;

class PriceService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var AdminUserService
     */
    protected $adminUserService;

    /**
     * @var UserGroupsService
     */
    protected $userGroupsService;

    /**
     * PriceService constructor.
     *
     * @param EntityManager $entityManager
     * @param Router $router
     * @param AdminUserService $adminUserService
     * @param UserGroupsService $userGroupsService
     */
    public function __construct(
        EntityManager $entityManager,
        Router $router,
        AdminUserService $adminUserService,
        UserGroupsService $userGroupsService
    ) {
        $this->em = $entityManager;
        $this->router = $router;
        $this->adminUserService = $adminUserService;
        $this->userGroupsService = $userGroupsService;
    }

    /**
     * @param Model $model
     * @param PriceType $priceType
     * @param int $value
     */
    public function managePrice(Model $model, PriceType $priceType, float $value)
    {
        $user = $this->userGroupsService->getUserOrUserSupervisor();
        $admin = $this->adminUserService->getAdminUser();
        $price = $this->getPriceByType($model, $priceType->getId(), $user->getId());
        if (!$price && $value) {
            $price = (new Price())
                ->setModel($model)
                ->setOwner($user)
                ->addUser($user)
                ->setPriceType($priceType);
            $price->setValue((int) $value * 100);
            $this->em->persist($price);
        }
        if ($price) {
            $price->setValue($value * 100);
        }

        if ($this->isPriceSynchronized($model, $priceType->getId(), $user->getId(), $admin->getId())) {
            $adminPrice = $this->getPriceByType($model, $priceType->getId(), $admin->getId());
            $adminPrice->removeUser($user);
        }

        $this->em->flush();
    }

    /**
     * @param User $user
     * @param Price $userPrice
     * @param Price $adminPrice
     */
    public function synchronizePrice(User $user, ?Price $userPrice, Price $adminPrice)
    {
        $adminPrice->addUser($user);
        if ($userPrice) {
            $this->em->remove($userPrice);
        }

        $this->em->flush();
    }

    /**
     * @param Model $model
     * @param int $priceTypeId
     * @param int $ownerId
     *
     * @return Price|null
     */
    public function getPriceByType(Model $model, int $priceTypeId, int $ownerId)
    {
        foreach ($model->getPrices() as $item) {
            if ($item->getPriceType()->getId() == $priceTypeId && $item->getOwner()->getId() == $ownerId) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @param Model $model
     * @param int $priceTypeId
     * @param int $userId
     * @param int $adminId
     *
     * @return bool
     */
    public function isPriceSynchronized(Model $model, int $priceTypeId, int $userId, int $adminId)
    {
        $price = $this->getPriceByType($model, $priceTypeId, $adminId);
        if ($price) {
            foreach ($price->getUsers() as $user) {
                if ($userId == $user->getId()) {
                    return true;
                }
            }
        }

        return false;
    }
}
