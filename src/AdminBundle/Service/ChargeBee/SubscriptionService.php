<?php

namespace AdminBundle\Service\ChargeBee;

use AdminBundle\Event\DnsEvent;
use AdminBundle\Service\DomainNameService;
use ChargeBeeBundle\Packet\Subscriptions\Cancel;
use ChargeBeeBundle\Packet\Subscriptions\Create;
use ChargeBeeBundle\Packet\Subscriptions\ListAll;
use ChargeBeeBundle\Packet\Subscriptions\Retrieve;
use ChargeBeeBundle\Service\SubscriptionsService;
use DateInterval;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Billing\Plan;
use ThreeWebOneEntityBundle\Entity\Billing\PlanInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Entity\Billing\SubscriptionInterface;
use ThreeWebOneEntityBundle\Entity\Domain\Zone;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class SubscriptionService.
 */
class SubscriptionService
{
    const STATUS_CONVERSION_MAP = [
        'in_trial' => SubscriptionInterface::STATUS_TRIAL,
        'future' => SubscriptionInterface::STATUS_FUTURE,
        'active' => SubscriptionInterface::STATUS_ACTIVE,
        'non_renewing' => SubscriptionInterface::STATUS_ENDING,
        'cancelled' => SubscriptionInterface::STATUS_CANCELLED,
    ];

    const CHARGE_INTERVAL_CONVERSION_MAP = [
        'year' => PlanInterface::PERIOD_YEAR,
        'month' => PlanInterface::PERIOD_MONTH,
    ];

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Subscription
     */
    protected $subscription;

    /**
     * @var bool
     */
    protected $subscriber = false;

    /**
     * @var bool
     */
    protected $sellSubscribed = false;

    /**
     * @var bool
     */
    protected $buySubscribed = false;

    /**
     * @var bool
     */
    protected $repairSubscribed = false;

    /**
     * @var SubscriptionsService
     */
    private $subscriptionsService;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var TokenInterface
     */
    private $token;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var int
     */
    private $subscriptionStatus;

    /**
     * SubscriptionService constructor.
     *
     * @param TokenStorage             $tokenStorage
     * @param SubscriptionsService     $subscriptionsService
     * @param EntityManagerInterface   $entityManager
     * @param ValidatorInterface       $validator
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        TokenStorage $tokenStorage,
        SubscriptionsService $subscriptionsService,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this
            ->setToken($tokenStorage->getToken())
            ->setSubscriptionsService($subscriptionsService)
            ->setEntityManager($entityManager)
            ->setValidator($validator)
            ->setEventDispatcher($eventDispatcher)
            ->initialize();
    }

    public function initialize()
    {
        $this->setAllFeaturesUnsubscribed();

        if ($this->hasToken() === false) {
            //no token, not authenticated at all
            return;
        }

        if (!$this->hasUser()) {
            /** @var User $user */
            $user = $this->getToken()->getUser();

            if ($user instanceof User) {
                //check if current user is not the owner of service
                if ($user->getSupervisor()) {
                    $user = $user->getSupervisor();
                }

                $this->setUser($user);
            }
        }

        if ($this->hasUser() === false) {
            return;
        }

        $user = $this->getUser();

        //for super admin user just allow everything
        if ($user->hasRole(UserInterface::ROLE_SUPER_ADMIN)) {
            $this->setAllFeaturesSubscribed();

            return;
        }

        if ($user->hasBillingCustomer()) {
            $customer = $user->getBillingCustomer();
            if ($customer->hasSubscriptions()) {
                $subscriptions = $customer->getSubscriptions();
                /** @var Subscription $subscription */
                foreach ($subscriptions as $subscription) {
                    if ($this->isSubscriptionActive($subscription)) {
                        $this->setSubscriptionStatus($subscription->getStatus());
                        $this->setSubscription($subscription);
                        break;
                    }
                }

                if ($this->hasSubscription()) {
                    $this->setSubscriber(true);
                }
            }
        }

        if ($this->isSubscriber()) {
            $subscribedItems = $this->subscription->getItems();

            foreach (SubscriptionInterface::ITEMS_LIST as $item) {
                if (isset($subscribedItems[$item]) && $subscribedItems[$item] === true) {
                    if ($item === SubscriptionInterface::ITEM_BUY) {
                        $this->setBuySubscribed(true);
                    }

                    if ($item === SubscriptionInterface::ITEM_SELL) {
                        $this->setSellSubscribed(true);
                    }

                    if ($item === SubscriptionInterface::ITEM_REPAIR) {
                        $this->setRepairSubscribed(true);
                    }
                }
            }
        }
    }

    /**
     * Sets all the features as unsibsribed.
     */
    protected function setAllFeaturesUnsubscribed()
    {
        $this->setSubscriber(false);
        $this->setSellSubscribed(false);
        $this->setBuySubscribed(false);
        $this->setRepairSubscribed(false);
    }

    /**
     * @return TokenInterface
     */
    public function getToken(): TokenInterface
    {
        return $this->token;
    }

    /**
     * @return bool
     */
    public function hasToken(): bool
    {
        return $this->token !== null;
    }

    /**
     * @param TokenInterface|null $token
     *
     * @return SubscriptionService
     */
    public function setToken(TokenInterface $token = null): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function hasUser(): bool
    {
        return $this->user !== null;
    }

    /**
     * @param User $user
     *
     * @return SubscriptionService
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Sets all the features as Subscribed, mostly used for edge cases like for admin user, etc.
     */
    protected function setAllFeaturesSubscribed()
    {
        $this->setSubscriber(true);
        $this->setSellSubscribed(true);
        $this->setBuySubscribed(true);
        $this->setRepairSubscribed(true);
    }

    /**
     * @param Subscription $subscription
     *
     * @return bool
     */
    protected function isSubscriptionActive(Subscription $subscription): bool
    {
        $subscriptionStatus = $subscription->getStatus();
        $isTrialAvailable = $subscription->getCustomer()->isTrialPeriodAvailable();
        $activeStatusesList = [SubscriptionInterface::STATUS_ACTIVE, SubscriptionInterface::STATUS_ENDING];
        if ($subscriptionStatus != SubscriptionInterface::STATUS_CANCELLED) {
            if ($subscriptionStatus === SubscriptionInterface::STATUS_TRIAL && $isTrialAvailable) {
                return true;
            } elseif (in_array($subscriptionStatus, $activeStatusesList)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isSubscriber(): bool
    {
        return $this->subscriber;
    }

    /**
     * @return bool
     */
    public function hasSubscriber(): bool
    {
        return $this->subscriber !== null;
    }

    /**
     * @param bool $subscriber
     *
     * @return SubscriptionService
     */
    public function setSubscriber(bool $subscriber): self
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsSubscriptionTrial(): bool
    {
        return $this->isSubscriber() && $this->getSubscriptionStatus() === SubscriptionInterface::STATUS_TRIAL;
    }

    /**
     * @return int|null
     */
    public function getSubscriptionStatus(): ?int
    {
        return $this->subscriptionStatus;
    }

    /**
     * @return bool
     */
    public function hasSubscriptionStatus(): bool
    {
        return !is_null($this->subscriptionStatus);
    }

    /**
     * @param int|null $subscriptionStatus
     *
     * @return SubscriptionService
     */
    public function setSubscriptionStatus(int $subscriptionStatus = null): SubscriptionService
    {
        if (!in_array($subscriptionStatus, SubscriptionInterface::STATUSES_LIST)) {
            throw new \InvalidArgumentException('unknown $subscriptionStatus <![CDATA[' . $subscriptionStatus . ']]>');
        }

        $this->subscriptionStatus = $subscriptionStatus;

        return $this;
    }

    /**
     * @param Subscription $subscription
     *
     * @return bool
     */
    public function cancel(Subscription $subscription): bool
    {
        $subscriptionCancelMethod = new Cancel();
        $subscriptionCancelMethod->setId($subscription->getChargeBeeId());
        $subscriptionData = $this->getSubscriptionsService()->cancel($subscriptionCancelMethod);

        if (isset($subscriptionData['subscription']['id'])) {
            $subscription = $this->getObjectFromData($subscriptionData, $subscription);
            $this->getEntityManager()->persist($subscription);
            $this->getEntityManager()->flush();

            return true;
        }

        return false;
    }

    /**
     * @return SubscriptionsService
     */
    public function getSubscriptionsService(): SubscriptionsService
    {
        return $this->subscriptionsService;
    }

    /**
     * @return bool
     */
    public function hasSubscriptionsService(): bool
    {
        return $this->subscriptionsService !== null;
    }

    /**
     * @param SubscriptionsService $subscriptionsService
     *
     * @return SubscriptionService
     */
    public function setSubscriptionsService(SubscriptionsService $subscriptionsService): self
    {
        $this->subscriptionsService = $subscriptionsService;

        return $this;
    }

    /**
     * @param array             $data
     * @param Subscription|null $subscription
     *
     * @return Subscription
     */
    public function getObjectFromData(array $data, Subscription $subscription = null): Subscription
    {
        if ($subscription === null) {
            $subscription = new Subscription();
        }
        $subscriptionData = $data['subscription'];
        $cardData = isset($data['card']) ? $data['card'] : [];

        if (isset(self::STATUS_CONVERSION_MAP[$subscriptionData['status']])) {
            $subscriptionStatus = self::STATUS_CONVERSION_MAP[$subscriptionData['status']];
        } else {
            throw new InvalidArgumentException('unknown status: <![CDATA[' . $subscriptionData['status'] . ']]>');
        }

        if (isset(self::CHARGE_INTERVAL_CONVERSION_MAP[$subscriptionData['billing_period_unit']])) {
            $chargeIntervalUnit = self::CHARGE_INTERVAL_CONVERSION_MAP[$subscriptionData['billing_period_unit']];
        } else {
            throw new InvalidArgumentException(
                'unknown billing period unit: <![CDATA[' . $subscriptionData['billing_period_unit'] . ']]>'
            );
        }

        $subscription
            ->setChargeBeeId($subscriptionData['id'])
            ->setStatus($subscriptionStatus)
            ->setChargeInterval($subscriptionData['billing_period'])
            ->setChargeIntervalUnit($chargeIntervalUnit);

        if (isset($cardData['status'])) {
            $subscription
                ->setHasCard(true);
        }

        if (isset($subscriptionData['started_at'])) {
            $subscription->setStartedAt($subscriptionData['started_at']);
        }

        if (isset($subscriptionData['next_billing_at'])) {
            $subscription->setNextChargeAt($subscriptionData['next_billing_at']);
        }

        if (isset($subscriptionData['activated_at'])) {
            $subscription->setActivatedAt($subscriptionData['activated_at']);
        }

        if (isset($subscriptionData['trial_start'])) {
            $subscription->setTrialStart($subscriptionData['trial_start']);
        }

        if (isset($subscriptionData['trial_end'])) {
            $subscription->setTrialEnd($subscriptionData['trial_end']);
        }

        if (isset($subscriptionData['current_term_start'])) {
            $subscription->setCurrentPeriodStart($subscriptionData['current_term_start']);
        }

        if (isset($subscriptionData['current_term_end'])) {
            $subscription->setCurrentPeriodEnd($subscriptionData['current_term_end']);
        }

        if (isset($subscriptionData['cancelled_at'])) {
            $subscription->setCancelledAt($subscriptionData['cancelled_at']);
        }

        $constraintViolations = $this->getValidator()->validate($subscription);
        if ($constraintViolations->count() > 0) {
            throw new ValidatorException((string) $constraintViolations);
        }

        return $subscription;
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @return bool
     */
    public function hasValidator(): bool
    {
        return $this->validator !== null;
    }

    /**
     * @param ValidatorInterface $validator
     *
     * @return SubscriptionService
     */
    public function setValidator(ValidatorInterface $validator): self
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return bool
     */
    public function hasEntityManager(): bool
    {
        return $this->entityManager !== null;
    }

    /**
     * @param EntityManagerInterface $entityManager
     *
     * @return SubscriptionService
     */
    public function setEntityManager(EntityManagerInterface $entityManager): self
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @param Customer $customer
     * @param Plan     $plan
     * @param array    $sites
     * @param bool     $immediate
     *
     * @return Subscription
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function subscribe(Customer $customer, Plan $plan, array $sites, bool $immediate = false): Subscription
    {
        $subscribeMethod = new Create();

        $subscribeMethod
            ->setCustomerId($customer->getChargeBeeId())
            ->setPlanId($plan->getChargeBeeId());

        if ($immediate === true) {
            //have to be active from now
            $subscribeMethod->setTrialEnd(0);
        }

        if ($plan->hasHasTrial()) {
            $trialPeriodEnd = clone $customer->getTrialStartedAt();

            $planTrialInterval = $plan->getTrialInterval();
            $planTrialIntervalUnit = $plan->getTrialIntervalUnit();

            if ($planTrialIntervalUnit == PlanInterface::PERIOD_DAY) {
                $trialPeriodShift = new DateInterval('P' . $planTrialInterval . 'D');
            } else {
                $trialPeriodShift = new DateInterval('P' . $planTrialInterval . 'M');
            }

            $trialPeriodEnd->add($trialPeriodShift);
            $customer->setTrialEndedAt($trialPeriodEnd);

            $this->entityManager->persist($customer);
        }

        $subscription = $this->subscriptionsService->create($subscribeMethod);

        $subscriptionEntity = $this->getObjectFromData($subscription);

        $subscriptionEntity
            ->setPlan($plan)
            ->setCustomer($customer)
            ->setItems($sites);

        $this->entityManager->persist($subscriptionEntity);

        $this->entityManager->flush();

        return $subscriptionEntity;
    }

    /**
     * @return bool
     */
    public function isSellSubscribed(): bool
    {
        return $this->sellSubscribed;
    }

    /**
     * @return bool
     */
    public function hasSellSubscribed(): bool
    {
        return $this->sellSubscribed !== null;
    }

    /**
     * @param bool $sellSubscribed
     *
     * @return SubscriptionService
     */
    public function setSellSubscribed(bool $sellSubscribed): self
    {
        $this->sellSubscribed = $sellSubscribed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBuySubscribed(): bool
    {
        return $this->buySubscribed;
    }

    /**
     * @return bool
     */
    public function hasBuySubscribed(): bool
    {
        return $this->buySubscribed !== null;
    }

    /**
     * @param bool $buySubscribed
     *
     * @return SubscriptionService
     */
    public function setBuySubscribed(bool $buySubscribed): self
    {
        $this->buySubscribed = $buySubscribed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRepairSubscribed(): bool
    {
        return $this->repairSubscribed;
    }

    /**
     * @return bool
     */
    public function hasRepairSubscribed(): bool
    {
        return $this->repairSubscribed !== null;
    }

    /**
     * @param bool $repairSubscribed
     *
     * @return SubscriptionService
     */
    public function setRepairSubscribed(bool $repairSubscribed): self
    {
        $this->repairSubscribed = $repairSubscribed;

        return $this;
    }

    /**
     * @return Subscription
     */
    public function getSubscription(): Subscription
    {
        return $this->subscription;
    }

    /**
     * @return bool
     */
    public function hasSubscription(): bool
    {
        return $this->subscription !== null;
    }

    /**
     * @param Subscription $subscription
     *
     * @return SubscriptionService
     */
    public function setSubscription(Subscription $subscription): self
    {
        $this->subscription = $subscription;

        return $this;
    }

    /**
     * @param string|null $subscriptionId
     *
     * @return void
     */
    public function synchronize(string $subscriptionId = null)
    {
        $subscriptionsService = $this->getSubscriptionsService();
        $subscriptions = [];

        $em = $this->getEntityManager();
        $repo = $em->getRepository(Subscription::class);

        if ($subscriptionId !== null) {
            $retrieveSubscriptionMethod = (new Retrieve())->setId($subscriptionId);
            $subscription = $subscriptionsService->retrieve($retrieveSubscriptionMethod);

            $subscriptions[$subscription['subscription']['id']] = $subscription;

            $internalSubscriptions = [$repo->findBy(['chargeBeeId' => $subscriptionId])];
        } else {
            $listAllSubscriptionsMethod = new ListAll();
            $keepFetching = true;
            while ($keepFetching) {
                $subscriptionsPart = $subscriptionsService->listAll($listAllSubscriptionsMethod);

                if (!empty($subscriptionsPart['nextOffset'])) {
                    $listAllSubscriptionsMethod->setOffset($subscriptionsPart['nextOffset']);
                } else {
                    $keepFetching = false;
                }

                foreach ($subscriptionsPart['list'] as $partSubscription) {
                    $subscriptions[$partSubscription['subscription']['id']] = $partSubscription;
                }
            }

            $internalSubscriptions = $repo->getAll();
        }

        /** @var Subscription $internalSubscription */
        foreach ($internalSubscriptions as $internalSubscription) {
            $internalSubscription = $internalSubscription[0];

            $internalCBId = $internalSubscription->getChargeBeeId();

            if (isset($subscriptions[$internalCBId])) {
                $oldStatus = $internalSubscription->getStatus();
                $internalSubscription = $this->getObjectFromData(
                    $subscriptions[$internalCBId],
                    $internalSubscription
                );
                $newStatus = $internalSubscription->getStatus();

                if ($oldStatus != $newStatus) {
                    $this->actualizeZoneState($internalSubscription);
                }

                $em->persist($internalSubscription);
            }
        }

        $em->flush();
    }

    /**
     * @param Subscription $subscription
     */
    protected function actualizeZoneState(Subscription $subscription)
    {
        $event = new DnsEvent();
        $user = $subscription->getCustomer()->getUser();
        if ($this->isSubscriptionActive($subscription) === false) {
            if ($user->hasDomainZone()) {
                $event->setZone($user->getDomainZone());
                $this->getEventDispatcher()->dispatch(DomainNameService::EVENT_DNS_UNLINK, $event);
            }
        } else {
            if ($user->hasDomainZone() === false) {
                $zone = (new Zone())
                    ->setDomain($user->getStoreAddress())
                    ->setOwner($user);
                $event->setZone($zone);

                $this->getEventDispatcher()->dispatch(DomainNameService::EVENT_DNS_LINK, $event);
            }
        }
    }

    /**
     * @return EventDispatcherInterface|null
     */
    public function getEventDispatcher(): ?EventDispatcherInterface
    {
        return $this->eventDispatcher;
    }

    /**
     * @return bool
     */
    public function hasEventDispatcher(): bool
    {
        return !is_null($this->eventDispatcher);
    }

    /**
     * @param EventDispatcherInterface|null $eventDispatcher
     *
     * @return SubscriptionService
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher = null): SubscriptionService
    {
        $this->eventDispatcher = $eventDispatcher;

        return $this;
    }
}
