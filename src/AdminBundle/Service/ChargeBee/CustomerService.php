<?php

namespace AdminBundle\Service\ChargeBee;

use ChargeBeeBundle\Packet\Customers\Create;
use ChargeBeeBundle\Packet\Customers\ListAll;
use ChargeBeeBundle\Packet\Customers\Retrieve;
use ChargeBeeBundle\Service\CustomersService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Address;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Billing\CustomerInterface;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class CustomerService.
 */
class CustomerService
{
    const CARD_STATUS_CONVERSION_MAP = [
        'no_card' => CustomerInterface::CARD_STATUS_NONE,
        'valid' => CustomerInterface::CARD_STATUS_VALID,
        'expiring' => CustomerInterface::CARD_STATUS_EXPIRING,
        'expired' => CustomerInterface::CARD_STATUS_EXPIRED,
    ];

    /**
     * @var CustomersService
     */
    private $customersService;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * PaymentProcessInitializationListener constructor.
     *
     * @param CustomersService       $customersService
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface     $validator
     */
    public function __construct(
        CustomersService $customersService,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator
    ) {
        $this
            ->setCustomersService($customersService)
            ->setEntityManager($entityManager)
            ->setValidator($validator);
    }

    /**
     * @param User $user
     *
     * @return Customer
     */
    public function createCustomerVia(User $user): Customer
    {
        $createCustomerCommand = new Create();

        $createCustomerCommand
            ->setEmail($user->getEmail())
            ->setCompany($user->getStoreName() ?? '')
            ->setLastName($user->getFirstname() ?? '')
            ->setLastName($user->getLastname() ?? '');

        $locale = $user->getLocale();
        if (!empty($locale)) {
            $createCustomerCommand->setLocale($locale);
        }

        $customer = $this->getCustomersService()->create($createCustomerCommand);
        $em = $this->getEntityManager();

        $customerEntity = $this->getObjectFromData($customer)->setUser($user);

        $errors = $this->validator->validate($customerEntity);
        if ($errors->count() > 0) {
            throw new ValidatorException((string) $errors);
        }

        $em->persist($customerEntity);
        $em->flush();

        return $customerEntity;
    }

    /**
     * @return CustomersService
     */
    public function getCustomersService(): CustomersService
    {
        return $this->customersService;
    }

    /**
     * @return bool
     */
    public function hasCustomersService(): bool
    {
        return $this->customersService !== null;
    }

    /**
     * @param CustomersService|null $customersService
     *
     * @return CustomerService
     */
    public function setCustomersService(CustomersService $customersService = null): CustomerService
    {
        $this->customersService = $customersService;

        return $this;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return bool
     */
    public function hasEntityManager(): bool
    {
        return $this->entityManager !== null;
    }

    /**
     * @param EntityManagerInterface|null $entityManager
     *
     * @return CustomerService
     */
    public function setEntityManager(EntityManagerInterface $entityManager = null): CustomerService
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @param array         $data
     * @param Customer|null $customer
     *
     * @return Customer
     */
    public function getObjectFromData(array $data, Customer $customer = null): Customer
    {
        if ($customer === null) {
            $customer = new Customer();
        }

        if ($customer->hasAddress() === false) {
            $address = (new Address())->setCustomer($customer);
            $customer->setAddress($address);
        }

        $address = $customer->getAddress();

        $externalCustomer = $data['customer'];

        $billingAddress = isset($externalCustomer['billing_address']) ? $externalCustomer['billing_address'] : [];

        if (isset($billingAddress['first_name'])) {
            $address->setFirstName($billingAddress['first_name']);
        }

        if (isset($billingAddress['last_name'])) {
            $address->setLastName($billingAddress['last_name']);
        }

        if (isset($billingAddress['company'])) {
            $address->setCompany($billingAddress['company']);
        }

        if (isset($billingAddress['phone'])) {
            $address->setPhone($billingAddress['phone']);
        }

        if (isset($billingAddress['line1'])) {
            $address->setLine1($billingAddress['line1']);
        }

        if (isset($billingAddress['line2'])) {
            $address->setLine2($billingAddress['line2']);
        }

        if (isset($billingAddress['line3'])) {
            $address->setLine3($billingAddress['line3']);
        }

        if (isset($billingAddress['city'])) {
            $address->setCity($billingAddress['city']);
        }

        if (isset($billingAddress['state'])) {
            $address->setState($billingAddress['state']);
        }

        if (isset($billingAddress['country'])) {
            $address->setCountry($billingAddress['country']);
        }

        if (isset($billingAddress['zip'])) {
            $address->setZip($billingAddress['zip']);
        }

        if (isset($billingAddress['validation_status'])) {
            $address->setValidationStatus($billingAddress['validation_status']);
        }

        if (isset(self::CARD_STATUS_CONVERSION_MAP[$externalCustomer['card_status']])) {
            $externalCardStatus = self::CARD_STATUS_CONVERSION_MAP[$externalCustomer['card_status']];
        } else {
            throw new \InvalidArgumentException(
                'unknown card status: <![CDATA[' . $externalCustomer['card_status'] . ']]'
            );
        }

        $customer
            ->setChargeBeeId($externalCustomer['id'])
            ->setCardStatus($externalCardStatus);

        $constraintViolations = $this->getValidator()->validate($customer);
        if ($constraintViolations->count() > 0) {
            throw new ValidatorException((string) $constraintViolations);
        }

        return $customer;
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @return bool
     */
    public function hasValidator(): bool
    {
        return $this->validator !== null;
    }

    /**
     * @param ValidatorInterface|null $validator
     *
     * @return CustomerService
     */
    public function setValidator(ValidatorInterface $validator = null): CustomerService
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * @param string|null $customerId
     *
     * @return void
     */
    public function synchronize(string $customerId = null)
    {
        $customersService = $this->getCustomersService();
        $em = $this->getEntityManager();

        /** @var \ThreeWebOneEntityBundle\Repository\Billing\CustomerRepository $repo */
        $repo = $em->getRepository(Customer::class);

        if ($customerId !== null) {
            $retrieveCustomer = (new Retrieve())->setId($customerId);
            $customer = $customersService->retrieve($retrieveCustomer);
            $externalCustomers[$customer['customer']['id']] = $customer;

            $internalCustomers = [$repo->findBy(['chargeBeeId' => $customerId])];
        } else {
            $internalCustomers = $repo->getAll();
            $listAllCustomers = new ListAll();

            $externalCustomers = [];
            $keepFetching = true;
            while ($keepFetching) {
                $customersPart = $customersService->listAll($listAllCustomers);

                if (!empty($customersPart['nextOffset'])) {
                    $listAllCustomers->setOffset($customersPart['nextOffset']);
                } else {
                    $keepFetching = false;
                }

                foreach ($customersPart['list'] as $partCustomer) {
                    $externalCustomers[$partCustomer['customer']['id']] = $partCustomer;
                }
            }
        }

        /** @var Customer $internalCustomer */
        foreach ($internalCustomers as $internalCustomer) {
            $internalCustomer = $internalCustomer[0];

            $internalCBId = $internalCustomer->getChargeBeeId();

            if (isset($externalCustomers[$internalCBId])) {
                $externalCustomer = $externalCustomers[$internalCBId];

                $internalCustomer = $this->getObjectFromData($externalCustomer, $internalCustomer);

                $em->persist($internalCustomer);
            } else {
                $em->detach($internalCustomer);
            }
        }

        $em->flush();
    }
}
