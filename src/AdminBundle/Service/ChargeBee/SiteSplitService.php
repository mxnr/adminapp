<?php

namespace AdminBundle\Service\ChargeBee;

use InvalidArgumentException;
use Symfony\Component\HttpKernel\KernelInterface;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\Order;

/**
 * Class SiteSplitService
 */
class SiteSplitService
{
    const MULTI_WEBSITE_LANDING_PAGE = 'multi_website_landing_page';

    const MULTI_WEBSITE_LANDING_TITLE = 'multi_website_landing_title';

    const PRICE_TYPE_BUY = 'price_type_buy';

    const PRICE_TYPE_SALE = 'price_type_sale';

    const PRICE_TYPE_REPAIR = 'price_type_repair';

    const THEME_BUY = 'theme_buy';

    const THEME_SALE = 'theme_sale';

    const THEME_REPAIR = 'theme_repair';

    const TITLE_DESCRIPTION_BUY = 'title_description_buy';

    const TITLE_DESCRIPTION_SALE = 'title_description_sale';

    const TITLE_DESCRIPTION_REPAIR = 'title_description_repair';

    const SLIDER_DISPLAY_BUY = 'slider_display_buy';

    const SLIDER_DISPLAY_SALE = 'slider_display_sale';

    const SLIDER_DISPLAY_REPAIR = 'slider_display_repair';

    const OFFERS = 'offers';

    const SOCIAL = 'social';

    const SEO_BUY = 'seo_buy';

    const SEO_SALE = 'seo_sale';

    const SEO_REPAIR = 'seo_repair';

    const INVENTORY = 'inventory';

    const INVENTORY_WEBSITE_LISTENING_FORM = 'inventory_website_listening_form';

    const ORDER_BUY = 'order_buy';

    const ORDER_SALE = 'order_sale';

    const ORDER_REPAIR = 'order_repair';

    const USERS = 'users';

    const CALCULATOR = 'calculator';

    const AFFILIATE_PAGE = 'affiliate_page';

    const QUALITY_CHECKS_QUICK_LINKS = 'quality_checks_quick_links';

    const DASHBOARD = 'dashboard';

    const STATISTICS_BUY_INVENTORY = 'statistics_buy_inventory';

    const STATISTICS_SALE = 'statistics_sale';

    const STATISTICS_REPAIR = 'statistics_repair';

    const WEBSITE_SALE = 'website_sale';

    const WEBSITE_BUY = 'website_buy';

    const WEBSITE_REPAIR = 'website_repair';


    const RESTRICTIONS = [
        self::MULTI_WEBSITE_LANDING_PAGE,
        self::MULTI_WEBSITE_LANDING_TITLE,
        self::PRICE_TYPE_BUY,
        self::PRICE_TYPE_SALE,
        self::PRICE_TYPE_REPAIR,
        self::THEME_BUY,
        self::THEME_SALE,
        self::THEME_REPAIR,
        self::TITLE_DESCRIPTION_BUY,
        self::TITLE_DESCRIPTION_SALE,
        self::TITLE_DESCRIPTION_REPAIR,
        self::SLIDER_DISPLAY_BUY,
        self::SLIDER_DISPLAY_SALE,
        self::SLIDER_DISPLAY_REPAIR,
        self::OFFERS,
        self::SOCIAL,
        self::SEO_BUY,
        self::SEO_SALE,
        self::SEO_REPAIR,
        self::INVENTORY,
        self::INVENTORY_WEBSITE_LISTENING_FORM,
        self::ORDER_BUY,
        self::ORDER_SALE,
        self::ORDER_REPAIR,
        self::USERS,
        self::CALCULATOR,
        self::AFFILIATE_PAGE,
        self::QUALITY_CHECKS_QUICK_LINKS,
        self::DASHBOARD,
        self::STATISTICS_BUY_INVENTORY,
        self::STATISTICS_SALE,
        self::STATISTICS_REPAIR,
    ];

    const ORDER_TYPE_CONNECTOR = [
        Order::SALE => self::ORDER_SALE,
        Order::BUYBACK => self::ORDER_BUY,
        Order::REPAIR => self::ORDER_REPAIR,
    ];

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var array
     */
    private $rules;

    /**
     * SiteSplitService constructor.
     *
     * @param SubscriptionService $subscriptionService
     * @param KernelInterface     $kernel
     * @param array               $rules
     */
    public function __construct(SubscriptionService $subscriptionService, KernelInterface $kernel, array $rules)
    {
        $this->subscriptionService = $subscriptionService;

        $this->rules = $rules;
    }

    /**
     * @param string $restriction
     *
     * @return bool
     */
    public function isAccessible(string $restriction): bool
    {
        $restriction = strtolower($restriction);

        if (in_array($restriction, static::RESTRICTIONS) === false) {
            throw new InvalidArgumentException('unknown restriction: <![CDATA[' . $restriction . ']]>');
        }

        if (isset($this->rules[$restriction]) === false) {
            throw new \RuntimeException('missed restriction in config file: <![CDATA[' . $restriction . ']]>');
        }

        $rulesSet = $this->rules[$restriction];

        foreach ($rulesSet as $rule) {
            $ruleResult = true;
            foreach ($rule as $subscribedRestriction => $expectedSubscriptionStatus) {
                if ($expectedSubscriptionStatus !== $this->getNamedRestrictionValue($subscribedRestriction)) {
                    $ruleResult = false;
                    continue;
                }
            }

            if ($ruleResult === true) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    protected function getNamedRestrictionValue(string $name): bool
    {
        $name = strtolower($name);
        if ($name === 'buy') {
            return $this->subscriptionService->isBuySubscribed();
        }

        if ($name === 'sale') {
            return $this->subscriptionService->isSellSubscribed();
        }

        if ($name === 'repair') {
            return $this->subscriptionService->isRepairSubscribed();
        }

        throw new InvalidArgumentException('unknown named restriction <![CDATA[' . $name . ']]>');
    }
}
