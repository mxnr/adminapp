<?php

namespace AdminBundle\Service\ChargeBee;

use ChargeBeeBundle\Packet\Embed\Subscription as SubscriptionEmbed;
use ChargeBeeBundle\Packet\HostedPages\CheckoutExisting;
use ChargeBeeBundle\Packet\HostedPages\Embed\Customer as CustomerEmbed;
use ChargeBeeBundle\Packet\HostedPages\UpdatePaymentMethod;
use ChargeBeeBundle\Service\HostedPagesService;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Router;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Billing\HostedPage;
use ThreeWebOneEntityBundle\Entity\Billing\HostedPageInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Entity\Billing\SubscriptionInterface;
use ThreeWebOneEntityBundle\Repository\Billing\HostedPageRepository;
use Zend\Code\Exception\RuntimeException;

/**
 * Class HostedPageService
 * @package AdminBundle\Service\ChargeBee
 */
class HostedPageService
{
    const TYPE_CONVERSION_MAP = [
        'checkout_existing' => HostedPageInterface::TYPE_CHECKOUT_SUBSCRIPTION,
        'checkout_new' => HostedPageInterface::TYPE_CHECKOUT_NEW,
        'update_payment_method' => HostedPageInterface::TYPE_UPDATE_PAYMENT_METHOD,
        'manage_payment_sources' => HostedPageInterface::TYPE_MANAGE_PAYMENT_SOURCES,
        'collect_now' => HostedPageInterface::TYPE_COLLECT_NOW,
    ];

    const STATE_CONVERSION_MAP = [
        'created' => HostedPageInterface::STATE_CREATED,
        'requested' => HostedPageInterface::STATE_REQUESTED,
        'succeeded' => HostedPageInterface::STATE_SUCCEEDED,
        'cancelled' => HostedPageInterface::STATE_CANCELLED,
        'acknowledged' => HostedPageInterface::STATE_ACKNOWLEDGED,
    ];

    /**
     * @var array
     */
    private $pagesList = [];

    /**
     * @var HostedPagesService
     */
    private $hostedPagesService;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Router
     */
    private $router;

    /**
     * HostedPageService constructor.
     *
     * @param HostedPagesService     $hostedPagesService
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface     $validator
     * @param RequestStack           $requestStack
     * @param Router                 $router
     */
    public function __construct(
        HostedPagesService $hostedPagesService,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        RequestStack $requestStack,
        Router $router
    ) {
        $this
            ->setHostedPagesService($hostedPagesService)
            ->setEntityManager($entityManager)
            ->setValidator($validator)
            ->setRequest($requestStack->getMasterRequest())
            ->setRouter($router);
    }

    /**
     * @param Subscription $subscription
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkOut(Subscription $subscription): array
    {
        if (!isset($this->pagesList[__METHOD__])) {
            $this->pagesList[__METHOD__] = [];
        }

        if ($this->hasHostedPagesService() && $this->hasEntityManager()) {
            $service = $this->getHostedPagesService();

            $subscriptionId = $subscription->getId();

            if (!array_key_exists($subscriptionId, $this->pagesList[__METHOD__])) {
                $this->pagesList[__METHOD__][$subscriptionId] = ['url' => ''];

                $em = $this->getEntityManager();
                /** @var HostedPageRepository $hostedPageRepository */
                $hostedPageRepository = $em->getRepository(HostedPage::class);

                /** @var HostedPage|null $hostedPage */
                $hostedPage = $hostedPageRepository
                    ->getCheckoutSubscriptionQuery($subscription)
                    ->getQuery()->getOneOrNullResult();

                if ($hostedPage !== null && $hostedPage->isExpired()) {
                    //remove object if expired
                    $em->remove($hostedPage);
                    $hostedPage = null;
                }

                if ($hostedPage === null) {
                    $checkoutExistingMethod = (new CheckoutExisting())
                        ->setRedirectUrl($this->getReturnUrl())
                        ->setCancelUrl($this->getReturnUrl());
                    $subscriptionEmbed = new SubscriptionEmbed();

                    $subscriptionEmbed
                        ->setId($subscription->getChargeBeeId())
                        ->setTrialEnd(0);

                    if ($subscription->getStatus() === SubscriptionInterface::STATUS_CANCELLED) {
                        $checkoutExistingMethod->setReactivate(true);
                    }

                    $checkoutExistingMethod->setSubscription($subscriptionEmbed);

                    $hostedPage = $this
                        ->getObjectFromData($service->checkoutExisting($checkoutExistingMethod))
                        ->setCustomer($subscription->getCustomer())
                        ->setEntityId($subscriptionId);

                    $em->persist($hostedPage);
                    $em->flush();
                }

                $this->pagesList[__METHOD__][$subscriptionId] = $hostedPage->getPayload();
            }

            return $this->pagesList[__METHOD__][$subscriptionId];
        }

        throw new RuntimeException('hostedPagesService and entityManager is required to continue the process');
    }

    /**
     * @return HostedPagesService
     */
    public function getHostedPagesService(): HostedPagesService
    {
        return $this->hostedPagesService;
    }

    /**
     * @return bool
     */
    public function hasHostedPagesService(): bool
    {
        return $this->hostedPagesService !== null;
    }

    /**
     * @param HostedPagesService|null $hostedPagesService
     *
     * @return HostedPageService
     */
    public function setHostedPagesService(HostedPagesService $hostedPagesService = null): HostedPageService
    {
        $this->hostedPagesService = $hostedPagesService;

        return $this;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return bool
     */
    public function hasEntityManager(): bool
    {
        return $this->entityManager !== null;
    }

    /**
     * @param EntityManagerInterface|null $entityManager
     *
     * @return HostedPageService
     */
    public function setEntityManager(EntityManagerInterface $entityManager = null): HostedPageService
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @return string
     */
    public function getReturnUrl(): string
    {
        if ($this->hasRequest() === false) {
            return '';
        }
        $request = $this->getRequest();

        return $request->getSchemeAndHttpHost() . $this->getRouter()->generate('admin_billing_chargebee_callback');
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return bool
     */
    public function hasRequest(): bool
    {
        return $this->request !== null;
    }

    /**
     * @param Request|null $request
     *
     * @return HostedPageService
     */
    public function setRequest(Request $request = null): HostedPageService
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @return Router
     */
    public function getRouter(): Router
    {
        return $this->router;
    }

    /**
     * @return bool
     */
    public function hasRouter(): bool
    {
        return $this->router !== null;
    }

    /**
     * @param Router|null $router
     *
     * @return HostedPageService
     */
    public function setRouter(Router $router = null): HostedPageService
    {
        $this->router = $router;

        return $this;
    }

    /**
     * @param array           $data
     * @param HostedPage|null $hostedPage
     *
     * @return HostedPage
     */
    public function getObjectFromData(array $data, HostedPage $hostedPage = null): HostedPage
    {
        if ($hostedPage === null) {
            $hostedPage = new HostedPage();
        }

        if (isset(self::TYPE_CONVERSION_MAP[$data['type']])) {
            $pageType = self::TYPE_CONVERSION_MAP[$data['type']];
        } else {
            throw new InvalidArgumentException('unknown type: <![CDATA[' . $data['type'] . ']]>');
        }

        if (isset(self::STATE_CONVERSION_MAP[$data['state']])) {
            $pageState = self::STATE_CONVERSION_MAP[$data['state']];
        } else {
            throw new InvalidArgumentException('unknown state: <![CDATA[' . $data['state'] . ']]>');
        }

        $hostedPage
            ->setChargeBeeId($data['id'])
            ->setType($pageType)
            ->setPayload($data)
            ->setState($pageState)
            ->setCreatedAt((new DateTime())->setTimestamp($data['created_at']))
            ->setExpiresAt((new DateTime())->setTimestamp($data['expires_at']));

        $constraintViolations = $this->getValidator()->validate($hostedPage);
        if ($constraintViolations->count() > 0) {
            throw new ValidatorException((string) $constraintViolations);
        }

        return $hostedPage;
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @return bool
     */
    public function hasValidator(): bool
    {
        return $this->validator !== null;
    }

    /**
     * @param ValidatorInterface|null $validator
     *
     * @return HostedPageService
     */
    public function setValidator(ValidatorInterface $validator = null): HostedPageService
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * @param Customer $customer
     *
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function paymentMethod(Customer $customer): array
    {
        if (!isset($this->pagesList[__METHOD__])) {
            $this->pagesList[__METHOD__] = [];
        }

        if ($this->hasEntityManager() && $this->hasHostedPagesService()) {
            $customerId = $customer->getId();
            if (!array_key_exists($customerId, $this->pagesList[__METHOD__])) {
                $em = $this->getEntityManager();
                /** @var HostedPageRepository $hostedPageRepository */
                $hostedPageRepository = $em->getRepository(HostedPage::class);

                /** @var null|HostedPage $hostedPage */
                $hostedPage = $hostedPageRepository
                    ->getUpdatePaymentMethodQuery($customer)
                    ->getQuery()
                    ->getOneOrNullResult();

                if ($hostedPage !== null && $hostedPage->isExpired()) {
                    $em->remove($hostedPage);
                    $hostedPage = null;
                }

                if ($hostedPage === null) {
                    $updatePaymentMethod = (new UpdatePaymentMethod())
                        ->setRedirectUrl($this->getReturnUrl())
                        ->setCancelUrl($this->getReturnUrl());
                    $customerEmbed = new CustomerEmbed();
                    $customerEmbed->setId($customer->getChargeBeeId());
                    $updatePaymentMethod->setCustomer($customerEmbed);
                    $hostedPage = $this
                        ->getObjectFromData(
                            $this->getHostedPagesService()->updatePaymentMethod($updatePaymentMethod)['hostedPage']
                        )
                        ->setCustomer($customer);

                    $em->persist($hostedPage);
                    $em->flush();
                }

                $this->pagesList[__METHOD__][$customerId] = $hostedPage->getPayload();
            }

            return $this->pagesList[__METHOD__][$customerId];
        }

        throw new RuntimeException('hostedPagesService and entityManager is required to continue the process');
    }
}
