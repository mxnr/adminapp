<?php

namespace AdminBundle\Service\ChargeBee;

use ChargeBeeBundle\Packet\Plans\ListAll;
use ChargeBeeBundle\Service\PlansService;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Plan;
use ThreeWebOneEntityBundle\Entity\Billing\PlanInterface;

/**
 * Class PlanService.
 */
class PlanService
{
    const TRIAL_PERIOD_INTERVAL_UNIT_CONVERSION_MAP = [
        'month' => PlanInterface::PERIOD_MONTH,
        'day' => PlanInterface::PERIOD_DAY,
    ];

    const CHARGE_INTERVAL_UNIT_CONVERSION_MAP = [
        'month' => PlanInterface::PERIOD_MONTH,
        'year' => PlanInterface::PERIOD_YEAR,
    ];

    /**
     * @var PlansService
     */
    private $plansService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * PlanService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param PlansService           $plansService
     * @param ValidatorInterface     $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        PlansService $plansService,
        ValidatorInterface $validator
    ) {
        $this
            ->setEntityManager($entityManager)
            ->setPlansService($plansService)
            ->setValidator($validator);
    }

    /**
     * @return void
     */
    public function synchronize()
    {
        $plansService = $this->getPlansService();
        $em = $this->getEntityManager();

        /** @var \ThreeWebOneEntityBundle\Repository\Billing\PlanRepository $plansRepo */
        $plansRepo = $em->getRepository(Plan::class);

        $listAllPlansMethod = new ListAll();
        $listAllPlansMethod->setLimit(10);

        $plans = [];

        $keepFetching = true;
        while ($keepFetching) {
            $plansPart = $plansService->listAll($listAllPlansMethod);

            if (!empty($plansPart['nextOffset'])) {
                $listAllPlansMethod->setOffset($plansPart['nextOffset']);
            } else {
                $keepFetching = false;
            }

            foreach ($plansPart['list'] as $partPlan) {
                $plans[$partPlan['id']] = $partPlan;
            }
        }

        $internalPlans = $plansRepo->getAll();

        /** @var Plan $internalPlan */
        foreach ($internalPlans as $internalPlan) {
            $internalPlan = $internalPlan[0];
            $internalPlanCBId = $internalPlan->getChargeBeeId();
            if (!isset($plans[$internalPlanCBId])) {
                $internalPlan->setActive(false);
            } else {
                $this->getObjectFromData($plans[$internalPlanCBId], $internalPlan);
                unset($plans[$internalPlanCBId]);
            }

            $em->persist($internalPlan);
        }
        $em->flush();

        foreach ($plans as $plan) {
            $em->persist($this->getObjectFromData($plan));
        }

        $em->flush();
    }

    /**
     * @return PlansService
     */
    public function getPlansService(): PlansService
    {
        return $this->plansService;
    }

    /**
     * @return bool
     */
    public function hasPlansService(): bool
    {
        return $this->plansService !== null;
    }

    /**
     * @param PlansService|null $plansService
     *
     * @return PlanService
     */
    public function setPlansService(PlansService $plansService = null): PlanService
    {
        $this->plansService = $plansService;

        return $this;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return bool
     */
    public function hasEntityManager(): bool
    {
        return $this->entityManager !== null;
    }

    /**
     * @param EntityManagerInterface|null $entityManager
     *
     * @return PlanService
     */
    public function setEntityManager(EntityManagerInterface $entityManager = null): PlanService
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @param array     $planData
     * @param Plan|null $plan
     *
     * @return Plan
     */
    public function getObjectFromData(array $planData, Plan $plan = null): Plan
    {
        if ($plan === null) {
            $plan = new Plan();
        }

        if (isset(self::CHARGE_INTERVAL_UNIT_CONVERSION_MAP[$planData['period_unit']])) {
            $chargeIntervalUnit = self::CHARGE_INTERVAL_UNIT_CONVERSION_MAP[$planData['period_unit']];
        } else {
            throw new InvalidArgumentException('unknown period unit: <![CDATA[' . $planData['period_unit'] . ']]>');
        }

        $plan
            ->setChargeBeeId($planData['id'])
            ->setName($planData['name'] ?? '')
            ->setDescription($planData['description'] ?? '')
            ->setPrice($planData['price'])
            ->setCurrencyCode($planData['currency_code'])
            ->setActive('active' == $planData['status'])
            ->setChargeInterval($planData['period'])
            ->setChargeIntervalUnit($chargeIntervalUnit)
            ->setSetupFee($planData['setup_cost'] ?? 0)
            ->setMetaData($planData['meta_data'] ?? [])
            ->setHasTrial(false);

        if (isset($planData['trial_period'])) {
            if ($planData['trial_period'] > 0) {
                if (isset(self::TRIAL_PERIOD_INTERVAL_UNIT_CONVERSION_MAP[$planData['trial_period_unit']])) {
                    $trialPeriodIntervalUnit =
                        self::TRIAL_PERIOD_INTERVAL_UNIT_CONVERSION_MAP[$planData['trial_period_unit']];
                } else {
                    throw new InvalidArgumentException(
                        'unknown trial_period_unit: <![CDATA[' . $planData['trial_period_unit'] . ']]>'
                    );
                }

                $plan
                    ->setHasTrial(true)
                    ->setTrialInterval($planData['trial_period'])
                    ->setTrialIntervalUnit($trialPeriodIntervalUnit);
            }
        }

        $constraintViolations = $this->getValidator()->validate($plan);
        if ($constraintViolations->count() > 0) {
            throw new ValidatorException((string) $constraintViolations);
        }

        return $plan;
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @return bool
     */
    public function hasValidator(): bool
    {
        return $this->validator !== null;
    }

    /**
     * @param ValidatorInterface|null $validator
     *
     * @return PlanService
     */
    public function setValidator(ValidatorInterface $validator = null): PlanService
    {
        $this->validator = $validator;

        return $this;
    }
}
