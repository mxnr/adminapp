<?php

namespace AdminBundle\Service;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\File\File;

class ImportService
{
    /**
     * @param File $file
     *
     * @return array
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function proceed(File $file)
    {
       $reader =IOFactory::createReaderForFile($file);
       $reader->setReadDataOnly(true);
       $spreadsheet = $reader->load($file);

       $worksheet = $spreadsheet->getActiveSheet();
       $result = [];
       foreach ($worksheet->getRowIterator(2) as $row) {
           $cellIterator = $row->getCellIterator('A', 'C');
           $cellIterator->setIterateOnlyExistingCells(FALSE);
           $resultRow = [];
           foreach ($cellIterator as $cell) {
               $resultRow[] = $cell->getValue();
           }
           $result[] = $resultRow;
       }

       return $result;
    }
}
