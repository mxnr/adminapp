<?php

namespace AdminBundle\Service;

use AdminBundle\Form\Type\UpgradeFormType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use RuntimeException;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use ThreeWebOneEntityBundle\Entity\Billing\Embed\Upgrade;
use ThreeWebOneEntityBundle\Entity\Billing\Plan;

/**
 * Class UpgradeService.
 */
class UpgradeService
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UpgradeService constructor.
     *
     * @param RequestStack           $request
     * @param FormFactory            $formFactory
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RequestStack $request, FormFactory $formFactory, EntityManagerInterface $entityManager)
    {
        $this
            ->setRequest($request->getCurrentRequest())
            ->setFormFactory($formFactory)
            ->setEntityManager($entityManager);
    }

    /**
     * @return Plan
     */
    public function getRequestedPlan(): Plan
    {
        $upgrade = $this->getUpgradeObject();

        $sitesRequested = $this->getRequestedSites();
        $requestedSitesCount = 0;
        foreach ($sitesRequested as $key => $value) {
            if ($value) {
                ++$requestedSitesCount;
            }
        }

        $availablePlans = $this->getAvailablePlansList();
        $periodUnit = $upgrade->getBillingPeriodUnit();

        /** @var Plan $plan */
        foreach ($availablePlans[$periodUnit] as $plan) {
            $plan = $plan[0];
            $metaData = $plan->getMetaData();

            if ($metaData['sitesCount'] == $requestedSitesCount) {
                return $plan;
            }
        }

        throw new RuntimeException('unable to get requested plan');
    }

    /**
     * @return Upgrade
     */
    private function getUpgradeObject(): Upgrade
    {
        $upgrade = new Upgrade();
        $upgradeForm = $this->getFormFactory()->create(UpgradeFormType::class, $upgrade);

        $upgradeForm->handleRequest($this->getRequest());

        return $upgrade;
    }

    /**
     * @return FormFactory
     */
    public function getFormFactory(): FormFactory
    {
        return $this->formFactory;
    }

    /**
     * @return bool
     */
    public function hasFormFactory(): bool
    {
        return !is_null($this->formFactory);
    }

    /**
     * @param FormFactory|null $formFactory
     *
     * @return UpgradeService
     */
    public function setFormFactory(FormFactory $formFactory = null): UpgradeService
    {
        $this->formFactory = $formFactory;

        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return bool
     */
    public function hasRequest(): bool
    {
        return !is_null($this->request);
    }

    /**
     * @param Request|null $request
     *
     * @return UpgradeService
     */
    public function setRequest(Request $request = null): UpgradeService
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @return array
     */
    public function getRequestedSites(): array
    {
        $upgrade = $this->getUpgradeObject();

        return [
            'sell' => $upgrade->isSellingSite(),
            'buy' => $upgrade->isBuyingSite(),
            'repair' => $upgrade->isRepairingSite(),
        ];
    }

    /**
     * Returns plans list.
     *
     * @return array
     */
    public function getAvailablePlansList(): array
    {
        /** @var \ThreeWebOneEntityBundle\Repository\Billing\PlanRepository $plansRepo */
        $plansRepo = $this->getEntityManager()->getRepository(Plan::class);

        $activePlansIterator = $plansRepo->getActive();

        return $this->processIterator($activePlansIterator);
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return bool
     */
    public function hasEntityManager(): bool
    {
        return !is_null($this->entityManager);
    }

    /**
     * @param EntityManagerInterface|null $entityManager
     *
     * @return UpgradeService
     */
    public function setEntityManager(EntityManagerInterface $entityManager = null): UpgradeService
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @param IterableResult $iterableResult
     *
     * @return array
     */
    private function processIterator(IterableResult $iterableResult): array
    {
        $plans = [Plan::PERIOD_YEAR => [], Plan::PERIOD_MONTH => []];
        /** @var Plan[] $item */
        foreach ($iterableResult as $item) {
            $item = $item[0];

            if ($item->hasMetaData()) {
                $itemMetaData = $item->getMetaData();
                if (isset($itemMetaData['sitesCount']) && is_int($itemMetaData['sitesCount'])) {
                    $plans[$item->getChargeIntervalUnit()][$itemMetaData['sitesCount']][] = $item;
                }
            }
        }

        foreach ($plans as $period => $periodPlans) {
            ksort($plans[$period]);
        }

        return $plans;
    }

    /**
     * @return array
     */
    public function getAvailablePlansWithTrialList(): array
    {
        /** @var \ThreeWebOneEntityBundle\Repository\Billing\PlanRepository $plansRepo */
        $plansRepo = $this->getEntityManager()->getRepository(Plan::class);

        $activePlansIterator = $plansRepo->getActiveWithTrial();

        return $this->processIterator($activePlansIterator);
    }
}
