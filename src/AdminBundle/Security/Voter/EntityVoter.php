<?php

namespace AdminBundle\Security\Voter;

use AdminBundle\Service\UserGroupsService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use ThreeWebOneEntityBundle\Entity\Helper\OwnerInterface;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class EventVoter
 * @package AdminBundle\Security\Voter
 */
class EntityVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var UserGroupsService
     */
    private $userGroupService;

    /**
     * EntityVoter constructor.
     * @param EntityManager $em
     * @param UserGroupsService $userGroupsService
     */
    public function __construct(EntityManager $em, UserGroupsService $userGroupsService)
    {
        $this->em = $em;
        $this->userGroupService = $userGroupsService;
    }

    /**
     * @param string $attribute
     * @param mixed  $subject
     *
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!$subject instanceof OwnerInterface) {
            return false;
        }

        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE])) {
            return false;
        }

        return true;
    }

    /**
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $this->userGroupService->getUserOrUserSupervisor($token->getUser());

        switch ($attribute) {
            case self::VIEW:
                return $this->canViewEntity($subject, $user);
                break;
            case self::EDIT:
            case self::DELETE:
                return $this->canEditEntity($subject, $user);
                break;
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param OwnerInterface $subject
     * @param User           $user
     *
     * @return bool
     */
    private function canEditEntity(OwnerInterface $subject, $user)
    {
        return $subject->getOwner()->getId() === $user->getId();
    }

    /**
     * @param OwnerInterface $subject
     * @param User           $user
     *
     * @return bool
     */
    private function canViewEntity(OwnerInterface $subject, $user)
    {
        $admin = $this->em->getRepository(User::class)->findOneBy(['username' => 'admin']);

        return in_array($subject->getOwner()->getId(), [$user->getId(), $admin->getId()]);
    }
}
