<?php

namespace AdminBundle\Security\Voter;

use AdminBundle\Service\ChargeBee\SiteSplitService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class SiteSplitVoter.
 */
class SiteSplitVoter extends Voter
{
    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (in_array($attribute, SiteSplitService::RESTRICTIONS)) {
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        return $subject->isAccessible($attribute);
    }
}
