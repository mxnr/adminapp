<?php

namespace AdminBundle\Security\Voter;

use AdminBundle\Admin\BaseAdmin;
use AdminBundle\Admin\Billing\AbstractBillingAdmin;
use AdminBundle\Service\ChargeBee\SubscriptionService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class SubscriptionVoter.
 */
class SubscriptionVoter extends Voter
{
    /**
     * @var SubscriptionService
     */
    protected $subscriptionService;

    /**
     * SubscriptionVoter constructor.
     *
     * @param SubscriptionService $subscriptionService
     */
    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        //Always allow access to the billing subsystem
        if ($subject instanceof AbstractBillingAdmin) {
            return true;
        }

        //restrict access while subscriptions service is not provided or user is not subscriber;
        if (false === $this->hasSubscriptionService() || false === $this->getSubscriptionService()->isSubscriber()) {
            return false;
        }

        //allow
        return true;
    }

    /**
     * @return SubscriptionService
     */
    public function getSubscriptionService(): SubscriptionService
    {
        return $this->subscriptionService;
    }

    /**
     * @return bool
     */
    public function hasSubscriptionService(): bool
    {
        return !empty($this->subscriptionService);
    }

    /**
     * @param SubscriptionService $subscriptionService
     *
     * @return SubscriptionVoter
     */
    public function setSubscriptionService(SubscriptionService $subscriptionService): self
    {
        $this->subscriptionService = $subscriptionService;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if ($subject instanceof BaseAdmin) {
            return true;
        }

        return false;
    }
}
