<?php

namespace AdminBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;

class SubscriptionRestrictVoter extends Voter
{
    // Types of restriction
    const DOMAIN_RESTRICT = 'domain_restrict';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if ($attribute !== self::DOMAIN_RESTRICT) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // the user must be logged in; if not, deny access
        if (!$user instanceof User || $user->hasRole('ROLE_SUPER_ADMIN')) {
            return false;
        }

        $subscription = $user->getBillingCustomer()->getActiveSubscription();

        // the user must have subscription; if not, deny access
        if (!$subscription instanceof Subscription) {
            return false;
        }

        if ($attribute === self::DOMAIN_RESTRICT) {
            return $this->canUseDomainService($subscription);
        }

        //allow
        return true;
    }

    /**
     * Can Use Domain Service
     *
     * @param Subscription $subscription
     * @return bool
     */
    private function canUseDomainService(Subscription $subscription)
    {
        return $subscription->getStatus() === Subscription::STATUS_ACTIVE;
    }
}
