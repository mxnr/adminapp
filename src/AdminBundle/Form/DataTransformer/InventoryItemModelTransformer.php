<?php

namespace AdminBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Model;

class InventoryItemModelTransformer implements DataTransformerInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  InventoryItem|null $inventoryItem
     * @return string
     */
    public function transform($inventoryItem)
    {
        if (!$inventoryItem instanceof InventoryItem) {
            return '';
        }

        $model = $inventoryItem->getModel();
        if ($model) {
            return json_encode(['id' => $model->getId(), 'name' => (string) $model]);
        }

        $model = $inventoryItem->getPrice() ? $inventoryItem->getPrice()->getModel() : null;
        if ($model) {
            return json_encode(['id' => $model->getId(), 'name' => (string) $model]);
        }

        if ($inventoryItem->getTitle()) {
            return json_encode(['id' => -1, 'name' => $inventoryItem->getTitle()]);
        }

        return '';
    }

    /**
     * @param  string $jsonData
     * @return Model|string
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($jsonData)
    {
        if (!$jsonData) {
            return '';
        }

        $data = json_decode($jsonData, true);

        if ($data['id'] != -1) {

            $model = $this->em
                ->getRepository(Model::class)
                // query for the issue with this id
                ->find($data['id']);

            if (null === $model) {
                throw new TransformationFailedException(sprintf(
                    'A barcode with number "%s" does not exist!',
                    $model
                ));
            }

            return $model;
        } else {

            return $data['name'];
        }
    }
}
