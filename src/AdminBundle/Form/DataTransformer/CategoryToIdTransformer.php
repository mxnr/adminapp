<?php

namespace AdminBundle\Form\DataTransformer;

use ThreeWebOneEntityBundle\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CategoryToIdTransformer implements DataTransformerInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  Category|null $category
     * @return string
     */
    public function transform($category)
    {
        if (null == $category) {
            return '';
        }

        return $category->getId();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $categoryId
     * @return Category|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($categoryId)
    {
        // no issue number? It's optional, so that's ok
        if (!$categoryId) {
            return;
        }

        $category = $this->em
            ->getRepository(Category::class)
            // query for the issue with this id
            ->find($categoryId)
        ;

        if (null === $category) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A category with number "%s" does not exist!',
                $categoryId
            ));
        }

        return $category;
    }
}
