<?php

namespace AdminBundle\Form\DataTransformer;

use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CustomerToIdTransformer implements DataTransformerInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  Customer|null $customer
     * @return string
     */
    public function transform($customer)
    {
        if (null == $customer) {
            return '';
        }

        return $customer->getId();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $customerId
     * @return Customer|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($customerId)
    {
        // no issue number? It's optional, so that's ok
        if (!$customerId) {
            return;
        }

        $customer = $this->em
            ->getRepository(Customer::class)
            // query for the issue with this id
            ->find($customerId)
        ;

        if (null === $customer) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A customer with number "%s" does not exist!',
                $customerId
            ));
        }

        return $customer;
    }
}
