<?php

namespace AdminBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;

class BarcodeToIdTransformer implements DataTransformerInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  InventoryItemBarcode|null $barcode
     * @return string
     */
    public function transform($barcode)
    {
        if (null == $barcode) {
            return '';
        }

        return $barcode->getId();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $barcodeId
     * @return InventoryItemBarcode|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($barcodeId)
    {
        // no issue number? It's optional, so that's ok
        if (!$barcodeId) {
            return;
        }

        $barcode = $this->em
            ->getRepository(InventoryItemBarcode::class)
            // query for the issue with this id
            ->find($barcodeId)
        ;

        if (null === $barcode) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'A barcode with number "%s" does not exist!',
                $barcodeId
            ));
        }

        return $barcode;
    }
}
