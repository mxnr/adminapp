<?php

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Valid;
use ThreeWebOneEntityBundle\Entity\Billing\Plan;

/**
 * {@inheritDoc}
 */
class UpgradeFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sellingSite', CheckboxType::class, ['label' => false, 'required' => false])
            ->add('buyingSite', CheckboxType::class, ['label' => false, 'required' => false])
            ->add('repairingSite', CheckboxType::class, ['label' => false, 'required' => false])
            ->add(
                'billingPeriodUnit',
                ChoiceType::class,
                [
                    'choices' => [
                        'Monthly' => Plan::PERIOD_MONTH,
                        'Yearly' => Plan::PERIOD_YEAR,
                    ],
                    'multiple' => false,
                    'expanded' => true,
                ]
            )
            ->add(
                'user',
                RegistrationFormType::class,
                [
                    'constraints' => [
                        new Valid(),
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_user_upgrade';
    }
}
