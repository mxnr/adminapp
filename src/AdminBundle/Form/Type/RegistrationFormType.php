<?php

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Class RegistrationFormType.
 */
class RegistrationFormType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'csrf_protection' => false,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'firstName',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new Regex(['pattern' => '/^[A-z]+$|^[A-z]+\s[A-z]+$/', 'message' => 'Only letters allowed.']),
                        new Length([
                            'min' => 2,
                        ])
                    ],
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'required' => true,
                    'constraints' => [
                        new Regex(['pattern' => '/^[A-z]+$|^[A-z]+\s[A-z]+$/', 'message' => 'Only letters allowed.']),
                        new Length([
                            'min' => 2,
                        ])
                    ],
                ]
            )
            ->add(
                'email',
                EmailType::class
            )
            ->add(
                'plainPassword',
                PasswordType::class
            )
            ->add(
                'storeName',
                TextType::class,
                [
                    'constraints' => [
                        new Regex([
                            'pattern' => '/^([0-9]|[A-z]|\-)+$/',
                            'message' => 'Allowed letters numbers and "-".'
                        ]),
                        new Length([
                            'min' => 2,
                        ])
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_user_registration';
    }
}
