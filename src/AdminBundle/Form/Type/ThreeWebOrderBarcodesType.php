<?php

namespace AdminBundle\Form\Type;

use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Model\ModelManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;

class ThreeWebOrderBarcodesType extends ModelType
{
    /**
     * ThreeWebOrderBarcodesType constructor.
     * @param \Symfony\Component\PropertyAccess\PropertyAccessorInterface $pa
     */
    public function __construct($pa)
    {
        parent::__construct($pa);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['btn_add'] = $options['btn_add'];
        $view->vars['btn_list'] = $options['btn_list'];
        $view->vars['btn_delete'] = $options['btn_delete'];
        $view->vars['btn_catalogue'] = $options['btn_catalogue'];
        $view->vars['items'] = $this->buildItems(
            $options['model_manager'],
            $options['query'],
            $options['asked_quantity']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'three_web_order_barcodes_type';
    }

    /**
     * @param ModelManagerInterface $modelManager
     * @param $query
     *
     * @return array
     */
    private function buildItems(ModelManagerInterface $modelManager, $query, $askedQuantity)
    {
        $composite = [];
        $inventoryItems = [];
        /** @var InventoryItemBarcode[] $barcodes */
        $barcodes = $modelManager->executeQuery($query);
        foreach ($barcodes as $barcode) {
            $inventoryItem = $barcode->getInventoryItem();
            $composite[$inventoryItem->getId()][] = $barcode->getId();
            $inventoryItems[$inventoryItem->getId()] = [
              'title' => $inventoryItem->getTitle(),
              'model' => $inventoryItem->getModel(),
              'askedQuantity' => $askedQuantity[$inventoryItem->getId()] ?? null,
            ];
        }

        return [
            'composite' => $composite,
            'inventoryItems' => $inventoryItems
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('asked_quantity', null);
    }
}
