<?php

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ImageOnlyType extends AbstractType
{
    public function getParent()
    {
        return VichImageType::class;
    }
}
