<?php

namespace AdminBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EbayTemplatesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('templateType', ChoiceType::class, [
                'choices'  => array(
                    'Shop Model' => 1,
                ),
            ])
            ->add('inventoryItem', EntityType::class, [
                'class' => InventoryItem::class,
                'choices' => $options['inventoryItems'],
                'choice_label' => 'title',
                'constraints' => [
                    new NotBlank(),
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inventoryItems' => null
        ]);
    }
}
