<?php

namespace AdminBundle\Form\Type;

use AdminBundle\Form\DataTransformer\InventoryItemModelTransformer;
use AdminBundle\Service\UserGroupsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\PriceType as PriceTypeEntity;

/**
 * Class InventoryItemType.
 */
class InventoryItemType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserGroupsService
     */
    private $userService;

    /**
     * InventoryItemType constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserGroupsService $userGroupsService
     */
    public function __construct(EntityManagerInterface $entityManager, UserGroupsService $userGroupsService)
    {
        $this->entityManager = $entityManager;
        $this->userService = $userGroupsService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fullForm = false;
        if (!empty($options['order_site_type'])) {
            $priceTypeQuery = $this->entityManager
                ->getRepository(PriceTypeEntity::class)
                ->getUserPriceTypesQueryBySiteType(
                    $this->userService->getUserOrUserSupervisor(),
                    $options['order_site_type']
                );
            $fullForm = true;
        }

        $builder
            ->add(
                'searchModel',
                ThreeWebModelType::class,
                [
                    'required' => false,
                    'mapped' => false,
                    'model_var' => 'vmRow.modelSelected',
                    'search_function' => 'vmRow.getModels()',
                    'label' => false,
                    'inline' => false,
                    'refresh_function' => 'vmRow.refreshResults($select)',
                    'attr' => [
                        'ng-model' => 'vmRow.modelSelectedJson',
                        'class' => 'hidden-field'
                    ]
                ]
            )
            ->add(
                'purchasePrice',
                MoneyType::class,
                [
                    'divisor' => 100,
                    'label' => false,
                    'currency' => false,
                    'constraints' => new GreaterThan(['value' => 0]),
                    'attr' => [
                        'class' => 'form-control',
                        'ng-model' => 'vmRow.price',
                        'ng-change' => 'vmRow.priceChange()',
                        'ng-blur' => 'vmRow.formatPrice()',
                    ]
                ]
            );
        if ($fullForm) {
            $builder
                ->add(
                    'priceType',
                    EntityType::class,
                    [
                        'class' => PriceTypeEntity::class,
                        'label' => false,
                        'multiple' => false,
                        'expanded' => false,
                        'query_builder' => $priceTypeQuery,
                        'choice_label' => 'title',
                        'attr' => [
                            'class' => 'form-control price-type-select',
                            'data-sonata-select2' => 'false'
                        ]
                    ]
                );
        }
        $builder
            ->add(
                'quantity',
                IntegerType::class,
                [
                    'constraints' => new GreaterThan(['value' => 0]),
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'ng-model' => 'vmRow.quantity'
                    ]
                ]
            );
        if ($fullForm) {
            $builder
                ->add(
                    'total',
                    TextType::class,
                    [
                        'required' => false,
                        'mapped' => false,
                        'label' => false,
                        'attr' => [
                            'class' => 'form-control',
                            'readonly' => 'readonly',
                            'ng-model' => 'vmRow.total'
                        ]
                    ]
                )
                ->add(
                    'description',
                    TextareaType::class,
                    [
                        'required' => false,
                        'mapped' => false,
                        'attr' => [
                            'class' => 'form-control'
                        ]
                    ]
                )
                ->add(
                    'adminNote',
                    TextareaType::class,
                    [
                        'required' => false,
                        'mapped' => false,
                        'attr' => [
                            'class' => 'form-control'
                        ]
                    ]
                );
        }

        $inventoryItemModelTransformer = new InventoryItemModelTransformer($this->entityManager);
        $builder->get('searchModel')->addModelTransformer($inventoryItemModelTransformer);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            /** @var InventoryItem $item */
            $item = $event->getData();
            if (null != $item) {
                $form = $event->getForm();
                $orderItem = $item->getOrderItem();
                $form->get('searchModel')->setData($item);
                if (null != $orderItem && $form->has('adminNote') && $form->has('description')) {
                    $form->get('adminNote')->setData($orderItem->getAdminNote());
                    $form->get('description')->setData($orderItem->getDescription());
                }
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => InventoryItem::class,
            'order_site_type' => null
        ]);
    }
}
