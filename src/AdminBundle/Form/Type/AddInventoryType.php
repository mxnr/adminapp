<?php

namespace AdminBundle\Form\Type;

use AdminBundle\Form\DataTransformer\CustomerToIdTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Order\OrderDeliveryTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\OrderPaymentTypeInterface;

/**
 * Class OrderType.
 */
class AddInventoryType extends AbstractType
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * OrderType constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Order $order */
        $order = $builder->getData();

        $builder
            ->add(
                'customer',
                ThreeWebCustomerType::class,
                [
                    'required' => false,
                    'data' => $order->getCustomer(),
                    'data_class' => null,
                    'model_var' => 'vm.customerSelected',
                    'model_default_var' => 'vm.presetCustomer',
                    'search_function' => 'vm.getCustomers()',
                    'placeholder' => 'Add supplier to inventory',
                    'attr' => [
                        'ng-model' => 'vm.customerSelected',
                        'class' => 'hidden-field'
                    ]
                ]
            )
            ->add(
                'items',
                CollectionType::class,
                [
                    'entry_type' => InventoryItemType::class,
                    'entry_options' => [
                        'label' => false,
                    ],
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => false,
                ]
            )
            ->add(
                'signatureImage',
                SignatureCaptureType::class,
                [
                    'required' => false,
                    'mapped' => false
                ]
            );

        $customerToIdTransformer = new CustomerToIdTransformer($this->entityManager);
        $builder->get('customer')->addModelTransformer($customerToIdTransformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            'user' => null,
        ]);
    }
}
