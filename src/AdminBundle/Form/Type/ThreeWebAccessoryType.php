<?php

namespace AdminBundle\Form\Type;

use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Model\ModelManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use ThreeWebOneEntityBundle\Entity\Accessory;

/**
 * Class ThreeWebAccessoryType.
 */
class ThreeWebAccessoryType extends ModelType
{
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['btn_add'] = $options['btn_add'];
        $view->vars['btn_list'] = $options['btn_list'];
        $view->vars['btn_delete'] = $options['btn_delete'];
        $view->vars['btn_catalogue'] = $options['btn_catalogue'];
        $view->vars['composite'] = $this->buildComposite($options['model_manager'], $options['query']);
        $view->vars['items_disabled'] = $this->buildDisabled($options['model_manager'], $options['query']);
    }

    /**
     * Build associative array with parent to child accessories.
     *
     * @param ModelManagerInterface $modelManager
     * @param $query
     *
     * @return array
     */
    private function buildComposite(ModelManagerInterface $modelManager, $query)
    {
        $accessories = $modelManager->executeQuery($query);
        $composite = [];
        /** @var Accessory $accessory */
        foreach ($accessories as $accessory) {
            if ($accessory->getParent()) {
                $composite[$accessory->getParent()->getTitle()][] = $accessory->getId();
            }
        }

        return $composite;
    }

    /**
     * @param ModelManagerInterface $modelManager
     * @param $query
     *
     * @return array
     */
    private function buildDisabled(ModelManagerInterface $modelManager, $query)
    {
        $accessories = $modelManager->executeQuery($query);
        $disabled = [];
        /** @var Accessory $accessory */
        foreach ($accessories as $accessory) {
            if (Accessory::STATUS_INACTIVE === $accessory->getStatus()) {
                $disabled[] = $accessory->getId();
            }
        }

        return $disabled;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'three_web_accessories_type';
    }
}
