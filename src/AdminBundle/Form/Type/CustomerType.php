<?php

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;

/**
 * Class InventoryItemType.
 */
class CustomerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'company',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'firstname',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'email',
                EmailType::class
            )
            ->add(
                'businessPhone',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'phone',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'driverLicense',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'addressLine1',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'addressLine2',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'city',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'zip',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'country',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'state',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'note',
                TextareaType::class,
                [
                    'required' => false,
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
            'csrf_protection' => false,
        ]);
    }
}
