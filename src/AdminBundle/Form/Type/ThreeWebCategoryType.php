<?php

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ThreeWebCategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['model_var'] = $options['model_var'];
        $view->vars['model_default_var'] = $options['model_default_var'];
        $view->vars['search_function'] = $options['search_function'];
        $view->vars['btn_add_flag'] = $options['btn_add_flag'];
        $view->vars['add_function'] = $options['add_function'];
        $view->vars['refresh_function'] = $options['refresh_function'];
        $view->vars['image_model'] = $options['image_model'];
        $view->vars['search_model'] = $options['search_model'];
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return TextType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'three_web_category_type';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'model_var' => null,
                'model_default_var' => null,
                'search_function' => null,
                'btn_add_flag' => null,
                'add_function' => null,
                'refresh_function' => null,
                'image_model' => null,
                'search_model' => null,
            ]
        );
    }
}
