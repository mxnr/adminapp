<?php

namespace AdminBundle\Form\Type;

use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Model\ModelManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use ThreeWebOneEntityBundle\Entity\Filter;

class ThreeWebFilterType extends ModelType
{
    public function __construct($pa)
    {
        parent::__construct($pa);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['btn_add'] = $options['btn_add'];
        $view->vars['btn_list'] = $options['btn_list'];
        $view->vars['btn_delete'] = $options['btn_delete'];
        $view->vars['btn_catalogue'] = $options['btn_catalogue'];
        $view->vars['composite'] = $this->buildComposite($options['model_manager'], $options['query']);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'three_web_filters_type';
    }

    /**
     * Build associative array with parent to child filters
     *
     * @param ModelManagerInterface $modelManager
     * @param $query
     *
     * @return array
     */
    private function buildComposite(ModelManagerInterface $modelManager, $query)
    {
        $filters = $modelManager->executeQuery($query);
        $composite = [];
        /** @var Filter $filter */
        foreach ($filters as $filter) {
            if ($filter->getParent()) {
                if (isset($composite[$filter->getParent()->getId()])) {
                    array_push($composite[$filter->getParent()->getId()]['filters'], $filter->getId());
                } else {
                    $composite[$filter->getParent()->getId()]['filters'] = [$filter->getId()];
                    $composite[$filter->getParent()->getId()]['parent'] = $filter->getParent();
                }
            }
        }

        return $composite;
    }
}
