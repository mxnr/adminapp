<?php

namespace AdminBundle\Form\Type;

use AdminBundle\Form\DataTransformer\BarcodeToIdTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use ThreeWebOneEntityBundle\Entity\Order\OrderItem;

/**
 * Class OrderItemType.
 */
class OrderItemType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * OrderItemType constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'sellPrice',
                MoneyType::class,
                [
                    'divisor' => 100,
                    'currency' => false,
                    'label' => false,
                    'required' => false,
                    'mapped' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'ng-model' => 'vmRow.price',
                        'ng-change' => 'vmRow.priceChange()',
                        'ng-blur' => 'vmRow.formatPrice()',
                    ]
                ]
            )
            ->add(
                'barcode',
                TextType::class,
                [
                    'attr' => [
                        'class' => 'hidden-field'
                    ],
                    'label' => false,
                ]
            )
            ->add(
                'description',
                TextareaType::class,
                [
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ]
            )
            ->add(
                'adminNote',
                TextareaType::class,
                [
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ]
            )
        ;

        $barcodeToIdTransformer = new BarcodeToIdTransformer($this->entityManager);
        $builder->get('barcode')->addModelTransformer($barcodeToIdTransformer);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            /** @var OrderItem $item */
            $item = $event->getData();
            if (null != $item && $item->getBarcode()) {
                $form = $event->getForm();
                $sellPrice = $item->getBarcode()->getSellPrice()/100;
                if (null != $sellPrice) {
                    $form->get('sellPrice')->setData($sellPrice);
                }
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderItem::class,
        ]);
    }
}
