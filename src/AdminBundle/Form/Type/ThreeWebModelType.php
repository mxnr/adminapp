<?php

namespace AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ThreeWebModelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['model_var'] = $options['model_var'];
        $view->vars['model_default_var'] = $options['model_default_var'];
        $view->vars['search_function'] = $options['search_function'];
        $view->vars['refresh_function'] = $options['refresh_function'];
        $view->vars['inline'] = $options['inline'];
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return TextType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'three_web_model_type';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'model_var' => null,
                'model_default_var' => null,
                'search_function' => null,
                'refresh_function' => null,
                'inline' => true,
            ]
        );
    }
}
