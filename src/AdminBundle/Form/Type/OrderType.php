<?php

namespace AdminBundle\Form\Type;

use AdminBundle\Form\DataTransformer\CustomerToIdTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Order\OrderDeliveryTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\OrderPaymentTypeInterface;

/**
 * Class OrderType.
 */
class OrderType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * OrderType constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Order $order */
        $order = $builder->getData();

        $builder
            ->add(
                'customer',
                ThreeWebCustomerType::class,
                [
                    'required' => false,
                    'mapped' => false,
                    'data' => $order->getCustomer(),
                    'data_class' => null,
                    'model_var' => 'vm.customerSelected',
                    'model_default_var' => 'vm.presetCustomer',
                    'search_function' => 'vm.getCustomers()',
                    'placeholder' => 'Add customer to order',
                    'attr' => [
                        'ng-model' => 'vm.customerSelected',
                        'class' => 'hidden-field'
                    ]
                ]
            )
            ->add(
                'paymentType',
                ChoiceType::class,
                [
                    'expanded' => false,
                    'multiple' => false,
                    'choices' => OrderPaymentTypeInterface::PAYMENT_TYPE_ARRAY,
                ]
            )
            ->add(
                'deliveryType',
                ChoiceType::class,
                [
                    'expanded' => false,
                    'multiple' => false,
                    'choices' => OrderDeliveryTypeInterface::DELIVERY_TYPE_ARRAY,
                ]
            )
            ->add(
                'signatureImage',
                SignatureCaptureType::class,
                [
                    'required' => false,
                    'mapped' => false
                ]
            )
            ->add(
                'amountPaid',
                MoneyType::class,
                [
                    'divisor' => 100,
                    'required' => false,
                    'label' => false,
                    'currency' => false,
                ]
            )
            ->add(
                'tax',
                MoneyType::class,
                [
                    'divisor' => 100,
                    'mapped' => false,
                    'required' => false,
                    'label' => false,
                    'currency' => false,
                ]
            );
        if ($order->getSiteType() === Order::SALE) {
            $builder
                ->add(
                    'searchBarcode',
                    ThreeWebOrderSelectBarcodesType::class,
                    [
                        'required' => false,
                        'mapped' => false,
                        'data' => null,
                        'data_class' => null,
                        'model_var' => 'vm.purchaseBarcodeSelected',
                        'search_function' => 'vm.getBarcodes()',
                        'attr' => [
                            'ng-model' => 'vm.purchaseBarcodeSelected',
                            'class' => 'hidden-field'
                        ]
                    ]
                )
                ->add(
                    'orderItems',
                    CollectionType::class,
                    [
                        'entry_type' => OrderItemType::class,
                        'entry_options' => ['label' => false],
                        'allow_add' => true,
                        'allow_delete' => true,
                        'by_reference' => false,
                        'mapped' => false,
                        'label' => false,
                        'data' => $options['orderItems'],
                    ]
                )
                ->add(
                    'shippingAddress',
                    TextType::class,
                    [
                        'label' => 'Shipping address',
                        'required' => false,
                    ]
                );
        }
        if (in_array($order->getSiteType(), [Order::BUYBACK, Order::REPAIR])) {
            $builder
                ->add(
                    'items',
                    CollectionType::class,
                    [
                        'entry_type' => InventoryItemType::class,
                        'entry_options' => [
                            'label' => false,
                            'order_site_type' => $order->getSiteType()
                        ],
                        'allow_add' => true,
                        'allow_delete' => true,
                        'by_reference' => false,
                        'label' => false,
                    ]
                );
        }

        $customerToIdTransformer = new CustomerToIdTransformer($this->entityManager);
        $builder->get('customer')->addModelTransformer($customerToIdTransformer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
            'user' => null,
            'orderItems' => null,
        ]);
    }
}
