<?php

namespace AdminBundle\Form\Type\Domain;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * {@inheritDoc}
 */
class DomainContactsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'first_name',
                TextType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )->add(
                'last_name',
                TextType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )->add(
                'login',
                TextType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new Regex([
                            'pattern' => '/[#@`!\s$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/',
                            'match' => false,
                            'message' => 'The only allowed characters are all alphanumerics (A-Z, a-z, 0-9).',
                        ])
                    ]
                ]
            )->add(
                'password',
                PasswordType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new Length([
                            'min' => 10,
                            'max' => 20,
                            'minMessage' => 'Password should contain at least 10 characters.',
                            'maxMessage' => 'Password should contain at most 20 characters.',
                        ])
                    ]
                ]
            )->add(
                'country',
                CountryType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )->add(
                'org_name',
                TextType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )->add(
                'phone',
                TextType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new Regex([
                            'pattern' => '/\+\d+/',
                            'match' => true,
                            'message' => 'Phone must be in this format +141612341234'
                        ])
                    ]
                ]
            )->add(
                'fax',
                TextType::class,
                [
                    'label' => false,
                    'required' => false,
                ]
            )->add(
                'state',
                TextType::class,
                [
                    'label' => false,
                    'required' => false
                ]
            )->add(
                'email',
                EmailType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )->add(
                'city',
                TextType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )->add(
                'postal_code',
                TextType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new Length([
                            'min' => 4,
                            'max' => 7,
                            'minMessage' => 'Postal code should contain at least 4 digits.',
                            'maxMessage' => 'Postal code should contain at most 7 digits.',
                        ])
                    ]
                ]
            )->add(
                'address1',
                TextType::class,
                [
                    'label' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )->add(
                'address2',
                TextType::class,
                [
                    'label' => false,
                    'required' => false
                ]
            );
    }
}
