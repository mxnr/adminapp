<?php

namespace AdminBundle\Twig;

use AdminBundle\Service\UserGroupsService;
use ThreeWebOneEntityBundle\Entity\User;

class UserGroupExtension extends \Twig_Extension
{
    /**
     * @var UserGroupsService
     */
    private $userGroupService;

    /**
     * @param UserGroupsService $userGroupsService
     */
    public function setUserGroupService(UserGroupsService $userGroupsService)
    {
        $this->userGroupService = $userGroupsService;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getUserOrUserSupervisor', [$this, 'getUserOrUserSupervisor']),
        ];
    }

    /**
     * @param User $user
     * @return User
     */
    public function getUserOrUserSupervisor(User $user): User
    {
        return $this->userGroupService->getUserOrUserSupervisor($user);
    }
}
