<?php

namespace AdminBundle\Twig;

class PriceExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('format_price', [$this, 'formatPrice']),
        ];
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function formatPrice($value)
    {
        return number_format($value/100, 2, '.', '');
    }
}
