<?php

namespace AdminBundle\Twig;

use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\UserConfig\Location;
use ThreeWebOneEntityBundle\Status\StatusInterface;
use ThreeWebOneEntityBundle\Status\StatusTrait;

class ShowExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('showStatus', [$this, 'showStatus']),
            new \Twig_SimpleFunction('showCategoryType', [$this, 'showCategoryType']),
            new \Twig_SimpleFunction('showFilterType', [$this, 'showFilterType']),
            new \Twig_SimpleFunction('showPriceTypeType', [$this, 'showPriceTypeType']),
            new \Twig_SimpleFunction('showServiceType', [$this, 'showServiceType']),
        ];
    }

    /**
     * @param int $status
     * @return string
     */
    public function showStatus(int $status): string
    {
        $choices =  [
            StatusInterface::STATUS_ACTIVE => 'Active',
            StatusInterface::STATUS_INACTIVE => 'Inactive',
        ];

        return $choices[$status] ?? 'Undefined';
    }

    /**
     * @param int $type
     * @return string
     */
    public function showCategoryType(int $type): string
    {
        $choices =  Category::TYPES_ARRAY;

        return $choices[$type] ?? 'Undefined';
    }

    /**
     * @param int $type
     * @return string
     */
    public function showFilterType(int $type): string
    {
        $choices =  [
            Filter::TYPE_CAPACITY => 'Capacity',
            Filter::TYPE_PROVIDER => 'Provider',
            Filter::TYPE_COLOR => 'Color',
        ];

        return $choices[$type] ?? 'Undefined';
    }

    /**
     * @param int $type
     * @return string
     */
    public function showPriceTypeType(int $type): string
    {
        $choices =  [
            PriceType::SALE => 'Selling',
            PriceType::BUYBACK => 'Buyback',
            PriceType::REPAIR => 'Repair',
        ];

        return $choices[$type] ?? 'Undefined';
    }

    /**
     * @param $services
     * @return string
     */
    public function showServiceType($services): string
    {
        $result = '';
        foreach ($services as $item) {
            if ($item == Location::SALE) {
                $result .= 'Selling';
            }
            if ($item == Location::BUYBACK) {
                $result .= 'Buyback';
            }
            if ($item == Location::REPAIR) {
                $result .= 'Repair';
            }
            if ($item != end($services)) {
                $result .= ', ';
            }
        }

        return $result;
    }
}
