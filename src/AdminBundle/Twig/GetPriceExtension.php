<?php

namespace AdminBundle\Twig;

use AdminBundle\Service\AdminUserService;
use AdminBundle\Service\PriceService;
use AdminBundle\Service\UserGroupsService;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Price;
use Twig_Extension;
use Twig_SimpleFunction;

class GetPriceExtension extends Twig_Extension
{
    /**
     * @var PriceService
     */
    protected $priceService;

    /**
     * GetPriceExtension constructor.
     *
     * @param PriceService $priceService
     */
    public function __construct(PriceService $priceService)
    {
        $this->priceService = $priceService;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('getPriceByType', [$this, 'getPriceByType']),
            new Twig_SimpleFunction('isPriceSynchronized', [$this, 'isPriceSynchronized']),
        ];
    }

    /**
     * @param Model $model
     * @param int $priceTypeId
     * @param int $ownerId
     *
     * @return Price|null
     */
    public function getPriceByType(Model $model, int $priceTypeId, int $ownerId)
    {
        return $this->priceService->getPriceByType($model, $priceTypeId, $ownerId);
    }

    /**
     * @param Model $model
     * @param int $priceTypeId
     * @param int $userId
     * @param int $adminId
     *
     * @return bool
     */
    public function isPriceSynchronized(Model $model, int $priceTypeId, int $userId, int $adminId)
    {
        return $this->priceService->isPriceSynchronized($model, $priceTypeId, $userId, $adminId);
    }
}
