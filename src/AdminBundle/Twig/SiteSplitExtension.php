<?php

namespace AdminBundle\Twig;

use AdminBundle\Service\ChargeBee\SiteSplitService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Status\StatusInterface;
use Twig_Extension;
use Twig_SimpleFunction;

class SiteSplitExtension extends Twig_Extension
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var SiteSplitService
     */
    private $siteSplitService;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * SiteSplitExtension constructor.
     *
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param SiteSplitService $siteSplitService
     */
    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        SiteSplitService $siteSplitService,
        EntityManager $entityManager
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->siteSplitService = $siteSplitService;
        $this->em = $entityManager;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('isAccessibleOrderType', [$this, 'isAccessibleOrderType']),
            new Twig_SimpleFunction('getPriceTypeCategoryName', [$this, 'getPriceTypeCategoryName']),
            new Twig_SimpleFunction('isBuyBackOrRepairAllowed', [$this, 'isBuyBackOrRepairAllowed']),
            new \Twig_SimpleFunction('filterPrices', [$this, 'filterPrices']),
        ];
    }

    /**
     * @param int $type
     *
     * @return bool
     */
    public function isAccessibleOrderType(int $type): bool
    {
        if (!in_array($type, array_keys(SiteSplitService::ORDER_TYPE_CONNECTOR))) {
            throw new NotFoundHttpException($type . ' type doesn\'t exist');
        }

        return $this->authorizationChecker
            ->isGranted(SiteSplitService::ORDER_TYPE_CONNECTOR[$type], $this->siteSplitService);
    }

    /**
     * @return string
     */
    public function getPriceTypeCategoryName(): string
    {
        $mangePriceName = 'Condition/';
        if ($this->isAccessibleOrderType(Order::REPAIR)) {
            $mangePriceName .= 'Service';
        }

        return trim($mangePriceName, '/');
    }

    /**
     * @return bool
     */
    public function isBuyBackOrRepairAllowed(): bool
    {
        return (
            $this->authorizationChecker->isGranted(SiteSplitService::ORDER_BUY, $this->siteSplitService) ||
            $this->authorizationChecker->isGranted(SiteSplitService::ORDER_REPAIR, $this->siteSplitService)
        );
    }

    /**
     * Used to display only allowed prices for user
     *
     * @param Price[] $prices
     * @param User $user
     *
     * @return array
     */
    public function filterPrices(array $prices, User $user)
    {
        $includeBuybackPrice = $this->authorizationChecker
            ->isGranted(SiteSplitService::ORDER_BUY, $this->siteSplitService);
        $includeRepairkPrice = $this->authorizationChecker
            ->isGranted(SiteSplitService::ORDER_REPAIR, $this->siteSplitService);

        $allowedPrices = [];
        $admin = $this->em->getRepository(User::class)->findOneBy(['username' => 'admin']);

        foreach ($prices as $price) {
            if (
                in_array($price->getOwner(), [$admin, $user]) &&
                $price->getPriceType() &&
                in_array($price->getPriceType()->getSite(), [PriceType::BUYBACK, PriceType::REPAIR]) &&
                (
                    ($price->getPriceType()->getSite() == PriceType::BUYBACK && $includeBuybackPrice) ||
                    ($price->getPriceType()->getSite() == PriceType::REPAIR && $includeRepairkPrice)
                )
            ) {
                $allowedPrices[] = $price;
            }
        }

        return $allowedPrices;
    }
}
