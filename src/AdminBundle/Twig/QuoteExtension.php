<?php

namespace AdminBundle\Twig;

use AdminBundle\Service\UserGroupsService;
use ThreeWebOneEntityBundle\Entity\User;

class QuoteExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('escapeQuotes', [$this, 'escapeQuotes']),
        ];
    }

    /**
     * @param string $string
     * @return string
     */
    public function escapeQuotes(string $string = null): string
    {
        return str_replace("'", "\\'", $string);
    }
}
