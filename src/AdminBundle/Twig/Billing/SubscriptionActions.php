<?php

namespace AdminBundle\Twig\Billing;

use AdminBundle\Service\ChargeBee\HostedPageService;
use Symfony\Component\Routing\Router;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Class SubscriptionHostedPage.
 */
class SubscriptionActions extends Twig_Extension
{
    /**
     * @var array
     */
    protected $urlList = [];

    /**
     * @var HostedPageService
     */
    protected $hostedPageService;

    /**
     * @var Router
     */
    private $router;

    /**
     * SubscriptionHostedPage constructor.
     *
     * @param HostedPageService $hostedPageService
     * @param Router            $router
     */
    public function __construct(HostedPageService $hostedPageService, Router $router)
    {
        $this
            ->setHostedPageService($hostedPageService)
            ->setRouter($router);
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction('getSubscriptionPayUrl', [$this, 'getCheckoutPageUrl']),
            new Twig_SimpleFunction('getSubscriptionCancelUrl', [$this, 'getCancelUrl']),
        ];
    }

    /**
     * @param Subscription $subscription
     *
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCheckoutPageUrl(Subscription $subscription): string
    {
        if ($subscription->getStatus() === Subscription::STATUS_TRIAL) {
            return $this->getHostedPageService()->checkOut($subscription)['url'];
        }

        return '';
    }

    /**
     * @return HostedPageService
     */
    public function getHostedPageService(): HostedPageService
    {
        return $this->hostedPageService;
    }

    /**
     * @return bool
     */
    public function hasHostedPageService(): bool
    {
        return !is_null($this->hostedPageService);
    }

    /**
     * @param HostedPageService|null $hostedPageService
     *
     * @return SubscriptionActions
     */
    public function setHostedPageService(HostedPageService $hostedPageService = null): SubscriptionActions
    {
        $this->hostedPageService = $hostedPageService;

        return $this;
    }

    /**
     * @param Subscription $subscription
     *
     * @return string
     */
    public function getCancelUrl(Subscription $subscription): string
    {
        if (
            true === $this->hasRouter() &&
            true === in_array(
                $subscription->getStatus(),
                [Subscription::STATUS_FUTURE, Subscription::STATUS_ENDING, Subscription::STATUS_ACTIVE]
            )) {
            return $this->getRouter()->generate('admin_billing_subscriptions_cancel', ['id' => $subscription->getId()]);
        }

        return '';
    }

    /**
     * @return Router
     */
    public function getRouter(): Router
    {
        return $this->router;
    }

    /**
     * @return bool
     */
    public function hasRouter(): bool
    {
        return !is_null($this->router);
    }

    /**
     * @param Router|null $router
     *
     * @return SubscriptionActions
     */
    public function setRouter(Router $router = null): SubscriptionActions
    {
        $this->router = $router;

        return $this;
    }
}
