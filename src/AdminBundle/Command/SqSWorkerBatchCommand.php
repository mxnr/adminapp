<?php

namespace AdminBundle\Command;

use AdminBundle\Service\SQS\BatchWorker;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TriTran\SqsQueueBundle\Command\QueueWorkerCommand;
use TriTran\SqsQueueBundle\Service\BaseQueue;
use TriTran\SqsQueueBundle\Service\BaseWorker;

/**
 * {@inheritDoc}
 */
class SqSWorkerBatchCommand extends QueueWorkerCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName('admin:sqs_worker:batch')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Queue Name to process.',
                null
            )
            ->addArgument(
                'prefetchCount',
                InputArgument::OPTIONAL,
                'Prefetch items count.',
                10
            )
            ->addArgument(
                'timePeriod',
                InputArgument::OPTIONAL,
                'Work time limit in seconds, useful for lambda.'
            )
            ->setDescription('Start a worker that will listen to a specified SQS queue');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queueName = $input->getArgument('name');

        $prefetchCount = $input->getArgument('prefetchCount');

        $workTimeLimit = $input->getArgument('timePeriod');

        if (!$this->getContainer()->has(sprintf('tritran.sqs_queue.%s', $queueName))) {
            throw new \InvalidArgumentException(sprintf('Queue [%s] does not exist.', $queueName));
        }

        $io = new SymfonyStyle($input, $output);
        $io->title(sprintf('Start listening to queue <comment>%s</comment>', $queueName));

        /** @var BaseQueue $queue */
        $queue = $this->getContainer()->get(sprintf('tritran.sqs_queue.%s', $queueName));

        /** @var BatchWorker $worker */
        $worker = $this->getContainer()->get('tritran.sqs_queue.queue_worker');
        $worker->start($queue, $prefetchCount, $workTimeLimit);
    }
}
