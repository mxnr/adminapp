<?php

namespace AdminBundle\Command\DomainName;

use AdminBundle\Service\DomainName\DomainQueueService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UnlockDomainCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('admin:domain:unlock')->setDescription('Unlock all domains');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var DomainQueueService $service */
        $service = $this->getApplication()->getKernel()->getContainer()->get('admin.domain_name_queue');
        $service->unlockAllDomains();
    }
}