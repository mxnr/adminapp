<?php

namespace AdminBundle\Command\DomainName;

use AdminBundle\Service\DomainName\DomainNameService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DomainIterationCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('admin:domain:start-iteration')->setDescription('Starting iterations to the new domains');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var DomainNameService $service */
        $service = $this->getApplication()->getKernel()->getContainer()->get('admin.new_domain_name_service');
        $service->process();
    }
}