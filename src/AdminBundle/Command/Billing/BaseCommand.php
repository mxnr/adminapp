<?php

namespace AdminBundle\Command\Billing;

use Symfony\Component\Console\Command\Command;

/**
 * Class BaseCommand.
 */
abstract class BaseCommand extends Command
{
    /**
     * @param string $id
     *
     * @return mixed
     */
    protected function getService(string $id)
    {
        return $this->getApplication()->getKernel()->getContainer()->get($id);
    }
}
