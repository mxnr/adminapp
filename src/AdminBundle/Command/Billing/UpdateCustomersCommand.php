<?php

namespace AdminBundle\Command\Billing;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateCustomersCommand.
 */
class UpdateCustomersCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('admin:billing:update:customers')
            ->addArgument('customerId', InputArgument::OPTIONAL, 'Id of customer to proceed')
            ->setDescription('Command updates customers or concrete customer if id passed');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this
            ->getService('admin.charge_bee.customer')
            ->synchronize($input->getArgument('customerId'));
    }
}
