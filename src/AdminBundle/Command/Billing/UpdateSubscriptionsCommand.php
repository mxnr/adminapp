<?php

namespace AdminBundle\Command\Billing;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateSubscriptionsCommand.
 */
class UpdateSubscriptionsCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->addArgument('subscriptionId', InputArgument::OPTIONAL, 'Id of subscription to proceed')
            ->setName('admin:billing:update:subscriptions')
            ->setDescription('Command updates subscriptions or concrete subscription if id passed');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this
            ->getService('admin.charge_bee.subscription')
            ->synchronize($input->getArgument('subscriptionId'));
    }
}
