<?php

namespace AdminBundle\Command\Billing;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdatePlansCommand.
 */
class UpdatePlansCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('admin:billing:update:plans')
            ->setDescription('Command updates plans list');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this
            ->getService('admin.charge_bee.plan')
            ->synchronize();
    }
}
