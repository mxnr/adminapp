<?php

namespace AdminBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class ClearLogsCommand extends Command
{
    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var null|string
     */
    private $logsDir;

    /**
     * @var
     */
    private $env;

    /**
     * ClearLogsCommand constructor.
     *
     * @param null|string $logsDir
     * @param String $env
     */
    public function __construct($logsDir, String $env)
    {
        parent::__construct();
        $this->logsDir = $logsDir;
        $this->env = $env;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('admin:logs:clear')
            ->setDescription('Deletes all log files');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->fs = new Filesystem();
        $log = $this->logsDir . '/' . $this->env . '.log';
        $this->io->comment(sprintf('Clearing the logs for the <info>%s</info> environment', $this->env));
        $this->fs->remove($log);
        if (!$this->fs->exists($log)) {
            $this->io->success(sprintf('Logs for the "%s" environment was successfully cleared.', $this->env));
        } else {
            $this->io->error(sprintf('Logs for the "%s" environment could not be cleared.', $this->env));
        }
    }
}