<?php

namespace AdminBundle\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use ThreeWebOneEntityBundle\Entity\Helper\Sequential\SequentialInterface;

class SequentialListener
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if (!$object instanceof SequentialInterface) {
            return;
        }

        $em = $args->getObjectManager();
        /** @var SequentialInterface $lastItem */
        $lastItem = $em->getRepository(get_class($object))->findLastSequentialItem($object->getOwner());
        $storeId = 1;
        if ($lastItem) {
            $storeId = $lastItem->getStoreId();
            $storeId += 1;
        }
        $object->setStoreId($storeId);
        $em->flush();
    }
}
