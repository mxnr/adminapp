<?php

namespace AdminBundle\EventListener;

use AdminBundle\Event\DomainEvent;
use AdminBundle\Service\DomainName\AcmService;
use AdminBundle\Service\DomainName\DomainQueueService;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomainQueue;

class DomainListener implements EventSubscriberInterface
{
    /**
     * @var DomainQueueService
     */
    protected $domainQueueService;

    /**
     * @var AcmService
     */
    protected $acmService;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * DomainListener constructor.
     * @param DomainQueueService $domainQueueService
     * @param AcmService $acmService
     * @param LoggerInterface $logger
     */
    public function __construct(DomainQueueService $domainQueueService, AcmService $acmService, LoggerInterface $logger)
    {
        $this->domainQueueService = $domainQueueService;
        $this->acmService = $acmService;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            DomainEvent::DOMAIN_CREATE => 'createDomain',
            DomainEvent::CUSTOM_DOMAIN_APPROVED => 'customDomainApproved',
        ];
    }

    /**
     * @param DomainEvent $domainEvent
     */
    public function createDomain(DomainEvent $domainEvent)
    {
        $this->domainQueueService->addDomain($domainEvent->getDomainClient()->getDomain());
    }

    /**
     * @param DomainEvent $domainEvent
     */
    public function customDomainApproved(DomainEvent $domainEvent)
    {
        $clientDomain = $domainEvent->getDomainClient();

        if ($clientDomain->getDomainType() !== ClientDomain::CLIENT_DOMAIN_TYPE) {
            return;
        }

        try {
            $this->domainQueueService->addDomain(
                $clientDomain->getDomain(),
                ClientDomainQueue::OWNERSHIP_EXTERNAL_REGISTERER
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
