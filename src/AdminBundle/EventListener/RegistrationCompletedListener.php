<?php

namespace AdminBundle\EventListener;

use AdminBundle\Service\ChargeBee\CustomerService;
use AdminBundle\Service\ChargeBee\SubscriptionService;
use AdminBundle\Service\ClientDomainService;
use AdminBundle\Service\DomainNameService;
use AdminBundle\Service\InitialSynchronizationService;
use AdminBundle\Service\UpgradeService;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;
use ThreeWebOneEntityBundle\Entity\User;
use Doctrine\ORM\EntityManager;

/**
 * Class RegistrationCompletedListener.
 */
class RegistrationCompletedListener implements EventSubscriberInterface
{
    /**
     * @var InitialSynchronizationService
     */
    private $initialSynchronizeService;

    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var UpgradeService
     */
    private $upgradeService;

    /**
     * @var DomainNameService
     */
    private $domainNameService;

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ClientDomainService
     */
    private $clientDomainService;

    /**
     * RegistrationCompletedListener constructor.
     *
     * @param InitialSynchronizationService $initialSynchronizationService
     * @param CustomerService               $customerService
     * @param SubscriptionService           $subscriptionService
     * @param UpgradeService                $upgradeService
     * @param DomainNameService             $domainNameService
     * @param UserManagerInterface          $userManager
     * @param EntityManager                 $entityManager
     * @param ClientDomainService           $clientDomainService
     */
    public function __construct(
        InitialSynchronizationService $initialSynchronizationService,
        CustomerService $customerService,
        SubscriptionService $subscriptionService,
        UpgradeService $upgradeService,
        DomainNameService $domainNameService,
        UserManagerInterface $userManager,
        EntityManager $entityManager,
        ClientDomainService $clientDomainService
    ) {
        $this->initialSynchronizeService = $initialSynchronizationService;
        $this->customerService = $customerService;
        $this->subscriptionService = $subscriptionService;
        $this->upgradeService = $upgradeService;
        $this->domainNameService = $domainNameService;
        $this->userManager = $userManager;
        $this->entityManager = $entityManager;
        $this->clientDomainService = $clientDomainService;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
        ];
    }

    /**
     * Processes event.
     *
     * @param FilterUserResponseEvent $event
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        /** @var User $user */
        $user = $event->getUser();

        if (false === $user->hasRole('ROLE_SUPER_ADMIN') && true === $user->hasRole('ROLE_ADMIN')) {
            $this->getInitialSynchronizeService()->scheduleSynchronizationFor($user);
            $customer = $this->getCustomerService()->createCustomerVia($user);
            $this->getSubscriptionService()->subscribe(
                $customer,
                $this->getUpgradeService()->getRequestedPlan(),
                $this->getUpgradeService()->getRequestedSites()
            );

            $frontDomain = $this->getDomainNameService()->getCloudFrontServiceHost();

            $clientDomain = (new ClientDomain())
            ->setOwner($user)
            ->setActive(ClientDomain::ACTIVE)
            ->setDomainType(ClientDomain::SUB_DOMAIN_TYPE)
            ->setDomain($this->clientDomainService->convertStringToSubDomain($user->getStoreName()))
            ->setStatus(ClientDomain::ADDED_MANUALLY);
            $this->entityManager->persist($clientDomain);

            $user->setStoreAddress(str_replace([' '], '-', $user->getStoreName()));
            $this->getUserManager()->updateUser($user);
        }
    }

    /**
     * @return InitialSynchronizationService
     */
    public function getInitialSynchronizeService(): InitialSynchronizationService
    {
        return $this->initialSynchronizeService;
    }

    /**
     * @return bool
     */
    public function hasInitialSynchronizeService(): bool
    {
        return !empty($this->initialSynchronizeService);
    }

    /**
     * @return CustomerService
     */
    public function getCustomerService(): CustomerService
    {
        return $this->customerService;
    }

    /**
     * @return bool
     */
    public function hasCustomerService(): bool
    {
        return !empty($this->customerService);
    }

    /**
     * @return SubscriptionService
     */
    public function getSubscriptionService(): SubscriptionService
    {
        return $this->subscriptionService;
    }

    /**
     * @return bool
     */
    public function hasSubscriptionService(): bool
    {
        return !empty($this->subscriptionService);
    }

    /**
     * @param SubscriptionService $subscriptionService
     *
     * @return RegistrationCompletedListener
     */
    public function setSubscriptionService(SubscriptionService $subscriptionService): RegistrationCompletedListener
    {
        $this->subscriptionService = $subscriptionService;

        return $this;
    }

    /**
     * @return UpgradeService
     */
    public function getUpgradeService(): UpgradeService
    {
        return $this->upgradeService;
    }

    /**
     * @return bool
     */
    public function hasUpgradeService(): bool
    {
        return !empty($this->upgradeService);
    }

    /**
     * @param UpgradeService $upgradeService
     *
     * @return RegistrationCompletedListener
     */
    public function setUpgradeService(UpgradeService $upgradeService): RegistrationCompletedListener
    {
        $this->upgradeService = $upgradeService;

        return $this;
    }

    /**
     * @return DomainNameService|null
     */
    public function getDomainNameService(): ?DomainNameService
    {
        return $this->domainNameService;
    }

    /**
     * @return bool
     */
    public function hasDomainNameService(): bool
    {
        return !is_null($this->domainNameService);
    }

    /**
     * @param DomainNameService|null $domainNameService
     *
     * @return RegistrationCompletedListener
     */
    public function setDomainNameService(DomainNameService $domainNameService = null): RegistrationCompletedListener
    {
        $this->domainNameService = $domainNameService;

        return $this;
    }

    /**
     * @return UserManagerInterface|null
     */
    public function getUserManager(): ?UserManagerInterface
    {
        return $this->userManager;
    }

    /**
     * @return bool
     */
    public function hasUserManager(): bool
    {
        return !is_null($this->userManager);
    }
}
