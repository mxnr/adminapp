<?php

namespace AdminBundle\EventListener;

use AdminBundle\Service\ChargeBee\HostedPageService;
use AdminBundle\Service\UserGroupsService;
use AdminBundle\Twig\SiteSplitExtension;
use Sonata\AdminBundle\Event\ConfigureMenuEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class MenuBuilderListener.
 */
class MenuBuilderListener
{
    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var UserGroupsService
     */
    private $userGroupsService;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var SiteSplitExtension
     */
    private $siteSplitExtension;

    /**
     * MenuBuilderListener constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param UserGroupsService $userGroupsService
     * @param RequestStack $requestStack
     * @param SiteSplitExtension $siteSplitExtension
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        UserGroupsService $userGroupsService,
        RequestStack $requestStack,
        SiteSplitExtension $siteSplitExtension
    ) {
        $this->token = $tokenStorage;
        $this->userGroupsService = $userGroupsService;
        $this->requestStack = $requestStack;
        $this->siteSplitExtension = $siteSplitExtension;
    }

    /**
     * @param ConfigureMenuEvent $event
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function addMenuItems(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();
        /** @var User $user */
        $user = $this->token->getToken()->getUser();
        $menu->getChild('Inventory')->addChild('Item add', [
            'label' => 'Add inventory',
            'route' => 'admin_add_inventory_items',
        ])->setExtras([
            'icon' => '<i class="fa fa-plus"></i>',
        ]);

        $menu->addChild('profit-calculator', [
            'label' => 'Profit calculator',
            'route' => 'admin_profit_calculator',
        ])->setExtras([
            'icon' => '<i class="fa fa-calculator"></i>',
        ]);

        $menu->addChild('business-performance', [
            'label' => 'Business performance',
            'route' => 'admin_business_performance',
        ])->setExtras([
            'icon' => '<i class="fa fa-signal"></i>',
        ]);

        if ($user->hasRole('ROLE_ADMIN') || $user->hasRole('ROLE_SUPER_ADMIN')) {
            $menu->addChild('quick_links', [
                'label' => 'Quick links',
                'route' => 'admin_quick_links_list',
            ])->setExtras([
                'icon' => '<i class="fa fa-link"></i>',
            ]);
            $menu->addChild('advertise', [
                'label' => ' Apps & Affiliates',
                'route' => 'config_apps_list',
            ])->setExtras([
                'icon' => '<i class="fas fa-bullhorn"></i>',
            ]);
        }

        if (
            $user->hasRole('ROLE_ADMIN') ||
            $user->hasRole('ROLE_SUPER_ADMIN') ||
            ($user->hasRole('ROLE_USER') && $user->hasRole('ROLE_ADMIN_USER_CONFIG_CONFIG_ALL'))
        ) {
            $user = $this->userGroupsService->getUserOrUserSupervisor($user);
            $menu->getChild('Settings')->addChild('Design', [
                'label' => 'Design',
                'route' => 'user_config_config_edit',
                'routeParameters' => [
                    'id' => $user->getConfig()->getId()
                ]
            ])->setExtras([
                'icon' => '<i class="fa fa-angle-double-right"></i>',
            ]);
            if ($this->requestStack->getCurrentRequest()->get('_route') == 'user_config_config_edit') {
                $menu->getChild('Settings')->getChild('Design')->setCurrent(true);
            }
        }

        $menu->removeChild('Billing');
        $menu->removeChild('Images');
        $advertises = $menu->getChild('Advertise');
        $menu->removeChild('Advertise');
        $quickLinks = $menu->getChild('Quick links');
        $menu->removeChild('Quick links');
        $menu->removeChild('Settings');
        $menu->removeChild('sonata_user');
        $menu->removeChild('Organize website');
        $menu->getChild('Inventory')->removeChild('Active listing');
        $menu->getChild('Customers/Suppliers')
            ->setExtras([
                'icon' => '<i class="fa fa-user-friends"></i>',
            ])
            ->addChild('Add new', [
            'label' => 'Add new',
            'route' => 'admin_customer_create',
        ])->setExtras([
            'icon' => '<i class="fa fa-angle-double-right"></i>',
        ]);
        if ($this->siteSplitExtension->isAccessibleOrderType(Order::SALE)) {
            $menu->getChild('Orders')->addChild('Create selling order', [
                'label' => 'Create selling order',
                'route' => 'admin_create_order',
                'routeParameters' => [
                    'type' => Order::SALE
                ],
            ])->setExtras([
                'icon' => '<i class="fa fa-angle-double-right"></i>',
            ]);
        }
        if ($this->siteSplitExtension->isAccessibleOrderType(Order::BUYBACK)) {
            $menu->getChild('Orders')->addChild('Create buyback order', [
                'label' => 'Create buyback order',
                'route' => 'admin_create_order',
                'routeParameters' => [
                    'type' => Order::BUYBACK
                ],
            ])->setExtras([
                'icon' => '<i class="fa fa-angle-double-right"></i>',
            ]);
        }
        if ($this->siteSplitExtension->isAccessibleOrderType(Order::REPAIR)) {
            $menu->getChild('Orders')->addChild('Create repair order', [
                'label' => 'Create repair order',
                'route' => 'admin_create_order',
                'routeParameters' => [
                    'type' => Order::REPAIR
                ],
            ])->setExtras([
                'icon' => '<i class="fa fa-angle-double-right"></i>',
            ]);
        }

        $menu->getChild('Orders')->setExtras([
            'icon' => '<i class="fa fa-cart-plus"></i>',
        ]);
        $menu->getChild('Inventory')->setExtras([
            'icon' => '<i class="fa fa-boxes"></i>',
        ]);

        $menu->addChild('settings', [
            'label' => 'Settings',
            'route' => 'admin_settings_default',
        ])->setExtras([
            'icon' => '<i class="fa fa-cog"></i>',
        ]);
    }
}
