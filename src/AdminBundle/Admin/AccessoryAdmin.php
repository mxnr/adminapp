<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use ThreeWebOneEntityBundle\Entity\Accessory;
use ThreeWebOneEntityBundle\Entity\AccessoryImage;
use ThreeWebOneEntityBundle\Repository\AccessoryRepository;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * Class AccessoryAdmin.
 */
class AccessoryAdmin extends BaseAdmin
{
    /**
     * @var bool
     */
    protected $useRepoQuery = true;

    /**
     * Route Name.
     *
     * @var string
     */
    protected $baseRouteName = 'admin_accessory';

    /**
     * Route Pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-accessory';

    /**
     * {@inheritdoc}
     */
    protected function getQueryParams(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID => $this->getAdminUser()->getId(),
            AdminQueryInterface::PARAM_OWNER_USER_ID => $this->getUser()->getId(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->tab('Item')
            ->with('General', ['class' => 'col-md-8'])
            ->add('title')
            ->add('status', null, ['template' => 'AdminBundle:Show:status.html.twig'])
            ->add(
                'parent.title',
                null,
                [
                    'label' => 'Parent',
                ]
            )
            ->end()
            ->with('Image', ['class' => 'col-md-4'])
            ->add(
                'image',
                null,
                [
                    'template' => 'AdminBundle:Show:image.html.twig',
                    'label' => false,
                ]
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        $user = $this->getUser();

        $actions = [];
        if (
            $this->hasRoute('edit') &&
            $this->hasAccess('edit') &&
            $user->hasRole('ROLE_SUPER_ADMIN') === false
        ) {
            $actions['Synchronize'] = array(
                'ask_confirmation' => true,
            );
            $actions['Desynchronize'] = array(
                'ask_confirmation' => true,
            );
        }

        return $actions;
    }

    /**
     * @param Accessory $accessory
     */
    public function prePersist($accessory)
    {
        $accessory->addUser($this->getUser());
        $accessory->setOwner($this->getUser());
        $this->manageEmbeddedImage($accessory);
    }

    /**
     * Manages Images Persistent. Since Image Entity has file property
     * that Doctrine doesn't knows about we need to change property of
     * Image to trigger load Event.
     *
     * @param Accessory $accessory
     */
    private function manageEmbeddedImage($accessory)
    {
        /** @var AccessoryImage $image */
        foreach ($accessory->getImages() as $image) {
            if ($image) {
                if ($image->getImageFile()) {
                    $user = $this->getUser();
                    // update the Image to trigger file management
                    $image->refreshUpdated();
                    $image->setEntity($accessory);
                    $image->setOwner($this->getUser());
                    if (false === in_array($user, $image->getUsers()->toArray())) {
                        $image->addUser($this->getUser());
                    }
                } elseif (!$image->getImageFile() && !$image->getImageName()) {
                    // prevent Sf/Sonata trying to create and persist an empty Image
                    $accessory->removeImage($image);
                }
            }
        }
    }

    /**
     * @param Accessory $accessory
     */
    public function preUpdate($accessory)
    {
        $this->manageEmbeddedImage($accessory);
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $this->addActionsToRouteCollection($collection);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Accessory')
            ->with('General', ['class' => 'col-md-6'])
            ->add('title', 'text')
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        'disabled' => Accessory::STATUS_INACTIVE,
                        'enabled' => Accessory::STATUS_ACTIVE,
                    ],
                ]
            )
            ->add(
                'parent',
                EntityType::class,
                [
                    'class' => 'ThreeWebOneEntityBundle:Accessory',
                    'choice_label' => 'title',
                    'required' => false,
                    'placeholder' => 'None',
                    'empty_data' => null,
                    'query_builder' => function (AccessoryRepository $repo) {
                        return $repo->getUserParentAccessoriesQuery($this->getUser());
                    },
                ],
                ['admin_code' => 'admin.parent_accessory']
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add(
                'title',
                null,
                [
                    'show_filter' => true,
                ]
            )
            ->add(
                'parent',
                'doctrine_orm_model_autocomplete',
                ['admin_code' => 'admin.parent_accessory'],
                null,
                [
                    'property' => 'title',
                ]
            )
            ->add('status');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('title')
            ->addIdentifier(
                'parent',
                null,
                [
                    'associated_property' => 'title',
                    'admin_code' => 'admin.parent_accessory',
                ]
            )
            ->add(
                'status',
                'choice',
                [
                    'editable' => true,
                    'choices' => [
                        Accessory::STATUS_ACTIVE => 'Active',
                        Accessory::STATUS_INACTIVE => 'Inactive',
                    ],
                ]
            );
        $this->addActionToListMapper($listMapper);
    }
}
