<?php

namespace AdminBundle\Admin\Page;

use AdminBundle\Admin\BaseAdmin;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use ThreeWebOneEntityBundle\Entity\Page\Page;
use ThreeWebOneEntityBundle\Entity\Page\PageTypeInterface;

class PageAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_page';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-page';

    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->where($query->getRootAliases()[0] . '.owner = :user')
            ->setParameter('user', $this->getUser())
            ->andWhere($query->getRootAliases()[0] . '.pageType in (:types)')
            ->setParameter('types', $this->getDisclaimerTypesBySubscription());

        return $query;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Page')
            ->with('General', ['class' => 'col-md-12'])
            ->add('title')
            ->add(
                'description',
                SimpleFormatterType::class,
                [
                    'format' => 'markdown',
                    'ckeditor_context' => 'default',
                    'attr' => ['class' => 'span10 ckeditor', 'rows' => 20],
                ]
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('title')
            ->add('description');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->add(
                'page_type',
                'choice',
                [
                    'editable' => false,
                    'choices' => PageTypeInterface::PAGES_ARRAY,
                ]
            )
            ->add('title');
    }

    /**
     * @return array
     */
    private function getDisclaimerTypesBySubscription(): array
    {
        $types = [Page::TERMS, Page::PRIVACY];
        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_SALE)) {
            $types[] = Page::DISCLAIMER_SALE;
        }
        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_BUY)) {
            $types[] = Page::DISCLAIMER_BUYBACK;
        }
        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_REPAIR)) {
            $types[] = Page::DISCLAIMER_REPAIR;
        }

        return $types;
    }
}
