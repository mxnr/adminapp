<?php
namespace AdminBundle\Admin;

use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

class CategoryAdmin extends BaseCategoryAdmin
{
    /**
     * @var bool
     */
    protected $useRepoQuery = true;

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_category_category';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-category-category';

    /**
     * @var integer
     */
    protected $parentCategoryId = Category::TYPE_CATEGORY;

    /**
     * @var string
     */
    protected $adminCode = 'admin.category';

    /**
     * {@inheritdoc}
     */
    protected function getQueryParams(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID => $this->getAdminUser()->getId(),
            AdminQueryInterface::PARAM_OWNER_USER_ID => $this->getUser()->getId(),
            AdminQueryInterface::PARAM_PARENT_CATEGORY_ID => $this->parentCategoryId,
        ];
    }
}
