<?php

namespace AdminBundle\Admin\Sequential;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

trait SequentialTrait
{
    /**
     * @param mixed $id
     * @return mixed|object
     */
    public function getObject($id)
    {
        $object = $this->getModelManager()->findBy(
            $this->getClass(),
            [
                'storeId' => $id,
                'owner' => $this->getUser(),
            ]
        );

        return $object[0] ?? null;
    }

    /**
     * @param mixed $entity
     * @return string
     */
    public function getNormalizedIdentifier($entity)
    {
        return $entity->getStoreId();
    }

    /**
     * @param string $name
     * @param mixed $object
     * @param array $parameters
     * @param int $absolute
     * @return string
     */
    public function generateObjectUrl($name, $object, array $parameters = [], $absolute = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        $parameters['id'] = $object->getStoreId();

        return $this->generateUrl($name, $parameters, $absolute);
    }
}
