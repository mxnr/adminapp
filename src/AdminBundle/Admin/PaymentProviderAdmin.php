<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use ThreeWebOneEntityBundle\Entity\PaymentConfig;
use ThreeWebOneEntityBundle\Entity\QuickLink;

class PaymentProviderAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_payment_provider';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-payment-provider';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->where($query->getRootAliases()[0] . '.owner = :owner');
        $query->setParameter('owner', $this->getUser());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Link')
            ->with('General', ['class' => 'col-md-12'])
            ->add(
                'paymentType',
                'choice',
                [
                    'choices' => [
                        PaymentConfig::PAYPAL_NAME => PaymentConfig::PAYPAL,
                    ],
                ]
            )
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        'Disabled' => PaymentConfig::STATUS_INACTIVE,
                        'Active' => PaymentConfig::STATUS_ACTIVE,
                    ],
                ]
            )
            ->add('account', 'text')
            ->add('clientId', 'text')
            ->add('clientSecret', 'text')
            ->end()
            ->end();

        $builder = $formMapper->getFormBuilder();
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            /** @var PaymentConfig $obj */
            $obj = $event->getForm()->getData();
            $existingPaymentConfig = $this->getUser()->getPaymentConfigByType($data['paymentType']);
            if ($existingPaymentConfig && $existingPaymentConfig->getId() != $obj->getId()) {
                $event->getForm()->addError(new FormError(
                    'You already have configured payment provider of this type'
                ));
            }
            if (!empty($data['status'])) {
                if (empty($data['account']) || empty($data['clientId']) || empty($data['clientSecret'])) {
                    $event->getForm()->addError(new FormError(
                        'In order to activate payment method all necessary fields must be filled: \'Account\',  \'Client Id\', \'Client Secret\''
                    ));
                }
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        PaymentConfig::STATUS_ACTIVE => 'Active',
                        PaymentConfig::STATUS_INACTIVE => 'Inactive',
                    ],
                ]
            )
            ->add(
                'paymentType',
                'choice',
                [
                    'choices' => [
                        PaymentConfig::PAYPAL => PaymentConfig::PAYPAL_NAME,
                    ],
                ]
            );
    }

    /**
     * @param QuickLink $quickLink
     */
    public function prePersist($quickLink)
    {
        $quickLink->setOwner($this->getUser());
    }
}
