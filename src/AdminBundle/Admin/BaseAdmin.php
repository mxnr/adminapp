<?php
namespace AdminBundle\Admin;

use AdminBundle\Security\Voter\SiteSplitVoter;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use AdminBundle\Service\ChargeBee\SubscriptionService;
use RuntimeException;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminExtensionInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

abstract class BaseAdmin extends AbstractAdmin
{
    /**
     * @var bool
     */
    protected $useRepoQuery = false;

    /**
     * @var SiteSplitService
     */
    protected $siteSplitService;

    /**
     * @var SiteSplitVoter
     */
    protected $siteSplitVoter;

    /**
     * @var SubscriptionService
     */
    protected $subscriptionService;

    /**
     * @var AccessDecisionManagerInterface
     */
    protected $decisionManager;

    /**
     * @return bool
     */
    protected function useRepoQuery():bool
    {
        return $this->useRepoQuery;
    }

    /**
     * @return SiteSplitService
     */
    public function getSiteSplitService(): SiteSplitService
    {
        if (is_null($this->siteSplitService)) {
            $this->siteSplitService = $this->getConfigurationPool()->getContainer()->get('admin.charge_bee.site_split');
        }

        return $this->siteSplitService;
    }

    /**
     * @param AccessDecisionManagerInterface $decisionManager
     *
     * @return BaseAdmin
     */
    public function setDecisionManager(AccessDecisionManagerInterface $decisionManager): self
    {
        $this->decisionManager = $decisionManager;

        return $this;
    }

    /**
     * @return AccessDecisionManagerInterface
     */
    public function getDecisionManager(): AccessDecisionManagerInterface
    {
        if (is_null($this->decisionManager)) {
            throw new RuntimeException('decisionManager is not provided!');
        }

        return $this->decisionManager;
    }

    /**
     * @param string $featureName
     *
     * @return bool
     */
    protected function isSiteFeatureGranted(string $featureName): bool
    {
        $token = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken();
        $siteSplitService = $this->getSiteSplitService();

        return $this->getDecisionManager()->decide($token, [$featureName], $siteSplitService);
    }

    /**
     * @return SiteSplitVoter
     */
    public function getSiteSplitVoter(): SiteSplitVoter
    {
        if (is_null($this->siteSplitVoter)) {
            $this->siteSplitVoter = $this->getConfigurationPool()->getContainer()->get('admin.site_split_voter');
        }

        return $this->siteSplitVoter;
    }

    /**
     * @return SubscriptionService
     */
    public function getSubscriptionService(): SubscriptionService
    {
        if (is_null($this->subscriptionService)) {
            $this->subscriptionService = $this->getConfigurationPool()->getContainer()->get(
                'admin.charge_bee.subscription'
            );
        }

        return $this->subscriptionService;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var AdminQueryInterface $repository */
        $repository = $this->modelManager->getEntityManager($this->getClass())->getRepository($this->getClass());

        if ($this->useRepoQuery() === true) {
            $queryBuilder = $repository->getAdminQuery($this->getQueryParams());
            $proxiedQuery = new ProxyQuery($queryBuilder);

            /** @var AdminExtensionInterface $extension */
            foreach ($this->extensions as $extension) {
                $extension->configureQuery($this, $proxiedQuery);
            }

            return $proxiedQuery;
        } else {
            return parent::createQuery($context);
        }
    }

    /**
     * Gets Current scope User from Container
     *
     * @return User
     */
    protected function getUser() : User
    {
        $user = $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        return $this->getConfigurationPool()
            ->getContainer()
            ->get('admin.user_group_service')
            ->getUserOrUserSupervisor($user);
    }

    /**
     * Gets Current User from Container
     *
     * @return User
     */
    protected function getRealUser() : User
    {
        return $this->getConfigurationPool()
            ->getContainer()
            ->get('security.token_storage')
            ->getToken()
            ->getUser();
    }

    /**
     * Shortcut for isGranted in admin classes
     *
     * @param string $role
     *
     * @return bool
     */
    protected function checkRole(string $role) : bool
    {
        return $this->getConfigurationPool()
            ->getContainer()
            ->get('security.authorization_checker')
            ->isGranted($role);
    }

    /**
     * Get admin user
     *
     * @return User
     */
    protected function getAdminUser() : User
    {
        return $this->getConfigurationPool()
            ->getContainer()
            ->get('admin.admin_user')
            ->getAdminUser();
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function addActionToListMapper(ListMapper $listMapper)
    {
        $actions = [
            'actions' => [
                'synchronize' => [
                    'template' => 'AdminBundle:CRUD:list__action_synchronize.html.twig'
                ],
                'delete' =>
                [
                    'template' => 'AdminBundle:CRUD:list__action_delete.html.twig'
                ],
                'clone' => [
                    'template' => 'AdminBundle:CRUD:list__action_clone.html.twig'
                ]
            ]
        ];

        $listMapper->add('_action', null, $actions);
    }

    /**
     * @param RouteCollection $collection
     */
    protected function addActionsToRouteCollection(RouteCollection $collection)
    {
        $collection->add('synchronize', $this->getRouterIdParameter() . '/synchronize');
        $collection->add('clone', $this->getRouterIdParameter() . '/clone');
    }

    /**
     * Get options for sonata_type_collection images field
     *
     * @return array
     */
    protected function getOptionsForImagesField() : array
    {
        return $this->getSubject()->getImages()->count() ? ['btn_add' => false] : [];
    }

    /**
     * @return array
     */
    protected function getQueryParams(): array
    {
        throw new RuntimeException('you have to implement ' . __METHOD__ . ' in child classes');
    }

    /**
     * Callback filter to find item by it sequential id
     *
     * @param $queryBuilder
     * @param $alias
     * @param $field
     * @param $value
     * @return bool
     */
    public function getSequentialIdFilter($queryBuilder, $alias, $field, $value)
    {
        if (empty($value['value'])) {
            return false;
        }

        $user = $this->getUser();

        $queryBuilder
            ->andWhere(sprintf('%1$s.%2$s = :%2$s', $alias, $field))
            ->andWhere(sprintf('%1$s.owner = :user', $alias))
            ->setParameter($field, $value['value'])
            ->setParameter('user', $user)
        ;

        return true;
    }
}
