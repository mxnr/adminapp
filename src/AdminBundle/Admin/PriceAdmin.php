<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;

class PriceAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_price';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-price';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->innerJoin($query->getRootAliases()[0] . '.users', 'u');
        $query->where('u.id = :user_id');
        $query->setParameter('user_id', $this->getUser()->getId());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Price')
            ->with('General', ['class' => 'col-md-6'])
            ->add(
                'value',
                MoneyType::class,
                [
                    'divisor' => 100,
                    'currency' => 'USD',
                    'required' => false,
                ]
            );
        if ($this->getRoot()->getClass() == Price::class) {
            $formMapper
                ->add(
                    'model',
                    'sonata_type_model',
                    [
                        'required' => false,
                        'expanded' => false,
                        'multiple' => false,
                        'query' => $this->modelManager
                            ->getEntityManager(Model::class)
                            ->getRepository('ThreeWebOneEntityBundle:Model')
                            ->getUserModelsQuery($this->getUser()),
                    ],
                    [
                        'admin_code' => 'admin.model',
                    ]
                );
        }
        $formMapper
            ->add(
                'priceType',
                'sonata_type_model',
                [
                    'required' => false,
                    'expanded' => false,
                    'multiple' => false,
                    'query' => $this->modelManager
                        ->getEntityManager(PriceType::class)
                        ->getRepository('ThreeWebOneEntityBundle:PriceType')
                        ->getUserPriceTypesBuybackAndRepairQuery($this->getUser()),
                ],
                [
                    'admin_code' => 'admin.price_type',
                ]
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('value')
            ->add(
                'model',
                'doctrine_orm_model_autocomplete',
                ['admin_code' => 'admin.model'],
                null,
                [
                    'property' => 'title',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('value')
            ->addIdentifier(
                'model',
                null,
                [
                    'associated_property' => 'title',
                    'admin_code' => 'admin.model',
                ]
            );
    }

    /**
     * @param Price $price
     */
    public function prePersist($price)
    {
        $price->addUser($this->getUser());
        $price->setOwner($this->getUser());
    }
}
