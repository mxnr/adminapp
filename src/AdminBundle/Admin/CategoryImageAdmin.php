<?php

namespace AdminBundle\Admin;

class CategoryImageAdmin extends ImageAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_category_image';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-category-image';
}
