<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Status\StatusInterface;

class InventorySellItemsAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_inventory_sell_items';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-inventory-sell-items';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $alias = $query->getRootAliases()[0];
        $query->innerJoin($alias . '.priceType', 'pt')
            ->innerJoin($alias . '.price', 'p')
            ->innerJoin($alias . '.model', 'm')
            ->innerJoin($alias . '.category', 'c')
            ->Where($alias . '.status = :status')
            ->andWhere($alias . '.owner = :owner')
            ->setParameter('status', InventoryItem::STATUS_ACTIVE)
            ->setParameter('owner', $this->getUser());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id')
            ->add('model.title')
            ->add(
                'priceValue',
                null,
                [
                    'label' => 'Selling price',
                    'template' => '@Admin/List/price.html.twig',
                ]
            )
            ->add(
                'quantity',
                null,
                [
                    'label' => 'Quantity available'
                ]
            )
            ->add(
                'status',
                'choice',
                [
                    'editable' => true,
                    'choices' => array_flip(StatusInterface::STATUS_ARRAY),
                ]
            )
            ->add('createdAt');
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
}
