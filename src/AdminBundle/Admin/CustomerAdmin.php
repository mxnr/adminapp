<?php
namespace AdminBundle\Admin;

use AdminBundle\Admin\Sequential\SequentialTrait;
use AdminBundle\Form\Type\ThreeWebTextType;
use AdminBundle\Service\CustomerService;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CustomerAdmin extends BaseAdmin
{
    use SequentialTrait;

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_customer';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-customer';

    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @param CustomerService $customerService
     */
    public function setCustomerService(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->where($query->getRootAliases()[0] . '.owner = :owner');
        $query->setParameter('owner', $this->getUser());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->setFormTheme(['@Admin/form/form_theme.html.twig']);
        $this->formOptions['validation_groups'] = array('Profile', 'Registration');
        $formMapper
            ->tab('Customer')
            ->with('Customer', ['class' => 'col-md-6'])
            ->add(
                'company',
            TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'firstname',
                ThreeWebTextType::class,
                [
                    'required' => false,
                    'parent_div_class' => 'left-half'
                ]
            )
            ->add(
                'lastName',
                ThreeWebTextType::class,
                [
                    'required' => false,
                    'parent_div_class' => 'right-half'
                ]
            )
            ->add(
                'email',
                EmailType::class
            )
            ->add(
                'businessPhone',
                TextType::class,
                [
                    'label' => 'Business phone',
                    'required' => false
                ]
            )
            ->add(
                'phone',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'Mobile phone'
                ]
            )
            ->add(
                'driverLicense',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'ID\Drivers license'
                ]
            )
            ->add(
                'addressLine1',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'addressLine2',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'city',
                ThreeWebTextType::class,
                [
                    'required' => false,
                    'parent_div_class' => 'left-half',
                ]
            )
            ->add(
                'zip',
                ThreeWebTextType::class,
                [
                    'required' => false,
                    'parent_div_class' => 'right-half',
                    'label' => 'Zip/postal code'
                ]
            )
            ->add(
                'country',
                ThreeWebTextType::class,
                [
                    'required' => false,
                    'parent_div_class' => 'left-half',
                ]
            )
            ->add(
                'state',
                ThreeWebTextType::class,
                [
                    'required' => false,
                    'parent_div_class' => 'right-half',
                ]
            )
            ->add(
                'note',
                TextType::class,
                [
                    'required' => false,
                ]
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add(
                'storeId',
                CallbackFilter::class,
                [
                    'callback' => [$this, 'getSequentialIdFilter'],
                    'field_type' => 'text',
                    'label' => 'id'
                ]
            )
            ->add('username')
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('phone');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add(
                'id',
                null,
                [
                    'template' => 'AdminBundle:List:list_customer_action_details.html.twig',
                ]
            )
            ->add('company', null, ['label' => 'Business name'])
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('phone');
    }

    /**
     * @param Customer $customer
     */
    public function prePersist($customer)
    {
        //I think there should be logic of sending email for new customers created by admin
        $customer->setOwner($this->getUser());
        $customer->setPlainPassword($this->customerService->generateRandomPassword());
    }
}
