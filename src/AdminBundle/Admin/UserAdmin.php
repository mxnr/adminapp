<?php

namespace AdminBundle\Admin;

use AdminBundle\Service\DomainNameService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ThreeWebOneEntityBundle\Entity\Domain\Zone;
use ThreeWebOneEntityBundle\Entity\User;

class UserAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_user';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-user';

    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @var DomainNameService
     */
    protected $domainNameService;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * Default roles for client admins
     * @var array
     */
    protected $defaultRoles = [
        'Category' => 'ROLE_ADMIN_CATEGORY_ALL',
        'Provider' => 'ROLE_ADMIN_PROVIDER_ALL',
        'Product' => 'ROLE_ADMIN_PRODUCT_ALL',
        'Model' => 'ROLE_ADMIN_MODEL_ALL',
        'Filter' => 'ROLE_ADMIN_PARENT_FILTER_ALL',
        'Price type' => 'ROLE_ADMIN_PRICE_TYPE_ALL',
        'Inventory item' => 'ROLE_ADMIN_INVENTORY_ITEM_ALL',
        'Inventory item in income list' => 'ROLE_ADMIN_BASE_INVENTORY_ITEM_ALL',
        'Customer' => 'ROLE_ADMIN_CUSTOMER_ALL',
        'Income list' => 'ROLE_ADMIN_SELL_ORDER_ALL',
        'Active listing' => 'ROLE_ADMIN_INVENTORY_SELL_ITEMS_ALL',
        'Design' => 'ROLE_ADMIN_USER_CONFIG_CONFIG_ALL',
        'SEO' => 'ROLE_ADMIN_CONFIG_SEO_ALL',
        'Question' => 'ROLE_ADMIN_CONFIG_QUESTION_ALL',
        'Location' => 'ROLE_ADMIN_CONFIG_LOCATION_ALL',
        'Testimonial' => 'ROLE_ADMIN_CONFIG_TESTIMONIAL_ALL',
        'Order' => 'ROLE_ADMIN_ORDER_ALL',
        'Payment provider' => 'ROLE_ADMIN_PAYMENT_PROVIDER_ALL',
        'Order item' => 'ROLE_ADMIN_ORDER_ITEM_ALL',
        'Page' => 'ROLE_ADMIN_PAGE_ALL',
        'Price' => 'ROLE_ADMIN_BATCH_PRICE_ALL',
        'Images' => 'ROLE_IMAGES',
    ];

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        if (!$this->checkRole('ROLE_SUPER_ADMIN')) {
            $query->where($query->getRootAliases()[0] . '.supervisor = :user_id');
            $query->setParameter('user_id', $this->getUser()->getId());
        }

        return $query;
    }

    /**
     * @param RouteCollection $collection
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }

    /**
     * {@inheritdoc}
     */
    public function getFormBuilder()
    {
        $this->formOptions['data_class'] = $this->getClass();

        $options = $this->formOptions;
        $options['validation_groups'] =
            (!$this->getSubject() || is_null($this->getSubject()->getId())) ? 'Registration' : 'Profile';

        $formBuilder = $this->getFormContractor()->getFormBuilder($this->getUniqid(), $options);

        $this->defineFormBuilder($formBuilder);

        return $formBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFields()
    {
        // avoid security field to be exported
        return array_filter(
            parent::getExportFields(),
            function ($v) {
                return !in_array($v, array('password', 'salt'));
            }
        );
    }

    /**
     * @param User $user
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function preUpdate($user)
    {
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);

        $this->updateStoreAddress($user);
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }

    /**
     * @param UserManagerInterface $userManager
     */
    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param User $user
     *
     * @return string
     */
    private function getStoreAddressForUser(User $user): string
    {
        $storeAddress = $user->getStoreAddress();

        if (!is_null($storeAddress)) {
            if ($user->isStoreHasOwnAddress() === false) {
                $frontDomain = $this->getDomainNameService()->getCloudFrontServiceHost();
                if (strpos($frontDomain, '.') !== 0) {
                    $frontDomain = '.' . $frontDomain;
                }

                if (stripos($storeAddress, $frontDomain) === false) {
                    $storeAddress = $storeAddress . $frontDomain;
                }
            }
        }

        return $storeAddress;
    }

    /**
     * @param User $user
     */
    private function updateStoreAddress(User $user)
    {
        $isTrial = $this->getSubscriptionService()->getIsSubscriptionTrial();

        if ($isTrial) {
            $user->setStoreHasOwnAddress(false);
        }

        $entityManager = $this->getEntityManager();

        $oldEntity = $entityManager->getUnitOfWork()->getOriginalEntityData($user);
        $oldDomain = $oldEntity['storeAddress'];
        $oldZone = $oldEntity['domainZone'];

        $newDomain = $user->getStoreAddress();

        if ($this->getDomainNameService()->isServiceEnabledFlag() &&
            ($oldDomain !== $newDomain || empty($oldZone))
        ) {
            $zoneCanBeAdded = true;
            if (!empty($oldZone)) {
                if ($this->clearZone($oldZone)) {
                    $entityManager->remove($oldZone);
                    $entityManager->flush();
                } else {
                    $zoneCanBeAdded = false;
                }
            }

            if ($zoneCanBeAdded) {
                if ($isTrial === false && $user->isStoreHasOwnAddress()) {
                    $newZone = (new Zone())
                        ->setOwner($user)
                        ->setDomain($newDomain);

                    if ($this->createZone($newDomain, $newZone)) {
                        $entityManager->persist($newZone);
                        $user->setDomainZone($newZone);
                        $entityManager->flush();
                    }
                } else {
                    $user->setDomainZone();
                }
            } else {
                //got error, revert changes
                $this->getSession()->getFlashBag()->add(
                    'sonata_flash_error',
                    'Unable to update/create domain for your store.'
                );
                $user->setStoreAddress($oldDomain);
            }

            $user->setStoreAddress($this->getStoreAddressForUser($user));
        }
    }

    /**
     * @return EntityManagerInterface|null
     */
    public function getEntityManager(): ?EntityManagerInterface
    {
        if (!$this->hasEntityManager()) {
            $this->setEntityManager(
                $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager')
            );
        }

        return $this->entityManager;
    }

    /**
     * @return bool
     */
    public function hasEntityManager(): bool
    {
        return !is_null($this->entityManager);
    }

    /**
     * @param EntityManagerInterface|null $entityManager
     *
     * @return UserAdmin
     */
    public function setEntityManager(EntityManagerInterface $entityManager = null): UserAdmin
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @return DomainNameService|null
     */
    public function getDomainNameService(): ?DomainNameService
    {
        if (!$this->hasDomainNameService()) {
            $this->setDomainNameService(
                $this->getConfigurationPool()->getContainer()->get('admin.domain_name_service')
            );
        }

        return $this->domainNameService;
    }

    /**
     * @return bool
     */
    public function hasDomainNameService(): bool
    {
        return !is_null($this->domainNameService);
    }

    /**
     * @param DomainNameService|null $domainNameService
     *
     * @return UserAdmin
     */
    public function setDomainNameService(DomainNameService $domainNameService = null): UserAdmin
    {
        $this->domainNameService = $domainNameService;

        return $this;
    }

    /**
     * @param Zone $zone
     *
     * @return bool
     */
    private function clearZone(Zone $zone): bool
    {
        if ($this->getDomainNameService()->getCloudFrontType() === DomainNameService::CLOUFDRONT_TYPE_CF) {
            return
                $this->getDomainNameService()->removeCname($zone->getDomain()) &&
                $this->getDomainNameService()->clearRecordsForZone($zone) &&
                $this->getDomainNameService()->unlinkZone($zone);
        } else {
            return
                $this->getDomainNameService()->clearRecordsForZone($zone) &&
                $this->getDomainNameService()->unlinkZone($zone);
        }
    }

    /**
     * @param string    $domain
     * @param Zone|null $zone
     *
     * @return bool
     */
    private function createZone(string $domain, Zone $zone = null): bool
    {
        if (is_null($zone)) {
            $zone = new Zone();
        }

        if ($this->getDomainNameService()->getCloudFrontType() === DomainNameService::CLOUFDRONT_TYPE_CF) {
            if ($this->getDomainNameService()->addCname($domain) === true) {
                return (
                    $this->getDomainNameService()->linkZone($zone) &&
                    $this->getDomainNameService()->addCloudFrontAlias($zone)
                );
            }
        } else {
            return (
                $this->getDomainNameService()->linkZone($zone) &&
                $this->getDomainNameService()->addRecord(
                    $zone,
                    'A',
                    $this->getDomainNameService()->getCloudFrontDistributionHost(),
                    true
                )
            );
        }

        return false;
    }

    /**
     * @return SessionInterface|null
     */
    public function getSession(): ?SessionInterface
    {
        if (!$this->hasSession()) {
            $this->setSession(
                $this->getConfigurationPool()->getContainer()->get('session')
            );
        }

        return $this->session;
    }

    /**
     * @return bool
     */
    public function hasSession(): bool
    {
        return !is_null($this->session);
    }

    /**
     * @param SessionInterface|null $session
     *
     * @return UserAdmin
     */
    public function setSession(SessionInterface $session = null): UserAdmin
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @param User $user
     */
    public function prePersist($user)
    {
        $user->setSupervisor($this->getUser());
        $user->setStoreName($this->getUser()->getStoreName());
        $user->setStoreAddress($this->getUser()->getStoreAddress());
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, ['label' => 'id'])
            ->addIdentifier('username')
            ->add('email')
            ->add('enabled', null, array('editable' => true))
            ->add('createdAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper
            ->add('id')
            ->add('username')
            ->add('email')
            ->add('groups');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General1')
            ->add('username')
            ->add('email')
            ->end()
            ->with('Groups')
            ->add('groups')
            ->end()
            ->with('Profile')
            ->add('dateOfBirth')
            ->add('firstname')
            ->add('lastname')
            ->add('website')
            ->add('biography')
            ->add('gender')
            ->add('locale')
            ->add('timezone')
            ->add('phone')
            ->end()
            ->with('Social')
            ->add('facebookUid')
            ->add('facebookName')
            ->add('twitterUid')
            ->add('twitterName')
            ->add('gplusUid')
            ->add('gplusName')
            ->end()
            ->with('Security')
            ->add('token')
            ->add('twoStepVerificationCode')
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        //Can access to only yourself acc or to acc. of sub or to all if has SUPER_ADMIN_ROLE
        /** @var User $user */
        $user = $this->getSubject();

        if (!$this->checkRole('ROLE_SUPER_ADMIN') && $user != $this->getRealUser()) {
            if (
                !$this->checkRole('ROLE_ADMIN') ||
                (
                    $this->checkRole('ROLE_ADMIN') &&
                    $user->getSupervisor() != $this->getRealUser() &&
                    $user->getId()
                )
            ) {
                throw new NotFoundHttpException();
            }
        }

        //set all permissions to sub acc on create
        if ($this->checkRole('ROLE_ADMIN') && !$user->getId()) {
            $user->setRoles(array_values($this->defaultRoles));
        }

        // NEXT_MAJOR: Keep FQCN when bumping Symfony requirement to 2.8+.
        if (method_exists('Symfony\Component\Form\AbstractType', 'getBlockPrefix')) {
            $textType = 'Symfony\Component\Form\Extension\Core\Type\TextType';
            $securityRolesType = 'Sonata\UserBundle\Form\Type\SecurityRolesType';
        } else {
            $textType = 'text';
            $securityRolesType = 'sonata_security_roles';
        }

        $formMapper
            ->tab('User')
            ->with('General', ['class' => 'col-md-6'])
            ->add('username')
            ->add(
                'email',
                null,
                [
                    'label' => 'Email',
                ]
            )
            ->add(
                'plainPassword',
                $textType,
                [
                    'required' => (!$user || is_null($user->getId())),
                    'label' => 'Password',
                ]
            )
            ->end();
        if (
            $user != $this->getRealUser() && (
                $this->checkRole('ROLE_SUPER_ADMIN') ||
                $this->checkRole('ROLE_ADMIN') && $user->getSupervisor() == $this->getRealUser() ||
                $user->getId() === null
            )
        ) {
            $formMapper
                ->with('Status', ['class' => 'col-md-4'])
                ->add('enabled', null, ['required' => false])
                ->end();
        }
        if (
            $user != $this->getRealUser() && (
                !$user->getId() ||
                $this->checkRole('ROLE_SUPER_ADMIN') && !$user->hasRole('ROLE_ADMIN') ||
                $this->checkRole('ROLE_ADMIN') && $user->getSupervisor() == $this->getRealUser()
            )
        ) {
            $formMapper
                ->with('Roles', ['class' => 'col-md-12'])
                ->add(
                    'realRoles',
                    $securityRolesType,
                    [
                        'label' => 'Permissions',
                        'expanded' => true,
                        'multiple' => true,
                        'required' => false,
                        'choices' => $this->defaultRoles,
                    ]
                )
                ->end();
        }
        $host = $this->getDomainNameService()->getCloudFrontServiceHost();
        $formMapper->end();
        if ($this->checkRole('ROLE_SUPER_ADMIN') && $user->hasRole('ROLE_ADMIN') ||
            $this->checkRole('ROLE_ADMIN') && $user == $this->getRealUser()
        ) {
            $formMapper->tab(
                'Store',
                ['description' => 'This section contains settings of your store.']
            )
                ->with('Config', ['class' => 'col-md-12'])
                ->add(
                    'storeName',
                    'text',
                    ['label' => 'Store name', 'required' => true, 'help' => 'Name of your store.']
                );

            if ($this->getSubscriptionService()->getIsSubscriptionTrial()) {
                $formMapper
                    ->add(
                        'storeAddress',
                        'text',
                        [
                            'label' => 'Store address in the world wide web',
                            'required' => true,
                            'help' => 'Specify the subdomain of the' . $host . '. For example jimmystore.' . $host,
                        ]
                    );

            } else {
                $formMapper
                    ->add(
                        'storeAddress',
                        'text',
                        [
                            'label' => 'Store address in the world wide web',
                            'required' => true,
                            'help' => 'Specify the subdomain of the ' . $host . '. For example jimmystore.' .
                                $host . ' or specify full qualified domain name with the checkbox below.',
                        ]
                    )
                    ->add(
                        'storeHasOwnAddress',
                        CheckboxType::class,
                        [
                            'label' => 'The store has own address',
                            'required' => false,
                            'help' => 'Use this checkbox if you want to provide already registered by your own domain name.',
                        ]
                    );
            }

            if ($user->hasDomainZone()) {
                //clearing old queue.
                $this->getSession()->getFlashBag()->get('sonata_flash_success');
                $this->getSession()->getFlashBag()->add(
                    'sonata_flash_success',
                    'To proper handle of the domain by our system, you have to set this list of nameservers: ' .
                    implode(', ', $user->getDomainZone()->getNsList()) .
                    ' at settings page of the registrar of your domain.'
                );
            }

            $formMapper->end();

        }
    }
}
