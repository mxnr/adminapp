<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use ThreeWebOneEntityBundle\Entity\QuickLink;

class QuickLinksAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_quick_links';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-quick-links';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->where($query->getRootAliases()[0] . '.owner = :owner');
        $query->setParameter('owner', $this->getUser());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Link')
            ->with('General', ['class' => 'col-md-6'])
            ->add('title', 'text')
            ->add('link', 'text')
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add(
                'title',
                null,
                [
                    'show_filter' => true
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('title')
            ->add(
                'link',
                'choice',
                [
                    'template' => '@Admin/List/quick_link_link.html.twig',
                ]
            );
    }

    /**
     * @param QuickLink $quickLink
     */
    public function prePersist($quickLink)
    {
        $quickLink->setOwner($this->getUser());
    }
}
