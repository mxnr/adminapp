<?php

namespace AdminBundle\Admin\Billing;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use ThreeWebOneEntityBundle\Entity\Billing\PlanInterface;

/**
 * Class PlanAdmin.
 */
class PlanAdmin extends AbstractBillingAdmin
{
    /**
     * Route Name.
     *
     * @var string
     */
    protected $baseRouteName = 'admin_billing_plans';

    /**
     * Route Pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = 'billing-plans';

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'show']);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->add('name')
            ->add('price', null, ['template' => '@Admin/Billing/List/int_price.html.twig'])
            ->add('currencyCode')
            ->add(
                'chargeIntervalUnit',
                'choice',
                [
                    'choices' => [
                        PlanInterface::PERIOD_MONTH => 'Monthly',
                        PlanInterface::PERIOD_YEAR => 'Yearly',
                    ],
                ]
            )
            ->add('active')
            ->add('hasTrial');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->tab('Plan')
            ->with('General', ['class' => 'col-md-8'])
            ->add('id')
            ->add('name')
            ->add('price', null, ['template' => '@Admin/Billing/Show/int_price.html.twig'])
            ->add('currencyCode')
            ->add('chargeInterval')
            ->add(
                'chargeIntervalUnit',
                'choice',
                [
                    'choices' => [
                        PlanInterface::PERIOD_MONTH => 'Month',
                        PlanInterface::PERIOD_YEAR => 'Year',
                    ],
                ]
            )
            ->add('active')
            ->add('hasTrial')
            ->end()
            ->with('Description')
            ->add('description')
            ->end()
            ->end();
    }
}
