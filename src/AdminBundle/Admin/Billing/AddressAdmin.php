<?php

namespace AdminBundle\Admin\Billing;

use ChargeBeeBundle\Packet\Customers\UpdateBillingInfo;
use ChargeBeeBundle\Packet\Embed\Address;
use ChargeBeeBundle\Service\CustomersService;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use ThreeWebOneEntityBundle\Entity\Billing\Address as AddressEntity;

/**
 * Class AddressAdmin.
 */
class AddressAdmin extends AbstractBillingAdmin
{
    /**
     * Route Name.
     *
     * @var string
     */
    protected $baseRouteName = 'admin_billing_address';

    /**
     * Route Pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = 'billing-address';

    /**
     * @var CustomersService
     */
    private $customersService;

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $qb */
        $qb = parent::createQuery($context);

        $qb
            ->leftJoin($qb->getRootAliases()[0] . '.customer', 'bc')
            ->leftJoin('bc.user', 'u')
            ->andWhere('u.id = :me')
            ->setParameter('me', $this->getUser()->getId());

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function postUpdate($object)
    {
        $result = parent::postUpdate($object);
        if (is_null($result)) {
            $result = $this->updateChargeBeeBillingAddress($object);
        }

        return $result;
    }

    /**
     * @param AddressEntity $address
     */
    protected function updateChargeBeeBillingAddress(AddressEntity $address)
    {
        if ($this->hasCustomersService()) {
            $customersService = $this->getCustomersService();

            $billingAddress = new Address();
            $gotDataForSend = false;

            if ($address->hasFirstName()) {
                $gotDataForSend = true;
                $billingAddress->setFirstName($address->getFirstName());
            }

            if ($address->hasLastName()) {
                $gotDataForSend = true;
                $billingAddress->setLastName($address->getLastName());
            }

            if ($address->hasEmail()) {
                $gotDataForSend = true;
                $billingAddress->setEmail($address->getEmail());
            }

            if ($address->hasCompany()) {
                $gotDataForSend = true;
                $billingAddress->setCompany($address->getCompany());
            }

            if ($address->hasPhone()) {
                $gotDataForSend = true;
                $billingAddress->setPhone($address->getPhone());
            }

            if ($address->hasLine1()) {
                $billingAddress->setLine1($address->getLine1());
            }

            if ($address->hasLine2()) {
                $gotDataForSend = true;
                $billingAddress->setLine2($address->getLine2());
            }

            if ($address->hasLine3()) {
                $gotDataForSend = true;
                $billingAddress->setLine3($address->getLine3());
            }

            if ($address->hasCity()) {
                $gotDataForSend = true;
                $billingAddress->setCity($address->getCity());
            }

            if ($address->hasStateCode()) {
                $gotDataForSend = true;
                $billingAddress->setStateCode($address->getStateCode());
            }

            if ($address->hasState()) {
                $gotDataForSend = true;
                $billingAddress->setState($address->getState());
            }

            if ($address->hasCountry()) {
                $gotDataForSend = true;
                $billingAddress->setCountry($address->getCountry());
            }

            if ($address->hasZip()) {
                $gotDataForSend = true;
                $billingAddress->setZip($address->getZip());
            }

            if (true === $gotDataForSend) {
                $updateBillingInfoMethod = (new UpdateBillingInfo())
                    ->setId($address->getCustomer()->getChargeBeeId())
                    ->setBillingAddress($billingAddress);

                $customersService->updateBillingInfo($updateBillingInfoMethod);
            }
        }
    }

    /**
     * @return bool
     */
    public function hasCustomersService(): bool
    {
        return !empty($this->customersService);
    }

    /**
     * @return CustomersService
     */
    public function getCustomersService(): CustomersService
    {
        return $this->customersService;
    }

    /**
     * @param CustomersService $customersService
     *
     * @return AddressAdmin
     */
    public function setCustomersService(CustomersService $customersService): self
    {
        $this->customersService = $customersService;

        return $this;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['edit']);
    }

    /**
     * @param FormMapper $form
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('firstName')
            ->add('lastName')
            ->add('email')
            ->add('company')
            ->add('phone')
            ->add('line1')
            ->add('line2')
            ->add('line3')
            ->add('city')
            ->add('stateCode')
            ->add('state')
            ->add('zip')
            ->add('country');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->add('country')
            ->add('state')
            ->add('city')
            ->add('line1')
            ->add('firstName')
            ->add('lastName');
    }
}
