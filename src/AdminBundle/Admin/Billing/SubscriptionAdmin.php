<?php

namespace AdminBundle\Admin\Billing;

use AdminBundle\Service\ChargeBee\CustomerService;
use AdminBundle\Service\ChargeBee\SubscriptionService;
use AdminBundle\Service\UpgradeService;
use ChargeBeeBundle\Service\SubscriptionsService;
use Doctrine\ORM\QueryBuilder;
use RuntimeException;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Billing\PlanInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Entity\Billing\SubscriptionInterface;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class SubscriptionInfoAdmin.
 */
class SubscriptionAdmin extends AbstractBillingAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_billing_subscriptions';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'billing-subscriptions';

    /**
     * @var SubscriptionsService
     */
    private $chargeBeeSubscriptionsService;


    /**
     * @var CustomerService
     */
    private $customerService;

    /**
     * @var SubscriptionService
     */
    private $adminSubscriptionService;

    /**
     * @var UpgradeService
     */
    private $upgradeService;

    /**
     * @var ValidatorInterface
     */
    private $validatorService;

    /**
     * @return CustomerService
     */
    public function getCustomerService(): CustomerService
    {
        return $this->customerService;
    }

    /**
     * @return bool
     */
    public function hasCustomerService(): bool
    {
        return $this->customerService !== null;
    }

    /**
     * @param CustomerService|null $customerService
     *
     * @return SubscriptionAdmin
     */
    public function setCustomerService(CustomerService $customerService = null): SubscriptionAdmin
    {
        $this->customerService = $customerService;

        return $this;
    }

    /**
     * @return SubscriptionsService
     */
    public function getChargeBeeSubscriptionsService(): SubscriptionsService
    {
        return $this->chargeBeeSubscriptionsService;
    }

    /**
     * @return bool
     */
    public function hasChargeBeeSubscriptionsService(): bool
    {
        return !empty($this->chargeBeeSubscriptionsService);
    }

    /**
     * @param SubscriptionsService|null $chargeBeeSubscriptionsService
     *
     * @return SubscriptionAdmin
     */
    public function setChargeBeeSubscriptionsService(
        SubscriptionsService $chargeBeeSubscriptionsService = null
    ): SubscriptionAdmin {
        $this->chargeBeeSubscriptionsService = $chargeBeeSubscriptionsService;

        return $this;
    }

    /**
     * @return SubscriptionService
     */
    public function getAdminSubscriptionService(): SubscriptionService
    {
        return $this->adminSubscriptionService;
    }

    /**
     * @return bool
     */
    public function hasAdminSubscriptionService(): bool
    {
        return !empty($this->adminSubscriptionService);
    }

    /**
     * @param SubscriptionService|null $adminSubscriptionService
     *
     * @return SubscriptionAdmin
     */
    public function setAdminSubscriptionService(SubscriptionService $adminSubscriptionService = null): SubscriptionAdmin
    {
        $this->adminSubscriptionService = $adminSubscriptionService;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $qb */
        $qb = parent::createQuery($context);
        /** @var string $alias */
        $alias = $qb->getRootAliases()[0] . '.';

        $qb->innerJoin($alias . 'customer', 'bc')
            ->innerJoin('bc.user', 'u')
            ->andWhere('u.id = :me')
            ->setParameter('me', $this->getUser()->getId());

        return $qb;
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($object)
    {
        if ($this->hasCustomerService() &&
            $this->hasAdminSubscriptionService() &&
            $this->hasChargeBeeSubscriptionsService() &&
            $this->hasUpgradeService()
        ) {
            $itemsToSave = [
                SubscriptionInterface::ITEM_SELL => false,
                SubscriptionInterface::ITEM_BUY => false,
                SubscriptionInterface::ITEM_REPAIR => false,
            ];

            /** @var Subscription $subscription */
            $subscription = $object;

            /** @var User $user */
            $user = $this->getUser();

            if ($user->hasBillingCustomer()) {
                /** @var Customer $customer */
                $customer = $this->getUser()->getBillingCustomer();
            } else {
                /** @var Customer $customer */
                $customer = $this->getCustomerService()->createCustomerVia($user);
            }

            $chargeIntervalUnit = $subscription->getChargeIntervalUnit();

            $items = $subscription->getItems();

            foreach ($items as $item) {
                $itemsToSave[$item] = true;
            }

            $selectedSitesCount = count($items);

            $availablePlans = $this->getUpgradeService()->getAvailablePlansList();
            $plansForInterval = $availablePlans[$chargeIntervalUnit];

            $subscriptionPlan = null;
            foreach ($plansForInterval as $sitesCount => $planForSitesCount) {
                foreach ($planForSitesCount as $plan) {
                    if ($selectedSitesCount == $sitesCount) {
                        $subscriptionPlan = $plan;
                        break;
                    }
                }
            }

            if ($subscriptionPlan === null) {
                throw new RuntimeException(
                    'no plan found for requested set of params, probably plans was not imported'
                );
            }

            $subscriptionService = $this->getSubscriptionService();
            if ($subscriptionService->hasSubscription()) {
                $subscription = $subscriptionService->getSubscription();
                $subscriptionService->cancel($subscription);
            }

            $subscriptionService->subscribe(
                $customer,
                $subscriptionPlan,
                $itemsToSave,
                $customer->haveValidCard()
            );

            $response = new RedirectResponse($this->generateUrl('list', ['filter' => $this->getFilterParameters()]));
            $response->send();
        }
    }

    /**
     * @return UpgradeService
     */
    public function getUpgradeService(): UpgradeService
    {
        return $this->upgradeService;
    }

    /**
     * @return bool
     */
    public function hasUpgradeService(): bool
    {
        return !empty($this->upgradeService);
    }

    /**
     * @param UpgradeService|null $upgradeService
     *
     * @return SubscriptionAdmin
     */
    public function setUpgradeService(UpgradeService $upgradeService = null): SubscriptionAdmin
    {
        $this->upgradeService = $upgradeService;

        return $this;
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidatorService(): ValidatorInterface
    {
        return $this->validatorService;
    }

    /**
     * @return bool
     */
    public function hasValidatorService(): bool
    {
        return $this->validatorService !== null;
    }

    /**
     * @param ValidatorInterface|null $validatorService
     *
     * @return SubscriptionAdmin
     */
    public function setValidatorService(ValidatorInterface $validatorService = null): SubscriptionAdmin
    {
        $this->validatorService = $validatorService;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'show', 'create']);
        $collection->add('cancel', $this->getRouterIdParameter() . '/cancel');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('id')
            ->add('plan', null, ['route' => ['name' => 'show']])
            ->add('nextChargeAt')
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        SubscriptionInterface::STATUS_ACTIVE => 'Active',
                        SubscriptionInterface::STATUS_TRIAL => 'Trial period',
                        SubscriptionInterface::STATUS_CANCELLED => 'Cancelled',
                        SubscriptionInterface::STATUS_ENDING => 'Ending',
                        SubscriptionInterface::STATUS_FUTURE => 'Future',
                    ],
                ]
            )
            ->add('hasCard', null, ['label' => 'Card provided'])
            ->add(
                '_charge',
                null,
                [
                    'label' => 'Action',
                    'template' => '@Admin/Billing/List/charge.html.twig',
                ]

            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add(
                'items',
                'choice',
                [
                    'multiple' => true,
                    'expanded' => true,
                    'label' => 'Site type',
                    'choices' => [
                        'Buyback' => SubscriptionInterface::ITEM_BUY,
                        'Selling' => SubscriptionInterface::ITEM_SELL,
                        'Repair' => SubscriptionInterface::ITEM_REPAIR,
                    ],
                ]
            )
            ->add(
                'chargeIntervalUnit',
                'choice',
                [
                    'multiple' => false,
                    'expanded' => true,
                    'label' => 'Charge period',
                    'choices' => [
                        'Monthly' => PlanInterface::PERIOD_MONTH,
                        'Yearly' => PlanInterface::PERIOD_YEAR,
                    ],
                ]

            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->tab('Subscription')
            ->with('General', ['class' => 'col-md-8'])
            ->add(
                'items',
                null,
                [
                    'label' => 'Site types',
                    'template' => '@Admin/Billing/Show/sites.html.twig',
                    'choices' => [

                        SubscriptionInterface::ITEM_BUY => 'Buyback',
                        SubscriptionInterface::ITEM_SELL => 'Selling',
                        SubscriptionInterface::ITEM_REPAIR => 'Repair',
                    ],
                ]
            )
            ->add('chargeInterval')
            ->add(
                'chargeIntervalUnit',
                'choice',
                [
                    'label' => 'Charge period',
                    'choices' => [
                        PlanInterface::PERIOD_MONTH => 'Month',
                        PlanInterface::PERIOD_YEAR => 'Year',
                    ],
                ]

            )
            ->add('createdAt', 'datetime')
            ->add('startedAt', 'datetime')
            ->add('activatedAt', 'datetime')
            ->add('nextChargeAt', 'datetime')
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        SubscriptionInterface::STATUS_ACTIVE => 'Active',
                        SubscriptionInterface::STATUS_TRIAL => 'Trial period',
                        SubscriptionInterface::STATUS_CANCELLED => 'Cancelled',
                        SubscriptionInterface::STATUS_ENDING => 'Ending',
                        SubscriptionInterface::STATUS_FUTURE => 'Future',
                    ],
                ]
            )
            ->add('hasCard', null, ['label' => 'Card provided'])
            ->end()
            ->with('Trial', ['class' => 'col-md-4'])
            ->add('trialStart')
            ->add('trialEnd')
            ->end()
            ->with('Main', ['class' => 'col-md-4'])
            ->add('currentPeriodStart')
            ->add('currentPeriodEnd')
            ->end()
            ->end();
    }
}
