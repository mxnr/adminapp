<?php

namespace AdminBundle\Admin\Billing;

use AdminBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class PaymentInfoAdmin.
 */
class PaymentInfoAdmin extends BaseAdmin
{
    /**
     * Route Name.
     *
     * @var string
     */
    protected $baseRouteName = 'admin_billing_payment_info';

    /**
     * Route Pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = 'billing-payment-info';

    /**
     * {@inheritdoc}
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list']);
    }
}
