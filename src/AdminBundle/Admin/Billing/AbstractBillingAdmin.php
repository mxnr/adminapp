<?php

namespace AdminBundle\Admin\Billing;

use AdminBundle\Admin\BaseAdmin;

/**
 * Class AbstractBillingAdmin.
 *
 * This is base ancestor for all billing subsystem admin classes.
 */
class AbstractBillingAdmin extends BaseAdmin
{
}
