<?php

namespace AdminBundle\Admin;

use AdminBundle\Form\Type\ShowBarcodesButtonType;
use AdminBundle\Form\Type\ThreeWebAccessoryType;
use AdminBundle\Form\Type\ThreeWebFilterType;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\GreaterThan;
use ThreeWebOneEntityBundle\Entity\Accessory;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Image;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemImage;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\ModelImage;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\UserConfig\Seo;

class InventoryItemAdmin extends BaseInventoryItemAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_inventory_item';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-inventory-item';

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create')
            ->add('template', $this->getRouterIdParameter() . '/template');

    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->modifyRequest();
        $subject = $this->getSubject();
        if ($this->getRequest()->getMethod() == 'GET' && $subject->getMainImageId()) {
            $this->getMainImage($subject);
        }
        if (!$subject->getSeoMeta()) {
            $subject->setSeoMeta(new Seo());
        }
        $formMapper
            ->tab('Inventory Item')
            ->with('Inventory', ['class' => 'col-md-6'])
            ->add(
                'title',
                'text',
                [
                    'required' => false,
                    'attr' => [
                        'ng-model' => 'vm.title',
                        'uib-typeahead' => 'title for title in vm.titles | filter:$viewValue | limitTo:8',
                        'autocomplete' => "off"
                    ]
                ]
            )
            ->add(
                'purchasePrice',
                MoneyType::class,
                [
                    'divisor' => 100,
                    'currency' => 'USD',
                    'constraints' => new GreaterThan(['value' => 0]),
                ]
            )
            ->add(
                'quantity',
                IntegerType::class,
                [
                    'attr' => [
                        'readonly' => 'readonly'
                    ]
                ]
            );
        if ($subject && $subject->getInventoryItemBarcodes()->count()) {
            $formMapper
                ->add(
                    'showBarcodes',
                    ShowBarcodesButtonType::class,
                    [
                        'mapped' => false,
                        'required' => false,
                        'data' => $subject->getId()
                    ]
                );
        }
        $formMapper
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        'Disabled' => InventoryItem::STATUS_INACTIVE,
                        'Active' => InventoryItem::STATUS_ACTIVE,
                    ],
                ]
            )
            ->end()
            ->with('Website listing', ['class' => 'col-md-6'])
            ->add(
                'priceValue',
                MoneyType::class,
                [
                    'divisor' => 100,
                    'currency' => 'USD',
                    'required' => false,
                    'label' => 'Price',
                    'constraints' => new GreaterThan(['value' => 0]),
                ]
            )
            ->add(
                'shippingPrice',
                MoneyType::class,
                [
                    'divisor' => 100,
                    'currency' => 'USD',
                    'required' => false,
                    'label' => 'Shipping price',
                    'constraints' => new GreaterThan(['value' => 0]),
                ]
            )
            ->add(
                'priceType',
                'sonata_type_model',
                [
                    'by_reference' => true,
                    'label' => 'Condition',
                    'required' => false,
                    'expanded' => false,
                    'multiple' => false,
                    'property' => 'title',
                    'query' => $this->modelManager
                        ->getEntityManager(PriceType::class)
                        ->getRepository(PriceType::class)
                        ->getUserPriceTypesSellQuery($this->getUser()),
                ],
                [
                    'admin_code' => 'admin.price_type',
                ]
            )
            ->add(
                'category',
                'sonata_type_model',
                [
                    'by_reference' => true,
                    'required' => false,
                    'expanded' => false,
                    'multiple' => false,
                    'query' => $this->modelManager
                        ->getEntityManager(Category::class)
                        ->getRepository(Category::class)
                        ->getUserCategoriesByTypeQuery($this->getUser(), Category::TYPE_CATEGORY),
                ],
                [
                    'admin_code' => 'admin.category',
                ]
            )
            ->end()
            ->with('Filters', ['class' => 'col-md-6'])
            ->add(
                'filters',
                ThreeWebFilterType::class,
                [
                    'required' => false,
                    'expanded' => true,
                    'multiple' => true,
                    'label' => false,
                    'query' => $this->modelManager
                        ->getEntityManager(Filter::class)
                        ->getRepository(Filter::class)
                        ->getUserFiltersQuery($this->getUser()),
                ],
                [
                    'admin_code' => 'admin.parent_filter',
                ]
            )
            ->end()
            ->with('Description', ['class' => 'col-md-6'])
            ->add(
                'description',
                'textarea',
                [
                    'required' => false
                ]
            )
            ->end()
            ->end()
            ->tab('Add images')
            ->with('Images', ['class' => 'col-md-12'])
            ->add(
                'searchImages',
                'sonata_type_model',
                [
                    'by_reference' => true,
                    'required' => false,
                    'expanded' => false,
                    'multiple' => true,
                    'mapped' => false,
                    'class' => Image::class,
                    'query' => $this->modelManager
                        ->getEntityManager(Image::class)
                        ->getRepository(Image::class)
                        ->getUserImagesForInventoryItemQuery($this->getUser()),
                ],
                [
                    'admin_code' => 'admin.model_image',
                ]
            )
            ->add(
                'images',
                'sonata_type_collection',
                [],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'admin.inventory_item_image',
                ]
            )
            ->end()
            ->end()
            ->tab('Seo')
            ->with('Seo', ['class' => 'col-md-12'])
            ->add('seoMeta.title', null, ['required' => false])
            ->add(
                'seoMeta.keyword',
                null,
                [
                    'label' => 'Meta keyword',
                    'required' => false
                ]
            )
            ->add(
                'seoMeta.description',
                'textarea',
                [
                    'label' => 'Meta description',
                    'required' => false
                ]
            )
            ->end()
            ->end();

        $builder = $formMapper->getFormBuilder();
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            if (!empty($data['status'])) {
                if (empty($data['priceType']) || empty($data['priceValue']) || empty($data['title'])) {
                    $event->getForm()->addError(new FormError(
                        'In order to push Inventory Item to sale site all necessary fields must be filled: \'Title\', \'Price type\', \'Price\''
                    ));
                }

                if (empty($data['category'])) {
                    $this->getRequest()->getSession()->getFlashBag()->add(
                        'warning',
                        'Due to empty category Item will not be displayed on site, but you can operate with it on admin side'
                    );
                }
            }
        });
    }

    /**
     * @param ListMapper $listMapper
     */
    public function configureListFields(ListMapper $listMapper)
    {
        parent::configureListFields($listMapper);
        $listMapper
            ->add('_action', null, [
                'actions' => [
                    'template' => [
                        'template' => 'AdminBundle:CRUD:list__action_template.html.twig'
                    ]
                ]
            ]);
    }

    /**
     * @param InventoryItem $item
     */
    public function preUpdate($item)
    {
        $this->manageSearchImages($this->getForm()->get('searchImages')->getData()->toArray(), $item);
        $this->removeRelations($item);
        $this->manageEmbeddedImage($item);
        if ($item->getPriceValue() && $item->getPriceType()) {
            if (!$item->getPrice()) {
                $this->inventoryItemService->createPrice($item, $this->getUser());
            } else {
                $this->inventoryItemService->updatePrice($item);
            }
        }

        if (!$item->getSeoMeta()->getId()) {
            $item->getSeoMeta()->setOwner($this->getUser())->setSite(Seo::TYPE_SALE);
            $this->modelManager->getEntityManager(InventoryItem::class)->persist($item->getSeoMeta());
        }
    }

    /**
     * @param $item
     */
    public function postUpdate($item)
    {
        $this->setMainImage($item);
        $this->modelManager->getEntityManager(InventoryItem::class)->flush();
    }

    /**
     * Manages Images Persistion. Since Image Entity has file property
     * that Doctrine doesn't knows about we need to change property of
     * Image to trigger load Event.
     *
     * @param InventoryItem $item
     */
    private function manageEmbeddedImage($item)
    {
        foreach ($item->getImages() as $image) {
            if ($item) {
                if ($image->getImageFile()) {
                    // update the Image to trigger file management
                    $image->refreshUpdated();
                    $image->addEntity($item)
                        ->setOwner($this->getUser());
                } elseif (!$image->getImageFile() && !$image->getImageName()) {
                    // prevent Sf/Sonata trying to create and persist an empty Image\
                    $item->removeImage($image);
                }
            }
        }
    }

    /**
     * @param array $images
     * @param InventoryItem $item
     */
    private function manageSearchImages(array $images, InventoryItem $item)
    {
        foreach ($images as $image) {
            if ($image instanceof ModelImage) {
                $image = (new InventoryItemImage())
                    ->setOwner($this->getUser())
                    ->setImageName($image->getImageName());
                $item->addImage($image);
                $this->modelManager->getEntityManager(InventoryItem::class)->persist($image);
            }
            if ($image instanceof InventoryItemImage) {
                $item->addImage($image);
            }
        }
    }

    /**
     * @param InventoryItem $item
     */
    private function removeRelations(InventoryItem $item)
    {
        $oldImages = $this->modelManager->getEntityManager(InventoryItemImage::class)
            ->getRepository(InventoryItemImage::class)
            ->getRelated($item);
        $images = $item->getImages();
        foreach ($oldImages as $image) {
            if (!$images->contains($image)) {
                $image->removeEntity($item);
            }
        }
    }

    /**
     * Modifies request to accept filters field
     */
    private function modifyRequest()
    {
        $request = $this->getRequest();
        if ($request->getMethod() === Request::METHOD_POST) {
            $key = key($request->request->all());
            $data = $request->request->get($key);
            if (!empty($data['filters'])) {
                $filters = $data['filters'];
                $modifiedFilters = [];
                foreach ($filters as $item) {
                    if (is_array($item)) {
                        $modifiedFilters[] = reset($item);
                    }
                }
                $data['filters'] = $modifiedFilters;
                $request->request->set($key, $data);
                $this->setRequest($request);
            }
        }
    }

    /**
     * @param InventoryItem $item
     */
    private function setMainImage(InventoryItem $item)
    {
        foreach ($item->getImages() as $image) {
            if ($image->isMain) {
                $item->setMainImageId($image->getId());

                return;
            }
        }

        return;
    }

    /**
     * @param InventoryItem $item
     */
    private function getMainImage(InventoryItem $item)
    {
        foreach ($item->getImages() as $image) {
            if ($image->getId() == $item->getMainImageId()) {
                $image->isMain = true;
                break;
            }
        }

        return;
    }
}
