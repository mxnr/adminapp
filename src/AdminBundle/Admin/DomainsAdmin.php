<?php

namespace AdminBundle\Admin;

use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use AdminBundle\Event\DomainEvent;

class DomainsAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_domains';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-domains';

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * DomainsAdmin constructor.
     * @param string $code
     * @param string $class
     * @param string $baseControllerName
     * @param EntityManager $em
     */
    public function __construct($code, $class, $baseControllerName, EntityManager $em, EventDispatcherInterface $eventDispatcher)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', ['class' => 'col-md-6'])
            ->add('domain')
            ->add('owner')
            ->add(
                'active',
                'choice',
                [
                    'choices' => [
                        'Active' => ClientDomain::ACTIVE,
                        'Inactive' => ClientDomain::INACTIVE
                    ],
                ]
            )
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        'Payed without domain' => ClientDomain::PAYED_WITHOUT_DOMAIN,
                        'Payed with domain' => ClientDomain::PAYED_WITH_DOMAIN,
                        'Added manually' => ClientDomain::ADDED_MANUALLY,
                        'Added manually approved' => ClientDomain::ADDED_MANUALLY_APPROVED,
                        'Duplicate' => ClientDomain::DUPLICATE,
                    ],
                ]
            )
            ->add(
                'domainType',
                'choice',
                [
                    'choices' => [
                        'Domain' => ClientDomain::CLIENT_DOMAIN_TYPE,
                        'Sub domain' => ClientDomain::SUB_DOMAIN_TYPE,
                    ],
                ]
            )
        ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('domain')
            ->add('owner')
            ->add(
                'active',
                'doctrine_orm_choice',
                [],
                'choice',
                [
                    'choices' => [
                        'Active' =>ClientDomain::ACTIVE,
                        'Inactive' => ClientDomain::INACTIVE,
                    ],
                ]
            )
            ->add(
                'status',
                'doctrine_orm_choice',
                [],
                'choice',
                [
                    'choices' => [
                        'Payed without domain' => ClientDomain::PAYED_WITHOUT_DOMAIN,
                        'Payed with domain' => ClientDomain::PAYED_WITH_DOMAIN,
                        'Added manually' => ClientDomain::ADDED_MANUALLY,
                        'Added manually approved' => ClientDomain::ADDED_MANUALLY_APPROVED,
                        'Duplicate' => ClientDomain::DUPLICATE,
                    ],
                ]
            )
            ->add(
                'domainType',
                'doctrine_orm_choice',
                [],
                'choice',
                [
                    'choices' => [
                        'Domain' => ClientDomain::CLIENT_DOMAIN_TYPE,
                        'Sub domain' => ClientDomain::SUB_DOMAIN_TYPE,
                    ],
                ]
            )
            ->add(
                'updatedAt',
                'doctrine_orm_date_range',
                [
                'field_type' => 'sonata_type_date_range_picker'
                ]
            )
            ->add(
                'createdAt',
                'doctrine_orm_date_range',
                [
                    'field_type' => 'sonata_type_date_range_picker'
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->add('domain')
            ->add('owner')
            ->add(
                'active',
                'choice',
                [
                    'editable' => true,
                    'choices' => [
                        ClientDomain::ACTIVE => 'Active',
                        ClientDomain::INACTIVE => 'Inactive',
                    ]
                ]
            )
            ->add(
                'domainType',
                'choice',
                [
                    'editable' => false,
                    'choices' => [
                        ClientDomain::CLIENT_DOMAIN_TYPE => 'Domain',
                        ClientDomain::SUB_DOMAIN_TYPE => 'Sub domain',
                    ]
                ]
            )
            ->add(
                'status',
                'choice',
                [
                    'editable' => true,
                    'choices' => [
                        ClientDomain::PAYED_WITHOUT_DOMAIN => 'Payed without domain',
                        ClientDomain::PAYED_WITH_DOMAIN => 'Payed with domain',
                        ClientDomain::ADDED_MANUALLY => 'Added manually',
                        ClientDomain::ADDED_MANUALLY_APPROVED => 'Added manually approved',
                        ClientDomain::DUPLICATE => 'Duplicate',
                    ]
                ]
            )
            ->add('createdAt')
            ->add('updatedAt');
    }
}
