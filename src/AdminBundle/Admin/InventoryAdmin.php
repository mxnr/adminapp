<?php

namespace AdminBundle\Admin;

use ThreeWebOneEntityBundle\Entity\Inventory\Inventory;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class InventoryAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_inventory';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-inventory';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->where($query->getRootAliases()[0] . '.owner = :user');
        $query->setParameter('user', $this->getUser());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Inventory')
            ->with('General', ['class' => 'col-md-12'])
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        'Disabled' => Inventory::STATUS_INACTIVE,
                        'Active' => Inventory::STATUS_ACTIVE,
                    ],
                ]
            )
            ->add(
                'inventoryItems',
                'sonata_type_collection',
                ['by_reference' => false],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'admin.inventory_item',
                ]
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('status')
            ->add('type')
            ->add(
                'inventoryItems',
                'doctrine_orm_model_autocomplete',
                ['admin_code' => 'admin.inventory_item'],
                null,
                [
                    'property' => 'id',
                ]
            )
            ->add('updatedAt')
            ->add('createdAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->add(
                'status',
                'choice',
                [
                    'editable' => true,
                    'choices' => [
                        Inventory::STATUS_ACTIVE => 'Active',
                        Inventory::STATUS_INACTIVE => 'Inactive',
                    ],
                ]
            )
            ->add(
                'type',
                'choice',
                [
                    'editable' => false,
                    'choices' => [
                        Inventory::SALE => 'Selling',
                        Inventory::BUYBACK => 'Buyback',
                        Inventory::REPAIR => 'Repair',
                    ],
                ]
            )
            ->add(
                'inventoryItems',
                null,
                [
                    'associated_property' => 'id',
                    'admin_code' => 'admin.inventory_item',
                ]
            )
            ->add('updatedAt')
            ->add('createdAt');
    }
}
