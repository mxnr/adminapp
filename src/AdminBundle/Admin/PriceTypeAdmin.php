<?php
namespace AdminBundle\Admin;

use AdminBundle\Service\ChargeBee\SiteSplitService;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

/**
 * {@inheritDoc}
 */
class PriceTypeAdmin extends BaseAdmin
{
    /**
     * @var bool
     */
    protected $useRepoQuery = true;

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_price_type';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-price-type';

    /**
     * {@inheritdoc}
     */
    protected function getQueryParams(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID => $this->getAdminUser()->getId(),
            AdminQueryInterface::PARAM_OWNER_USER_ID => $this->getUser()->getId(),
            AdminQueryInterface::PARAM_SITES => $this->getPaidSitesList(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function configureBatchActions($actions)
    {
        $user = $this->getUser();

        $actions = [];
        if (
            $this->hasRoute('edit') &&
            $this->hasAccess('edit') &&
            $user->hasRole('ROLE_SUPER_ADMIN') === false
        ) {
            $actions['Synchronize'] = array(
                'ask_confirmation' => true,
            );
            $actions['Desynchronize'] = array(
                'ask_confirmation' => true,
            );
        }

        return $actions;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $this->addActionsToRouteCollection($collection);
    }

    /**
     * @return array
     */
    protected function getPaidChoicesList(): array
    {
        $choices = [];

        if ($this->isSiteFeatureGranted(SiteSplitService::PRICE_TYPE_SALE)) {
            $choices['Selling'] = PriceType::SALE;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::PRICE_TYPE_BUY)) {
            $choices['Buyback'] = PriceType::BUYBACK;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::PRICE_TYPE_REPAIR)) {
            $choices['Repair'] = PriceType::REPAIR;
        }

        return $choices;
    }

    /**
     * @return array
     */
    protected function getPaidSitesList(): array
    {
        $sites = [];

        if ($this->isSiteFeatureGranted(SiteSplitService::PRICE_TYPE_SALE)) {
            $sites[] = PriceType::SALE;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::PRICE_TYPE_BUY)) {
            $sites[] = PriceType::BUYBACK;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::PRICE_TYPE_REPAIR)) {
            $sites[] = PriceType::REPAIR;
        }

        return $sites;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('PriceType')
            ->with('General', ['class' => 'col-md-6'])
            ->add('title', 'text')
            ->add('description', 'textarea')
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        'disabled' => PriceType::STATUS_INACTIVE,
                        'enabled' => PriceType::STATUS_ACTIVE,
                    ],
                ]
            )
            ->add(
                'site',
                'choice',
                [
                    'choices' => $this->getPaidChoicesList(),
                ]
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('title')
            ->add('description')
            ->add('status')
            ->add('site');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('title')
            ->addIdentifier('description')
            ->add(
                'status',
                'choice',
                [
                    'editable' => true,
                    'choices' => [
                        PriceType::STATUS_ACTIVE => 'Active',
                        PriceType::STATUS_INACTIVE => 'Inactive',
                    ],
                ]
            )
            ->add(
                'site',
                'choice',
                [
                    'editable' => false,
                    'choices' => [
                        PriceType::SALE => 'Selling',
                        PriceType::BUYBACK => 'Buyback',
                        PriceType::REPAIR => 'Repair',
                    ],
                ]
            );
        $this->addActionToListMapper($listMapper);
    }

    /**
     * {@inheritdoc}
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->tab('Item')
            ->with('General', ['class' => 'col-md-8'])
            ->add('title')
            ->add('description')
            ->add('site', null, ['template' => 'AdminBundle:Show:price_type_type.html.twig'])
            ->add('status', null, ['template' => 'AdminBundle:Show:status.html.twig'])
            ->end()
            ->end();
    }

    /**
     * @param PriceType $priceType
     */
    public function prePersist($priceType)
    {
        $priceType->setOwner($this->getUser());
        $priceType->addUser($this->getUser());
    }
}
