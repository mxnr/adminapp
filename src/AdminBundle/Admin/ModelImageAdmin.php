<?php

namespace AdminBundle\Admin;

class ModelImageAdmin extends ImageAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_model_image';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-model-image';
}
