<?php
namespace AdminBundle\Admin;

use ThreeWebOneEntityBundle\Entity\Image;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ImageAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_image';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-image';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->innerJoin($query->getRootAliases()[0] . '.users', 'u');
        $query->where('u.id = :user_id');
        $query->setParameter('user_id', $this->getUser()->getId());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'imageFile',
                VichImageType::class,
                [
                    'required' => false,
                    'allow_delete' => true,
                    'label' => 'Img',
                    'download_link' => false,
                ]
            )->add(
                'imageName',
                null,
                [
                    'required' => false,
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('imageName');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('imageName')
            ->add('picture', null, [
                'template' => '@Admin/partials/sonata.image-preview.html.twig'
            ]);
    }

    /**
     * @param Image $image
     */
    public function prePersist($image)
    {
        $image->addUser($this->getUser());
    }
}
