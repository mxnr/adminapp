<?php

namespace AdminBundle\Admin;

use AdminBundle\Form\Type\ThreeWebFilterType;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\PersistentCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\Product;
use ThreeWebOneEntityBundle\Repository\ModelRepository;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

class ModelPriceAdmin extends BaseAdmin
{
    /**
     * @var bool
     */
    protected $useRepoQuery = true;

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_batch_prices';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-batch-prices';

    /**
     * {@inheritdoc}
     */
    protected function getQueryParams(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID => $this->getAdminUser()->getId(),
            AdminQueryInterface::PARAM_OWNER_USER_ID => $this->getUser()->getId(),
            ModelRepository::PARAM_SWITCH_QUERY => ModelRepository::QUERY_MODEL_PRICE,
        ];
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'batch']);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add(
                'title',
                null,
                [
                    'show_filter' => true
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        if (
            !($this->isSiteFeatureGranted(SiteSplitService::ORDER_REPAIR) ||
            $this->isSiteFeatureGranted(SiteSplitService::ORDER_BUY))
        ) {
            throw new NotFoundHttpException();
        }
        $choices = [];
        if ($this->isSiteFeatureGranted(SiteSplitService::PRICE_TYPE_BUY)) {
            $choices[] = PriceType::BUYBACK;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::PRICE_TYPE_REPAIR)) {
            $choices[] = PriceType::REPAIR;
        }

        $priceTypes = $this->modelManager
            ->getEntityManager(PriceType::class)
            ->getRepository(PriceType::class)
            ->getUsersOwnedPriceTypesBuybackAndRepair(
                [$this->getUser()->getId(), $this->getAdminUser()->getId()],
                $choices
            );

        $listMapper->addIdentifier('id')
            ->add('title');
        foreach ($priceTypes as $item) {
            $listMapper->add(
                (string) $item,
                null,
                [
                    'priceTypeId' => $item->getId(),
                    'userId' => $this->getUser()->getId(),
                    'adminId' => $this->getAdminUser()->getId(),
                    'template' => '@Admin/List/editable_admin_price.html.twig',
                    'editable' => false,
                ]
                );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        $user = $this->getUser();

        $actions = [];
        if (
            $this->hasAccess('edit') &&
            $user->hasRole('ROLE_SUPER_ADMIN') === false
        ) {
            $actions['Synchronize'] = array(
                'ask_confirmation' => true,
            );
            $actions['Desynchronize'] = array(
                'ask_confirmation' => true,
            );
        }

        return $actions;
    }
}
