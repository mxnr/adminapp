<?php

namespace AdminBundle\Admin;

use AdminBundle\Form\DataTransformer\CategoryToIdTransformer;
use AdminBundle\Form\Type\ThreeWebCategoryType;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Repository\ModelRepository;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

class ModelAdmin extends BaseAdmin
{
    /**
     * @var bool
     */
    protected $useRepoQuery = true;

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_model';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-model';

    /**
     * {@inheritdoc}
     */
    protected function getQueryParams(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID => $this->getAdminUser()->getId(),
            AdminQueryInterface::PARAM_OWNER_USER_ID => $this->getUser()->getId(),
            ModelRepository::PARAM_SWITCH_QUERY => ModelRepository::QUERY_MODEL,
        ];
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $this->addActionsToRouteCollection($collection);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Model')
            ->with('General', ['class' => 'col-md-4'])
            ->add('title', 'text')
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        'disabled' => Model::STATUS_INACTIVE,
                        'enabled' => Model::STATUS_ACTIVE,
                    ],
                ]
            )
            ->add('description', 'textarea', ['required' => false])
            ->end()
            ->with('Categories', ['class' => 'col-md-8'])
            ->add(
                'category',
                ThreeWebCategoryType::class,
                [
                    'required' => false,
                    'mapped' => false,
                    'label' => 'Main Category',
                    'data' => $this->getSubject()->getCategory(),
                    'data_class' => null,
                    'model_var' => 'vm.categorySelected',
                    'model_default_var' => 'vm.presetCategory',
                    'search_function' => 'vm.getCategories($select.search)',
                    'btn_add_flag' => 'vm.showAddCategory',
                    'add_function' => 'vm.createCategory()',
                    'refresh_function' => 'vm.getSearchOfCategory($select.search)',
                    'image_model' => 'vm.imageCategory',
                    'search_model' => 'vm.categorySearch',
                    'attr' => [
                        'ng-model' => 'vm.categorySelected',
                        'class' => 'hidden-field'
                    ]
                ]
            )
            ->add(
                'product',
                ThreeWebCategoryType::class,
                [
                    'required' => false,
                    'mapped' => false,
                    'label' => '2nd Category',
                    'data' => $this->getSubject()->getProduct(),
                    'data_class' => null,
                    'model_var' => 'vm.productSelected',
                    'model_default_var' => 'vm.presetProduct',
                    'search_function' => 'vm.getProducts($select.search)',
                    'btn_add_flag' => 'vm.showAddProduct',
                    'add_function' => 'vm.createProduct()',
                    'refresh_function' => 'vm.getSearchOfProduct($select.search)',
                    'image_model' => 'vm.imageProduct',
                    'search_model' => 'vm.productSearch',
                    'attr' => [
                        'ng-model' => 'vm.productSelected',
                        'class' => 'hidden-field'
                    ]
                ]
            )
            ->add(
                'provider',
                ThreeWebCategoryType::class,
                [
                    'required' => false,
                    'mapped' => false,
                    'label' => '3rd Category',
                    'data' => $this->getSubject()->getProvider(),
                    'data_class' => null,
                    'model_var' => 'vm.providerSelected',
                    'model_default_var' => 'vm.presetProvider',
                    'search_function' => 'vm.getProviders($select.search)',
                    'btn_add_flag' => 'vm.showAddProvider',
                    'add_function' => 'vm.createProvider()',
                    'refresh_function' => 'vm.getSearchOfProvider($select.search)',
                    'image_model' => 'vm.imageProvider',
                    'search_model' => 'vm.providerSearch',
                    'attr' => [
                        'ng-model' => 'vm.providerSelected',
                        'class' => 'hidden-field'
                    ]
                ]
            )
            ->end();
        if (
            $this->isSiteFeatureGranted(SiteSplitService::ORDER_REPAIR) ||
            $this->isSiteFeatureGranted(SiteSplitService::ORDER_BUY)
        ) {
            $formMapper
                ->with('Prices', ['class' => 'col-md-8'])
                ->add(
                    'prices',
                    'sonata_type_collection',
                    [],
                    [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                        'admin_code' => 'admin.price',
                    ]
                )
                ->end();
        }
        $formMapper
            ->end()
            ->tab('Images')
            ->with('Images', ['class' => 'col-md-12'])
            ->add(
                'images',
                'sonata_type_collection',
                [],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'admin.model_image',
                ]
            )
            ->end()
            ->end();

        $categoryToIdTransformer = new CategoryToIdTransformer($this->modelManager->getEntitymanager(Category::class));
        $formMapper->get('category')->addModelTransformer($categoryToIdTransformer);
        $formMapper->get('provider')->addModelTransformer($categoryToIdTransformer);
        $formMapper->get('product')->addModelTransformer($categoryToIdTransformer);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add(
                'title',
                null,
                [
                    'show_filter' => true
                ]
            )
            ->add('status')
            ->add(
                'filters',
                'doctrine_orm_model_autocomplete',
                ['admin_code' => 'admin.parent_filter'],
                null,
                [
                    'property' => 'title',
                ]
            )
            ->add(
                'prices',
                'doctrine_orm_model_autocomplete',
                ['admin_code' => 'admin.price'],
                null,
                [
                    'property' => 'value',
                ]
            )
            ->add(
                'categories',
                'doctrine_orm_model_autocomplete',
                ['admin_code' => 'admin.product'],
                null,
                [
                    'property' => 'title',
                ]
            )
            ->add('createdAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('title')
            ->add(
                'categories',
                null,
                [
                    'associated_property' => 'title',
                    'admin_code' => 'admin.category',
                    'template' => 'AdminBundle:List/model:list_orm_many_to_many.html.twig',
                    'link_parameters' => [
                        'show_only_sell' => $this->isShowOnlySell(),
                    ]
                ]
            )
            ->add(
                'status',
                'choice',
                [
                    'editable' => true,
                    'choices' => [
                        Model::STATUS_ACTIVE => 'Active',
                        Model::STATUS_INACTIVE => 'Inactive',
                    ],
                ]
            )
            ->add(
                'filters',
                null,
                [
                    'associated_property' => 'title',
                    'admin_code' => 'admin.parent_filter',
                    'template' => 'AdminBundle:List:list_orm_many_to_many.html.twig',
                ]
            )
            ->addIdentifier('createdAt');
        $this->addActionToListMapper($listMapper);
    }

    /**
     * {@inheritdoc}
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
        $isShowOnlySell = $this->isShowOnlySell();
        $categoryTitle = 'Main Category, 2nd Category, 3rd Category, filters';
        if ($isShowOnlySell) {
            $categoryTitle = 'Main Category filters';
        }
        $showMapper
            ->tab('Item')
            ->with('General', ['class' => 'col-md-8'])
            ->add('title')
            ->add('status', null, ['template' => 'AdminBundle:Show:status.html.twig'])
            ->end()
            ->with('Image', ['class' => 'col-md-4'])
            ->add(
                'image',
                null,
                [
                    'template' => 'AdminBundle:Show:image.html.twig',
                    'label' => false,
                ]
            )
            ->end()
            ->with($categoryTitle, ['class' => 'col-md-8'])
            ->add(
                'category',
                null,
                [
                    'label' => 'Main Category'
                ]
            );
        if (!$isShowOnlySell) {
            $showMapper
                ->add(
                    'product',
                    null,
                    [
                        'label' => '2nd Category'
                    ]
                )
                ->add(
                    'provider',
                    null,
                    [
                        'label' => '3rd Category'
                    ]
                );
        }
        $showMapper
            ->add(
                'filters',
                null,
                [
                    'template' => 'AdminBundle:Show:one_to_many.html.twig',
                    'admin_code' => 'admin.parent_filter',
                ]
            )
            ->end();
        if (!$isShowOnlySell) {
            $showMapper
                ->with('Prices', ['class' => 'col-md-8'])
                ->add(
                    'prices',
                    null,
                    [
                        'template' => 'AdminBundle:Show:model_prices.html.twig',
                        'admin_code' => 'admin.price',
                        'label' => false,
                    ]
                )
                ->end();
        }
        $showMapper
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        $user = $this->getUser();

        $actions = [];
        if (
            $this->hasRoute('edit') &&
            $this->hasAccess('edit') &&
            $user->hasRole('ROLE_SUPER_ADMIN') === false
        ) {
            $actions['Synchronize'] = array(
                'ask_confirmation' => true,
            );
            $actions['Desynchronize'] = array(
                'ask_confirmation' => true,
            );
        }

        return $actions;
    }

    /**
     * @param Model $model
     */
    public function prePersist($model)
    {
        $this->managePrices($model);
        $this->manageCategories($model);
        $model->addUser($this->getUser());
        $model->setOwner($this->getUser());
        $this->manageEmbeddedImage($model);
    }

    /**
     * @param Model $model
     */
    public function preUpdate($model)
    {
        $this->managePrices($model);
        $this->manageCategories($model);
        $this->manageEmbeddedImage($model);
    }

    /**
     * Manages Images Persistion. Since Image Entity has file property
     * that Doctrine doesn't knows about we need to change property of
     * Image to trigger load Event.
     *
     * @param Model $model
     */
    private function manageEmbeddedImage($model)
    {
        foreach ($model->getImages() as $image) {
            if ($image) {
                if ($image->getImageFile()) {
                    // update the Image to trigger file management
                    $image->refreshUpdated();
                    $image->setEntity($model);
                } elseif (!$image->getImageFile() && !$image->getImageName()) {
                    // prevent Sf/Sonata trying to create and persist an empty Image\
                    $model->removeImage($image);
                }
            }
        }
    }

    /**
     * @param Model $model
     */
    private function manageCategories(Model $model)
    {
        $model
            ->setCategoryByType(Category::TYPE_CATEGORY, $this->getForm()->get('category')->getData())
            ->setCategoryByType(Category::TYPE_PROVIDER, $this->getForm()->get('provider')->getData())
            ->setCategoryByType(Category::TYPE_PRODUCT, $this->getForm()->get('product')->getData());
    }

    /**
     * @return bool
     */
    private function isShowOnlySell(): bool
    {
        return !(bool) (
            $this->isSiteFeatureGranted(SiteSplitService::PRICE_TYPE_BUY) ||
            $this->isSiteFeatureGranted(SiteSplitService::PRICE_TYPE_REPAIR)
        );
    }

    /**
     * @param Model $model
     */
    private function managePrices(Model $model)
    {
        if ($this->getForm()->has('prices')) {
            foreach ($model->getPrices() as $item) {
                $item->setModel(null);
            }
            foreach ($this->getForm()->get('prices')->getData() as $item) {
                $item->setModel($model)
                    ->setOwner($this->getUser())
                    ->addUser($this->getUser());
            }
        }
    }
}
