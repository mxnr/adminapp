<?php
namespace AdminBundle\Admin\Order;

use AdminBundle\Admin\BaseAdmin;
use AdminBundle\Form\DataTransformer\CustomerToIdTransformer;
use AdminBundle\Form\Type\ShowAskedQuantityType;
use AdminBundle\Form\Type\ThreeWebCustomerType;
use AdminBundle\Form\Type\ThreeWebOrderBarcodesType;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use AdminBundle\Service\InventoryItemService;
use AdminBundle\Service\OrderService;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Order\OrderDeliveryTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\OrderPaymentTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\OrderStatusInterface;

class OrderAdmin extends BaseAdmin
{
    /**
     * @var InventoryItemService
     */
    protected $inventoryItemService;

    /**
     * @param InventoryItemService $inventoryItemService
     */
    public function setInventoryItemService(InventoryItemService $inventoryItemService)
    {
        $this->inventoryItemService = $inventoryItemService;
    }

    /**
     * @var OrderService
     */
    protected $orderService;

    /**
     * @param OrderService $orderService
     */
    public function setOrderService(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_order';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-order';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);
        $query
            ->where($query->getRootAliases()[0] . '.owner = :user')
            ->setParameter('user', $this->getUser())
            ->andWhere($query->getRootAliases()[0] . '.siteType in (:sites)')
            ->setParameter('sites', $this->getPaidSitesList());

        return $query;
    }

    /**
     * @return array
     */
    public function getPaidSitesList(): array
    {
        $sites = [];

        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_SALE)) {
            $sites[] = Order::SALE;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_BUY)) {
            $sites[] = Order::BUYBACK;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_REPAIR)) {
            $sites[] = Order::REPAIR;
        }

        return $sites;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('print', $this->getRouterIdParameter().'/print');
    }

    /**
     * {@inheritdoc}
     */
    public function configureActionButtons($action, $object = null)
    {
        $list['new_order'] = [
            'template' =>  'AdminBundle:List:new_order.html.twig',
        ];

        return $list;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add(
            'storeId',
            CallbackFilter::class,
            [
                'callback' => [$this, 'getSequentialIdFilter'],
                'field_type' => 'text',
                'label' => 'Order #',
                'show_filter' => true,
            ]
        )
            ->add(
                'product',
                'doctrine_orm_callback',
                [
                    'show_filter' => true,
                    'callback' => function($queryBuilder, $alias, $field, $value) {
                        if (!$value['value']) {
                            return;
                        }

                        $queryBuilder->leftJoin(sprintf('%s.items', $alias), 'i')
                            ->leftJoin(sprintf('%s.purchaseBarcodes', $alias), 'pb')
                            ->leftJoin('pb.inventoryItem', 'ii')
                            ->andWhere('(i.title = :title) or (ii.title = :title)')
                            ->setParameter('title', $value['value']);

                        return true;
                    },
                ],
                null,
                [
                    'attr' => [
                        'ng-model' => 'vm.title',
                        'uib-typeahead' => 'title for title in vm.titles | filter:$viewValue | limitTo:8',
                        'autocomplete' => "off"
                    ]
                ]
            )
            ->add(
                'barcode',
                'doctrine_orm_callback',
                [
                    'show_filter' => true,
                    'callback' => function($queryBuilder, $alias, $field, $value) {
                        if (!$value['value']) {
                            return;
                        }

                        $queryBuilder->innerJoin(sprintf('%s.purchaseBarcodes', $alias), 'pb')
                            ->andWhere('pb.barcode = :barcode')
                            ->setParameter('barcode', $value['value']);

                        return true;
                    },
                ],
                null,
                [
                    'attr' => [
                        'ng-model' => 'vm.barcode',
                        'uib-typeahead' => 'title for title in vm.barcodes | filter:$viewValue | limitTo:8',
                        'autocomplete' => "off"
                    ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier(
                'id',
                null,
                [
                    'template' => 'AdminBundle:List:custom_order_edit_link.html.twig'
                ]
            )
            ->add('customer.email')
            ->add(
                'Product',
                null,
                [
                    'template' => 'AdminBundle:List:order_product.html.twig'
                ]
            )
            ->add(
                'status',
                'choice',
                [
                    'editable' => false,
                    'choices' => [
                        OrderStatusInterface::NEW_ORDER => 'New Order',
                        OrderStatusInterface::IN_PROGRESS => 'In progress',
                        OrderStatusInterface::COMPLETED => 'Completed',
                        OrderStatusInterface::REJECTED => 'Rejected',
                    ],
                ]
            )
            ->add(
                'siteType',
                'choice',
                [
                    'editable' => false,
                    'choices' => [
                        Order::SALE => 'Selling',
                        Order::BUYBACK => 'Buyback',
                        Order::REPAIR => 'Repair',
                    ],
                ]
            )
            ->add(
                'items',
                null,
                [
                    'associated_property' => 'storeId',
                    'admin_code' => 'admin.inventory_item',
                    'template' => 'AdminBundle:Order:list_orm_many_to_many.html.twig',
                ]
            )
            ->add('createdAt')
            ->add('_action', null, [
                'actions' => [
                    'print' => [
                        'template' => 'AdminBundle:CRUD:list__action_print.html.twig'
                    ]
                ]
            ]);
    }
}
