<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Repository\FilterRepository;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

class FilterAdmin extends BaseAdmin
{
    /**
     * @var bool
     */
    protected $useRepoQuery = true;

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_filter';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-filter';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $this->addActionsToRouteCollection($collection);
    }

    /**
     * {@inheritdoc}
     */
    protected function getQueryParams(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID => $this->getAdminUser()->getId(),
            AdminQueryInterface::PARAM_OWNER_USER_ID => $this->getUser()->getId(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Filter')
            ->with('General', ['class' => 'col-md-6'])
            ->add('title', 'text')
            ->add(
                'parent',
                EntityType::class,
                [
                    'class' => 'ThreeWebOneEntityBundle:Filter',
                    'choice_label' => 'title',
                    'required' => false,
                    'placeholder' => 'None',
                    'empty_data' => null,
                    'query_builder' => function (FilterRepository $repo) {
                        return $repo->getUserParentFiltersQuery($this->getUser());
                    },
                ],
                ['admin_code' => 'admin.parent_filter',]
            )
            ->add(
                'isMultiSelect',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'Multi select'
                ]
            )
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        'disabled' => Filter::STATUS_INACTIVE,
                        'enabled' => Filter::STATUS_ACTIVE,
                    ],
                ]
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add(
                'title',
                null,
                [
                    'show_filter' => true
                ]
            )
            ->add(
                'parent',
                'doctrine_orm_model_autocomplete',
                ['admin_code' => 'admin.parent_filter'],
                null,
                [
                    'property' => 'title',
                ]
            )
            ->add('status');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('title')
            ->add(
                'status',
                'choice',
                [
                    'editable' => true,
                    'choices' => [
                        Filter::STATUS_ACTIVE => 'Active',
                        Filter::STATUS_INACTIVE => 'Inactive',
                    ],
                ]
            )
            ->add(
                'parent',
                'choice'
            );
        $this->addActionToListMapper($listMapper);
    }

    /**
     * {@inheritdoc}
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->tab('Item')
            ->with('General', ['class' => 'col-md-8'])
            ->add('title')
            ->add('type', null, ['template' => 'AdminBundle:Show:filter_type.html.twig'])
            ->add('status', null, ['template' => 'AdminBundle:Show:status.html.twig'])
            ->add(
                'parent.title',
                null,
                [
                    'label' => 'Parent'
                ]
            )
            ->end()
            ->with('Image', ['class' => 'col-md-4'])
            ->add(
                'image',
                null,
                [
                    'template' => 'AdminBundle:Show:image.html.twig',
                    'label' => false,
                ]
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        $user = $this->getUser();

        $actions = [];
        if (
            $this->hasRoute('edit') &&
            $this->hasAccess('edit') &&
            $user->hasRole('ROLE_SUPER_ADMIN') === false
        ) {
            $actions['Synchronize'] = array(
                'ask_confirmation' => true,
            );
            $actions['Desynchronize'] = array(
                'ask_confirmation' => true,
            );
        }

        return $actions;
    }

    /**
     * @param Filter $filter
     */
    public function prePersist($filter)
    {
        $filter->addUser($this->getUser());
        $filter->setOwner($this->getUser());
        $this->manageEmbeddedImage($filter);
    }

    /**
     * @param Filter $filter
     */
    public function preUpdate($filter)
    {
        $this->manageEmbeddedImage($filter);
    }

    /**
     * Manages Images Persistion. Since Image Entity has file property
     * that Doctrine doesn't knows about we need to change property of
     * Image to trigger load Event.
     *
     * @param Filter $filter
     */
    private function manageEmbeddedImage($filter)
    {
        foreach ($filter->getImages() as $image) {
            if ($image) {
                if ($image->getImageFile()) {
                    // update the Image to trigger file management
                    $image->refreshUpdated();
                    $image->setEntity($filter);
                } elseif (!$image->getImageFile() && !$image->getImageName()) {
                    // prevent Sf/Sonata trying to create and persist an empty Image
                    $filter->removeImage($image);
                }
            }
        }
    }
}
