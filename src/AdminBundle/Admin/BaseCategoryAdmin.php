<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\CategoryImage;
use ThreeWebOneEntityBundle\Entity\UserConfig\Seo;
use ThreeWebOneEntityBundle\Repository\CategoryRepository;

abstract class BaseCategoryAdmin extends BaseAdmin
{
    /**
     * Parent category id used in queries
     *
     * @var integer
     */
    protected $parentCategoryId;

    /**
     * Admin code
     *
     * @var string
     */
    protected $adminCode;

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $this->addActionsToRouteCollection($collection);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->parentCategoryId == Category::TYPE_CATEGORY && !$this->getSubject()->getSeoMeta()) {
            $this->getSubject()->setSeoMeta(new Seo());
        }

        $formMapper
            ->tab('Category')
            ->with('General', ['class' => 'col-md-6'])
            ->add('title', 'text')
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        'disabled' => Category::STATUS_INACTIVE,
                        'enabled' => Category::STATUS_ACTIVE,
                    ],
                ]
            )
            ->end()
            ->with('Images', ['class' => 'col-md-6'])
            ->add(
                'images',
                'sonata_type_collection',
                $this->getOptionsForImagesField(),
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'admin.category_image',
                ]
            )
            ->end()
            ->end();
        if ($this->parentCategoryId == Category::TYPE_CATEGORY) {
            $formMapper->tab('Seo')
                ->with('Seo', ['class' => 'col-md-12'])
                ->add('seoMeta.title', null, ['required' => false])
                ->add(
                    'seoMeta.keyword',
                    null,
                    [
                        'label' => 'Meta keyword',
                        'required' => false
                    ]
                )
                ->add(
                    'seoMeta.description',
                    'textarea',
                    [
                        'label' => 'Meta description',
                        'required' => false
                    ]
                )
                ->end()
                ->end();
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add(
                'title',
                null,
                [
                    'show_filter' => true
                ]
            )
            ->add('status')
            ->add('type');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('title')
            ->add(
                'status',
                'choice',
                [
                    'editable' => true,
                    'choices' => [
                        Category::STATUS_ACTIVE => 'Active',
                        Category::STATUS_INACTIVE => 'Inactive',
                    ],
                ]
            )
            ->add(
                'type',
                'choice',
                [
                    'editable' => true,
                    'choices' => Category::TYPES_ARRAY,
                ]
            );
        $this->addActionToListMapper($listMapper);
    }

    /**
     * {@inheritdoc}
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->tab('General')
            ->with('General', ['class' => 'col-md-6'])
            ->add('title')
            ->add('type', null, ['template' => 'AdminBundle:Show:category_type.html.twig'])
            ->add('status', null, ['template' => 'AdminBundle:Show:status.html.twig'])
            ->add(
                'parent.title',
                null,
                [
                    'label' => 'Parent'
                ]
            )
            ->end()
            ->with('Image', ['class' => 'col-md-6'])
            ->add(
                'image',
                null,
                [
                    'template' => 'AdminBundle:Show:image.html.twig',
                    'label' => false,
                ]
            )
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        $user = $this->getUser();

        $actions = [];
        if (
            $this->hasRoute('edit') &&
            $this->hasAccess('edit') &&
            $user->hasRole('ROLE_SUPER_ADMIN') === false
        ) {
            $actions['Synchronize'] = array(
                'ask_confirmation' => true,
            );
            $actions['Desynchronize'] = array(
                'ask_confirmation' => true,
            );
        }

        return $actions;
    }

    /**
     * @param Category $category
     */
    public function prePersist($category)
    {
        $category->setType($this->parentCategoryId);
        $category->addUser($this->getUser());
        $category->setOwner($this->getUser());
        $this->manageEmbeddedImage($category);
        $this->manageSeo($category);
    }

    /**
     * @param Category $category
     */
    public function preUpdate($category)
    {
        $this->manageEmbeddedImage($category);
        $this->manageSeo($category);
    }

    /**
     * Manages Images Persistion. Since Image Entity has file property
     * that Doctrine doesn't knows about we need to change property of
     * Image to trigger load Event.
     *
     * @param Category $category
     */
    private function manageEmbeddedImage($category)
    {
        foreach ($category->getImages() as $image) {
            if ($image) {
                if ($image->getImageFile()) {
                    // update the Image to trigger file management
                    $image->refreshUpdated();
                    $image->setEntity($category);
                } elseif (!$image->getImageFile() && !$image->getImageName()) {
                    // prevent Sf/Sonata trying to create and persist an empty Image
                    $category->removeImage($image);
                }
            }
        }
    }

    /**
     * @param Category $category
     */
    private function manageSeo(Category $category)
    {
        if (!$category->getSeoMeta()->getId()) {
            $category->getSeoMeta()->setOwner($this->getUser())->setSite(Seo::TYPE_SALE);
            $this->modelManager->getEntityManager(Category::class)->persist($category->getSeoMeta());
        }
    }
}
