<?php

namespace AdminBundle\Admin;

/**
 * Class AccessoryImageAdmin.
 */
class AccessoryImageAdmin extends ImageAdmin
{
    /**
     * Route Name.
     *
     * @var string
     */
    protected $baseRouteName = 'admin_accessory_image';

    /**
     * Route Pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-accessory-image';
}
