<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;

class BarcodeAdmin extends BaseAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('saleOrder');
    }
}
