<?php

namespace AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class InventoryItemImageAdmin extends ImageAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_inventory_item_image';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-inventory-item-image';

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'imageFile',
                VichImageType::class,
                [
                    'required' => false,
                    'allow_delete' => true,
                    'label' => 'Img',
                    'download_link' => false,
                ]
            )->add(
                'imageName',
                null,
                [
                    'required' => false,
                ]
            )->add(
                'isMain', CheckboxType::class, ['required' => false]
            )
        ;
    }
}
