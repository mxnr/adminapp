<?php

namespace AdminBundle\Admin\UserConfig;

use AdminBundle\Admin\ImageAdmin;

class ConfigLogoImageAdmin extends ImageAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_config_logo_image';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-config-logo-image';
}
