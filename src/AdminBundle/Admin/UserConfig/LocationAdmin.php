<?php

namespace AdminBundle\Admin\UserConfig;

use AdminBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use ThreeWebOneEntityBundle\Entity\UserConfig\Location;

class LocationAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'user_config_locations';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'user-config/locations';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->innerJoin($query->getRootAliases()[0] . '.owner', 'u');
        $query->where('u.id = :user_id');
        $query->setParameter('user_id', $this->getUser()->getId());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier(
                'name',
                null,
                [
                    'associated_property' => 'name',
                ]
            )
            ->add(
                'isMain',
                'choice',
                [
                    'editable' => false,
                    'choices' => [
                        true => 'Yes',
                    ],
                ]
            )
            ->addIdentifier(
                'hours',
                null,
                [
                    'associated_property' => 'hours',
                ]
            )->addIdentifier(
                'services',
                null,
                [
                    'associated_property' => 'services',
                    'template' => '@Admin/List/location/service.html.twig',
                ]
            )->addIdentifier(
                'phone',
                null,
                [
                    'associated_property' => 'phone',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('name')
            ->add('hours')
            ->add('phone');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Location')
            ->with('General', ['class' => 'col-md-6'])
            ->add('name')
            ->add(
                'isMain',
                CheckboxType::class,
                [
                    'required' => false,
                ]
            )
            ->add('description', 'textarea')
            ->add('hours', 'textarea')
            ->add('phone')
            ->add('services', 'choice', [
                'expanded' => true,
                'multiple' => true,
                'required' => true,
                'choices' => [
                    'Buyback' => Location::BUYBACK,
                    'Selling' => Location::SALE,
                    'Repair' => Location::REPAIR,
                ]
            ])
            ->end()
            ->end()
        ;
    }

    /**
     * @param Location $location
     */
    public function prePersist($location)
    {
        $location->setOwner($this->getUser());
        $this->checkMain($location);
    }

    /**
     * @param Location $location
     */
    public function preUpdate($location)
    {
        $this->checkMain($location);
    }

    /**
     * @param Location $location
     */
    private function checkMain(Location $location)
    {
        if ($location->getIsMain()) {
            $locations = $this->getConfigurationPool()->getContainer()->get('doctrine.orm.entity_manager')
                ->getRepository(Location::class)
                ->getUserMainLocations($this->getUser());
            /** @var Location $location */
            foreach ($locations as $item) {
                if ($item !== $location) {
                    $item->setIsMain(false);
                }
            }
        }
    }
}
