<?php

namespace AdminBundle\Admin\UserConfig;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use ThreeWebOneEntityBundle\Entity\UserConfig\Theme;

class ThemeAdmin extends AbstractAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'user_config_theme';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'user-config/theme';

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('title')
            ->addIdentifier(
                'type',
                'choice',
                [
                    'editable' => false,
                    'choices' => [
                        Theme::TYPE_ROOT => 'Root site',
                        Theme::TYPE_SELL => 'Selling site',
                        Theme::TYPE_BUY => 'Buyback site',
                        Theme::TYPE_REPAIR => 'Repair site',
                        ]
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('title');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Main')
            ->with('General', ['class' => 'col-md-6'])
            ->add('title')
            ->end()
            ->end();
    }
}
