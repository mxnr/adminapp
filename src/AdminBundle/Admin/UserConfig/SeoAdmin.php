<?php

namespace AdminBundle\Admin\UserConfig;

use AdminBundle\Admin\BaseAdmin;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use ThreeWebOneEntityBundle\Entity\UserConfig\Seo;
use ThreeWebOneEntityBundle\Entity\UserConfig\Testimonial;

class SeoAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'user_config_meta';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'user-config/meta';

    /**
     * @return array
     */
    public function getPaidSitesList(): array
    {
        $sites = [];

        if ($this->isSiteFeatureGranted(SiteSplitService::SEO_SALE)) {
            $sites[] = Seo::TYPE_SALE;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::SEO_REPAIR)) {
            $sites[] = Seo::TYPE_REPAIR;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::SEO_BUY)) {
            $sites[] = Seo::TYPE_BUYBACK;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE)) {
            $sites[] = Seo::TYPE_LANDING;
        }

        return $sites;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);
        $query
            ->innerJoin($query->getRootAliases()[0] . '.owner', 'u')
            ->where('u.id = :user_id')
            ->andWhere($query->getRootAliases()[0] . '.site in (:sites)')
            ->setParameter('user_id', $this->getUser()->getId())
            ->setParameter('sites', $this->getPaidSitesList());

        return $query;
    }

    /**
     * @param bool $reverse
     *
     * @return array
     */
    protected function getSiteChoices(bool $reverse = false): array
    {
        $sites = [];

        if ($this->isSiteFeatureGranted(SiteSplitService::SEO_SALE)) {
            $sites[Seo::TYPE_SALE] = 'Selling';
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::SEO_BUY)) {
            $sites[Seo::TYPE_BUYBACK] = 'Buyback';
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::SEO_REPAIR)) {
            $sites[Seo::TYPE_REPAIR] = 'Repair';
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE)) {
            $sites[Seo::TYPE_LANDING] = 'Landing';
        }

        if ($reverse) {
            return array_flip($sites);
        }

        return $sites;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier(
                'title',
                null,
                [
                    'associated_property' => 'title',
                ]
            )
            ->addIdentifier(
                'keyword',
                null,
                [
                    'associated_property' => 'keyword',
                    'label' => 'Meta keyword',
                ]
            )
            ->addIdentifier(
                'description',
                null,
                [
                    'associated_property' => 'description',
                    'label' => 'Meta description',
                ]
            )
            ->add(
                'site',
                'choice',
                [
                    'editable' => false,
                    'choices' => $this->getSiteChoices(),
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('title')
            ->add('keyword')
            ->add('description');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Seo')
            ->with('General', ['class' => 'col-md-6'])
            ->add('title')
            ->add(
                'keyword',
                null,
                [
                    'label' => 'Meta keyword'
                ]
            )
            ->add(
                'description',
                'textarea',
                [
                    'label' => 'Meta description'
                ]
            )
            ->add(
                'site',
                'choice',
                [
                    'choices' => $this->getSiteChoices(true),
                ]
            )
            ->end()
            ->end()
        ;
    }

    /**
     * @param Testimonial $testimonial
     */
    public function prePersist($testimonial)
    {
        $testimonial->setOwner($this->getUser());
    }
}
