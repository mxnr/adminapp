<?php

namespace AdminBundle\Admin\UserConfig;

use AdminBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use ThreeWebOneEntityBundle\Entity\UserConfig\Testimonial;
use ThreeWebOneEntityBundle\Entity\UserConfig\Theme;

class TestimonialAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'user_config_testimonials';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'user-config/testimonials';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->innerJoin($query->getRootAliases()[0] . '.owner', 'u');
        $query->where('u.id = :user_id');
        $query->setParameter('user_id', $this->getUser()->getId());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier(
                'testimonial',
                null,
                [
                    'associated_property' => 'testimonial',
                ]
            )
            ->addIdentifier(
                'location',
                null,
                [
                    'associated_property' => 'location',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('testimonial')
            ->add('location');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Testimonial')
            ->with('General', ['class' => 'col-md-6'])
            ->add('testimonial')
            ->add('location')
            ->add('content')
            ->end()
            ->end()
        ;
    }

    /**
     * @param Testimonial $testimonial
     */
    public function prePersist($testimonial)
    {
        $testimonial->setOwner($this->getUser());
    }
}
