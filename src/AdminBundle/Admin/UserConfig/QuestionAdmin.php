<?php

namespace AdminBundle\Admin\UserConfig;

use AdminBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use ThreeWebOneEntityBundle\Entity\UserConfig\Question;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

class QuestionAdmin extends BaseAdmin
{
    /**
     * @var bool
     */
    protected $useRepoQuery = true;

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'user_config_question';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'user-config/question';

    /**
     * {@inheritdoc}
     */
    protected function getQueryParams(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID => $this->getAdminUser()->getId(),
            AdminQueryInterface::PARAM_OWNER_USER_ID => $this->getUser()->getId(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier(
                'category',
                null,
                [
                    'associated_property' => 'category',
                ]
            )
            ->addIdentifier(
                'question',
                null,
                [
                    'associated_property' => 'question',
                ]
            )
            ->addIdentifier(
                'answer',
                null,
                [
                    'associated_property' => 'answer',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('category')
            ->add('question')
            ->add('answer');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Question')
            ->with('General', ['class' => 'col-md-12'])
            ->add(
                'category',
                null,
                [
                    'attr' => [
                        'data-provide' => 'typeahead',
                        'data-items' => '4',
                        'data-source' => $this->convertArrayToTypeaheadString(),
                        'autocomplete' => "off"
                    ]
                ]
            )
            ->add(
                'question',
                'textarea'
            )
            ->add(
                'answer',
                'textarea'
            )
            ->end()
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->tab('General')
            ->with('General', ['class' => 'col-md-12'])
            ->add('Category')
            ->add('Question')
            ->add('Answer');
    }

    /**
     * @param Question $question
     */
    public function prePersist($question)
    {
        $question->setOwner($this->getUser());
        $question->addUser($this->getUser());
    }

    /**
     * Converts array to string for typeahead
     *
     * @return string
     */
    private function convertArrayToTypeaheadString()
    {
        $categories = $this->modelManager
            ->getEntityManager(Question::class)
            ->getRepository(Question::class)
            ->getQuestionsCategoryForUser($this->getUser());

        $string = '[';
        foreach ($categories as $item) {
            $string .= sprintf('"%s"', $item['category']);
            if ($item !== end($categories)) {
                $string .= ',';
            }
        }
        $string .= ']';

        return $string;
    }
}
