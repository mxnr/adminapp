<?php

namespace AdminBundle\Admin\UserConfig;

use AdminBundle\Admin\BaseAdmin;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ThreeWebOneEntityBundle\Entity\UserConfig\Config;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogo;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogoDefaultImage;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigTexts;
use ThreeWebOneEntityBundle\Entity\UserConfig\DefaultImage;
use ThreeWebOneEntityBundle\Entity\UserConfig\Theme;
use Vich\UploaderBundle\Handler\UploadHandler;

class ConfigAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'user_config_config';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'user-config/config';

    /**
     * path to temporary file
     *
     * @var string
     */
    private $filePath;

    /**
     * @var UploadHandler
     */
    private $uploadHandler;

    /**
     * @param UploadHandler $uploadHandler
     */
    public function setUploadHandler(UploadHandler $uploadHandler)
    {
        $this->uploadHandler = $uploadHandler;
    }

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);
        $query->innerJoin($query->getRootAliases()[0] . '.owner', 'u');
        $query->where('u.id = :user_id');
        $query->setParameter('user_id', $this->getUser()->getId());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    public function configureBatchActions($actions)
    {
        return [];
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier(
                'rootTheme',
                null,
                [
                    'associated_property' => 'title',
                    'admin_code' => 'admin.user_config_theme',
                ]
            );
        if ($this->isSiteFeatureGranted(SiteSplitService::THEME_SALE)) {
            $listMapper
                ->addIdentifier(
                    'sellTheme',
                    null,
                    [
                        'associated_property' => 'title',
                        'admin_code' => 'admin.user_config_theme',
                    ]
                );
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::THEME_BUY)) {
            $listMapper
                ->addIdentifier(
                    'buyTheme',
                    null,
                    [
                        'associated_property' => 'title',
                        'admin_code' => 'admin.user_config_theme',
                    ]
                );
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::THEME_REPAIR)) {
            $listMapper
                ->addIdentifier(
                    'repairTheme',
                    null,
                    [
                        'associated_property' => 'title',
                        'admin_code' => 'admin.user_config_theme',
                    ]
                );
        }

        $listMapper
            ->addIdentifier(
                'owner',
                null,
                [
                    'associated_property' => 'username',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add(
                'rootTheme',
                'doctrine_orm_model_autocomplete',
                ['admin_code' => 'admin.user_config_theme'],
                null,
                [
                    'property' => 'title',
                ]
            )
            ->add(
                'owner',
                'doctrine_orm_model_autocomplete',
                [],
                null,
                [
                    'property' => 'username',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Main Theme')
            ->with('General', ['class' => 'col-md-6'])
            ->add(
                'rootTheme',
                'sonata_type_model',
                [
                    'required' => false,
                    'btn_add' => false,
                    'query' => $this->modelManager
                        ->getEntityManager(Theme::class)
                        ->getRepository(Theme::class)
                        ->getThemesByTypeQuery(Theme::TYPE_ROOT),
                ],
                [
                    'admin_code' => 'admin.user_config_theme',
                ]
            );

        if ($this->isSiteFeatureGranted(SiteSplitService::THEME_SALE)) {
            $formMapper
                ->add(
                    'sellTheme',
                    'sonata_type_model',
                    [
                        'required' => false,
                        'label' => "Selling Theme",
                        'btn_add' => false,
                        'query' => $this->modelManager
                            ->getEntityManager(Theme::class)
                            ->getRepository(Theme::class)
                            ->getThemesByTypeQuery(Theme::TYPE_SELL),
                    ],
                    [
                        'admin_code' => 'admin.user_config_theme',
                    ]
                );
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::THEME_BUY)) {
            $formMapper
                ->add(
                    'buyTheme',
                    'sonata_type_model',
                    [
                        'required' => false,
                        'label' => "Buyback Theme",
                        'btn_add' => false,
                        'query' => $this->modelManager
                            ->getEntityManager(Theme::class)
                            ->getRepository(Theme::class)
                            ->getThemesByTypeQuery(Theme::TYPE_BUY),
                    ],
                    [
                        'admin_code' => 'admin.user_config_theme',
                    ]
                );
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::THEME_REPAIR)) {
            $formMapper
                ->add(
                    'repairTheme',
                    'sonata_type_model',
                    [
                        'required' => false,
                        'label' => "Repair Theme",
                        'btn_add' => false,
                        'query' => $this->modelManager
                            ->getEntityManager(Theme::class)
                            ->getRepository(Theme::class)
                            ->getThemesByTypeQuery(Theme::TYPE_REPAIR),
                    ],
                    [
                        'admin_code' => 'admin.user_config_theme',
                    ]
                );
        }

        $formMapper
            ->add(
                'backgroundColor',
                'text',
                [
                    'attr' => [
                        'class' => 'jscolor'
                    ]
                ]
            )
            ->end()
            ->end()
            ->tab('Logos')
            ->with('logos', ['class' => 'col-md-12'])
            ->add(
                'configLogo.defaultImage',
                'entity',
                [
                    'label' => 'Default image',
                    'required' => false,
                    'class' => DefaultImage::class,
                    'choice_label' => 'title',
                    'attr' => [
                        'class' => 'target'
                    ]
                ]
            )
            ->add(
                'configLogo.logoSize',
                'choice',
                [
                    'choices' => [
                        'Medium' => ConfigLogo::MEDIUM,
                        'Large' => ConfigLogo::LARGE,
                        'Small' => ConfigLogo::SMALL,
                    ],
                ]
            )
            ->add(
                'configLogo.firstColor',
                'text',
                [
                    'label' => 'Image first color',
                    'attr' => [
                        'class' => 'jscolor defaul_image_color',
                        'data-param' => 'first'
                    ]
                ]
            )
            ->add(
                'configLogo.secondColor',
                'text',
                [
                    'label' => 'Image second color',
                    'attr' => [
                        'class' => 'jscolor defaul_image_color',
                        'data-param' => 'second'
                    ]
                ]
            )
            ->add(
                'configLogo.thirdColor',
                'text',
                [
                    'label' => 'Image third color',
                    'attr' => [
                        'class' => 'jscolor defaul_image_color',
                        'data-param' => 'third'
                    ]
                ]
            )
            ->add(
                'configLogo.useDefaultImage',
                'checkbox',
                [
                    'label' => 'Use default logo',
                    'required' => false
                ]
            )
            ->add(
                'configLogo.logoImages',
                'sonata_type_collection',
                [
                    'label' => 'Logo image',
                    'required' => false,
                    'btn_add' => !$this->getSubject()->getConfigLogo()->getLogoImages()->count() ? 'Add new' : false,
                ],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'admin.config_logo_image',
                ]
            )
            ->add(
                'configLogo.useSelectedAsFavicon',
                'checkbox',
                [
                    'label' => 'Use chosen logo as favicon',
                    'required' => false
                ]
            )
            ->add(
                'configLogo.faviconImages',
                'sonata_type_collection',
                [
                    'label' => 'Favicon image',
                    'required' => false,
                    'btn_add' => !$this->getSubject()->getConfigLogo()->getFaviconImages()->count() ? 'Add new' : false,
                ],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'admin.config_favicon_image',
                ]
            )
            ->end()
            ->end()
            ->tab('Titles')
            ->with('Titles', ['class' => 'col-md-6'])
            ->add(
                'configTexts.visibleBusinessName',
                'checkbox',
                [
                    'label' => 'Display business name',
                    'required' => false
                ]
            )
            ->add(
                'configTexts.businessName',
                'text',
                [
                    'label' => 'Business name',
                    'required' => false,
                ]
            )
            ->add(
                'configTexts.businessNameColor',
                'text',
                [
                    'label' => 'Business name color',
                    'required' => false,
                    'attr' => [
                        'class' => 'jscolor'
                    ]
                ]
            )
            ->add(
                'configTexts.font',
                'choice',
                [
                    'label' => 'Font',
                    'required' => false,
                    'choices' => [
                        'None' => '',
                        'Crille' => ConfigTexts::CRILLEE_REGULAR,
                        'Furore regular' => ConfigTexts::FURORE_REGULAR,
                        'Helvetica round' => ConfigTexts::HELVETICA_ROUND,
                        'Bitter' => ConfigTexts::BITTER_REGULAR,
                        'Raleway' => ConfigTexts::RALEWAY_REGULAR,
                        'Slicker' => ConfigTexts::SLICKER_REGULAR,
                        'Myriad pro' => ConfigTexts::MYRIAD_PRO_REGULAR,
                        'Virtue' => ConfigTexts::VIRTUE_REGULAR,
                        'Orbitron bold' => ConfigTexts::ORBITRON_REGULAR,
                        'Gayatri regular' => ConfigTexts::GAYATRI_REGULAR,
                        'Duke' => ConfigTexts::DUKE_REGULAR,
                        'Montserrat' => ConfigTexts::MONTSERRAT_REGULAR,
                        'Neo sans' => ConfigTexts::NEO_SANS_BOLD,
                        'Charlie brown m54' => ConfigTexts::CHARLIE_BROWN_M54_REGULAR,
                        'Quicksand' => ConfigTexts::QUICKSAND_REGULAR,
                        'Arial narrow' => ConfigTexts::ARIAL_NARROW,
                    ],
                ]
            )
            ->add(
                'configTexts.businessNameSize',
                'choice',
                [
                    'label' => 'Business name size',
                    'required' => false,
                    'choices' => [
                        'Small' => ConfigTexts::SIZE_SMALL,
                        'Medium' => ConfigTexts::SIZE_MEDIUM,
                        'Large' => ConfigTexts::SIZE_LARGE,
                    ],
                ]
            );

        if ($this->isSiteFeatureGranted(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE)) {
            $formMapper
                ->add(
                    'configTexts.landingPageTitle',
                    'text',
                    [
                        'label' => 'Landing page title',
                        'required' => false,
                    ]
                );
        }

        $formMapper
            ->add(
                'configTexts.testimonialsTitle',
                'text',
                [
                    'label' => 'Testimonials title',
                    'required' => false,
                ]
            );

        if ($this->isSiteFeatureGranted(SiteSplitService::TITLE_DESCRIPTION_SALE)) {
            $formMapper
                ->add(
                    'configTexts.sellTitle',
                    'text',
                    [
                        'label' => 'Selling site title',
                        'required' => false,
                    ]
                )
                ->add(
                    'configTexts.sellDescription',
                    'textarea',
                    [
                        'label' => 'Selling site description',
                        'required' => false,
                    ]
                );
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::TITLE_DESCRIPTION_BUY)) {
            $formMapper
                ->add(
                    'configTexts.buyTitle',
                    'text',
                    [
                        'label' => 'Buyback site title',
                        'required' => false,
                    ]
                )
                ->add(
                    'configTexts.buyDescription',
                    'textarea',
                    [
                        'label' => 'Buyback site description',
                        'required' => false,
                    ]
                );
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::TITLE_DESCRIPTION_REPAIR)) {
            $formMapper
                ->add(
                    'configTexts.repairTitle',
                    'text',
                    [
                        'label' => 'Repair site title',
                        'required' => false,
                    ]
                )
                ->add(
                    'configTexts.repairDescription',
                    'textarea',
                    [
                        'label' => 'Repair site description',
                        'required' => false,
                    ]
                );
        }

        $formMapper
            ->end()
            ->end()
            ->tab('Slider')
            ->with('Slider', ['class' => 'col-md-12 admin-order-slider-block']);

        if ($this->isSiteFeatureGranted(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE)) {
            $formMapper
                ->add(
                    'configSlider.displayLanding',
                    'checkbox',
                    [
                        'label' => 'Display on landing',
                        'required' => false,
                    ]
                );
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_SALE)) {
            $formMapper
                ->add(
                    'configSlider.displaySell',
                    'checkbox',
                    [
                        'label' => 'Display on selling site',
                        'required' => false,
                    ]
                );
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_BUY)) {
            $formMapper
                ->add(
                    'configSlider.displayBuy',
                    'checkbox',
                    [
                        'label' => 'Display on buyback site',
                        'required' => false,
                    ]
                );
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_REPAIR)) {
            $formMapper
                ->add(
                    'configSlider.displayRepair',
                    'checkbox',
                    [
                        'label' => 'Display on repair site',
                        'required' => false,
                    ]
                );
        }

        $formMapper
            ->add(
                'configSlider.sliderImages',
                'sonata_type_collection',
                [],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'admin.config_slider_image',
                ]
            )
            ->end()
            ->end();
        if ($this->isSiteFeatureGranted(SiteSplitService::OFFERS)) {
            $formMapper
                ->tab('Offers')
                ->with('Offers', ['class' => 'col-md-12'])
                ->add(
                    'offerImages',
                    'sonata_type_collection',
                    [
                        'type_options' => ['delete' => false],
                        'btn_add' => false,
                    ],
                    [
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                        'admin_code' => 'admin.config_offer_image',
                    ]
                )
                ->end()
                ->end();
        }
        if ($this->isSiteFeatureGranted(SiteSplitService::SOCIAL)) {
            $formMapper
                ->tab('Social')
                ->with('Social', ['class' => 'col-md-6'])
                ->add(
                    'configSocial.twitterLink',
                    'text',
                    [
                        'label' => 'Twitter link',
                        'required' => false
                    ]
                )
                ->add(
                    'configSocial.facebookLink',
                    'text',
                    [
                        'label' => 'Facebook link',
                        'required' => false
                    ]
                )
                ->add(
                    'configSocial.instagramLink',
                    'text',
                    [
                        'label' => 'Instagram link',
                        'required' => false
                    ]
                )
                ->end()
                ->end();
        }
    }

    /**
     * @param Config $config
     */
    public function prePersist($config)
    {
        $this->manageEmbeddedImage($config);
    }

    /**
     * @param Config $config
     */
    public function preUpdate($config)
    {
        if ($config->getConfigLogo()->getDefaultImage()) {
            $em = $this->getModelManager()->getEntityManager($this->getClass());
            $oldLogoConfig = $em->getUnitOfWork()->getOriginalEntityData($config->getConfigLogo());
            $logoConfig = $config->getConfigLogo();
            if (
                $logoConfig->getDefaultImage() !== $oldLogoConfig['defaultImage'] ||
                $logoConfig->getFirstColor() !== $oldLogoConfig['firstColor'] ||
                $logoConfig->getSecondColor() !== $oldLogoConfig['secondColor'] ||
                $logoConfig->getThirdColor() != $oldLogoConfig['thirdColor']
            ) {
                $this->createImageFromSVG($logoConfig);
            }
        }

        $this->manageEmbeddedImage($config);
    }

    /**
     * Cleanup temp files after image creation
     *
     * @param Config $config
     */
    public function postUpdate($config)
    {
        if ($this->filePath) {
            unlink($this->filePath);
        }
    }

    /**
     * Manages Images Persistion. Since Image Entity has file property
     * that Doctrine doesn't knows about we need to change property of
     * Image to trigger load Event.
     *
     * @param Config $config
     */
    private function manageEmbeddedImage(Config $config)
    {
        $logoConfig = $config->getConfigLogo();
        $sliderConfig = $config->getConfigSlider();
        $this->operateImage($config, 'Offer');
        $this->operateImage($logoConfig, 'Logo');
        $this->operateImage($logoConfig, 'Favicon');
        $this->operateImage($sliderConfig, 'Slider');
    }

    /**
     * Method to trigger load event
     *
     * @param $entity
     * @param string $methodVerb
     */
    private function operateImage($entity, string $methodVerb)
    {
        $methodGet = sprintf('get%sImages', $methodVerb);
        $methodRemove = sprintf('remove%sImage', $methodVerb);
        foreach ($entity->$methodGet() as $image) {
            if ($image->getImageFile()) {
                // update the Image to trigger file management
                $image->refreshUpdated();
                $image->setEntity($entity);
            } elseif (!$image->getImageFile() && !$image->getImageName()) {
                // prevent Sf/Sonata trying to create and persist an empty Image
                $entity->$methodRemove($image);
            }
        }
    }

    /**
     * Create image from svg
     *
     * @param ConfigLogo $logoConfig
     */
    private function createImageFromSVG(ConfigLogo $logoConfig)
    {
        $svg = $logoConfig->getDefaultImage()->getContent();
        $idColorArray = [
            'first' => $logoConfig->getFirstColor(),
            'second' => $logoConfig->getSecondColor(),
            'third' => $logoConfig->getThirdColor(),
        ];

        foreach ($idColorArray as $state => $color) {
            if ($color != 'FFFFFF') {
                $svg = preg_replace(
                    '/class="' . $state . '" style="fill:#([0-9a-fA-F]{6})/',
                    'class="' . $state . '" style="fill:#' . $color,
                    $svg
                );
            }
        }

        $fileName = sprintf('%s.png', uniqid());
        $filePath = sprintf('/tmp/%s', $fileName);
        $this->filePath = $filePath;

        $im = new \Imagick();
        $im->setBackgroundColor(new \ImagickPixel('transparent'));
        $im->readImageBlob($svg);
        $im->setImageFormat("png24");
        $im->writeImage($filePath);

        $file = new UploadedFile($filePath, $fileName .'.png');
        $im->clear();
        $im->destroy();

        $oldImages  = $logoConfig->getRenderedLogoImages();
        foreach ($oldImages as $image) {
            $this->uploadHandler->remove($image, 'imageFile');
            $logoConfig->removeRenderedLogoImage($image);
        }

        $renderedImage = new ConfigLogoDefaultImage();
        $renderedImage->setImageFile($file);
        $renderedImage->setImageName($fileName);
        $logoConfig->addRenderedLogoImage($renderedImage);
        $this->operateImage($logoConfig, 'RenderedLogo');
    }
}
