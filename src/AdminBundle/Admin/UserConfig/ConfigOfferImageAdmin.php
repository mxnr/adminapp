<?php

namespace AdminBundle\Admin\UserConfig;

use AdminBundle\Admin\ImageAdmin;
use AdminBundle\Form\Type\ImageOnlyType;
use Sonata\AdminBundle\Form\FormMapper;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ConfigOfferImageAdmin extends ImageAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_config_offer_image';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-config-offer-image';

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'imageFile',
                ImageOnlyType::class,
                [
                    'required' => false,
                    'allow_delete' => false,
                    'label' => 'Img',
                    'download_link' => false,
                ]
            )->add(
                'setting.active',
                'checkbox',
                [
                    'label' => 'Active',
                    'required' => false
                ]
            )->add(
                'setting.title',
                null,
                [
                    'label' => 'Title'
                ]
            )->add(
                'setting.description',
                'textarea',
                [
                    'label' => 'Description'
                ]
            )
        ;
    }
}
