<?php

namespace AdminBundle\Admin\UserConfig;

use AdminBundle\Admin\ImageAdmin;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use Sonata\AdminBundle\Form\FormMapper;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ConfigSliderImageAdmin extends ImageAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_config_slider_image';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-config-slider-image';

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'imageFile',
                VichImageType::class,
                [
                    'required' => false,
                    'allow_delete' => false,
                    'label' => 'Image picture',
                    'download_link' => false,
                ]
            )->add(
                'imageName',
                null,
                [
                    'label' => 'Image name',
                    'required' => false,
                ]
            )->add(
                'setting.link',
                'url',
                [
                    'label' => 'Link',
                    'required' => false,
                ]
            );
        if ($this->isSiteFeatureGranted(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE)) {
            $formMapper
                ->add(
                    'setting.displayLanding',
                    'checkbox',
                    [
                        'label' => 'Display on landing',
                        'required' => false
                    ]
                );
        }
        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_SALE)) {
            $formMapper->add(
                'setting.displaySell',
                'checkbox',
                [
                    'label' => 'Display on selling site',
                    'required' => false
                ]
            );
        }
        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_BUY)) {
            $formMapper->add(
                'setting.displayBuy',
                'checkbox',
                [
                    'label' => 'Display on buyback site',
                    'required' => false
                ]
            );
        }
        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_REPAIR)) {
            $formMapper->add(
                'setting.displayRepair',
                'checkbox',
                [
                    'label' => 'Display on repair site',
                    'required' => false
                ]
            );
        }
    }
}
