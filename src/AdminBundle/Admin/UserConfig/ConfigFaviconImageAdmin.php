<?php

namespace AdminBundle\Admin\UserConfig;

use AdminBundle\Admin\ImageAdmin;

class ConfigFaviconImageAdmin extends ImageAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_config_favicon_image';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-config-favicon-image';
}
