<?php

namespace AdminBundle\Admin;

class FilterImageAdmin extends ImageAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_filter_image';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-filter-image';
}
