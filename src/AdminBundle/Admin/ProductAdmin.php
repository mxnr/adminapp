<?php
namespace AdminBundle\Admin;

use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Sonata\Admin\AdminQueryInterface;

class ProductAdmin extends BaseCategoryAdmin
{
    /**
     * @var bool
     */
    protected $useRepoQuery = true;

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_category_product';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-2nd-category';

    /**
     * @var integer
     */
    protected $parentCategoryId = Category::TYPE_PRODUCT;

    /**
     * @var string
     */
    protected $adminCode = 'admin.product';

    /**
     * {@inheritdoc}
     */
    protected function getQueryParams(): array
    {
        return [
            AdminQueryInterface::PARAM_ADMIN_USER_ID => $this->getAdminUser()->getId(),
            AdminQueryInterface::PARAM_OWNER_USER_ID => $this->getUser()->getId(),
            AdminQueryInterface::PARAM_PARENT_CATEGORY_ID => $this->parentCategoryId,
        ];
    }
}
