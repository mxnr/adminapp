<?php
namespace AdminBundle\Admin;

use AdminBundle\Service\ChargeBee\SiteSplitService;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\PopularSearch;

/**
 * {@inheritDoc}
 */
class PopularSearchAdmin extends BaseAdmin
{
    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_popular_search';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-popular-search';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->where($query->getRootAliases()[0] . '.owner = :owner');
        $query->setParameter('owner', $this->getUser());

        return $query;
    }

    /**
     * @return array
     */
    protected function getPaidSitesList(): array
    {
        $sites = [];

        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_SALE)) {
            $sites['Selling'] = PopularSearch::SALE;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_BUY)) {
            $sites['Buyback'] = PopularSearch::BUYBACK;
        }

        if ($this->isSiteFeatureGranted(SiteSplitService::ORDER_REPAIR)) {
            $sites['Repair'] = PopularSearch::REPAIR;
        }

        return $sites;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('PopularSearch')
            ->with('General', ['class' => 'col-md-6'])
            ->add('title', 'text')
            ->add(
                'type',
                'choice',
                [
                    'choices' => $this->getPaidSitesList(),
                ]
            )
            ->add(
                'category',
                'sonata_type_model',
                [
                    'required' => true,
                    'expanded' => false,
                    'multiple' => false,
                    'query' => $this->modelManager
                        ->getEntityManager(Category::class)
                        ->getRepository(Category::class)
                        ->getUserCategoriesByTypeQuery($this->getUser(), Category::TYPE_CATEGORY),
                    'btn_add' => false,

                ],
                [
                    'admin_code' => 'admin.category',
                ]
            )
            ->add(
                'position',
                IntegerType::class
                )
            ->add(
                'status',
                'choice',
                [
                    'choices' => [
                        'disabled' => PopularSearch::STATUS_INACTIVE,
                        'enabled' => PopularSearch::STATUS_ACTIVE,
                    ],
                ]
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('title')
            ->add('type');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id')
            ->addIdentifier('title')
            ->add('category.title')
            ->add('position')
            ->add(
                'status',
                'choice',
                [
                    'editable' => true,
                    'choices' => [
                        PopularSearch::STATUS_ACTIVE => 'Active',
                        PopularSearch::STATUS_INACTIVE => 'Inactive',
                    ],
                ]
            )
            ->add(
                'type',
                'choice',
                [
                    'editable' => false,
                    'choices' => [
                        PopularSearch::SALE => 'Selling',
                        PopularSearch::BUYBACK => 'Buyback',
                        PopularSearch::REPAIR => 'Repair',
                    ],
                ]
            );
    }

    /**
     * @param PopularSearch $popularSearch
     */
    public function prePersist($popularSearch)
    {
        $popularSearch->setOwner($this->getUser());
    }
}
