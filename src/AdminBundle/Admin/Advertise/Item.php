<?php

namespace AdminBundle\Admin\Advertise;

use AdminBundle\Admin\BaseAdmin;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use ThreeWebOneEntityBundle\Entity\Advertise;
use ThreeWebOneEntityBundle\Entity\Image;

/**
 * Class Item.
 */
class Item extends BaseAdmin
{
    protected $baseRouteName = 'config_apps';

    protected $baseRoutePattern = 'advertise_category_items';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);
        $query->leftJoin($query->getRootAliases()[0] . '.category', 'c');
        $query->orderBy('c.orderNumber', 'ASC');
        $query->addOrderBy($query->getRootAliases()[0] . '.orderNumber', 'ASC');

        return $query;
    }

    /**
     * @param Advertise $advertise
     */
    public function prePersist($advertise)
    {
        $this->manageEmbeddedImage($advertise);
    }

    /**
     * @param Advertise $advertise
     */
    public function preUpdate($advertise)
    {
        $this->manageEmbeddedImage($advertise);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Item')
            ->with('General', ['class' => 'col-md-6'])
            ->add(
                'category',
                'sonata_type_model',
                [
                    'required' => false,
                    'expanded' => false,
                    'multiple' => false,
                ],
                [
                    'admin_code' => 'admin.advertise_category',
                ]
            )
            ->add('name', 'text')
            ->add('url', 'text')
            ->add('description', 'textarea')
            ->add('orderNumber', 'number', ['label' => $this->trans('Ordering number')])
            ->end()
            ->with('Images', ['class' => 'col-md-6'])
            ->add(
                'images',
                'sonata_type_collection',
                $this->getOptionsForImagesField(),
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'admin.advertise_image',
                ]
            )
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        if (false === $this->checkRole('ROLE_SUPER_ADMIN')) {
            //CLIENTS SHOULD SEE DIFFERENT LIST TEMPLATE
            $this->setTemplate('list', '@Admin/List/client/list.html.twig');
            $this->setTemplate('outer_list_rows_list', '@Admin/List/client/list_outer_rows_list.html.twig');
            $this->setTemplate('inner_list_row', '@Admin/List/client/list_inner_row.html.twig');
            $this->setTemplate('base_list_field', '@Admin/List/client/base_list_field.html.twig');

            $listMapper
                ->addIdentifier('name')
                ->addIdentifier(
                    'image',
                    null,
                    [
                        'template' => '@Admin/List/client/partials/sonata.image-preview.html.twig',
                    ]
                )
                ->addIdentifier('description')
                ->addIdentifier(
                    'url',
                    null,
                    [
                        'template' => '@Admin/List/client/CRUD/list__action_redirect.html.twig',
                    ]
                );
        } else {
            $listMapper
                ->addIdentifier('name')
                ->addIdentifier(
                    'image',
                    null,
                    [
                        'template' => '@Admin/List/client/partials/sonata.image-preview.html.twig',
                    ]
                )
                ->addIdentifier('description')
                ->addIdentifier('category.name');
        }
    }

    /**
     * Manages Images Persistion. Since Image Entity has file property
     * that Doctrine doesn't knows about we need to change property of
     * Image to trigger load Event.
     *
     * @param Advertise $advertise
     */
    private function manageEmbeddedImage($advertise)
    {
        /** @var Image $image */
        foreach ($advertise->getImages() as $image) {
            if ($image) {
                if ($image->getImageFile()) {
                    $user = $this->getUser();

                    // update the Image to trigger file management
                    $image->refreshUpdated();
                    $image->setEntity($advertise);
                    $image->setOwner($this->getUser());
                    if (in_array($user, $image->getUsers()->toArray()) === false) {
                        $image->addUser($this->getUser());
                    }
                } elseif (!$image->getImageFile() && !$image->getImageName()) {
                    // prevent Sf/Sonata trying to create and persist an empty Image
                    $advertise->removeImage($image);
                }
            }
        }
    }
}
