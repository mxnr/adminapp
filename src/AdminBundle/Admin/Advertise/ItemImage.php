<?php

namespace AdminBundle\Admin\Advertise;

use AdminBundle\Admin\ImageAdmin;

/**
 * Class ItemImage.
 */
class ItemImage extends ImageAdmin
{
    /**
     * Route Name.
     *
     * @var string
     */
    protected $baseRouteName = 'admin_advertise_image';

    /**
     * Route Pattern.
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-advertise-image';
}
