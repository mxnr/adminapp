<?php

namespace AdminBundle\Admin\Advertise;

use AdminBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Class Category.
 */
class Category extends BaseAdmin
{
    protected $baseRouteName = 'advertise_category';

    protected $baseRoutePattern = 'advertise-categories';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->orderBy($query->getRootAliases()[0] . '.orderNumber', 'ASC');

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Category')
            ->with('General', ['class' => 'col-md-6'])
            ->add('name', 'text')
            ->add('orderNumber', 'number', ['label' => $this->trans('Ordering number')])
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }
}
