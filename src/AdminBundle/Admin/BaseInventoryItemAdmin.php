<?php

namespace AdminBundle\Admin;

use AdminBundle\Admin\Sequential\SequentialTrait;
use AdminBundle\Form\DataTransformer\InventoryItemModelTransformer;
use AdminBundle\Form\Type\ThreeWebModelType;
use AdminBundle\Service\InventoryItemService;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Validator\Constraints\GreaterThan;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;

class BaseInventoryItemAdmin extends BaseAdmin
{
    use SequentialTrait;

    /**
     * @var InventoryItemService
     */
    protected $inventoryItemService;

    /**
     * @param InventoryItemService $inventoryItemService
     */
    public function setInventoryItemService(InventoryItemService $inventoryItemService)
    {
        $this->inventoryItemService = $inventoryItemService;
    }

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_inventory_item_hidden';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-inventory-item-hidden';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->where($query->getRootAliases()[0] . '.owner = :owner')
            ->setParameter('owner', $this->getUser())
            ->andWhere($query->getRootAliases()[0] . '.status not in (:types)')
            ->setParameter('types', [InventoryItem::STATUS_PREPARED, InventoryItem::STATUS_COMPLETED_HIDDEN]);

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Inventory Item')
            ->with('General', ['class' => 'col-md-6'])
            ->add(
                'searchModel',
                ThreeWebModelType::class,
                [
                    'required' => false,
                    'mapped' => false,
                    'model_var' => 'vm.modelSelected',
                    'search_function' => 'vm.getModels()',
                    'refresh_function' => 'vm.refreshResults($select)',
                    'inline' => false,
                    'attr' => [
                        'ng-model' => 'vm.modelSelectedJson',
                        'class' => 'hidden-field'
                    ]
                ]
            )
            ->add(
                'purchasePrice',
                MoneyType::class,
                [
                    'divisor' => 100,
                    'currency' => 'USD',
                    'constraints' => new GreaterThan(['value' => 0]),
                ]
            )
            ->add(
                'quantity',
                IntegerType::class,
                [
                    'constraints' => new GreaterThan(['value' => 0]),
                ]
            )
            ->end();

        $inventoryItemModelTransformer = new InventoryItemModelTransformer(
            $this->modelManager->getEntitymanager(InventoryItem::class)
        );
        $formMapper->get('searchModel')->addModelTransformer($inventoryItemModelTransformer);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add(
            'storeId',
            CallbackFilter::class,
                [
                    'callback' => [$this, 'getSequentialIdFilter'],
                    'field_type' => 'text',
                    'label' => 'id'
                ]
            )
            ->add('purchasePrice', CallbackFilter::class, [
                'callback' => [$this, 'getPriceFilter'],
                'field_type' => 'text'
            ])
            ->add(
                'title',
                null,
                [
                    'show_filter' => true,
                ],
                null,
                [
                    'attr' => [
                        'ng-model' => 'vm.title',
                        'uib-typeahead' => 'title for title in vm.titles | filter:$viewValue | limitTo:8',
                        'autocomplete' => "off"
                    ]
                ]
            )
            ->add(
                'barcode',
                'doctrine_orm_callback',
                [
                    'show_filter' => true,
                    'callback' => function($queryBuilder, $alias, $field, $value) {
                        if (!$value['value']) {
                            return;
                        }

                        $queryBuilder->innerJoin(sprintf('%s.inventoryItemBarcodes', $alias), 'ib');
                        $queryBuilder->andWhere('ib.barcode = :barcode');
                        $queryBuilder->setParameter('barcode', $value['value']);

                        return true;
                    },
                ],
                null,
                [
                    'attr' => [
                        'ng-model' => 'vm.barcode',
                        'uib-typeahead' => 'title for title in vm.barcodes | filter:$viewValue | limitTo:8',
                        'autocomplete' => "off"
                    ]
                ]
            )
            ->add(
                'status',
                'doctrine_orm_choice',
                [
                    'field_options' => [
                        'choices' => InventoryItem::STATUS_ARRAY,
                        'required' => false,
                        'multiple' => true,
                        'expanded' => false,
                    ],
                    'field_type' => 'choice',
                ]
                )
            ->add('type')
            ->add('priceValue', CallbackFilter::class, [
                'callback' => [$this, 'getPriceFilter'],
                'field_type' => 'text',
                'label' => 'Price'
            ])
            ->add(
                'model',
                'doctrine_orm_model_autocomplete',
                ['admin_code' => 'admin.model'],
                null,
                [
                    'property' => 'title',
                ]
            )
            ->add('updatedAt')
            ->add('createdAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add(
                'id',
                null,
                [
                    'template' => 'AdminBundle:List:sequential_id.html.twig',
                ]
            )
            ->add('title')
            ->add('purchasePrice', null, ['template' => '@Admin/List/purchase_price.html.twig'])
            ->add('quantity')
            ->add('realQuantity', null, ['label' => 'Available Qty'])
            ->add(
                'status',
                'choice',
                [
                    'choices' => array_flip(InventoryItem::STATUS_ARRAY),                ]
            )
            ->add('createdAt');
    }

    /**
     * Callback filter to get prices
     *
     * @param $queryBuilder
     * @param $alias
     * @param $field
     * @param $value
     *
     * @return bool
     */
    public function getPriceFilter($queryBuilder, $alias, $field, $value)
    {
        if (empty($value['value'])) {
            return false;
        }

        $queryBuilder
            ->andWhere(sprintf('%1$s.%2$s = :%2$s', $alias, $field))
            ->setParameter($field, $value['value']*100)
            ;

        return true;
    }
}
