<?php
namespace AdminBundle\Admin;

use AdminBundle\Form\Type\SignatureCaptureType;
use AdminBundle\Service\InventoryItemService;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SaleOrderAdmin extends BaseAdmin
{
    /**
     * @var InventoryItemService
     */
    protected $inventoryItemService;

    /**
     * @param InventoryItemService $inventoryItemService
     */
    public function setInventoryItemService(InventoryItemService $inventoryItemService)
    {
        $this->inventoryItemService = $inventoryItemService;
    }

    /**
     * Route Name
     *
     * @var string
     */
    protected $baseRouteName = 'admin_income_list';

    /**
     * Route Pattern
     *
     * @var string
     */
    protected $baseRoutePattern = 'admin-income-list';

    /**
     * {@inheritdoc}
     */
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->where($query->getRootAliases()[0] . '.owner = :owner');
        $query->setParameter('owner', $this->getUser());

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('General')
            ->with('Customer', ['class' => 'col-md-6'])
            ->add(
                'customer',
                'sonata_type_model',
                [
                    'required' => false,
                    'expanded' => false,
                    'multiple' => false,
                    'mapped' => false,
                    'class' => Customer::class,
                    'query' => $this->modelManager
                        ->getEntityManager(Customer::class)
                        ->getRepository(Customer::class)
                        ->getUserCustomersQuery($this->getUser()),
                ],
                [
                    'admin_code' => 'admin.customer',
                ]
            )
            ->end()
            ->with('Details', ['class' => 'col-md-6'])
            ->add(
                'deliveryType',
                'choice',
                [
                    'choices' => [
                        'Pick up' => Order::PICK_UP,
                        'Shipping' => Order::SHIPPING,
                    ],
                ]
            )
            ->add(
                'paymentType',
                'choice',
                [
                    'choices' => [
                        'Credit card' => Order::CREDIT_CARD,
                        'Cash' => Order::CASH,
                    ],
                ]
            )
            ->end()
            ->with('Items', ['class' => 'col-md-12'])
            ->add(
                'items',
                'sonata_type_collection',
                ['by_reference' => false],
                [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    'admin_code' => 'admin.base_inventory_item',
                ]
            )
            ->end()
            ->with('Signature', ['class' => 'col-md-6'])
            ->add(
                'signatureImage',
                SignatureCaptureType::class,
                [
                    'required' => false,
                    'mapped' => false
                ]
            )
            ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('incomeCustomer.email')
            ->add('createdAt');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('incomeCustomer.email')
            ->add('createdAt');
    }

    /**
     * @param Order $order
     */
    public function prePersist($order)
    {
        $signatureData = $this->getForm()->get('signatureImage')->getData();
        if ($signatureData) {
            $this->inventoryItemService->setSignatureImage($order, $signatureData);
        }
        $inventoryItemsForm = $this->getForm()->get('items');
        foreach ($inventoryItemsForm as $form) {
            $this->inventoryItemService->manageModelSelectData($form, $this->getUser());
        }
        $customer = $order->getCustomer();
        if ($customer) {
            $order->setFirstName($customer->getFirstname())
                ->setLastName($customer->getLastname())
                ->setPhone($customer->getPhone())
                ->setEmail($customer->getEmail());
        }
        $order->setSiteType(Order::BUYBACK)
            ->setStatus(Order::COMPLETED)
            ->setTermsChecked(true)
            ->setOwner($this->getUser());
    }

    /**
     * @param Order $order
     */
    public function preUpdate($order)
    {
        $signatureData = $this->getForm()->get('signatureImage')->getData();
        if ($signatureData) {
            $this->inventoryItemService->setSignatureImage($order, $signatureData);
        }
        $inventoryItemsForm = $this->getForm()->get('items');
        foreach ($inventoryItemsForm as $form) {
            $this->inventoryItemService->manageModelSelectData($form, $this->getUser());
        }

        foreach ($order->getItems() as $item) {
            $this->inventoryItemService->updatePrice($item);
        }
    }

    /**
     * @param Order $order
     */
    public function postPersist($order)
    {
        foreach ($order->getItems() as $item) {
            $this->inventoryItemService->generateBarcodes($item, $order);
        }
    }
}
