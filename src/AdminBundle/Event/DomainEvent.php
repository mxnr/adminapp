<?php

namespace AdminBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;

class DomainEvent extends Event
{
    const DOMAIN_CREATE = 'admin.domain.create';

    const DOMAIN_ADD_SUCCESS = 'admin.domain.create.success';

    const CUSTOM_DOMAIN_APPROVED = 'admin.custom.domain.approved';

    /**
     * @var ClientDomain
     */
    protected $domainClient;

    /**
     * @return mixed
     */
    public function getDomainClient(): ClientDomain
    {
        return $this->domainClient;
    }

    /**
     * @param mixed $domainClient
     */
    public function setDomainClient(ClientDomain $domainClient)
    {
        $this->domainClient = $domainClient;
    }
}
