<?php

namespace AdminBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use ThreeWebOneEntityBundle\Entity\Domain\Zone;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class DnsEvent
 */
class DnsEvent extends Event
{
    /**
     * @var Zone
     */
    private $zone;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $distributionId;

    /**
     * @var string
     */
    private $distributionDomain;

    /**
     * @return string|null
     */
    public function getDistributionId(): ?string
    {
        return $this->distributionId;
    }

    /**
     * @return bool
     */
    public function hasDistributionId(): bool
    {
        return !is_null($this->distributionId);
    }

    /**
     * @param string|null $distributionId
     *
     * @return DnsEvent
     */
    public function setDistributionId(string $distributionId = null): DnsEvent
    {
        $this->distributionId = $distributionId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDistributionDomain(): ?string
    {
        return $this->distributionDomain;
    }

    /**
     * @return bool
     */
    public function hasDistributionDomain(): bool
    {
        return !is_null($this->distributionDomain);
    }

    /**
     * @param string|null $distributionDomain
     *
     * @return DnsEvent
     */
    public function setDistributionDomain(string $distributionDomain = null): DnsEvent
    {
        $this->distributionDomain = $distributionDomain;

        return $this;
    }

    /**
     * @return Zone|null
     */
    public function getZone(): ?Zone
    {
        return $this->zone;
    }

    /**
     * @return bool
     */
    public function hasZone(): bool
    {
        return !is_null($this->zone);
    }

    /**
     * @param Zone|null $zone
     *
     * @return DnsEvent
     */
    public function setZone(Zone $zone = null): DnsEvent
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function hasUser(): bool
    {
        return !is_null($this->user);
    }

    /**
     * @param User|null $user
     *
     * @return DnsEvent
     */
    public function setUser(User $user = null): DnsEvent
    {
        $this->user = $user;

        return $this;
    }
}
