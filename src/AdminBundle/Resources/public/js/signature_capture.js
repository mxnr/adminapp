import SignaturePad from '../../../../../node_modules/signature_pad/dist/signature_pad.js';

$(document).ready(function () {
    var canvas = document.getElementById('signature-pad');

    function resizeCanvas() {
        var ratio =  Math.max(window.devicePixelRatio || 1, 1);
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext("2d").scale(ratio, ratio);
    }

    window.onresize = resizeCanvas;
    resizeCanvas();

    var signaturePad = new SignaturePad(canvas, {
        backgroundColor: 'rgb(255, 255, 255)'
    });

    document.getElementById('clear-signature').addEventListener('click', function () {
        signaturePad.clear();
        document.getElementById('signature-encode').value = '';
    });
    document.getElementById('save-signature').addEventListener('click', function () {
        document.getElementById('signature-encode').value = signaturePad.toDataURL('image/png');
    });
});
