(function(){
    'use strict';

    angular.module('adminApp.register')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['$anchorScroll', '$location'];

    function RegisterController($anchorScroll, $location) {
        var vm = this;

        vm.sellingType = false;
        vm.buyBackType = false;
        vm.repairType = false;
        vm.checkedCount = 0;

        vm.checkboxClickHandler = checkboxClickHandler;
        vm.init = init;
        vm.storeNameInvalid = true;

        function init(sell, buy, repair, storeName, isFormSubmitted) {
            vm.sellingType = sell;
            vm.buyBackType = buy;
            vm.repairType = repair;
            vm.storeName = storeName;

            if (isFormSubmitted) {
                $location.hash('registration-data-form');
                $anchorScroll();
            }

            checkboxClickHandler(sell, buy, repair);
        }

        /**
         * Click handler for checkbox elements
         *
         * @returns {number}
         */
        function checkboxClickHandler() {
            vm.checkedCount = (function() {
                var count = 0;
                for (var arg in arguments) {
                    if (arguments[arg]) {
                        count += 1;
                    }
                }
                return count;
            })(vm.sellingType, vm.buyBackType, vm.repairType);
        }
    }

})();
