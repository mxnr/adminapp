(function () {
    'use strict';

    angular.module('adminApp.profitCalculator')
        .service('calculationService', calculationService);

    /**
     * @type {[string]}
     */
    calculationService.$inject = [];

    /**
     * Calculation Service
     * @returns {{getRecalculatedData: getRecalculatedData, getProfitPerDayCalculation: getProfitPerDayCalculation, getCostToAcquireProductsPerDay: getCostToAcquireProductsPerDay, getProfitPerMonthCalculation: getProfitPerMonthCalculation, getMonthlyProfitsCalculations: getMonthlyProfitsCalculations}}
     */
    function calculationService() {

        var fieldData = {};
        var productsAcquiredPerDay = [1, 5, 10, 20];

        return {
            getRecalculatedData: getRecalculatedData,
            getProfitPerDayCalculation: getProfitPerDayCalculation,
            getCostToAcquireProductsPerDay: getCostToAcquireProductsPerDay,
            getProfitPerMonthCalculation: getProfitPerMonthCalculation,
            getMonthlyProfitsCalculations: getMonthlyProfitsCalculations,
        };

        /**
         * Get Recalculated Data
         * @param {Object} data
         * @returns {{profit: string, dailyExpense: string, returnOnInvestment: number, itemsNeededToSellToMakeADailyProfit: number, suggestedCostToAcquire: string}}
         */
        function getRecalculatedData(data) {
            fieldData = data;
            return {
                profit: profitCalculation(),
                dailyExpense: dailyExpenseCalculation(),
                returnOnInvestment: returnOnInvestmentCalculation(),
                itemsNeededToSellToMakeADailyProfit: itemsNeededToSellToMakeADailyProfitCalculation(),
                suggestedCostToAcquire: suggestedCostToAcquire()
            };
        }

        /**
         * Profit Calculation
         * (Selling cost * ( 1 - (Channel selling fee% + Credit card processing fee%))) – (Cost to ship + Cost to acquire)
         * @returns {number}
         */
        function profitCalculation() {
            var result = ((fieldData.sellingCost *
                (1 - (valueToPercent(fieldData.channelSellingFee) + valueToPercent(fieldData.creditCardProcessingFee)))) -
                (fieldData.costToShip + fieldData.costToAcquire)) * 100;

            return Math.round(result) / 100 || 0;
        }

        /**
         * Daily Expense Calculation
         * (Total monthly business expenses minus product costs / Days open for business)
         * @returns {number}
         */
        function dailyExpenseCalculation() {
            var result = (fieldData.totalMonthlyBusinessExpensesMinusProductCosts / fieldData.daysOpenedForBusinessPerMonth) * 100;

            return Math.round(result) / 100 || 0;
        }

        /**
         * Return On InvestmentCalculation(ROI)
         * (Profit / Cost to acquire) * 100
         * @returns {number}
         */
        function returnOnInvestmentCalculation() {
            var profit = profitCalculation();

            return Math.round((profit / fieldData.costToAcquire) * 100) || 0;
        }

        /**
         * Items Needed To Sell To Make A Daily Profit Calculation
         * (Daily expense / Profit)
         * @returns {number}
         */
        function itemsNeededToSellToMakeADailyProfitCalculation() {
            var dailyExpanse = dailyExpenseCalculation();
            var profit = profitCalculation();

            return Math.round(dailyExpanse / profit) || 0;
        }

        /**
         * Suggested Cost To Acquire
         * ((selling price-(cost to ship+((channel selling fee+credit card processing fee)*selling price)))*100)/((desired return on investment*100)+100)
         * @returns {number}
         */
        function suggestedCostToAcquire() {
            var result = ((fieldData.sellingCost - (fieldData.costToShip + (valueToPercent(fieldData.channelSellingFee + fieldData.creditCardProcessingFee) * fieldData.sellingCost))) * 100) /
                (fieldData.desiredReturnOnInvestment + 100);

            return Math.round(result) || 0;
        }

        /**
         * Get Profit Per Day Calculation
         * (Profit x Products Acquired per Day)
         * @param itemCount
         * @returns {number}
         */
        function getProfitPerDayCalculation(itemCount) {
            var profit = profitCalculation();
            var result = (profit * itemCount) * 100;

            return Math.round(result) / 100 || 0;
        }

        /**
         * Get Profit Per Month Calculation
         * (Profits per day x Days open for business per month)
         * @param itemCount
         * @returns {number}
         */
        function getProfitPerMonthCalculation(itemCount) {
            var profitPerDay = getProfitPerDayCalculation(itemCount);
            var result = (profitPerDay * fieldData.daysOpenedForBusinessPerMonth) * 100;

            return Math.round(result) / 100 || 0;
        }

        /**
         * Get Cost To Acquire Products Per Day
         * (Cost to Acquire x Products Acquired per Day)
         * @param itemCount
         * @returns {number}
         */
        function getCostToAcquireProductsPerDay(itemCount) {
            var result = (fieldData.costToAcquire * itemCount) * 100;

            return Math.round(result) / 100 || 0;
        }

        /**
         * Get Monthly Profits Calculations
         * @private
         * @returns {Object}
         */
        function getMonthlyProfitsCalculations() {
            var result = {};

            var fields = ['costToAcquireProductsPerDay', 'profitPerDayCalculation', 'profitPerMonthCalculation'];

            for (var field in fields) {
                var fieldName = fields[field];
                result[fieldName] = {};

                for (var products in productsAcquiredPerDay) {
                    var itemCount = productsAcquiredPerDay[products];

                    if (fieldName === 'costToAcquireProductsPerDay') {
                        result[fieldName][itemCount] = getCostToAcquireProductsPerDay(itemCount);
                    }
                    if (fieldName === 'profitPerDayCalculation') {
                        result[fieldName][itemCount] = getProfitPerDayCalculation(itemCount);
                    }
                    if (fieldName === 'profitPerMonthCalculation') {
                        result[fieldName][itemCount] = getProfitPerMonthCalculation(itemCount);
                    }
                }
            }
            return result;
        }

        /**
         * Value To Percent
         * @private
         * @param value
         * @returns {number}
         */
        function valueToPercent(value) {
            return value / 100;
        }
    }
})();
