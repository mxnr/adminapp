(function(){
    'use strict';

    angular.module('adminApp.profitCalculator', ['adminApp.core', 'zingchart-angularjs']);

    angular.module('adminApp.profitCalculator')
        .value('zcRenderConfig', {
            width: "100%",
            height: 'auto',
            autoResize: true
        });

    angular.module('adminApp.profitCalculator')
        .value('zcChartConfig', {
            "type" : "bar",
            "background-color":"#EDF0F5",
            "plot":{
                "borderRadiusTopLeft": "5px",
                "borderRadiusTopRight": "5px",
                "value-box": {
                    "text": "$%v"
                },
                "tooltip": {
                    "text": "%v"
                },
                "styles":["#7cb5ec","#434348","#90ed7d", "#f7a35c", "#8085e9"],
                "animation": {
                    "delay": "100",
                    "effect": "4",
                    "method": "5",
                    "sequence": "1"
                }
            },
            "scale-x": {
                "values": [
                    "Selling Price",
                    "Cost",
                    "Cost to Ship",
                    "Profit",
                    "Daily Expanse"
                ]
            },
            "scale-y": {
                "offset": 25
            },
            "series": [
                {
                    "values": [0, 0, 0, 0, 0],
                }
            ]
        });

    angular.module('adminApp.profitCalculator')
        .constant('STORAGE_KEY', 'profit-calculator');
})();
