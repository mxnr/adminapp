(function() {
    'use strict';

    angular.module('adminApp.profitCalculator')
        .controller('ProfitCalculatorController', ProfitCalculatorController);

    /**
     * @type {[string,string,string,string,string,string,string]}
     */
    ProfitCalculatorController.$inject = [
        '$scope',
        '$timeout',
        'calculationService',
        'localStorageService',
        'zcRenderConfig',
        'zcChartConfig',
        'STORAGE_KEY'
    ];

    /**
     * Profit Calculator Controller
     * @param $scope
     * @param $timeout
     * @param calculationService
     * @param localStorageService
     * @param zcRenderConfig
     * @param zcChartConfig
     * @param STORAGE_KEY
     * @constructor
     */
    function ProfitCalculatorController(
        $scope,
        $timeout,
        calculationService,
        localStorageService,
        zcRenderConfig,
        zcChartConfig,
        STORAGE_KEY
    ) {
        var vm = this;

        // Default calculator field data
        vm.fields = {
            costToAcquire: 100,
            sellingCost: 200,
            costToShip: 50,
            totalMonthlyBusinessExpensesMinusProductCosts: 10000,
            daysOpenedForBusinessPerMonth: 24,
            channelSellingFee: 6,
            creditCardProcessingFee: 3,
            desiredReturnOnInvestment: 3,
        };

        vm.calculated = {
            profit: 0,
            dailyExpense: 0,
            returnOnInvestment: 0,
            itemsNeededToSellToMakeADailyProfit: 0,
            suggestedCostToAcquire: 0
        };

        // Render config for zing-chart
        vm.zcRenderConfig = zcRenderConfig;

        // Chart config for zing-chart
        vm.zcChartConfig = zcChartConfig;

        vm.init = function() {
            var savedData = localStorageService.get(STORAGE_KEY);
            if (savedData !== null) {
                vm.fields = savedData;
            }
        };

        vm.saveData = function() {
            localStorageService.set(STORAGE_KEY, vm.fields);
        };

        $scope.$watchCollection(
            function () {
                return vm.fields;
            },
            function () {
                if (isEmptyFields()) {
                    vm.calculated = calculationService.getRecalculatedData(vm.fields);
                    vm.monthlyProfitsCalculations = calculationService.getMonthlyProfitsCalculations();
                    $timeout(function(){
                        vm.zcChartConfig.series = [
                            {
                                values: [
                                    vm.fields.sellingCost,
                                    vm.fields.costToAcquire,
                                    vm.fields.costToShip,
                                    vm.calculated.profit,
                                    vm.calculated.dailyExpense,
                                ],
                            }
                        ];
                    });
                }
            }
        );

        /**
         * Is Empty Fields
         * @private
         * @returns {boolean}
         */
        function isEmptyFields() {
            for (var field in vm.fields) {
                if (vm.fields[field] === undefined) {
                    return false;
                }
            }

            return true;
        }
    }
})();
