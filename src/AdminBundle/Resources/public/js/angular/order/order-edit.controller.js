(function() {
    'use strict';

    angular.module('adminApp.orderEdit')
        .controller('OrderEditController', OrderEditController);

    /**
     * @type {[string,string]}
     */
    OrderEditController.$inject = ['orderDataService', '$scope', '$compile', '$rootScope'];

    function OrderEditController(orderDataService, $scope, $compile, $rootScope) {
        var vm = this;
        vm.customer = {
            'businessPhone' : '',
            'note' : '',
            'company' : '',
            'country' : '',
            'firstname' : '',
            'lastName' : '',
            'email' : '',
            'phone' : '',
            'driverLicense' : '',
            'addressLine1' : '',
            'addressLine2' : '',
            'city' : '',
            'state' : '',
            'zip' : ''
        };
        vm.submittedCustomer = false;
        vm.customers = '';
        vm.barcodes = '';
        vm.showSignature = true;
        vm.tax = 0;
        vm.subtotal = Number(0).toFixed(2);
        vm.total = Number(0).toFixed(2);
        vm.amountPaid = 0;
        vm.dueTo = Number(0).toFixed(2);
        vm.rowPrices = [];
        vm.disclaimer = '';
        vm.disclaimerType = '';

        vm.init = function (disclaimer, disclaimerType, amountPaid, tax) {
            vm.disclaimer = disclaimer;
            vm.disclaimerType = disclaimerType;
            vm.amountPaid = Number(amountPaid).toFixed(2);
            vm.tax = tax || 0;
            fetchCustomers();
            orderDataService.fetchBarcode().then(function (response) {
                vm.barcodes = response.data;
            });

            var holder = document.getElementById('inventory-items');
            if (holder) {
                var items = document.getElementsByClassName("sell-item");
                holder.dataset.index = parseInt(items.length + 1);
            }
        };

        vm.getCustomers = function() {
            return vm.customers;
        };

        vm.getBarcodes = function() {
            return vm.barcodes;
        };

        $scope.$watch('vm.purchaseBarcodeSelected', function(newVal, oldVal) {
            if (newVal && newVal !== oldVal) {
                vm.purchaseBarcodeSelected = '';
                var form = document.getElementById('order_item_' + newVal);
                if (!form) {
                    orderDataService.fetchBarcodeInformation(newVal).then(function (response) {
                        var holder = document.getElementById('sell-items');
                        var newForm = holder.dataset.prototype;
                        newForm = newForm.replace(/__name__/g, newVal);
                        var remove = createRemoveButton(newVal);
                        var formHeader = createInventoryItemHeader(response.data);
                        newForm = wrapContent(formHeader + newForm + remove, newVal, 'sell-item', response.data.price);
                        angular.element('#core').injector().invoke(['$rootScope', '$compile', function($rootScope, $compile) {
                            angular.element('#sell-items').append($compile(newForm)($rootScope));
                        }]);
                        document.getElementById('order_orderItems_' + newVal + '_barcode').value = newVal;
                        document.getElementById('order_orderItems_' + newVal + '_sellPrice').value = response.data.price;
                    });
                }
            }
        });

        vm.addInventoryItem = function() {
            var holder = document.getElementById('sell-items');
            var newForm = holder.dataset.prototype;
            var newVal = holder.dataset.index;
            holder.dataset.index = Number(newVal) + 1;
            newForm = newForm.replace(/__name__/g, newVal);
            var remove = createRemoveButton(newVal);
            newForm = wrapContent(newForm + remove, newVal, 'buyback-item');

            angular.element('#core').injector().invoke(['$rootScope', '$compile', function($rootScope, $compile) {
                angular.element('#sell-items').append($compile(newForm)($rootScope));
            }]);
        };

        $rootScope.$on('price-change', function (event, data) {
            var add = true;
            vm.rowPrices.forEach(function (item) {
                if (item.row == data.row) {
                    item.price = data.price;
                    add = false;
                }
            });
            if (add) {
                vm.rowPrices.push(data);
            }

            calculatePrices()
        });

        vm.amountPaidChange = function() {
            if (vm.amountPaid) {
                vm.amountPaid = vm.amountPaid.replace(/[^.0-9]/gim, '');
                vm.amountPaid = vm.amountPaid.replace(/^([^\.]*\.)|\./g, '$1');
            }

            calculatePrices();
        };

        vm.taxChange = function() {
            if (vm.tax) {
                vm.tax = vm.tax.replace(/[^.0-9]/gim, '');
                vm.tax = vm.tax.replace(/^([^\.]*\.)|\./g, '$1');
            }

            calculatePrices();
        };

        $rootScope.$on('remove-row', function (event, data) {
            document.getElementById('order_item_' + data.row).remove();
            vm.rowPrices.forEach(function (item) {
                if (item.row == data.row) {
                    item.price = 0;
                }
            });
            calculatePrices();
        });

        vm.formatAmountPaid = function () {
            if (vm.amountPaid) {
                vm.amountPaid = Number(vm.amountPaid).toFixed(2);
            }
        };

        function calculatePrices() {
            vm.subtotal = 0;
            if (!vm.amountPaid) {
                vm.amountPaid = '';
            }
            vm.rowPrices.forEach(function (item) {
                vm.subtotal += item.price;
            });
            vm.subtotal = Number(vm.subtotal).toFixed(2);
            vm.total = Number(parseFloat(vm.subtotal) + parseFloat(vm.subtotal * vm.tax / 100)).toFixed(2);
            vm.dueTo = Number(vm.total - parseFloat(vm.amountPaid ? vm.amountPaid : 0)).toFixed(2);
        }

        vm.editDisclaimer = function () {
            var data = {
                'type': vm.disclaimerType,
                'content': CKEDITOR.instances.disclaimer_content.getData()
            };
            orderDataService.editDisclaimer(data).then(function (response) {});
            vm.disclaimer = data.content;
            document.getElementById('close_disclaimer_modal').click();
        };

        vm.createCustomer  = function () {
            vm.submittedCustomer = true;
            if (vm.customer.email) {
                orderDataService.createCustomer(vm.customer).then(
                    function(response){
                        clearCustomerForm();
                        document.getElementById('close_customer_modal').click();
                        fetchCustomers();
                        vm.submittedCustomer = false;
                    },
                    function(response){
                        document.getElementById('close_customer_modal').click();
                        vm.submittedCustomer = false;
                    }
                );
            }
        };

        function clearCustomerForm() {
            for(var index in vm.customer) {
                vm.customer[index] = '';
            }
        }
        function fetchCustomers() {
            orderDataService.fetchCustomer().then(function (response) {
                vm.customers = response.data;
                vm.customerSelected = parseInt(vm.presetCustomer);
                vm.showSignature = false;
            });
        }

        function createInventoryItemHeader(data) {
            var formHeader = '<div class="sell-item-header">';
            formHeader += '<div>' + data.model + '</div>';
            formHeader += '<div>' + data.barcode + '</div>';
            formHeader += '<div>' + data.priceType + '</div>';
            formHeader += '<div>$' + data.price.toFixed(2) + '</div>';
            formHeader += '</div>';

            return formHeader;
        }

        function createRemoveButton(value) {
            return '<div class="btn btn-danger remove-button" ng-click="vmRow.removeRow(' + value + ')">Remove</div>';
        }

        function wrapContent(content, value, rowClass, price) {
            return '<div ng-controller="ModelWidgetController as vmRow" ng-init="vmRow.price='+ price +';vmRow.init(' + value + ')" class="' + rowClass + '" id ="order_item_' + value + '">' + content + '</div>';
        }
    }
})();