(function() {
    'use strict';

    angular.module('adminApp.orderEdit')
        .factory('orderDataService', orderDataService);

    orderDataService.$inject = ['$http'];

    function orderDataService($http) {
        return {
            fetchCustomer: fetchCustomer,
            fetchBarcode: fetchBarcode,
            fetchBarcodeInformation: fetchBarcodeInformation,
            createCustomer: createCustomer,
            editDisclaimer: editDisclaimer,
            sendFileForImport: sendFileForImport
        };

        /**
         * Fetch customers data
         *
         * @returns {promise} $http
         */
        function fetchCustomer() {
            return $http({
                method: 'GET',
                url: '/admin/api/customers'
            });
        }

        /**
         * Fetch barcodes data
         *
         * @returns {promise} $http
         */
        function fetchBarcode() {
            return $http({
                method: 'GET',
                url: '/admin/api/barcodes'
            });
        }

        /**
         * Fetch barcode info data
         *
         * @returns {promise} $http
         */
        function fetchBarcodeInformation(id) {
            return $http({
                method: 'GET',
                url: '/admin/api/barcode/' + id
            });
        }

        /**
         * Create customer
         *
         * @returns {promise} $http
         */
        function createCustomer(data) {
            return $http({
                method: 'POST',
                url: '/admin/api/customer',
                data: data
            });
        }

        /**
         * Edit Disclaimer
         * @param data
         * @returns {promise}
         */
        function editDisclaimer(data) {
            return $http({
                method: 'POST',
                url: '/admin/api/disclaimer',
                data: data
            })
        }

        /**
         * Sends file for import
         *
         * @param data
         * @returns {promise}
         */
        function sendFileForImport(data) {
            var fd = new FormData();
            fd.append('file', data);
            return $http.post('/admin/api/inventory/import', fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
        }
    }
})();