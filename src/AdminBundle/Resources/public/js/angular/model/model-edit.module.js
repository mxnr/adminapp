(function(){
    'use strict';

    angular.module('adminApp.modelEdit', ['ui.select', 'ngSanitize']);
})();
