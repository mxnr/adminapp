(function() {
    'use strict';

    angular.module('adminApp.modelEdit')
        .controller('ModelEditController', ModelEditController);

    /**
     * @type {[string]}
     */
    ModelEditController.$inject = ['categoriesDataService', '$scope'];

    function ModelEditController(categoriesDataService, $scope) {
        var vm = this;
        var TYPE_CATEGORY = 1;
        var TYPE_PRODUCT = 2;
        var TYPE_PROVIDER = 3;
        vm.categories = '';
        vm.providers = '';
        vm.products = '';
        vm.showAddCategory = false;
        vm.showAddprovider = false;
        vm.showAddProduct = false;
        vm.categorySearch = '';
        vm.providerSearch = '';
        vm.productSearch = '';

        vm.init = function () {
            categoriesDataService.fetchUsersCategories(TYPE_CATEGORY).then(function (response) {
                vm.categories = response.data;
                vm.categorySelected = parseInt(vm.presetCategory);
            });
            categoriesDataService.fetchUsersCategories(TYPE_PROVIDER).then(function (response) {
                vm.providers = response.data;
                vm.providerSelected = parseInt(vm.presetProvider);
            });
            categoriesDataService.fetchUsersCategories(TYPE_PRODUCT).then(function (response) {
                vm.products = response.data;
                vm.productSelected = parseInt(vm.presetProduct);
            });
        };

        vm.getCategories = function(search) {
            var newCategories = vm.categories.slice();
            var elementExist = searchInArray(search, newCategories);
            if (search) {
                if (elementExist) {
                    vm.showAddCategory = false;
                } else {
                    vm.showAddCategory = true;
                }
            }
            return newCategories;
        };

        vm.getSearchOfCategory = function(value) {
            if (value) {
                vm.categorySearch = value;
            }
        };

        vm.createCategory = function () {
            getBase64(vm.imageCategory).then(function(result) {
                categoriesDataService.createCategory(JSON.stringify({'data': vm.categorySearch, 'image': result}), TYPE_CATEGORY).then(function (response) {
                    vm.categories.push(response.data);
                    vm.categorySelected = response.data.id;
                    vm.showAddCategory = false;
                });
            });
        };

        vm.getProviders = function(search) {
            var newProviders = vm.providers.slice();
            var elementExist = searchInArray(search, newProviders);
            if (search) {
                if (elementExist) {
                    vm.showAddProvider = false;
                } else {
                    vm.showAddProvider = true;
                }
            }
            return newProviders;
        };

        vm.getSearchOfProvider = function(value) {
            if (value) {
                vm.providerSearch = value;
            }
        };

        vm.createProvider = function () {
            categoriesDataService.createCategory(JSON.stringify({'data': vm.providerSearch}), TYPE_PROVIDER).then(function (response) {
                vm.providers.push(response.data);
                vm.providerSelected = response.data.id;
                vm.showAddProvider = false;
            });
        };

        vm.getProducts = function(search) {
            var newProducts = vm.products.slice();
            var elementExist = searchInArray(search, newProducts);
            if (search) {
                if (elementExist) {
                    vm.showAddProduct = false;
                } else {
                    vm.showAddProduct = true;
                }
            }
            return newProducts;
        };

        vm.getSearchOfProduct = function(value) {
            if (value) {
                vm.productSearch = value;
            }
        };

        vm.createProduct = function () {
            categoriesDataService.createCategory(JSON.stringify({'data': vm.productSearch}), TYPE_PRODUCT).then(function (response) {
                vm.products.push(response.data);
                vm.productSelected = response.data.id;
                vm.showAddProduct = false;
            });
        };

        $scope.$watch('vm.categorySelected', function(newVal, oldVal) {
            if (newVal) {
                vm.showAddCategory = false;
            }
        });

        $scope.$watch('vm.providerSelected', function(newVal, oldVal) {
            if (newVal) {
                vm.showAddProvider = false;
            }
            console.log(newVal)
        });

        $scope.$watch('vm.productSelected', function(newVal, oldVal) {
            if (newVal) {
                vm.showAddProduct = false;
            }
        });

        function searchInArray(nameKey, myArray){
            for (var i=0; i < myArray.length; i++) {
                if (myArray[i].name.indexOf(nameKey) !== -1) {
                    return myArray[i];
                }
            }
        }

        function getBase64(file) {
            return new Promise(function(resolve, reject) {
                if (file) {
                    var reader = new FileReader();
                    reader.onload = function () {
                        resolve(reader.result);
                    };
                    reader.onerror = reject;
                    reader.readAsDataURL(file);
                } else {
                    resolve();
                }
            });
        }
    }
})();