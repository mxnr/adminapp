(function() {
    'use strict';

    angular.module('adminApp.modelEdit')
        .factory('categoriesDataService', categoriesDataService);

    categoriesDataService.$inject = ['$http'];

    function categoriesDataService($http) {
        return {
            fetchUsersCategories: fetchUsersCategories,
            createCategory: createCategory
        };

        /**
         * Fetch table data
         *
         * @param {string} type
         * @returns {promise} $http
         */
        function fetchUsersCategories(type) {
            return $http({
                method: 'GET',
                url: '/admin/api/categories/' + type
            });
        }

        /**
         * Get countries
         *
         * @returns {promise} $http
         */
        function createCategory(data, type) {
            return $http({
                method: 'POST',
                url: '/admin/api/categories/new/' + type,
                data: data
            });
        }
    }
})();