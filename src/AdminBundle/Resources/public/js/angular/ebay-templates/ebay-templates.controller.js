(function(){
    'use strict';

    angular.module('adminApp.ebayTemplates')
        .controller('EbayTemplatesController', EbayTemplatesController);

    /**
     * @type {Array}
     */
    EbayTemplatesController.$inject = [];

    /**
     * Ebay Templates Controller
     * @constructor
     */
    function EbayTemplatesController() {
        var vm = this;

        vm.supported = false;

        vm.textToCopy = angular.element('.js-copy')[0].value;

        /**
         * angular-clipboard error handler
         * @param err
         */
        vm.fail = function (err) {
            console.error('angular-clipboard error: ', err);
        };
    }
})();
