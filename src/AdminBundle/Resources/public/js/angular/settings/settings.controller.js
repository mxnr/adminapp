(function() {
    'use strict';

    angular.module('adminApp.settings')
        .controller('SettingsController', SettingsController);

    /**
     * @type {[]}
     */
    SettingsController.$inject = [];

    function SettingsController() {
        var vm = this;
        vm.alert = false;
        vm.alertMessage = '';

        vm.showMessage = function () {
            vm.alert = true;
            vm.alertMessage = 'Your plan doesn\'t include a selling website';
        }
    }
})();