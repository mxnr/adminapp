(function() {
    'use strict';

    angular.module('adminApp.orderEdit')
        .factory('modelsDataService', modelsDataService);

    modelsDataService.$inject = ['$http'];

    function modelsDataService($http) {
        var modelsData = '';

        return {
            fetchUsersModels: fetchUsersModels
        };

        /**
         * Fetch users models
         *
         * @param callbackFn
         */
        function fetchUsersModels(callbackFn) {
            if (modelsData) {
                callbackFn(modelsData);
                return;
            }

            $http({
                method: 'GET',
                url: '/admin/api/models'
            }).then(function (response) {
                modelsData = response.data;
                callbackFn(modelsData)
            });
        }
    }
})();