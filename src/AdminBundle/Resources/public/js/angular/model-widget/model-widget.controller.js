(function() {
    'use strict';

    angular.module('adminApp.orderEdit')
        .controller('ModelWidgetController', ModelWidgetController);

    /**
     * @type {[string]}
     */
    ModelWidgetController.$inject = ['modelsDataService', '$scope'];

    function ModelWidgetController(modelsDataService, $scope) {
        var vm = this;
        vm.models = '';
        vm.modelSelected = '';
        vm.price = '';
        vm.total = 0;
        vm.row = '';

        vm.init = function (data, modelData) {
            if (modelData) {
                if (typeof modelData !== 'object') {
                    vm.modelSelected = JSON.parse(modelData);
                    vm.modelSelectedJson = modelData;
                } else {
                    vm.modelSelected = modelData;
                    vm.modelSelectedJson = JSON.stringify(modelData);
                }
            }
            vm.row = data;
            modelsDataService.fetchUsersModels(function (data) {
                vm.models = data;
            });
            vm.total = (parseInt(vm.quantity ? vm.quantity : 1) * parseFloat(vm.price));
            emitData({'price': vm.total, 'row': vm.row});
            vm.formatPrice();
        };

        vm.getModels = function() {
            return vm.models;
        };

        vm.refreshResults = function($select){
            var search = $select.search,
                list = angular.copy($select.items),
                FLAG = -1;
            //remove last user input
            list = list.filter(function(item) {
                return item.id !== FLAG;
            });

            if (!search) {
                //use the predefined list
                $select.items = list;
            } else {
                //manually add user input and set selection
                var userInputItem = {
                    id: FLAG,
                    name: search
                };
                $select.items = [userInputItem].concat(list);
                $select.selected = userInputItem;
            }
        };

        vm.removeRow = function(row) {
            console.log(row);
            $scope.$emit('remove-row', {'row': row})
        };

        $scope.$watch('vmRow.modelSelected', function(newVal, oldVal) {
            if (newVal && newVal !== oldVal) {
                vm.modelSelectedJson =  JSON.stringify(newVal)
            }
        });

        $scope.$watch('vmRow.price', function(newVal, oldVal) {
            if (!vm.quantity) {
                vm.quantity = 1;
            }
            if (!newVal) {
                newVal = 0;
            }
            if (newVal != oldVal || !oldVal) {
                vm.total = parseFloat(newVal) * parseInt(vm.quantity);
                emitData({'price': vm.total, 'row': vm.row});
                vm.total = Number(vm.total).toFixed(2);
            }
        });

        $scope.$watch('vmRow.quantity', function(newVal, oldVal) {
            if (newVal != oldVal && vm.price) {
                vm.total = (parseInt(vm.quantity) * parseFloat(vm.price));
                emitData({'price': vm.total, 'row': vm.row})
            }
        });

        vm.priceChange = function () {
            if (vm.price) {
                vm.price = vm.price.replace(/[^.0-9]/gim, '');
                vm.price = vm.price.replace(/^([^\.]*\.)|\./g, '$1');
            }
        };

        vm.formatPrice = function () {
            if (vm.price) {
                vm.price = Number(vm.price).toFixed(2);
            }
        };

        function emitData(data) {
            $scope.$emit('price-change', data)
        }

    }
})();