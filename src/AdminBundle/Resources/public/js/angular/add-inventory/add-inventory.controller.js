(function() {
    'use strict';

    angular.module('adminApp.orderEdit')
        .controller('AddInventoryController', AddInventoryController);

    /**
     * @type {[string,string]}
     */
    AddInventoryController.$inject = ['orderDataService', '$scope'];

    function AddInventoryController(orderDataService, $scope) {
        var vm = this;
        vm.customers = '';
        vm.customer = {
            'businessPhone' : '',
            'note' : '',
            'company' : '',
            'country' : '',
            'firstname' : '',
            'lastName' : '',
            'email' : '',
            'phone' : '',
            'driverLicense' : '',
            'addressLine1' : '',
            'addressLine2' : '',
            'city' : '',
            'state' : '',
            'zip' : ''
        };
        vm.submittedCustomer = false;
        vm.showSignature = true;
        vm.disclaimer = '';
        vm.disclaimerType = '';

        vm.init = function (disclaimer, disclaimerType) {
            fetchCustomers();

            vm.disclaimer = disclaimer;
            vm.disclaimerType = disclaimerType;
            var buttons = document.getElementsByClassName("remove-button");

            for (var i = 0; i < buttons.length; i++) {
                buttons[i].onclick = function () {
                    document.getElementById('order_item_' + this.dataset.id).remove();
                }
            }
            var holder = document.getElementById('inventory-items');
            if (holder) {
                var items = document.getElementsByClassName("sell-item");
                holder.dataset.index = parseInt(items.length + 1);
            }
            vm.addInventoryItem();
        };

        vm.getCustomers = function() {
            return vm.customers;
        };

        vm.addInventoryItem = function(item) {
            var holder = document.getElementById('sell-items');
            var newForm = holder.dataset.prototype;
            var newVal = holder.dataset.index;
            holder.dataset.index = newVal + 1;
            newForm = newForm.replace(/__name__/g, newVal);
            var remove = createRemoveButton(newVal);
            newForm = wrapContent(newForm + remove, newVal, item);

            angular.element('#core').injector().invoke(['$rootScope', '$compile', function($rootScope, $compile) {
                angular.element('#sell-items').append($compile(newForm)($rootScope));
            }]);
            document.getElementById('remove_' + newVal).onclick = function () {
                document.getElementById('order_item_' + this.dataset.id).remove();
            };
        };

        vm.createCustomer  = function () {
            vm.submittedCustomer = true;
            if (vm.customer.email) {
                orderDataService.createCustomer(vm.customer).then(
                    function(response){
                        clearCustomerForm();
                        document.getElementById('close_customer_modal').click();
                        fetchCustomers();
                        vm.submittedCustomer = false;
                    },
                    function(response){
                        document.getElementById('close_customer_modal').click();
                        vm.submittedCustomer = false;
                    }
                );
            }
        };

        function createRemoveButton(value) {
            return '<div class="btn btn-danger remove-button" id="remove_' + value + '" data-id="' + value +'">Remove</div>';
        }

        function wrapContent(content, value, item) {
            if (item) {
                return '<div ng-controller="ModelWidgetController as vmRow" ng-init="vmRow.price = ' + item[1] + ';vmRow.quantity = ' + item[2] + ';vmRow.init(' + value + ', { id: -1, name:\'' + item[0] + '\'})"  class="sell-item" id ="order_item_' + value + '">' + content + '</div>';
            }
            return '<div ng-controller="ModelWidgetController as vmRow" ng-init="vmRow.init()"  class="sell-item" id ="order_item_' + value + '">' + content + '</div>';
        }

        function clearCustomerForm() {
            for(var index in vm.customer) {
                vm.customer[index] = '';
            }
        }
        function fetchCustomers() {
            orderDataService.fetchCustomer().then(function (response) {
                vm.customers = response.data;
                vm.customerSelected = parseInt(vm.presetCustomer);
                vm.showSignature = false;
            });
        }

        vm.editDisclaimer = function () {
            var data = {
                'type': vm.disclaimerType,
                'content': CKEDITOR.instances.disclaimer_content.getData()
            };
            orderDataService.editDisclaimer(data).then(function (response) {});
            vm.disclaimer = data.content;
            document.getElementById('close_disclaimer_modal').click();
        };

        vm.importFile = function() {
            var file = $scope.myFile;
            orderDataService.sendFileForImport(file).then(
                function (response) {
                    response.data.forEach(function (item) {
                        vm.addInventoryItem(item);
                    });
                    $scope.myFile = null;
                },
                function (response) {}
            );
        }
    }
})();