(function() {
    'use strict';

    angular.module('adminApp.customerEdit')
        .factory('customerDataService', customerDataService);

    customerDataService.$inject = ['$http'];

    function customerDataService($http) {
        return {
            fetchUserNote: fetchUserNote,
            deleteNote: deleteNote,
            createNote: createNote
        };

        /**
         * Fetch users notes
         *
         * @returns {promise} $http
         */
        function fetchUserNote() {
            return $http({
                method: 'GET',
                url: '/admin/api/notes'
            });
        }

        /**
         * Fetch users notes
         *
         * @returns {promise} $http
         */
        function deleteNote(id) {
            return $http({
                method: 'GET',
                url: '/admin/api/notes/delete/' + id
            });
        }

        /**
         * Fetch users notes
         *
         * @returns {promise} $http
         */
        function createNote(data) {
            return $http({
                method: 'POST',
                url: '/admin/api/notes/create',
                data: data
            });
        }
    }
})();