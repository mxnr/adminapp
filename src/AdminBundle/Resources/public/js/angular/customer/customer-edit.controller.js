(function() {
    'use strict';

    angular.module('adminApp.customerEdit')
        .controller('CustomerEditController', CustomerEditController);

    /**
     * @type {[string]}
     */
    CustomerEditController.$inject = ['customerDataService'];

    function CustomerEditController(customerDataService) {
        var vm = this;
        vm.notes = '';
        vm.content = '';

        vm.init = function () {
            fetchNotes()
        };

        vm.removeNote = function (id) {
            if (id) {
                customerDataService.deleteNote(id).then(function (response) {
                    fetchNotes();
                })
            }
        };

        vm.createNote = function () {
            if (vm.content) {
                customerDataService.createNote({'content': vm.content}).then(function (response) {
                    fetchNotes();
                    vm.content = '';
                })
            }
        };

        function fetchNotes() {
            customerDataService.fetchUserNote().then(function (response) {
                vm.notes = response.data;
            });
        }
    }
})();