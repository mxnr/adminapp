(function () {
    'use strict';

    angular.module('adminApp.businessPerformance')
        .service('businessPerformanceService', businessPerformanceService);

    /**
     * Business Performance Service
     * @type {[string]}
     */
    businessPerformanceService.$inject = ['$http'];

    /**
     * Business Performance Service
     * @param $http
     * @returns {{getSellingStats: getSellingStats}}
     */
    function businessPerformanceService($http) {

        return {
            getSellingStats: getSellingStats,
        };

        /**
         * Get Selling Stats
         * @param dateFrom
         * @param dateTo
         * @returns {*}
         */
        function getSellingStats(dateFrom, dateTo) {
            return $http({
                method: 'GET',
                url: '/admin/api/stats/selling',
                params: {dateFrom: dateFrom, dateTo: dateTo}
            });
        }
    }
})();
