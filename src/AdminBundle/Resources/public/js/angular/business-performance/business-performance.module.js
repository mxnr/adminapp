(function () {
    'use strict';

    angular.module('adminApp.businessPerformance', ['zingchart-angularjs', '720kb.datepicker']);

    angular.module('adminApp.businessPerformance')
        .value('zcRenderConfig', {
            width: "100%",
            height: 'auto',
            autoResize: true,
            overflow: 'auto'
        });

    angular.module('adminApp.businessPerformance')
        .value('zcChartConfig', {
            "layout": "1x2",
            "graphset": [
                {
                    "type": "bar",
                    "title": {
                        "text": "Inventory Sold"
                    },
                    "plot": {
                        "borderRadiusTopLeft": "5px",
                        "borderRadiusTopRight": "5px",
                        "value-box": {
                            "text": "$%v"
                        },
                        "tooltip": {
                            "text": "%v"
                        },
                        "animation": {
                            "delay": "100",
                            "effect": "4",
                            "method": "5",
                            "sequence": "1"
                        }
                    },
                    "scale-x": {
                        "values": []
                    },
                    "series": [
                        {"values": [0]}
                    ]
                },
                {
                    "type": "bar",
                    "title": {
                        "text": "Payments Collected"
                    },
                    "plot": {
                        "borderRadiusTopLeft": "5px",
                        "borderRadiusTopRight": "5px",
                        "value-box": {
                            "text": "%v"
                        },
                        "tooltip": {
                            "text": "%v"
                        },
                        "animation": {
                            "delay": "100",
                            "effect": "4",
                            "method": "5",
                            "sequence": "1"
                        }
                    },
                    "scale-x": {
                        "values": []
                    },
                    "series": [
                        {"values": [0]}
                    ]
                }
            ]
        });
})();
