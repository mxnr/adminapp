(function () {
    'use strict';

    angular.module('adminApp.businessPerformance')
        .controller('BusinessPerformanceController', BusinessPerformanceController);

    /**
     * @type {[string,string,string]}
     */
    BusinessPerformanceController.$inject = ['businessPerformanceService', 'zcRenderConfig', 'zcChartConfig'];

    /**
     * @param businessPerformanceService
     * @param zcRenderConfig
     * @param zcChartConfig
     * @constructor
     */
    function BusinessPerformanceController(businessPerformanceService, zcRenderConfig, zcChartConfig) {
        // Initial values
        var vm = this;
        vm.dateFrom = '';
        vm.dateTo = '';
        vm.data = {};
        vm.chartDates = [];
        vm.startDate = getFirstDayOfMonth();
        vm.endDate = getLastDayOfMonth();
        var inventorySoldValues = [];
        var paymentCollectedValues = [];

        // Constants
        var INVENTORY_SOLD_CHART_ID = 0;
        var PAYMENTS_COLLECTED_CHART_ID = 1;

        // Render config for zing-chart
        vm.zcRenderConfig = zcRenderConfig;

        // Chart config for zing-chart
        vm.zcChartConfig = zcChartConfig;

        vm.getStatsData = function () {
            businessPerformanceService.getSellingStats(vm.dateFrom, vm.dateTo).then(function (response) {
                var data = JSON.parse(response.data);

                if (data.hasOwnProperty('ordersInfo')) {
                    vm.data = data;
                    updateChart(data.ordersInfo);
                } else {
                    console.error('index \'ordersInfo\' doesn\'t exist');
                }
            });
        };

        /**
         * Update Chart
         * @private
         * @param {array} ordersInfo
         */
        function updateChart(ordersInfo) {
            //Reset chart data
            vm.chartDates = [];
            inventorySoldValues = [];
            paymentCollectedValues = [];

            for (var key in ordersInfo) {
                if (ordersInfo.hasOwnProperty(key)) {
                    vm.chartDates.push(ordersInfo[key].createdDate.date.slice(0, 10));
                    inventorySoldValues.push(ordersInfo[key].itemsPurchasePrice / 100);
                    paymentCollectedValues.push(ordersInfo[key].itemsCount);
                }
            }

            //Update inventory sold chart info
            vm.zcChartConfig.graphset[INVENTORY_SOLD_CHART_ID]['scale-x']['values'] = vm.chartDates;
            vm.zcChartConfig.graphset[INVENTORY_SOLD_CHART_ID]['series'][0]['values'] = inventorySoldValues;

            //Update payment collected chart info
            vm.zcChartConfig.graphset[PAYMENTS_COLLECTED_CHART_ID]['scale-x']['values'] = vm.chartDates;
            vm.zcChartConfig.graphset[PAYMENTS_COLLECTED_CHART_ID]['series'][0]['values'] = paymentCollectedValues;
        }

        /**
         * Get First Day Of Month
         * @private
         * @returns {string}
         */
        function getFirstDayOfMonth() {
            var nowDate = new Date();
            var date = new Date(nowDate.getFullYear(), nowDate.getMonth(), 1);

            return date.toString();
        }

        /**
         * getLastDayOfMonth
         * @private
         * @returns {string}
         */
        function getLastDayOfMonth() {
            var nowDate = new Date();
            var date = new Date(nowDate.getFullYear(), nowDate.getMonth() + 1, 0);

            return date.toString();
        }
    }
})();
