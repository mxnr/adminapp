(function(){
   'use strict';
    angular.module('adminApp.domain')
        .controller('DomainListController', DomainListController);

    DomainListController.$inject = ['$scope', 'domainRequestService'];

    function DomainListController($scope, domainRequestService) {
        var vm = this;
        var parent = $scope.$parent.DomainMainVm;

        vm.domains = [];

        vm.init = function(domains) {
            vm.domains = JSON.parse(domains);
        };

        vm.getDomainInfo = function($event, domain) {
            if (domain.loader === true) {
                return true;
            }

            if (domain.expireDate) {
                parent.toggleAdditionalBlock($event);
                return true;
            }

            domain.loader = true;

            domainRequestService.getExpireDateAction(domain.domain).then(function(response){
                var data = response.data;

                if (data.status === 'ok') {
                    domain.expireDate = data.content;
                }

                parent.toggleAdditionalBlock($event);
                domain.loader = false;
            }, function(response){
                domain.loader = false;
                var data = response.data;
                parent.displayErrorAlert(data.content);
            });
        };
    }
})();