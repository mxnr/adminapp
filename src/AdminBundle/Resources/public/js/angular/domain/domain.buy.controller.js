(function () {
    'use strict';

    angular.module('adminApp.domain')
        .controller('DomainBuyController', DomainBuyController);

    DomainBuyController.$inject = ['$scope', 'domainRequestService', 'tlds'];

    function DomainBuyController($scope, domainRequestService, tlds) {
        var vm = this;
        var parent = $scope.$parent.DomainMainVm;

        vm.domainName = '';
        vm.loader = false;
        vm.tlds = tlds;
        vm.domains = {};
        vm.formSubmitted = false;
        vm.owner = {
            "country": "",
            "org_name": "",
            "phone": "",
            "state":"",
            "address2":"",
            "email":"",
            "city":"",
            "postal_code":"",
            "fax":"",
            "address1":"",
            "first_name":"",
            "last_name": ""
        };

        vm.init = function(orgName, firstName, lastName, email, phone) {
            vm.owner.org_name = orgName;
            vm.owner.first_name = firstName;
            vm.owner.last_name = lastName;
            vm.owner.email = email;
            vm.owner.phone = phone;
        };

        vm.searchSuggestedDomains = function () {
            var tlds = getActiveTlds();

            vm.domains = [];

            if (vm.loader === true) {
                return true;
            }

            if (vm.domainName !== '' && tlds.length > 0) {
                vm.loader = true;
                parent.hideErrorAlert();
                domainRequestService.getSuggestionDomainAction(vm.domainName, tlds).then(
                    function (response) {
                        var data = response.data;

                        if (data.status === 'ok') {
                            vm.domains = data.content.lookup;
                        }

                        vm.loader = false;
                    }, function(response) {
                        var data = response.data;
                        vm.domains = [];
                        parent.displayErrorAlert(data.content);
                        vm.loader = false;
                    }
                );
            }
        };

        vm.toggleTld = function (tld) {
            if (tld in vm.tlds) {
                vm.tlds[tld] = !vm.tlds[tld];
            }
        };

        vm.hasDomains = function () {
            return !!Object.keys(vm.domains).length;
        };

        vm.getDomainPrice = function (event, domain) {
            vm.formSubmitted = false;

            if (domain.searchLoader === true) {
                return true;
            }

            if (domain.price) {
                parent.toggleAdditionalBlock(event);
            } else {
                domain.searchLoader = true;
                domainRequestService.getDomainPriceAction(domain.domain).then(function(response) {
                    var data = response.data;

                    if (data.status === 'ok') {
                        closeAllAdditionalInfoBlocs();

                        domain.price = data.content;
                        domain.domElement = parent.getDomElement(event);

                        parent.toggleAdditionalBlock(event);
                    }

                    domain.searchLoader = false;
                }, function(response) {
                    domain.searchLoader = false;
                    var data = response.data;
                    parent.displayErrorAlert(data.content);
                });
            }
        };

        /**
         * Buy Domain
         * @param domain
         * @param form
         */
        vm.buyDomain = function(domain, form) {
            vm.formSubmitted = true;

            if (domain.purchaseDomainLoader === true) {
                return true;
            }

            if (form.$valid) {
                domain.purchaseDomainLoader = true;
                domainRequestService.buyDomainAction(domain.domain, vm.owner).then(function(response){
                    var data = response.data;
                    if (data.status === 'ok') {
                        window.location.replace('/admin/domain/register/' + data.content);
                    }
                    domain.purchaseDomainLoader = false;
                }, function(response ) {
                    domain.purchaseDomainLoader = false;
                    var data = response.data;
                    parent.displayErrorAlert(data.content);
                });
             }
        };

        /**
         * Get Active Tlds
         * @returns {Array}
         */
        function getActiveTlds() {
            var result = [];
            for (var tld in vm.tlds) {
                if (vm.tlds[tld] === true) {
                    result.push(tld);
                }
            }

            return result;
        }

        function closeAllAdditionalInfoBlocs() {
            for (var key in vm.domains) {

                if ('domElement' in vm.domains[key]) {
                    parent.closeAdditionalBlock(vm.domains[key].domElement);
                }
            }
        }
    }
})();
