(function() {
    'use strict';

    angular.module('adminApp.domain')
        .service('domainRequestService', domainRequestService);

    domainRequestService.$inject = ['$http'];

    function domainRequestService($http) {
        var BASE_URL = '/admin/api/domain/';

        return {
            getSuggestionDomainAction: getSuggestionDomainAction,
            getDomainPriceAction: getDomainPriceAction,
            buyDomainAction: buyDomainAction,
            getExpireDateAction: getExpireDateAction
        };

        /**
         * Get Suggestion Domain Action
         * @param {string} domain
         * @param {string} tld
         * @returns {*}
         */
        function getSuggestionDomainAction(domain, tld) {
            return $http({
                method: 'POST',
                url: BASE_URL + 'suggestion',
                data: { domain: domain, tld: tld }
            });
        }

        /**
         * Get Domain Price Action
         * @param {string} domain
         * @returns {*}
         */
        function getDomainPriceAction(domain) {
            return $http({
                method: 'POST',
                url: BASE_URL + 'price',
                data: { domain: domain }
            });
        }

        /**
         * Buy Domain Action
         * @param {string} domain
         * @param {Object} ownerInfo
         * @returns {*}
         */
        function buyDomainAction(domain, ownerInfo) {
            return $http({
                method: 'POST',
                url: BASE_URL + 'buy',
                data: {
                    domain: domain,
                    ownerInfo: ownerInfo
                }
            });
        }

        /**
         * Get Expire Date Action
         * @param domain
         * @returns {*}
         */
        function getExpireDateAction(domain) {
            return $http({
                method: 'POST',
                url: BASE_URL + 'expire-date',
                data: { domain: domain }
            });
        }
    }
})();
