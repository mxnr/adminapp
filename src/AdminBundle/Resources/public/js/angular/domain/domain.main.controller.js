(function () {
    'use strict';

    angular.module('adminApp.domain')
        .controller('DomainMainController', DomainMainController);

    DomainMainController.$inject = ['urlHelperService', '$window'];

    function DomainMainController(urlHelperService, $window) {
        var vm = this;

        // Tabs visibility
        vm.domains = true;
        vm.addDomains = false;
        vm.buyDomains = false;

        vm.displayError = false;
        vm.errorText = '';

        vm.init = function (formStatus) {
            var tab = urlHelperService.getQueryVariable('tab');
            if (tab) {
                vm.openTab(tab);
            }

            if (formStatus) {
                vm.openTab('addDomains');
            }
        };

        vm.openTab = function (tab) {
            if (tab in vm) {
                closeAllTabs();
                vm[tab] = true;
            }
        };

        /**
         * Toggle Additional Block
         * @param {Object} $event
         */
        vm.toggleAdditionalBlock = function($event) {
            var domElement = vm.getDomElement($event)

            if (domElement.hasClass('active')) {
                vm.closeAdditionalBlock(domElement);
            } else {
                vm.openAdditionalBlock(domElement);
            }
        };

        /**
         * Close Additional Block
         * @param {Object} domElement
         */
        vm.closeAdditionalBlock = function (domElement) {
            domElement.removeClass('active');
        };

        /**
         * Open Additional Block
         * @param {Object} domElement
         */
        vm.openAdditionalBlock = function (domElement) {
            domElement.addClass('active');
        };

        /**
         * Get Root Element
         * @param {Object} $event
         */
        vm.getDomElement = function($event) {
            var el = angular.element($event.currentTarget);

            return el.parent().parent().parent();
        };

        /**
         * Display Error Alert
         * @param message
         */
        vm.displayErrorAlert = function(message)
        {
            if (typeof message === 'string') {
                vm.errorText = message;
                vm.displayError = true;

                $window.scrollTo(0, 0);
            }
        };

        vm.hideErrorAlert = function() {
            vm.displayError = false;
        };

        function closeAllTabs() {
            vm.domains = false;
            vm.addDomains = false;
            vm.buyDomains = false;
        }
    }
})();
