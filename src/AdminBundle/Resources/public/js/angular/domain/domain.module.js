(function () {
    'use strict';

    angular.module('adminApp.domain', ['adminApp.core']);

    angular.module('adminApp.domain')
        .value('tlds', {
            '.com': true,
            '.net': true,
            '.info': true,
            '.org': true,
            '.co': true,
            '.us': true,
            '.xyz': true,
            '.ly': true,
            '.co.in': true,
        });
})();
