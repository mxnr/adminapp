(function() {
    'use strict';

    angular.module('adminApp.core')
        .service('localStorageService', localStorageService);

    /**
     * @type {[string]}
     */
    localStorageService.$inject = ['$window'];

    /**
     * @param $window
     * @returns {{get: get, set: set, remove: remove}}
     */
    function localStorageService($window) {

        return {
            get: get,
            set: set,
            remove: remove,
        };

        /**
         * Get item from local storage
         *
         * @param {string} key
         * @returns {Object}
         */
        function get(key) {
            var item = $window.localStorage.getItem(key);

            return JSON.parse(item);
        }

        /**
         * Set item to local storage
         *
         * @param {string} key
         * @param {Object|Array} data
         */
        function set(key, data) {
            var item = JSON.stringify(data);

            $window.localStorage.setItem(key, item);
        }

        /**
         * Remove item from local storage
         *
         * @param key
         */
        function remove(key) {
            $window.localStorage.removeItem(key);
        }
    }
})();
