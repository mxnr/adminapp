(function () {
    'use strict';

    angular.module('adminApp.core')
        .service('urlHelperService', urlHelperService);

    /**
     * @type {[string]}
     */
    urlHelperService.$inject = ['$window'];


    function urlHelperService($window) {

        return {
            getQueryVariable: getQueryVariable,
        };

        /**
         * Get Query Variable
         * @param variable
         * @returns {boolean|string}
         */
        function getQueryVariable(variable) {
            var query = $window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == variable) {
                    return pair[1];
                }
            }
            return (false);
        }
    }
})();
