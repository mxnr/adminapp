(function() {
    'use strict';

    angular.module('adminApp.inventoryItem')
        .factory('inventoryItemDataService', inventoryItemDataService);

    inventoryItemDataService.$inject = ['$http'];

    function inventoryItemDataService($http) {
        return {
            fetchUsersModelsTitles: fetchUsersModelsTitles,
            fetchUsersInventoryItemsTitles: fetchUsersInventoryItemsTitles,
            fetchUsersBarcodes: fetchUsersBarcodes
        };

        /**
         * Fetch titles data
         *
         * @returns {promise} $http
         */
        function fetchUsersModelsTitles() {
            return $http({
                method: 'GET',
                url: '/admin/api/models-titles'
            });
        }

        /**
         * Fetch titles data
         * @param {string} type
         * @returns {promise} $http
         */
        function fetchUsersInventoryItemsTitles(type) {
            return $http({
                method: 'GET',
                url: '/admin/api/inventory-items-titles/' + type
            });
        }

        /**
         * Fetch titles data
         * @param {string} type
         * @returns {promise} $http
         */
        function fetchUsersBarcodes(type) {
            return $http({
                method: 'GET',
                url: '/admin/api/inventory-items-barcodes/' + type
            });
        }
    }
})();