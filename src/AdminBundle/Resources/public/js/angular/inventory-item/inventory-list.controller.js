(function() {
    'use strict';

    angular.module('adminApp.inventoryItem')
        .controller('InventoryItemListController', InventoryItemListController);

    /**
     * @type {[string]}
     */
    InventoryItemListController.$inject = ['inventoryItemDataService'];

    function InventoryItemListController(inventoryItemDataService) {
        var vm = this;
        vm.titles = [];
        vm.title = '';
        vm.barcodes = [];
        vm.barcode = '';

        vm.init = function (title, barcode) {
            vm.title = title;
            inventoryItemDataService.fetchUsersInventoryItemsTitles('inventory').then(function (response) {
                vm.titles = response.data;
            });

            vm.barcode = barcode;
            inventoryItemDataService.fetchUsersBarcodes('inventory').then(function (response) {
                vm.barcodes = response.data;
            })
        };
    }
})();