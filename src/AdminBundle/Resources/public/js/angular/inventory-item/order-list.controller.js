(function() {
    'use strict';

    angular.module('adminApp.inventoryItem')
        .controller('OrderListController', OrderListController);

    /**
     * @type {[string]}
     */
    OrderListController.$inject = ['inventoryItemDataService'];

    function OrderListController(inventoryItemDataService) {
        var vm = this;
        vm.titles = [];
        vm.title = '';
        vm.barcodes = [];
        vm.barcode = '';

        vm.init = function (title, barcode) {
            vm.title = title;
            inventoryItemDataService.fetchUsersInventoryItemsTitles('order').then(function (response) {
                vm.titles = response.data;
            });

            vm.barcode = barcode;
            inventoryItemDataService.fetchUsersBarcodes('order').then(function (response) {
                vm.barcodes = response.data;
            })
        };
    }
})();