(function() {
    'use strict';

    angular.module('adminApp.inventoryItem')
        .controller('InventoryItemController', InventoryItemController);

    /**
     * @type {[string]}
     */
    InventoryItemController.$inject = ['inventoryItemDataService'];

    function InventoryItemController(inventoryItemDataService) {
        var vm = this;
        vm.titles = [];
        vm.title = '';

        vm.init = function (title) {
            vm.title = title;
            inventoryItemDataService.fetchUsersModelsTitles().then(function (response) {
                vm.titles = response.data;
            })
        };
    }
})();