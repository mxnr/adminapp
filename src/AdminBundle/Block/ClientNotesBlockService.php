<?php

namespace AdminBundle\Block;

use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Sonata\BlockBundle\Block\BaseBlockService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ThreeWebOneEntityBundle\Entity\ClientNotes\ClientNotes;
use AdminBundle\Form\Type\ClientNotesType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class ClientNotesBlockService extends BaseBlockService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var Router
     */
    private $router;

    /**
     * ClientNoticeBlockService constructor.
     * @param EngineInterface $templating
     * @param EntityManager $entityManager
     * @param TokenStorage $tokenStorage
     * @param FormFactory $formFactory
     * @param Router $router
     */
    public function __construct(
        EngineInterface $templating,
        EntityManager $entityManager,
        TokenStorage $tokenStorage,
        FormFactory $formFactory,
        Router $router
    ) {
        parent::__construct('Client Notes', $templating);
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'title' => 'Stats service',
            'template' => 'AdminBundle:Block:sonata_block_client_notice.html.twig',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $form = $this->createForm(
            ClientNotesType::class,
            new ClientNotes(),
            [
                'action' => $this->router->generate('admin_create_client_note'),
                'method' => 'POST'
            ]
        );

        return $this->renderResponse($blockContext->getTemplate(), [
            'block' => $blockContext->getBlock(),
            'settings' => $blockContext->getSettings(),
            'form' => $form->createView(),
            'notes' => $this->getAllClientNotes()
        ], $response);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return "Stats service";
    }

    /**
     * @param string $type
     * @param null $data
     * @param array $options
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createForm(string $type, $data = null, array $options = [])
    {
        return $this->formFactory->create($type, $data, $options);
    }

    /**
     * @return ClientNotes[]
     */
    private function getAllClientNotes()
    {
        $clientNotesRepo = $this->entityManager->getRepository(ClientNotes::class);
        $user = $this->tokenStorage->getToken()->getUser();

        return $clientNotesRepo->getAllClientNotes($user);
    }
}
