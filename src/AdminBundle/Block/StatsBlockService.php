<?php

namespace AdminBundle\Block;

use AdminBundle\Service\StatsService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Sonata\BlockBundle\Block\BaseBlockService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class StatsBlockService extends BaseBlockService
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var StatsService
     */
    private $statsService;

    /**
     * StatsBlockService constructor.
     * @param EngineInterface $templating
     * @param EntityManager $entityManager
     * @param StatsService $statsService
     */
    public function __construct(
        EngineInterface $templating,
        EntityManager $entityManager,
        StatsService $statsService,
        TokenStorage $tokenStorage
    ) {
        parent::__construct('test Stats', $templating);
        $this->entityManager = $entityManager;
        $this->statsService = $statsService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'title' => 'Stats service',
            'template' => 'AdminBundle:Block:sonata_block_stats.html.twig',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        return $this->renderResponse($blockContext->getTemplate(), [
            'block' => $blockContext->getBlock(),
            'settings' => $blockContext->getSettings(),
            'statsData' => $this->statsService->getDashboardStatsInfo($this->tokenStorage->getToken()->getUser())
        ], $response);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return "Statistics service";
    }
}
