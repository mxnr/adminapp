<?php

namespace AdminBundle\Naming;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;
use Vich\UploaderBundle\Naming\Polyfill\FileExtensionTrait;

/**
 * Class OriginalNameUniqidNamer
 */
class OriginalNameUniqidNamer implements NamerInterface
{
    use FileExtensionTrait;

    /**
     * {@inheritdoc}
     */
    public function name($object, PropertyMapping $mapping)
    {
        $file = $mapping->getFile($object);
        $name = str_replace('.', '', uniqid($file->getClientOriginalName(), true));

        if ($extension = $this->getExtension($file)) {
            $name = sprintf('%s.%s', $name, $extension);
        }

        return $name;
    }
}
