var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('web/admin/build')
    .setPublicPath('/admin/build')
    .cleanupOutputBeforeBuild()
    .addEntry('app', './src/AdminBundle/Resources/public/Plugin/jscolor.min.js')
    .addEntry('typeahead', './src/AdminBundle/Resources/public/Plugin/typeahead.min.js')
    .addEntry('globalJs', [
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js'
    ])
    .addEntry('registerApp', [
        './node_modules/angular/angular.min.js',
        './src/AdminBundle/Resources/public/js/angular/register/register.module.js',
        './src/AdminBundle/Resources/public/js/angular/register/register.controller.js'
    ])
    .addEntry('settingsApp', [
        './node_modules/angular/angular.min.js',
        './src/AdminBundle/Resources/public/js/angular/settings/settings.module.js',
        './src/AdminBundle/Resources/public/js/angular/settings/settings.controller.js'
    ])
    .addEntry('modelEditApp', [
        './node_modules/angular/angular.min.js',
        './node_modules/angular-ui-select/select.js',
        './node_modules/angular-sanitize/angular-sanitize.js',
        './src/AdminBundle/Resources/public/js/angular/model/model-edit.module.js',
        './src/AdminBundle/Resources/public/js/angular/model/file-upload.directive.js',
        './src/AdminBundle/Resources/public/js/angular/model/model-edit.factory.js',
        './src/AdminBundle/Resources/public/js/angular/model/model-edit.controller.js'
    ])
    .addEntry('incomeListApp', [
        './node_modules/angular/angular.min.js',
        './node_modules/angular-ui-select/select.js',
        './node_modules/angular-sanitize/angular-sanitize.js',
        './src/AdminBundle/Resources/public/js/angular/order/order-edit.module.js',
        './src/AdminBundle/Resources/public/js/angular/order/order-edit.factory.js',
        './src/AdminBundle/Resources/public/js/angular/common/directives/file-model.directive.js',
        './src/AdminBundle/Resources/public/js/angular/add-inventory/add-inventory.controller.js',
        './src/AdminBundle/Resources/public/js/angular/model-widget/model-widget.factory.js',
        './src/AdminBundle/Resources/public/js/angular/model-widget/model-widget.controller.js'
    ])
    .addEntry('inventoryItemListApp', [
        './node_modules/angular/angular.min.js',
        './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
        './src/AdminBundle/Resources/public/js/angular/inventory-item/inventory-item.module.js',
        './src/AdminBundle/Resources/public/js/angular/inventory-item/inventory-item.factory.js',
        './src/AdminBundle/Resources/public/js/angular/inventory-item/inventory-list.controller.js'
    ])
    .addEntry('OrderListApp', [
        './node_modules/angular/angular.min.js',
        './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
        './src/AdminBundle/Resources/public/js/angular/inventory-item/inventory-item.module.js',
        './src/AdminBundle/Resources/public/js/angular/inventory-item/inventory-item.factory.js',
        './src/AdminBundle/Resources/public/js/angular/inventory-item/order-list.controller.js'
    ])
    .addEntry('customerEditApp', [
        './node_modules/angular/angular.min.js',
        './src/AdminBundle/Resources/public/js/angular/customer/customer-edit.module.js',
        './src/AdminBundle/Resources/public/js/angular/customer/customer-edit.factory.js',
        './src/AdminBundle/Resources/public/js/angular/customer/customer-edit.controller.js'
    ])
    .addEntry('inventoryItemApp', [
        './node_modules/angular/angular.min.js',
        './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
        './src/AdminBundle/Resources/public/js/angular/inventory-item/inventory-item.module.js',
        './src/AdminBundle/Resources/public/js/angular/inventory-item/inventory-item.factory.js',
        './src/AdminBundle/Resources/public/js/angular/inventory-item/inventory-item.controller.js'
    ])
    .addEntry('orderEditApp', [
        './node_modules/angular/angular.min.js',
        './node_modules/angular-ui-select/select.js',
        './node_modules/angular-sanitize/angular-sanitize.js',
        './src/AdminBundle/Resources/public/js/angular/order/order-edit.module.js',
        './src/AdminBundle/Resources/public/js/angular/order/order-edit.factory.js',
        './src/AdminBundle/Resources/public/js/angular/order/order-edit.controller.js',
        './src/AdminBundle/Resources/public/js/angular/model-widget/model-widget.factory.js',
        './src/AdminBundle/Resources/public/js/angular/model-widget/model-widget.controller.js'
    ])
    .addEntry('profitCalculatorApp', [
        './node_modules/angular/angular.min.js',
        './node_modules/zingchart/client/zingchart.min.js',
        './node_modules/zingchart-angularjs/src/zingchart-angularjs.js',
        './src/AdminBundle/Resources/public/js/angular/core/core.module.js',
        './src/AdminBundle/Resources/public/js/angular/core/services/local-storage/local-storage.service.js',
        './src/AdminBundle/Resources/public/js/angular/profit-calculator/profit-calculator.module.js',
        './src/AdminBundle/Resources/public/js/angular/profit-calculator/calculation.service.js',
        './src/AdminBundle/Resources/public/js/angular/profit-calculator/profit-calculator.controller.js',
    ])
    .addEntry('signature_capture', [
        './src/AdminBundle/Resources/public/js/signature_capture.js'
    ])
    .addEntry('EbayTemplatesApp', [
        './node_modules/angular/angular.min.js',
        './node_modules/angular-clipboard/angular-clipboard.js',
        './src/AdminBundle/Resources/public/js/angular/ebay-templates/ebay-templates.module.js',
        './src/AdminBundle/Resources/public/js/angular/ebay-templates/ebay-templates.controller.js'
    ])
    .addEntry('businessPerformanceApp', [
        './node_modules/angular/angular.min.js',
        './node_modules/zingchart/client/zingchart.min.js',
        './node_modules/zingchart-angularjs/src/zingchart-angularjs.js',
        './node_modules/angularjs-datepicker/dist/angular-datepicker.min.js',
        './src/AdminBundle/Resources/public/js/angular/business-performance/business-performance.module.js',
        './src/AdminBundle/Resources/public/js/angular/business-performance/business-performance.service.js',
        './src/AdminBundle/Resources/public/js/angular/business-performance/business-performance.controller.js'
    ])
    .addEntry('domainApp', [
        './node_modules/angular/angular.min.js',
        './src/AdminBundle/Resources/public/js/angular/core/core.module.js',
        './src/AdminBundle/Resources/public/js/angular/domain/domain.module.js',
        './src/AdminBundle/Resources/public/js/angular/core/services/local-storage/url-helper.service.js',
        './src/AdminBundle/Resources/public/js/angular/domain/domain.main.controller.js',
        './src/AdminBundle/Resources/public/js/angular/domain/domain.list.controller.js',
        './src/AdminBundle/Resources/public/js/angular/domain/domain.buy.controller.js',
        './src/AdminBundle/Resources/public/js/angular/domain/domain.request.service.js',
    ])
    .addStyleEntry('datepicker', './node_modules/angularjs-datepicker/dist/angular-datepicker.css')
    .addStyleEntry('style', './src/AdminBundle/Resources/public/scss/style.scss')
    .addStyleEntry('register', './src/AdminBundle/Resources/public/scss/register/register.scss')
    .addStyleEntry('barcode', './src/AdminBundle/Resources/public/css/barcode.css')
    .addStyleEntry('invoice', './src/AdminBundle/Resources/public/scss/order/invoice.scss')
    .addStyleEntry('signature', './src/AdminBundle/Resources/public/scss/signature/signature.scss')
    .addStyleEntry('income-list', './src/AdminBundle/Resources/public/scss/income-list.scss')
    .addStyleEntry('order', './src/AdminBundle/Resources/public/scss/order.scss')
    .addStyleEntry('config', './src/AdminBundle/Resources/public/scss/config.scss')
    .addStyleEntry('select-ui', './node_modules/angular-ui-select/select.css')
    .addStyleEntry('global', [
        './node_modules/bootstrap/dist/css/bootstrap.min.css'
    ])
    .addStyleEntry('fontAwesome', './src/AdminBundle/Resources/public/css-libs/font-awesome.scss')
    .addStyleEntry('domain', './src/AdminBundle/Resources/public/scss/domain/domain.scss')
    .addStyleEntry('domain-contacts', './src/AdminBundle/Resources/public/scss/domain/domain-contacts.scss')

    // CKEditor
    .addEntry('ckeditor-formatter', [
        './web/bundles/sonataformatter/vendor/markitup-markitup/markitup/jquery.markitup.js',
        './web/bundles/sonataformatter/markitup/sets/markdown/set.js',
        './web/bundles/sonataformatter/markitup/sets/html/set.js',
        './web/bundles/sonataformatter/markitup/sets/textile/set.js'
    ])
    .addStyleEntry('ckeditor', [
        './web/bundles/sonataformatter/markitup/skins/sonata/style.css',
        './web/bundles/sonataformatter/markitup/sets/markdown/style.css',
        './web/bundles/sonataformatter/markitup/sets/html/style.css',
        './web/bundles/sonataformatter/markitup/sets/textile/style.css'
    ])
    .enablePostCssLoader(function(options) {
         options.config = {
             path: 'config/postcss.config.js'
         };
     })
    .enableSassLoader()
    .autoProvidejQuery()
    .enableSourceMaps(!Encore.isProduction());

// export the final configuration
module.exports = Encore.getWebpackConfig();
