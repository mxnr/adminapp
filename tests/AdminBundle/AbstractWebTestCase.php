<?php

namespace Tests\AdminBundle;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class AbstractWebTestCase
 */
abstract class AbstractWebTestCase extends WebTestCase
{
    /**
     * Gets a service.
     *
     * @param string $id              The service identifier
     * @param int    $invalidBehavior The behavior when the service does not exist
     *
     * @return object The associated service
     *
     * @throws ServiceCircularReferenceException When a circular reference is detected
     * @throws ServiceNotFoundException          When the service is not defined
     *
     * @see Reference
     */
    public function get($id, $invalidBehavior = ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE)
    {
        $container = static::$kernel->getContainer();

        return $container->get($id, $invalidBehavior);
    }

    /**
     * Simulates that customer logined into the system
     *
     * @return User
     */
    protected function loginCustomer(): User
    {
        $user = new User();
        $user->setRoles([UserInterface::ROLE_DEFAULT]);
        $this->loginUser('user', $user);

        return $user;
    }

    /**
     * Imitates login of the user
     *
     * @param string        $firewallName
     * @param UserInterface $user
     * @param array         $options
     * @param array         $server
     *
     * @return void
     */
    protected function loginUser(
        $firewallName,
        UserInterface $user,
        array $options = array(),
        array $server = array()
    ) {
        $client = static::createClient();

        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        static::$kernel->getContainer()->get('security.token_storage')->setToken($token);

        $session = $client->getContainer()->get('session');
        $session->set('_security_' . $firewallName, serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);
    }

    /**
     * Simulates that admin logined into the system
     *
     * @return User
     */
    protected function loginAdmin(): User
    {
        $user = new User();
        $user->setRoles([UserInterface::ROLE_SUPER_ADMIN]);

        $this->loginUser('admin', $user);

        return $user;
    }

    protected function setUp()
    {
        self::bootKernel();
    }
}
