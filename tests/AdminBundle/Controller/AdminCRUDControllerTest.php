<?php

namespace Tests\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class AdminCRUDControllerTest extends WebTestCase
{
    /**
     * @var null
     */
    private $client = null;

    /**
     * Set Up
     */
    public function setUp()
    {
        $this->client = $this->createClient();
    }

    /**
     * Login stub
     */
    private function logIn()
    {
        $session = $this->client->getContainer()->get('session');

        // the firewall context defaults to the firewall name
        $firewallContext = 'user';

        $em = $this->client->getContainer()->get('doctrine')->getEntityManager();
        $user = $em->getRepository('ThreeWebOneEntityBundle:User')->findOneByUsername('admin');

        $token = new UsernamePasswordToken($user, 'admin', $firewallContext, array('ROLE_ADMIN'));
        $session->set('_security_' . $firewallContext, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }

    /**
     * Admin routes
     *
     * @return array
     */
    public function routeProvider()
    {
        return [
            ['admin-model'],
            ['admin-price'],
            ['admin-price-type'],
            ['admin-product'],
            ['admin-image'],
            ['admin-filter'],
            ['admin-category-brand'],
            ['admin-category-category'],
            ['admin-inventory-item'],
            ['admin-inventory'],
            ['admin-group'],
            ['admin-user'],
            ['admin-category-image'],
            ['admin-filter-image'],
            ['admin-model-image'],
            ['admin-product-image'],
            ['user-config/config'],
            ['user-config/theme'],
            ['admin-order-item'],
            ['admin-order'],
            ['admin-income-list'],
            ['admin-income-customer'],
        ];
    }

    /**
     * Check Authenticated Admin Page Response
     *
     * @param $page
     *
     * @dataProvider routeProvider
     */
    public function testAuthenticatedAdminPagesResponseAction($page)
    {
        $this->logIn();
        $actions = ['list', 'create'];

        foreach ($actions as $action) {
            $crawler = $this->client->request('GET', sprintf('/admin/%s/%s', $page, $action));
            $this->assertEquals(
                Response::HTTP_OK,
                $this->client->getResponse()->getStatusCode()
            );
            $this->assertContains('Welcome to 3web1', $crawler->html());
        }
    }

    /**
     * Check Not Authenticated Admin Page Response
     *
     * @param $page
     *
     * @dataProvider routeProvider
     */
    public function testNotAuthenticatedAdminPagesResponseAction($page)
    {
        $actions = ['list', 'create'];

        foreach ($actions as $action) {
            $this->client->request('GET', sprintf('/admin/%s/%s', $page, $action));
            $this->assertEquals(
                Response::HTTP_FOUND,
                $this->client->getResponse()->getStatusCode()
            );
        }
    }
}
