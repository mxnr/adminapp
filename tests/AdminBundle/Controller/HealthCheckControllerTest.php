<?php

namespace AdminBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HealthCheckControllerTest extends WebTestCase
{
    /**
     * Check the health check url
     */
    public function testHealthCheckAction()
    {
        $client = $this->createClient();
        $client->request('GET', '/admin/_healthcheck');
        $this->assertEquals('Health: ok', $client->getResponse()->getContent());
    }
}
