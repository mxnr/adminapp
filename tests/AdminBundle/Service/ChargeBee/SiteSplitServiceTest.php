<?php

namespace Tests\AdminBundle\Service\ChargeBee;

use AdminBundle\Service\ChargeBee\AccessMatrixInterface;
use AdminBundle\Service\ChargeBee\SiteSplitService;
use AdminBundle\Service\ChargeBee\SubscriptionService;
use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Yaml\Yaml;
use Tests\AdminBundle\AbstractWebTestCase;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Entity\Billing\SubscriptionInterface;

/**
 * Class SiteSplitServiceTest.
 */
class SiteSplitServiceTest extends AbstractWebTestCase
{
    public function testWrongServiceInstantiation()
    {
        $this->expectException(\ArgumentCountError::class);

        new SiteSplitService();
    }

    public function testNormalServiceInstantiation()
    {
        $this->loginAdmin();

        $subscriptionService = $this->get('admin.charge_bee.subscription');
        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];

        $siteSplitService = new SiteSplitService(
            $subscriptionService,
            $kernel,
            $rules
        );

        foreach (SiteSplitService::RESTRICTIONS as $RESTRICTION) {
            $this->assertTrue($siteSplitService->isAccessible($RESTRICTION));
        }
    }

    public function testConstructorAcceptsOnlyValidSubscriptionService()
    {
        $this->expectExceptionMessage('SubscriptionService');
        $subscriptionService = $this->get('admin.charge_bee.subscription');
        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];

        new SiteSplitService(
            new DateTime(),
            $kernel,
            $rules
        );
    }

    public function testConstructorAcceptsOnlyValidKernel()
    {
        $this->expectExceptionMessage('KernelInterface');
        $subscriptionService = $this->get('admin.charge_bee.subscription');
        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];
        new SiteSplitService(
            $subscriptionService,
            new DateTime(),
            $rules
        );
    }

    public function testConstructorAcceptsOnlyValidPath()
    {
        $this->expectExceptionMessage('array');
        $subscriptionService = $this->get('admin.charge_bee.subscription');
        $kernel = $this->get('kernel');

        new SiteSplitService(
            $subscriptionService,
            $kernel,
            new DateTime()
        );
    }

    public function testCaseWhenNothingSubscribed()
    {
        $subscriptionService = $this->getSubscriptionService(
            [
                SubscriptionInterface::ITEM_SELL => false,
                SubscriptionInterface::ITEM_REPAIR => false,
                SubscriptionInterface::ITEM_BUY => false,
            ]
        );

        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];
        $siteSplitService = new SiteSplitService(
            $subscriptionService,
            $kernel,
            $rules
        );

        foreach (SiteSplitService::RESTRICTIONS as $RESTRICTION) {
            $this->assertFalse($siteSplitService->isAccessible($RESTRICTION));
        }
    }

    protected function getSubscriptionService(array $items): SubscriptionService
    {
        $user = $this->loginCustomer();

        $customer = new Customer();

        $subscription = new Subscription();

        $subscription
            ->setStatus(SubscriptionInterface::STATUS_TRIAL)
            ->setCustomer($customer)
            ->setItems($items);
        $subscriptions = new ArrayCollection();
        $subscriptions->add($subscription);

        $trialEndDate = new DateTime();
        $trialEndDate->add(new DateInterval('PT1H'));
        $customer
            ->setTrialEndedAt($trialEndDate)
            ->setSubscriptions($subscriptions);

        $user->setBillingCustomer($customer);

        $tokenStorage = $this->get('security.token_storage');
        $subscriptionsService = $this->get('charge_bee.subscriptions');
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $eventDispatcher = $this->get('event_dispatcher');
        $validator = $this->get('validator');

        return new SubscriptionService($tokenStorage, $subscriptionsService, $entityManager, $validator, $eventDispatcher);
    }

    public function testCaseWhenOnlyRepairSubscribed()
    {
        $subscriptionService = $this->getSubscriptionService(
            [
                SubscriptionInterface::ITEM_SELL => false,
                SubscriptionInterface::ITEM_REPAIR => true,
                SubscriptionInterface::ITEM_BUY => false,
            ]
        );

        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];
        $siteSplitService = new SiteSplitService(
            $subscriptionService,
            $kernel,
            $rules
        );

        //landing
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //split
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_SALE));

        //theme
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::THEME_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::THEME_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::THEME_SALE));

        //landing title
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //users
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::USERS));

        //calculator
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::CALCULATOR));

        //affiliate page
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::AFFILIATE_PAGE));

        //quality checks
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::QUALITY_CHECKS_QUICK_LINKS));

        //dashboard
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::DASHBOARD));

        //statistics buyback/inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_BUY_INVENTORY));

        //statistics setting
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::STATISTICS_SALE));

        //statistics repair
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_REPAIR));
    }

    public function testCaseWhenOnlyBuySubscribed()
    {
        $subscriptionService = $this->getSubscriptionService(
            [
                SubscriptionInterface::ITEM_SELL => false,
                SubscriptionInterface::ITEM_REPAIR => false,
                SubscriptionInterface::ITEM_BUY => true,
            ]
        );

        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];
        $siteSplitService = new SiteSplitService(
            $subscriptionService,
            $kernel,
            $rules
        );

        //landing
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //split
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_SALE));

        //theme
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::THEME_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::THEME_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::THEME_SALE));

        //landing title
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //users
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::USERS));

        //calculator
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::CALCULATOR));

        //affiliate page
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::AFFILIATE_PAGE));

        //quality checks
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::QUALITY_CHECKS_QUICK_LINKS));

        //dashboard
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::DASHBOARD));

        //statistics buyback/inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_BUY_INVENTORY));

        //statistics setting
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::STATISTICS_SALE));

        //statistics repair
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::STATISTICS_REPAIR));
    }

    public function testCaseWhenOnlySellSubscribed()
    {
        $subscriptionService = $this->getSubscriptionService(
            [
                SubscriptionInterface::ITEM_SELL => true,
                SubscriptionInterface::ITEM_REPAIR => false,
                SubscriptionInterface::ITEM_BUY => false,
            ]
        );

        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];
        $siteSplitService = new SiteSplitService(
            $subscriptionService,
            $kernel,
            $rules
        );

        //landing
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //split
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_SALE));

        //theme
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::THEME_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::THEME_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::THEME_SALE));

        //landing title
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //users
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::USERS));

        //calculator
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::CALCULATOR));

        //affiliate page
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::AFFILIATE_PAGE));

        //quality checks
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::QUALITY_CHECKS_QUICK_LINKS));

        //dashboard
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::DASHBOARD));

        //statistics buyback/inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_BUY_INVENTORY));

        //statistics setting
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_SALE));

        //statistics repair
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::STATISTICS_REPAIR));
    }


    public function testCaseWhenSellRepairSubscribed()
    {
        $subscriptionService = $this->getSubscriptionService(
            [
                SubscriptionInterface::ITEM_SELL => true,
                SubscriptionInterface::ITEM_REPAIR => true,
                SubscriptionInterface::ITEM_BUY => false,
            ]
        );

        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];
        $siteSplitService = new SiteSplitService(
            $subscriptionService,
            $kernel,
            $rules
        );

        //landing
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //split
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_SALE));

        //theme
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::THEME_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::THEME_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::THEME_SALE));

        //landing title
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //users
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::USERS));

        //calculator
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::CALCULATOR));

        //affiliate page
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::AFFILIATE_PAGE));

        //quality checks
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::QUALITY_CHECKS_QUICK_LINKS));

        //dashboard
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::DASHBOARD));

        //statistics buyback/inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_BUY_INVENTORY));

        //statistics setting
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_SALE));

        //statistics repair
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_REPAIR));
    }

    public function testCaseWhenEverythingSubscribed()
    {
        $subscriptionService = $this->getSubscriptionService(
            [
                SubscriptionInterface::ITEM_SELL => true,
                SubscriptionInterface::ITEM_REPAIR => true,
                SubscriptionInterface::ITEM_BUY => true,
            ]
        );

        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];
        $siteSplitService = new SiteSplitService(
            $subscriptionService,
            $kernel,
            $rules
        );

        foreach (SiteSplitService::RESTRICTIONS as $RESTRICTION) {
            $this->assertTrue($siteSplitService->isAccessible($RESTRICTION));
        }
    }

    public function testCaseWhenBuyRepairSubscribed()
    {
        $subscriptionService = $this->getSubscriptionService(
            [
                SubscriptionInterface::ITEM_SELL => false,
                SubscriptionInterface::ITEM_REPAIR => true,
                SubscriptionInterface::ITEM_BUY => true,
            ]
        );

        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];
        $siteSplitService = new SiteSplitService(
            $subscriptionService,
            $kernel,
            $rules
        );

        //landing
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //split
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_SALE));

        //theme
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::THEME_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::THEME_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::THEME_SALE));

        //landing title
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //users
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::USERS));

        //calculator
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::CALCULATOR));

        //affiliate page
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::AFFILIATE_PAGE));

        //quality checks
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::QUALITY_CHECKS_QUICK_LINKS));

        //dashboard
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::DASHBOARD));

        //statistics buyback/inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_BUY_INVENTORY));

        //statistics setting
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::STATISTICS_SALE));

        //statistics repair
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_REPAIR));
    }

    public function testCaseWhenSellBuySubscribed()
    {
        $subscriptionService = $this->getSubscriptionService(
            [
                SubscriptionInterface::ITEM_SELL => true,
                SubscriptionInterface::ITEM_REPAIR => false,
                SubscriptionInterface::ITEM_BUY => true,
            ]
        );

        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@AdminBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];
        $siteSplitService = new SiteSplitService(
            $subscriptionService,
            $kernel,
            $rules
        );

        //landing
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //split
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::PRICE_TYPE_SALE));

        //theme
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::THEME_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::THEME_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::THEME_SALE));

        //landing title
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //users
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::USERS));

        //calculator
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::CALCULATOR));

        //affiliate page
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::AFFILIATE_PAGE));

        //quality checks
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::QUALITY_CHECKS_QUICK_LINKS));

        //dashboard
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::DASHBOARD));

        //statistics buyback/inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_BUY_INVENTORY));

        //statistics setting
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::STATISTICS_SALE));

        //statistics repair
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::STATISTICS_REPAIR));
    }
}
