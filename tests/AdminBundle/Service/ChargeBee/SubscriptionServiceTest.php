<?php

namespace Tests\AdminBundle\Service\ChargeBee;

use AdminBundle\Service\ChargeBee\SubscriptionService;
use ArgumentCountError;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Tests\AdminBundle\AbstractWebTestCase;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Entity\Billing\SubscriptionInterface;

/**
 * Class SubscriptionServiceTest.
 */
class SubscriptionServiceTest extends AbstractWebTestCase
{
    /**
     * checks that service do not instantiates without arguments
     */
    public function testWrongServiceInstantiation()
    {
        $this->expectException(ArgumentCountError::class);
        new SubscriptionService();
    }

    /**
     * checks that instantiation of the service works properly
     */
    public function testNormalServiceInstantiation()
    {
        $this->loginAdmin();

        $subscriptionsService = $this->get('charge_bee.subscriptions');
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $validator = $this->get('validator');

        $adminSubscriptionService = $this->getService();

        $this->assertTrue($adminSubscriptionService->hasToken());
        $this->assertInstanceOf(TokenInterface::class, $adminSubscriptionService->getToken());

        $this->assertTrue($adminSubscriptionService->hasSubscriptionsService());
        $this->assertEquals($subscriptionsService, $adminSubscriptionService->getSubscriptionsService());

        $this->assertTrue($adminSubscriptionService->hasEntityManager());
        $this->assertEquals($entityManager, $adminSubscriptionService->getEntityManager());

        $this->assertTrue($adminSubscriptionService->hasValidator());
        $this->assertEquals($validator, $adminSubscriptionService->getValidator());
    }

    /**
     * @return SubscriptionService
     */
    protected function getService(): SubscriptionService
    {
        $tokenStorage = $this->get('security.token_storage');
        $subscriptionsService = $this->get('charge_bee.subscriptions');
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $validator = $this->get('validator');
        $evDispatcher = $this->get('event_dispatcher');

        return new SubscriptionService($tokenStorage, $subscriptionsService, $entityManager, $validator, $evDispatcher);
    }

    /**
     * checks that all the setters works properly
     *
     * setted value must be equal to the getted one
     */
    public function testSettersWithOkData()
    {
        $this->loginAdmin();
        $tokenStorage = $this->get('security.token_storage');
        $subscriptionsService = $this->get('charge_bee.subscriptions');
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $validator = $this->get('validator');
        $evDispatcher = $this->get('event_dispatcher');

        $adminSubscriptionService = new SubscriptionService(
            $tokenStorage,
            $subscriptionsService,
            $entityManager,
            $validator,
            $evDispatcher
        );

        $adminSubscriptionService
            ->setToken($tokenStorage->getToken())
            ->setSubscriptionsService($subscriptionsService)
            ->setEntityManager($entityManager)
            ->setValidator($validator)
            ->setEventDispatcher($evDispatcher);

        $this->assertTrue($adminSubscriptionService->hasValidator());
        $this->assertEquals($tokenStorage->getToken(), $adminSubscriptionService->getToken());

        $this->assertTrue($adminSubscriptionService->hasSubscriptionsService());
        $this->assertEquals($subscriptionsService, $adminSubscriptionService->getSubscriptionsService());

        $this->assertTrue($adminSubscriptionService->hasEntityManager());
        $this->assertEquals($entityManager, $adminSubscriptionService->getEntityManager());

        $this->assertTrue($adminSubscriptionService->hasValidator());
        $this->assertEquals($validator, $adminSubscriptionService->getValidator());

        $this->assertTrue($adminSubscriptionService->hasEventDispatcher());
        $this->assertEquals($evDispatcher, $adminSubscriptionService->getEventDispatcher());
    }

    /**
     * Ensures that constructor accepts only valid token storage
     */
    public function testConstructorWithWrongTypeTokenStorage()
    {
        $this->expectExceptionMessage('TokenStorage');
        new SubscriptionService("UNDEFINED STRING");
    }

    /**
     * Ensures that setter accepts only valid token
     */
    public function testSetTokenWithWrongType()
    {
        $this->loginAdmin();
        $this->expectExceptionMessage('TokenInterface');
        $subscriptionService = $this->getService();

        $subscriptionService->setToken("UNDEFINED STRING");
    }

    /**
     * Ensures that constructor accepts only valid subscriptions service
     */
    public function testConstructorWithWrongTypeSubscriptionsService()
    {
        $tokenStorage = $this->get('security.token_storage');

        $this->expectExceptionMessage('SubscriptionsService');

        new SubscriptionService($tokenStorage, "UNDEFINED STRING");
    }

    /**
     * Ensures that setter accepts only valid subscriptions service
     */
    public function testSetSubscriptionsServiceWithWrongType()
    {
        $this->loginAdmin();
        $this->expectExceptionMessage('SubscriptionsService');
        $adminSubscriptionService = $this->getService();

        $adminSubscriptionService->setSubscriptionsService("UNDEFINED STRING");
    }

    /**
     * Ensures that setter accepts only valid event dispatcher
     */
    public function testEventDispatcherWithWrongType()
    {
        $this->loginAdmin();
        $this->expectExceptionMessage('EventDispatcherInterface');

        $adminSubscriptionServicee = $this->getService();

        $adminSubscriptionServicee->setEventDispatcher('UNDEFINED STRING');
    }

    /**
     * Ensures that constructor accepts only valid entity manager
     */
    public function testConstructorWithWrongTypeEntityManager()
    {
        $tokenStorage = $this->get('security.token_storage');
        $subscriptionsService = $this->get('charge_bee.subscriptions');

        $this->expectExceptionMessage('EntityManager');

        new SubscriptionService($tokenStorage, $subscriptionsService, "UNDEFINED STRING");
    }

    /**
     * Ensures that setter accepts only valid entity manager
     */
    public function testSetEntityManagerWithWrongType()
    {
        $this->loginAdmin();
        $this->expectExceptionMessage('EntityManager');
        $adminSubscriptionService = $this->getService();

        $adminSubscriptionService->setEntityManager("UNDEFINED STRING");
    }

    /**
     * Ensures that constructor accept only valid validator
     */
    public function testConstructorWithWrongTypeValidator()
    {
        $tokenStorage = $this->get('security.token_storage');
        $subscriptionsService = $this->get('charge_bee.subscriptions');
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $this->expectExceptionMessage('ValidatorInterface');

        new SubscriptionService($tokenStorage, $subscriptionsService, $entityManager, "UNDEFINED STRING");
    }

    /**
     * Ensures that set validator accepts only correct type
     */
    public function testSetValidatorWithWrongType()
    {
        $this->loginAdmin();
        $this->expectExceptionMessage('ValidatorInterface');
        $adminSubscriptionService = $this->getService();

        $adminSubscriptionService->setValidator("UNDEFINED STRING");
    }

    /**
     * Checks restrictions of admin user, of course without subscriptions
     */
    public function testAdminUserRestrictions()
    {
        $this->loginAdmin();
        $adminSubscriptionService = $this->getService();

        $this->assertTrue($adminSubscriptionService->isBuySubscribed());
        $this->assertTrue($adminSubscriptionService->isSellSubscribed());
        $this->assertTrue($adminSubscriptionService->isRepairSubscribed());
        $this->assertTrue($adminSubscriptionService->isSubscriber());
    }

    /**
     * Checks restrictions for usual customer without subscriptions
     */
    public function testCustomerUserRestrictions()
    {
        $this->loginCustomer();
        $adminSubscriptionService = $this->getService();

        $this->assertFalse($adminSubscriptionService->isSubscriber());
        $this->assertFalse($adminSubscriptionService->isSellSubscribed());
        $this->assertFalse($adminSubscriptionService->isRepairSubscribed());
        $this->assertFalse($adminSubscriptionService->isBuySubscribed());
    }

    /**
     * Checks behavior of subscription with three sites
     *
     * @throws \Exception
     */
    public function testCustomerUserWithSubscriptionInTrialWithAll()
    {
        $user = $this->loginCustomer();

        $customer = new Customer();

        $subscription = new Subscription();

        $subscription
            ->setStatus(SubscriptionInterface::STATUS_TRIAL)
            ->setCustomer($customer)
            ->setItems(
                [
                    SubscriptionInterface::ITEM_SELL => true,
                    SubscriptionInterface::ITEM_REPAIR => true,
                    SubscriptionInterface::ITEM_BUY => true,
                ]
            );
        $subscriptions = new ArrayCollection();
        $subscriptions->add($subscription);

        $trialEndDate = new \DateTime();
        $trialEndDate->add(new \DateInterval('PT1H'));
        $customer
            ->setTrialEndedAt($trialEndDate)
            ->setSubscriptions($subscriptions);

        $user->setBillingCustomer($customer);

        $adminSubscriptionService = $this->getService();

        $this->assertTrue($adminSubscriptionService->hasSubscription());

        $this->assertTrue($adminSubscriptionService->isSellSubscribed());
        $this->assertTrue($adminSubscriptionService->isBuySubscribed());
        $this->assertTrue($adminSubscriptionService->isRepairSubscribed());
    }

    /**
     * Checks behavior of subscription with two sites
     *
     * @throws \Exception
     */
    public function testCustomerUserWithSubscriptionInTrialWithTwo()
    {
        $user = $this->loginCustomer();

        $customer = new Customer();

        $subscription = new Subscription();

        $subscription
            ->setStatus(SubscriptionInterface::STATUS_TRIAL)
            ->setCustomer($customer)
            ->setItems(
                [
                    SubscriptionInterface::ITEM_SELL => true,
                    SubscriptionInterface::ITEM_REPAIR => true,
                    SubscriptionInterface::ITEM_BUY => false,
                ]
            );
        $subscriptions = new ArrayCollection();
        $subscriptions->add($subscription);

        $trialEndDate = new \DateTime();
        $trialEndDate->add(new \DateInterval('PT1H'));
        $customer
            ->setTrialEndedAt($trialEndDate)
            ->setSubscriptions($subscriptions);

        $user->setBillingCustomer($customer);

        $adminSubscriptionService = $this->getService();

        $this->assertTrue($adminSubscriptionService->hasSubscription());

        $this->assertTrue($adminSubscriptionService->isSellSubscribed());
        $this->assertFalse($adminSubscriptionService->isBuySubscribed());
        $this->assertTrue($adminSubscriptionService->isRepairSubscribed());
    }

    /**
     * Checks behavior of subscription with one site
     *
     * @throws \Exception
     */
    public function testCustomerUserWithSubscriptionInTrialWithOne()
    {
        $user = $this->loginCustomer();

        $customer = new Customer();

        $subscription = new Subscription();

        $subscription
            ->setStatus(SubscriptionInterface::STATUS_TRIAL)
            ->setCustomer($customer)
            ->setItems(
                [
                    SubscriptionInterface::ITEM_SELL => false,
                    SubscriptionInterface::ITEM_REPAIR => true,
                    SubscriptionInterface::ITEM_BUY => false,
                ]
            );
        $subscriptions = new ArrayCollection();
        $subscriptions->add($subscription);

        $trialEndDate = new \DateTime();
        $trialEndDate->add(new \DateInterval('PT1H'));
        $customer
            ->setTrialEndedAt($trialEndDate)
            ->setSubscriptions($subscriptions);

        $user->setBillingCustomer($customer);

        $adminSubscriptionService = $this->getService();

        $this->assertTrue($adminSubscriptionService->hasSubscription());

        $this->assertFalse($adminSubscriptionService->isSellSubscribed());
        $this->assertFalse($adminSubscriptionService->isBuySubscribed());
        $this->assertTrue($adminSubscriptionService->isRepairSubscribed());
    }

    /**
     * Checks that customer can't receive service from trial period if it passed out
     */
    public function testCustomerUserWithSubscriptionInTrialWhenTrialIsExpiredForCustomer()
    {
        $user = $this->loginCustomer();

        $customer = new Customer();

        $subscription = new Subscription();

        $subscription
            ->setStatus(SubscriptionInterface::STATUS_TRIAL)
            ->setCustomer($customer)
            ->setItems(
                [
                    SubscriptionInterface::ITEM_SELL => true,
                    SubscriptionInterface::ITEM_REPAIR => true,
                    SubscriptionInterface::ITEM_BUY => true,
                ]
            );
        $subscriptions = new ArrayCollection();
        $subscriptions->add($subscription);

        $trialEndDate = new \DateTime(); //right now date, must be expired
        $customer
            ->setTrialEndedAt($trialEndDate)
            ->setSubscriptions($subscriptions);

        $user->setBillingCustomer($customer);

        $adminSubscriptionService = $this->getService();

        $this->assertFalse($adminSubscriptionService->hasSubscription());

        $this->assertFalse($adminSubscriptionService->isSellSubscribed());
        $this->assertFalse($adminSubscriptionService->isBuySubscribed());
        $this->assertFalse($adminSubscriptionService->isRepairSubscribed());
    }
}
