<?php

namespace Tests\AdminBundle\Service;

use AdminBundle\Event\DnsEvent;
use AdminBundle\Service\DomainNameService;
use ArgumentCountError;
use Aws\Api\DateTimeResult;
use Aws\CloudFront\CloudFrontClient;
use Aws\Exception\AwsException;
use Aws\Result;
use Aws\Route53\Route53Client;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Log\LoggerInterface;
use Tests\AdminBundle\AbstractWebTestCase;
use ThreeWebOneEntityBundle\Entity\Domain\Zone;
use ThreeWebOneEntityBundle\Entity\User;

class DomainNameServiceTest extends AbstractWebTestCase
{
    /**
     * @var ObjectProphecy|Route53Client
     */
    private $route53Client;

    /**
     * @var ObjectProphecy|CloudFrontClient
     */
    private $cloudFrontClient;

    /**
     * @var EntityManagerInterface|ObjectProphecy
     */
    private $entityManager;

    /**
     * @var DomainNameService
     */
    private $domainNameService;

    /**
     * @var string
     */
    private $cloudFrontDistributionId = 'cfDistributionId';

    /**
     * @var string
     */
    private $cloudFrontDistributionHost = 'cfDistributionHost';

    /**
     * @var string
     */
    private $cloudFrontDistributionZoneId = 'cfDistributionZoneId';

    /**
     * @var string
     */
    private $cloudFrontHost = 'cfHost';

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function testWorngServiceInstantiate()
    {
        $this->expectException(ArgumentCountError::class);
        new DomainNameService();
    }

    public function testDNSGetters()
    {
        $this->assertTrue($this->domainNameService->hasEntityManager());
        $this->assertTrue($this->domainNameService->hasCloudFront());
        $this->assertTrue($this->domainNameService->hasRoute53());
        $this->assertTrue($this->domainNameService->hasCloudFrontDistributionHost());
        $this->assertTrue($this->domainNameService->hasCloudFrontDistributionId());
        $this->assertTrue($this->domainNameService->hasCloudFrontDistributionZoneId());


        $this->assertEquals(
            $this->cloudFrontDistributionZoneId,
            $this->domainNameService->getCloudFrontDistributionZoneId()
        );

        $this->assertEquals(
            $this->cloudFrontDistributionHost,
            $this->domainNameService->getCloudFrontDistributionHost()
        );

        $this->assertEquals(
            $this->cloudFrontDistributionId,
            $this->domainNameService->getCloudFrontDistributionId()
        );

        $this->assertEquals(
            $this->route53Client->reveal(),
            $this->domainNameService->getRoute53()
        );

        $this->assertEquals(
            $this->cloudFrontClient->reveal(),
            $this->domainNameService->getCloudFront()
        );
    }

    public function testRemoveCnameFailed()
    {
        $domain = 'domain.com';
        $dns = $this->domainNameService;

        $this->cloudFrontClient
            ->getDistribution(['Id' => $this->cloudFrontDistributionId])
            ->shouldBeCalled()
            ->willReturn([]);

        $this->assertFalse($dns->removeCname($domain));
    }

    public function testRemoveCname()
    {
        $dns = $this->domainNameService;
        $domain = 'domain.com';

        $this->predictRemoveCnameFullCycle($domain);

        $this->assertTrue($dns->removeCname($domain));
    }

    protected function predictRemoveCnameFullCycle($domain)
    {
        $this->cloudFrontClient
            ->getDistribution(['Id' => $this->cloudFrontDistributionId])
            ->shouldBeCalled()
            ->willReturn($this->getDistributionWithCustom($domain));

        $this->cloudFrontClient
            ->updateDistribution($this->getDistributionUpdate([]))
            ->shouldBeCalled();
    }

    public function getDistributionWithCustom($domain): Result
    {
        $baseDomain = [0 => 'baseDomain.com'];

        if (!is_array($domain)) {
            $domain = (array) $domain;
        }

        $domainsCount = count($domain);

        return new Result(
            [
                'Distribution' => [
                    'Id' => 'E2UGFSYHE8BM0E',
                    'ARN' => 'arn:aws:cloudfront::450268484995:distribution/E2UGFSYHE8BM0E',
                    'Status' => 'InProgress',
                    'LastModifiedTime' => DateTimeResult::__set_state(
                        ['date' => '2018-03-09 12:53:13.506000', 'timezone_type' => 2, 'timezone' => 'Z',]
                    ),
                    'InProgressInvalidationBatches' => 0,
                    'DomainName' => 'd372owdhff8jzo.cloudfront.net',
                    'ActiveTrustedSigners' => ['Enabled' => false, 'Quantity' => 0,],
                    'DistributionConfig' => [
                        'CallerReference' => '1498695911936',
                        'Aliases' => [
                            'Quantity' => 1 + $domainsCount,
                            'Items' => array_merge($baseDomain, $domain),
                        ],
                        'DefaultRootObject' => '',
                        'Origins' => [
                            'Quantity' => 3,
                            'Items' => [
                                0 => [
                                    'Id' => 'ELB-awseb-e-j-AWSEBLoa-Y1MFLHA5B4JU-686140712/admin',
                                    'DomainName' => 'awseb-e-n-AWSEBLoa-V3DUZ28CS0HS-2129765381.us-west-2.elb.amazonaws.com',
                                    'OriginPath' => '',
                                    'CustomHeaders' => ['Quantity' => 0,],
                                    'CustomOriginConfig' => [
                                        'HTTPPort' => 80,
                                        'HTTPSPort' => 443,
                                        'OriginProtocolPolicy' => 'http-only',
                                        'OriginSslProtocols' => [
                                            'Quantity' => 3,
                                            'Items' => [0 => 'TLSv1', 1 => 'TLSv1.1', 2 => 'TLSv1.2',],
                                        ],
                                        'OriginReadTimeout' => 30,
                                        'OriginKeepaliveTimeout' => 5,
                                    ],
                                ],
                                1 => [
                                    'Id' => 'ELB-awseb-AWSEB-AM8Z53UAOIMQ-520856569/sites',
                                    'DomainName' => 'awseb-e-q-AWSEBLoa-17YLAF7DV4E4X-2088369615.us-west-2.elb.amazonaws.com',
                                    'OriginPath' => '',
                                    'CustomHeaders' => ['Quantity' => 0,],
                                    'CustomOriginConfig' => [
                                        'HTTPPort' => 80,
                                        'HTTPSPort' => 443,
                                        'OriginProtocolPolicy' => 'http-only',
                                        'OriginSslProtocols' => [
                                            'Quantity' => 3,
                                            'Items' => [0 => 'TLSv1', 1 => 'TLSv1.1', 2 => 'TLSv1.2',],
                                        ],
                                        'OriginReadTimeout' => 30,
                                        'OriginKeepaliveTimeout' => 5,
                                    ],
                                ],
                                2 => [
                                    'Id' => 'Landing',
                                    'DomainName' => '9a1ac001cf65b94e87248e41c925422c-1159309368.us-west-2.elb.amazonaws.com',
                                    'OriginPath' => '',
                                    'CustomHeaders' => ['Quantity' => 0,],
                                    'CustomOriginConfig' => [
                                        'HTTPPort' => 80,
                                        'HTTPSPort' => 443,
                                        'OriginProtocolPolicy' => 'http-only',
                                        'OriginSslProtocols' => [
                                            'Quantity' => 3,
                                            'Items' => [0 => 'TLSv1', 1 => 'TLSv1.1', 2 => 'TLSv1.2',],
                                        ],
                                        'OriginReadTimeout' => 30,
                                        'OriginKeepaliveTimeout' => 5,
                                    ],
                                ],
                            ],
                        ],
                        'DefaultCacheBehavior' => [
                            'TargetOriginId' => 'Landing',
                            'ForwardedValues' => [
                                'QueryString' => true,
                                'Cookies' => ['Forward' => 'all',],
                                'Headers' => ['Quantity' => 1, 'Items' => [0 => '*',],],
                                'QueryStringCacheKeys' => ['Quantity' => 0,],
                            ],
                            'TrustedSigners' => ['Enabled' => false, 'Quantity' => 0,],
                            'ViewerProtocolPolicy' => 'redirect-to-https',
                            'MinTTL' => '0',
                            'AllowedMethods' => [
                                'Quantity' => 7,
                                'Items' => [
                                    0 => 'HEAD',
                                    1 => 'DELETE',
                                    2 => 'POST',
                                    3 => 'GET',
                                    4 => 'OPTIONS',
                                    5 => 'PUT',
                                    6 => 'PATCH',
                                ],
                                'CachedMethods' => [
                                    'Quantity' => 2,
                                    'Items' => [0 => 'HEAD', 1 => 'GET',],
                                ],
                            ],
                            'SmoothStreaming' => false,
                            'DefaultTTL' => '86400',
                            'MaxTTL' => '31536000',
                            'Compress' => true,
                            'LambdaFunctionAssociations' => ['Quantity' => 0,],
                        ],
                        'CacheBehaviors' => [
                            'Quantity' => 2,
                            'Items' => [
                                0 => [
                                    'PathPattern' => '/admin/*',
                                    'TargetOriginId' => 'ELB-awseb-e-j-AWSEBLoa-Y1MFLHA5B4JU-686140712/admin',
                                    'ForwardedValues' => [
                                        'QueryString' => true,
                                        'Cookies' => ['Forward' => 'all',],
                                        'Headers' => ['Quantity' => 1, 'Items' => [0 => '*',],],
                                        'QueryStringCacheKeys' => ['Quantity' => 0,],
                                    ],
                                    'TrustedSigners' => ['Enabled' => false, 'Quantity' => 0,],
                                    'ViewerProtocolPolicy' => 'redirect-to-https',
                                    'MinTTL' => '0',
                                    'AllowedMethods' => [
                                        'Quantity' => 7,
                                        'Items' => [
                                            0 => 'HEAD',
                                            1 => 'DELETE',
                                            2 => 'POST',
                                            3 => 'GET',
                                            4 => 'OPTIONS',
                                            5 => 'PUT',
                                            6 => 'PATCH',
                                        ],
                                        'CachedMethods' => [
                                            'Quantity' => 2,
                                            'Items' => [0 => 'HEAD', 1 => 'GET',],
                                        ],
                                    ],
                                    'SmoothStreaming' => false,
                                    'DefaultTTL' => '86400',
                                    'MaxTTL' => '31536000',
                                    'Compress' => true,
                                    'LambdaFunctionAssociations' => ['Quantity' => 0,],
                                ],
                                1 => [
                                    'PathPattern' => '/sites/*',
                                    'TargetOriginId' => 'ELB-awseb-AWSEB-AM8Z53UAOIMQ-520856569/sites',
                                    'ForwardedValues' => [
                                        'QueryString' => true,
                                        'Cookies' => ['Forward' => 'all',],
                                        'Headers' => ['Quantity' => 1, 'Items' => [0 => '*',],],
                                        'QueryStringCacheKeys' => ['Quantity' => 0,],
                                    ],
                                    'TrustedSigners' => ['Enabled' => false, 'Quantity' => 0,],
                                    'ViewerProtocolPolicy' => 'redirect-to-https',
                                    'MinTTL' => '0',
                                    'AllowedMethods' => [
                                        'Quantity' => 7,
                                        'Items' => [
                                            0 => 'HEAD',
                                            1 => 'DELETE',
                                            2 => 'POST',
                                            3 => 'GET',
                                            4 => 'OPTIONS',
                                            5 => 'PUT',
                                            6 => 'PATCH',
                                        ],
                                        'CachedMethods' => [
                                            'Quantity' => 3,
                                            'Items' => [0 => 'HEAD', 1 => 'GET', 2 => 'OPTIONS',],
                                        ],
                                    ],
                                    'SmoothStreaming' => false,
                                    'DefaultTTL' => '86400',
                                    'MaxTTL' => '31536000',
                                    'Compress' => true,
                                    'LambdaFunctionAssociations' => ['Quantity' => 0,],
                                ],
                            ],
                        ],
                        'CustomErrorResponses' => ['Quantity' => 0,],
                        'Comment' => '',
                        'Logging' => [
                            'Enabled' => true,
                            'IncludeCookies' => true,
                            'Bucket' => '3web1-admin-dev.s3.amazonaws.com',
                            'Prefix' => 'cloudfront',
                        ],
                        'PriceClass' => 'PriceClass_All',
                        'Enabled' => true,
                        'ViewerCertificate' => [
                            'ACMCertificateArn' => 'arn:aws:acm:us-east-1:450268484995:certificate/045c9ff1-2e36-4385-8c75-aae0a96b1139',
                            'SSLSupportMethod' => 'sni-only',
                            'MinimumProtocolVersion' => 'TLSv1.2_2018',
                            'Certificate' => 'arn:aws:acm:us-east-1:450268484995:certificate/045c9ff1-2e36-4385-8c75-aae0a96b1139',
                            'CertificateSource' => 'acm',
                        ],
                        'Restrictions' => [
                            'GeoRestriction' => [
                                'RestrictionType' => 'none',
                                'Quantity' => 0,
                            ],
                        ],
                        'WebACLId' => '',
                        'HttpVersion' => 'http2',
                        'IsIPV6Enabled' => true,
                    ],
                ],
                'ETag' => 'E2ZPJLQDSE0QTD',
                '@metadata' => [
                    'statusCode' => 200,
                    'effectiveUri' => 'https://cloudfront.amazonaws.com/2017-03-25/distribution/E2UGFSYHE8BM0E',
                    'headers' => [
                        'x-amzn-requestid' => 'e673a5d7-2398-11e8-9842-9becb513b349',
                        'etag' => 'E2ZPJLQDSE0QTD',
                        'content-type' => 'text/xml',
                        'content-length' => '6927',
                        'vary' => 'Accept-Encoding',
                        'date' => 'Fri, 09 Mar 2018 12:53:44 GMT',
                    ],
                    'transferStats' => ['http' => [0 => [],],],
                ],
            ]
        );
    }

    public function getDistributionUpdate($domain)
    {
        $plainDistribution = $this->getDistributionWithCustom($domain);

        return [
            'DistributionConfig' => $plainDistribution['Distribution']['DistributionConfig'],
            'Id' => $this->cloudFrontDistributionId,
            'IfMatch' => $plainDistribution['ETag'],
        ];
    }

    public function testAddCnameFailed()
    {
        $domain = 'domain.com';
        $dns = $this->domainNameService;

        $this->cloudFrontClient
            ->getDistribution(['Id' => $this->cloudFrontDistributionId])
            ->shouldBeCalled()
            ->willReturn([]);

        $this->assertFalse($dns->addCname($domain));
    }

    public function testAddCname()
    {
        $dns = $this->domainNameService;
        $domain = 'domain.com';

        $this->predictCnameFullCycle($domain);

        $this->assertTrue($dns->addCname($domain));
    }

    protected function predictCnameFullCycle($domain)
    {
        $this->cloudFrontClient
            ->getDistribution(['Id' => $this->cloudFrontDistributionId])
            ->shouldBeCalled()
            ->willReturn($this->getDistributionPlain());

        $this->cloudFrontClient
            ->updateDistribution($this->getDistributionUpdate($domain))
            ->shouldBeCalled();
    }

    public function getDistributionPlain()
    {
        return $this->getDistributionWithCustom([]);
    }

    public function testAddCloudFrontAliasFailed()
    {
        $this->route53Client
            ->changeResourceRecordSets(Argument::any())
            ->willThrow(AwsException::class)->shouldBeCalled();

        $zone = new Zone();

        $zone->setId(1)
            ->setOwner(new User())
            ->setDomain('domain.com');

        $this->assertFalse($this->domainNameService->addCloudFrontAlias($zone, $this->cloudFrontDistributionHost));
    }

    public function testAddCloudFrontAlias()
    {
        $zone = new Zone();

        $zone->setId(1)
            ->setOwner(new User())
            ->setZoneId('zoneId')
            ->setDomain('domain.com');

        $this->predictAddCloudFrontAliasFullCycle($zone->getZoneId(), $zone->getDomain());

        $this->assertTrue($this->domainNameService->addCloudFrontAlias($zone));
    }

    protected function predictAddCloudFrontAliasFullCycle($zoneId, $domain)
    {
        $updateRequest = [
            'ChangeBatch' => [
                'Changes' => [
                    [
                        'Action' => 'CREATE',
                        'ResourceRecordSet' => [
                            'AliasTarget' => [
                                'DNSName' => $this->cloudFrontDistributionHost,
                                'EvaluateTargetHealth' => false,
                                'HostedZoneId' => $this->cloudFrontDistributionZoneId,
                            ],
                            'Name' => $domain,
                            'Type' => 'A',
                        ],
                    ],
                ],
                'Comment' => 'Alias record for ' . $zoneId . ' zone',
            ],
            'HostedZoneId' => $zoneId,
        ];

        $this->route53Client
            ->changeResourceRecordSets($updateRequest)
            ->willReturn(new Result())
            ->shouldBeCalled();
    }

    public function testClearRecordsForZoneFailed()
    {
        $zone = new Zone();

        $this->assertFalse($this->domainNameService->clearRecordsForZone($zone));
    }

    public function testClearRecordsForZone()
    {
        $domain = 'test.com';

        $zone = new Zone();
        $zone->setId(102381)
            ->setDomain($domain)
            ->setZoneId('oef83fuh34f3h4');

        $this->predictClearRecordForZoneFullCycle($zone->getZoneId(), $zone->getDomain());

        $this->assertTrue($this->domainNameService->clearRecordsForZone($zone));
    }

    protected function predictClearRecordForZoneFullCycle($zoneId, $domain)
    {
        $zoneRecords = $this->getZoneRecordsSetWith($domain);

        $this->route53Client
            ->listResourceRecordSets(['HostedZoneId' => $zoneId])
            ->shouldBeCalled()
            ->willReturn($zoneRecords);


        $defaults = ['NS' => true, 'SOA' => true];
        $changesShouldBe = [];
        foreach ($zoneRecords['ResourceRecordSets'] as $resourceRecordSet) {
            if (!isset($defaults[$resourceRecordSet['Type']])) {
                $changesShouldBe[] = [
                    'Action' => 'DELETE',
                    'ResourceRecordSet' => $resourceRecordSet,
                ];
            }
        }


        $this->route53Client->changeResourceRecordSets(
            [
                'ChangeBatch' => [
                    'Changes' => $changesShouldBe,
                ],
                'HostedZoneId' => $zoneId,
            ]
        )->shouldBeCalled();
    }

    public function getZoneRecordsSetWith($domain)
    {
        return new Result(
            [
                'ResourceRecordSets' => [
                    0 => [
                        'Name' => $domain . '.',
                        'Type' => 'A',
                        'AliasTarget' =>
                            [
                                'HostedZoneId' => 'Z2FDTNDATAQYW2',
                                'DNSName' => 'd372owdhff8jzo.cloudfront.net.',
                                'EvaluateTargetHealth' => false,
                            ],
                    ],
                    1 => [
                        'Name' => $domain . '.',
                        'Type' => 'NS',
                        'TTL' => '172800',
                        'ResourceRecords' =>
                            [
                                0 => [
                                    'Value' => 'ns-67.awsdns-08.com.',
                                ],
                                1 => [
                                    'Value' => 'ns-979.awsdns-58.net.',
                                ],
                                2 => [
                                    'Value' => 'ns-1186.awsdns-20.org.',
                                ],
                                3 => [
                                    'Value' => 'ns-1645.awsdns-13.co.uk.',
                                ],
                            ],
                    ],
                    2 => [
                        'Name' => $domain . '.',
                        'Type' => 'SOA',
                        'TTL' => '900',
                        'ResourceRecords' =>
                            [
                                0 => [
                                    'Value' => 'ns-67.awsdns-08.com. awsdns-hostmaster.amazon.com.' .
                                        ' 1 7200 900 1209600 86400',
                                ],
                            ],
                    ],
                ],
                'IsTruncated' => false,
                'MaxItems' => '100',
                '@metadata' => [
                    'statusCode' => 200,
                    'effectiveUri' => 'https://route53.amazonaws.com/2013-04-01/hostedzone/Z1NV5V5WWHOCYB/rrset',
                    'headers' => [
                        'x-amzn-requestid' => 'd5236b34-2536-11e8-8478-ef5384ac245c',
                        'content-type' => 'text/xml',
                        'content-length' => '1162',
                        'date' => 'Sun, 11 Mar 2018 14:16:46 GMT',
                    ],
                    'transferStats' => [
                        'http' => [
                            0 => [],
                        ],
                    ],
                ],
            ]
        );
    }

    public function testLinkZoneFailedOwner()
    {
        $this->expectExceptionMessage('owner');
        $zone = new Zone();


        $this->assertFalse($this->domainNameService->linkZone($zone));
    }

    public function testLinkZoneFailedDomain()
    {
        $this->expectExceptionMessage('domain');
        $zone = new Zone();
        $zone->setOwner(new User());


        $this->assertFalse($this->domainNameService->linkZone($zone));
    }

    public function testLinkZone()
    {
        $owner = new User();
        $domain = 'test.com';
        $zoneId = 'SUPER_ZONE_TEST';
        $createZoneResponse = $this->getZoneCreateResponse($zoneId, $domain);

        $zoneExpected = (new Zone())
            ->setOwner($owner)
            ->setDomain($domain)
            ->setZoneId($zoneId)
            ->setNsList($createZoneResponse['DelegationSet']['NameServers'])
            ->setDistributionId($this->cloudFrontDistributionId);

        $this->predictLinkZoneFullCycle($zoneId, $domain);

        $zone = (new Zone())
            ->setOwner($owner)
            ->setDomain($domain);


        $this->assertTrue($this->domainNameService->linkZone($zone));
    }

    public function getZoneCreateResponse(string $zoneId, string $domain)
    {
        return new Result(
            [
                'Location' => 'https://route53.amazonaws.com/2013-04-01/hostedzone/ZOFCZITR8ULEE',
                'HostedZone' => [
                    'Id' => '/hostedzone/' . $zoneId,
                    'Name' => $domain . '.',
                    'CallerReference' => '205afca22d7eb06a067b52bbb3f60539',
                    'Config' => ['Comment' => 'Zone for "admin store" store.', 'PrivateZone' => false,],
                    'ResourceRecordSetCount' => '2',
                ],
                'ChangeInfo' => [
                    'Id' => '/change/C18RBHCTVKYPGG',
                    'Status' => 'PENDING',
                    'SubmittedAt' => DateTimeResult::__set_state(
                        ['date' => '2018-03-12 07:31:57.003000', 'timezone_type' => 2, 'timezone' => 'Z',]
                    ),
                ],
                'DelegationSet' => [
                    'NameServers' => [
                        0 => 'ns-592.awsdns-10.net',
                        1 => 'ns-1340.awsdns-39.org',
                        2 => 'ns-1687.awsdns-18.co.uk',
                        3 => 'ns-342.awsdns-42.com',
                    ],
                ],
                '@metadata' => [
                    'statusCode' => 201,
                    'effectiveUri' => 'https://route53.amazonaws.com/2013-04-01/hostedzone',
                    'headers' => [
                        'x-amzn-requestid' => '70ffc5b2-25c7-11e8-bb39-0bdbbcede010',
                        'location' => 'https://route53.amazonaws.com/2013-04-01/hostedzone/ZOFCZITR8ULEE',
                        'content-type' => 'text/xml',
                        'content-length' => '815',
                        'date' => 'Mon, 12 Mar 2018 07:31:56 GMT',
                    ],
                    'transferStats' => ['http' => [0 => []]],
                ],
            ]
        );
    }

    protected function predictLinkZoneFullCycle(string $zoneId, string $domain)
    {
        $this
            ->route53Client
            ->createHostedZone(Argument::any())
            ->willreturn(
                $this->getZoneCreateResponse($zoneId, $domain)
            );
    }

    public function testUnlinkZoneFailedZoneId()
    {
        $this->expectExceptionMessage('zoneId');
        $zone = new Zone();
        $this->route53Client->deleteHostedZone(['Id' => $zone->getZoneId()])->willThrow(AwsException::class);

        $this->assertFalse($this->domainNameService->unlinkZone($zone));
    }

    public function testUnlinkZoneFailed()
    {
        $zone = new Zone();
        $zone->setZoneId('zoneIdSimple');
        $this->route53Client->deleteHostedZone(['Id' => $zone->getZoneId()])->willThrow(AwsException::class);

        $this->assertFalse($this->domainNameService->unlinkZone($zone));
    }

    public function testUnlinkZone()
    {
        $zone = new Zone();
        $zone->setZoneId('TESTZONEID');

        $this->predictUnlinkZoneFullCycle($zone->getZoneId());

        $this->assertTrue($this->domainNameService->unlinkZone($zone));
    }

    protected function predictUnlinkZoneFullCycle(string $zoneId)
    {
        $this->route53Client->deleteHostedZone(['Id' => $zoneId])->shouldBeCalled();
    }

    public function testOnEventEventLinkZoneFailed()
    {
        $this->expectExceptionMessage('zone');
        $event = new DnsEvent();

        $this->domainNameService->onEventLink($event);
    }

    public function testOnEventEventLinkZoneDomainFailed()
    {
        $this->expectExceptionMessage('domain');
        $event = new DnsEvent();
        $event->setZone(new Zone());

        $this->domainNameService->onEventLink($event);
    }

    public function testOnEventLink()
    {
        $zone = new Zone();
        $owner = new User();
        $zoneId = 'testZoneId';

        $domain = 'test.com';

        $zone
            ->setDomain($domain)
            ->setOwner($owner);
        $event = new DnsEvent();
        $event->setZone($zone);

        //CNAME
        $this->predictCnameFullCycle($domain);

        //ZONE
        $this->predictLinkZoneFullCycle($zoneId, $domain);

        //ALIAS CREATE
        $this->predictAddCloudFrontAliasFullCycle($zoneId, $domain);

        $this->domainNameService->onEventLink($event);
    }

    public function testOnEventUnlinkFailedZone()
    {
        $this->expectExceptionMessage('zone');
        $event = new DnsEvent();

        $this->domainNameService->onEventUnlink($event);
    }

    public function testOnEventUnlinkFailedOwner()
    {
        $this->expectExceptionMessage('owner');
        $event = new DnsEvent();
        $event->setZone(new Zone());

        $this->domainNameService->onEventUnlink($event);
    }

    public function testOnEventUnlinkFailedDomain()
    {
        $this->expectExceptionMessage('domain');
        $event = new DnsEvent();
        $zone = new Zone();

        $zone
            ->setOwner(new User());

        $event->setZone($zone);

        $this->domainNameService->onEventUnlink($event);
    }

    public function testOnEventUnlink()
    {
        $zoneId = 'customZoneId';
        $zone = new Zone();
        $owner = new User();

        $domain = 'test.com';

        $zone
            ->setZoneId($zoneId)
            ->setDomain($domain)
            ->setOwner($owner);
        $event = new DnsEvent();
        $event->setZone($zone);

        //REMOVE CNAME
        $this->predictRemoveCnameFullCycle($domain);

        //CLEAR RECORDS
        $this->predictClearRecordForZoneFullCycle($zone->getZoneId(), $zone->getDomain());

        //UNLINK ZONE
        $this->predictUnlinkZoneFullCycle($zoneId);

        $this->domainNameService->onEventUnlink($event);
    }

    protected function setUp()
    {
        $this->domainNameService = $this->getDomainNameService();
        parent::setUp();
    }

    /**
     * @return DomainNameService
     */
    private function getDomainNameService(): DomainNameService
    {
        $this->route53Client = $this->prophesize(Route53Client::class);
        $this->cloudFrontClient = $this->prophesize(CloudFrontClient::class);
        $this->entityManager = $this->prophesize(EntityManagerInterface::class);
        $this->logger = $this->prophesize(Logger::class);

        return new DomainNameService(
            $this->route53Client->reveal(),
            $this->cloudFrontClient->reveal(),
            $this->entityManager->reveal(),
            $this->logger->reveal(),
            $this->cloudFrontDistributionId,
            $this->cloudFrontDistributionHost,
            $this->cloudFrontDistributionZoneId,
            $this->cloudFrontHost
        );
    }
}
